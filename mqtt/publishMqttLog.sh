#! /bin/sh -e

TARGET_HOST="${3:-localhost}"

pushed=0
errors=0

SECONDS=0
while read line; do
	topic="$( echo "$line" | cut -d' ' -f1 )"
	payload="$( echo "$line" | cut -d' ' -f2- )"

	mosquitto_pub -h "$TARGET_HOST" -t "$topic" -m "$payload" && pushed=$(( pushed + 1 )) || error=$(( error + 1 ))
done

>&2 echo "Pushed $pushed payloads in $SECONDS sec. Encoutered $errors error(s)."

