#! /bin/sh

# Usage: $@ [Source topic] [Source host] [Target host]

SOURCE_TOPIC="${1:-#}"
SOURCE_HOST="${2:-mqtt.mby.fr}"
TARGET_HOST="${3:-localhost}"

OUTPUT_LOG_FILE="replicatedMqttPayloads.log"
touch "$OUTPUT_LOG_FILE"
OUTPUT_LOG_FILE="$(readlink -f "$OUTPUT_LOG_FILE" )"

publishMessage() {
	while read line; do
		local topic="$( echo "$line" | cut -f1 -d' ' )"

		if [ -n "$topic" ]; then
			local message="$( echo "$line" | cut -f2 -d' ' )"
			echo "$line" | tee -a "$OUTPUT_LOG_FILE"
			mosquitto_pub -h "$TARGET_HOST" -t "$topic" -m "$message"
			>&2 echo "[$( date '+%F %T' )] Published new message for topic: [$topic]"
		fi
	done
}

onExit() {
	exit 0
}
trap onExit EXIT TERM QUIT ABRT INT

while true; do
	>&2 echo "Subbing to: [$SOURCE_HOST] on topic: [$SOURCE_TOPIC] ; publishing into: [$TARGET_HOST] ; logging into: [$OUTPUT_LOG_FILE] ..."
	mosquitto_sub -k 5 -h "$SOURCE_HOST" -t "$SOURCE_TOPIC" -R -v | publishMessage
done

