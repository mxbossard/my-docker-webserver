#! /bin/bash -e
scriptDir="$(dirname $(readlink -f $0))"
source $scriptDir/../../scripts/projectEnv.sh
source $SCRIPTS_DIR/unitTestsFramework.sh

# Need mosquitto-clients package

topic="amipo/foo/bar"
message="Foo_$(date +%s)"
mqttHostname="$PROXY_LOCAL_IP"

#echo "$topic $message"

assertTrue 'Posting MQTT message' mosquitto_pub -h $mqttHostname -p $SHIFTING_PORT_MQTT -t "$topic" -r -m "$message"

received=$( timeout 20 mosquitto_sub -v -h $mqttHostname -p $SHIFTING_PORT_MQTT -t "amipo/#" -C 1 )

assertEquals 'Validating polled MQTT message' "$topic $message" "$received"

assertNoError
