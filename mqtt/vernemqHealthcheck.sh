#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

vernemqIps=$( $SCRIPTS_DIR/listServiceIps.sh mqtt vernemq | awk '{print $1}' ) 
test -z "$vernemqIps" && >&2 echo "Unable to find an IP, container may not be running !" && exit 1 || true

for ip in $vernemqIps
do 
	curl -sS -f "http://$ip:8888/health"
done
