#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

QUIET=true
source $SCRIPT_DIR/../scripts/globalEnv.sh

test "$ENV" = "dev" || exit 0

subname="$1"
test -n "$subname"
echo "Adding dev ssl sub domain $subname"

cd $PARENT_DIR/nginx-proxy/dev/certs
ln -sf default.crt $subname.$ROOT_DOMAIN_NAME.crt
ln -sf default.key $subname.$ROOT_DOMAIN_NAME.key

