from flask import Flask, json, request, jsonify, make_response
from flask_restful import reqparse, abort, Api, Resource
import logging
import requests
from requests_futures import sessions
import datetime
import hashlib
import os

__log = logging.getLogger(__name__)

TAO_API_KEY = os.environ.get('TAO_API_KEY')
if not TAO_API_KEY: raise AttributeError('You must supply the TAO_API_KEY environment variable !')

TAO_API_URL = os.environ.get('TAO_API_URL')
if not TAO_API_URL: raise AttributeError('You must supply the TAO_API_URL environment variable !')

TAO_API_REQUEST_TIMEOUT = float(os.environ.get('TAO_API_REQUEST_TIMEOUT', '10'))

#format='%(levelname)s:%(message)s', 
LOG_LEVEL = os.environ.get('LOG_LEVEL', 'INFO').upper()
logging.basicConfig(level=LOG_LEVEL)

log = logging.getLogger(__name__)
log.info('LOG_LEVEL is: %s', LOG_LEVEL)
log.info('TAO_API_REQUEST_TIMEOUT is: %f', TAO_API_REQUEST_TIMEOUT)

app = Flask(__name__)
api = Api(app)

def txtPagesBuilder(*texts):
    container = {}
    pages = container['pages'] = []
    for text in texts:
        pages.append({'text': text})

    return container

## TAO ligne L
### Services
# {"ref":"L","refSup":"","operatorId":"TAO","mnemo":"L","name":"Ligne PONT EUROPE - CHEMIN HALAGE - BUS","type":"BUS","equipment":"","dests":["2","8570","8571","3"]}

### Dests
# {"ref":"8570","refSup":"","operatorId":"TAO","name":"Quais De Loire","direction":"A","service":"L"}
# {"ref":"8571","refSup":"","operatorId":"TAO","name":"Quais De Loire","direction":"A","service":"L"}

### Stops
# {"ref":"285","refSup":"","operatorId":"TAO","stopId":"285","name":"Cdt de Poli","x":6755878,"y":616632,"cap":270,"services":["L"],"dests":["8570"],"publicPlaces":[],"equipment":"","valuableStops":false},
# {"ref":"286","refSup":"","operatorId":"TAO","stopId":"286","name":"Cdt de Poli","x":6755875,"y":616640,"cap":91,"services":["L"],"dests":["8570"],"publicPlaces":[],"equipment":"","valuableStops":false}
# {"ref":"224","refSup":"","operatorId":"TAO","stopId":"224","name":"Chemin de Halage","x":6756114,"y":620090,"cap":78,"services":["L"],"dests":["8570"],"publicPlaces":[],"equipment":"","valuableStops":false}

## TAO ligne B
### Dests
# {"ref":"201","refSup":"","operatorId":"TAO","name":"Georges Pompidou","direction":"R","service":"B"} => OUEST
# {"ref":"225","refSup":"","operatorId":"TAO","name":"Clos Du Hameau","direction":"A","service":"B"} => EST
# {"ref":"225","refSup":"","operatorId":"TAO","name":"Clos Du Hameau","direction":"R","service":"B"}

### Stops
# {"ref":"1692","refSup":"","operatorId":"TAO","stopId":"1692","name":"Beaumonts","x":6756185,"y":616854,"cap":260,"services":["B"],"dests":["201"],"publicPlaces":[],"equipment":"","valuableStops":false} => OUEST
# {"ref":"1693","refSup":"","operatorId":"TAO","stopId":"1693","name":"Beaumonts","x":6756172,"y":616849,"cap":78,"services":["B"],"dests":["225"],"publicPlaces":[],"equipment":"","valuableStops":false} => EST

def buildTaoApiRollingKey():
    now_AAAAMMJJHH_24h = datetime.datetime.now().strftime("%Y%m%d%H") #From TAO doc, the format should be : %Y%m%d%H%M
    log.debug('TAO API key: [%s] ; Date token: [%s]', TAO_API_KEY, now_AAAAMMJJHH_24h)
    rollingKey = hashlib.md5(TAO_API_KEY.encode('utf-8') + now_AAAAMMJJHH_24h.encode('utf-8')).hexdigest()
    log.info('=> TAO API rolling key: [%s]', rollingKey)
    return rollingKey

def taoBusTimesQuery(taoServiceRef, taoStopId):
    rollingKey = buildTaoApiRollingKey()
    taoQuery = '%s?module=json&key=%s&function=getBusTimes&stopId=%d&refService=%s' % (TAO_API_URL, rollingKey, taoStopId, taoServiceRef)
    log.info('TAO query: [%s]', taoQuery)
    r = requests.get(taoQuery)
    r = requests.get(taoQuery, timeout=TAO_API_REQUEST_TIMEOUT)
    log.debug('TAO response: [%s]', r.text)

    resObj = json.loads(r.text)
    if len(resObj['busTimes']) > 0:
        return resObj['busTimes'][0]

    return None

def buildTaoBusTimesQuery(taoServiceRef, taoStopId):
    rollingKey = buildTaoApiRollingKey()
    taoQuery = '%s?module=json&key=%s&function=getBusTimes&stopId=%d&refService=%s' % (TAO_API_URL, rollingKey, taoStopId, taoServiceRef)
    log.info('TAO query: [%s]', taoQuery)

    return taoQuery

class TaoController(Resource):
    def get(self):
        session = sessions.FuturesSession(max_workers=10)
        httpFutures = []

        serviceRefAndStopIdArray = [('B', 'B go', 1693), ('L', 'L go', 286), ('L', 'L ret', 224)]
        text = ''

        # Async query TAO API in parallel
        for serviceRefAndStopId in serviceRefAndStopIdArray:
            serviceRef = serviceRefAndStopId[0]
            stopId = serviceRefAndStopId[2]
            #response = taoBusTimesQuery(serviceRef, stopId)
            query = buildTaoBusTimesQuery(serviceRef, stopId)
            #httpFutures.append(session.get(query))
            httpFutures.append(session.get(query, timeout=TAO_API_REQUEST_TIMEOUT))

        # Wait for async TAO API queries
        errorMessages = set()
        for i, future in enumerate(httpFutures):
            serviceRefAndStopId = serviceRefAndStopIdArray[i]
            serviceLabel = serviceRefAndStopId[1]
            stopId = serviceRefAndStopId[2]

            def manageError(response, message):
                message += ' Status code was: [%d]' % httpResponse.status_code
                log.error(message + ' ; URL was: [%s]' % (httpResponse.request.url))
                errorMessages.update([message])

            try:
                httpResponse = future.result()
                log.debug('TAO HTTP response: [%s]', httpResponse.text)
                response = None
                if httpResponse.status_code >= 500:
                    message = 'TAO server KO !'
                    manageError(httpResponse, message)
                    continue
                elif httpResponse.status_code == 401 or httpResponse.status_code == 403:
                    message = 'TAO Auth KO !'
                    manageError(httpResponse, message)
                    continue
                elif not httpResponse.status_code == 200:
                    message = 'TAO Resp KO !'
                    manageError(httpResponse, message)
                    continue

                resObj = json.loads(httpResponse.text)
                if not resObj:
                    message = 'TAO body empty !'
                    manageError(httpResponse, message)
                elif resObj.get('faultcode', None):
                    message = 'TAO err: ' + resObj['faultcode']
                    manageError(httpResponse, message)
                elif not resObj.get('busTimes', None):
                    message = 'TAO invalid format !'
                    manageError(httpResponse, message)
                elif len(resObj['busTimes']) < 1:
                    message = 'TAO timings missing !'
                    manageError(httpResponse, message)
                else:
                    response = resObj['busTimes'][0]

                text += '%s: ' % serviceLabel
                if response:
                    timeDatas = response['timeDatas']
                    for timeData in timeDatas[0:2]:
                        text += '%d ' % timeData['minutes']
                    
                    serviceDisruption = response['serviceDisruption']
                    if serviceDisruption:
                        text += '\n/!\\ %s' % serviceDisruption
                else:
                    #text += 'no data!'
                    text += 'N/A'
                
                text += '\n'
            except requests.exceptions.ConnectTimeout:
                errorMessage = 'TAO Connection Timeout (after %f seconds) !' % (TAO_API_REQUEST_TIMEOUT)
                errorMessages.update([errorMessage])
                log.error('Timeout connecting to TAO API !', exc_info=0)
            except requests.exceptions.ReadTimeout:
                errorMessage = 'TAO Response Timeout (after %f seconds) !' % (TAO_API_REQUEST_TIMEOUT)
                errorMessages.update([errorMessage])
                log.error('Timeout waiting TAO API response !', exc_info=0)
            except Exception as e:
                errorMessages.update(['HTTP Failure !'])
                log.error('Error dialoging with TAO API !', exc_info=1)
                #log.error(str(e))

        if errorMessages:
            text += 'Errors: %s\n' % '\n'.join(list(errorMessages))

        return txtPagesBuilder(text)

class StatusController(Resource):
    def get(self):
        return make_response({'status': 'ok'}, 200)

def loadApi(app):
    # Retrieve and format API_CONTEXT_ROOT
    API_CONTEXT_ROOT = os.environ.get('API_CONTEXT_ROOT', '')
    if API_CONTEXT_ROOT == '/':
        API_CONTEXT_ROOT = ''
    if len(API_CONTEXT_ROOT) > 0 and API_CONTEXT_ROOT[0] != '/':
        API_CONTEXT_ROOT = '/' + API_CONTEXT_ROOT

    API_VERSION = 'v1'
    TAO_PATH = f'{API_CONTEXT_ROOT}/{API_VERSION}/tao'
    STATUS_PATH = f'{API_CONTEXT_ROOT}/{API_VERSION}/status'
    API_ENDPOINTS = [TAO_PATH, STATUS_PATH]

    __log.info('API endpoints: %s', API_ENDPOINTS)

    api = Api(app)
    api.add_resource(StatusController, STATUS_PATH)
    api.add_resource(TaoController, TAO_PATH, '/tao')


if __name__ == '__main__': 
  
    app.run(host = "0.0.0.0", debug = True) 


