import logging
import os
from flask import Flask, json, request
#from flask_sqlalchemy import SQLAlchemy
import tao.tao_api as TaoApi

__log = logging.getLogger(__name__)

DB_DATA_DIR = os.environ.get('DB_DATA_DIR', None)
iotStorageDbFile = None

if (DB_DATA_DIR):
    iotStorageDbFile = DB_DATA_DIR + '/iot_storage.db'
    __log.info('Using file %s for iot_storage db.', iotStorageDbFile)
else:
    __log.error('DB_DATA_DIR environment variable is not defined !')
    exit(1)


app = Flask("mbd-api")

#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_BINDS'] = {}
app.config['BUNDLE_ERRORS'] = True

def main():
    TaoApi.loadApi(app)

    app.run(host = "0.0.0.0", debug = False)


if __name__ == '__main__':
    main()
