#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

source $SCRIPT_DIR/../../scripts/projectEnv.sh
source $SCRIPTS_DIR/unitTestsFramework.sh

API_BASE_URL="http://$VIRTUAL_HOST$API_CONTEXT_ROOT/v1"
CURL_ARGS=""

### Check new Device creation
assertHttpStatus 'Check status for /tao endpoint' GET "$API_BASE_URL/tao" "$HTTP_OK" "$CURL_ARGS" 

assertHttpResponseMatch 'Check response for /tao endpoint' GET "$API_BASE_URL/tao" '\{"pages": \[\{"text": ".*"\}\]\}' "$CURL_ARGS"

assertNoError
