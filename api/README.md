# API

## iot_storage_api

### Model
* Devices 
* Tags
* Mappings

1 Device can push metrics in LPP format.
1 Device can be associated with multiple Tags.
1 Device can be assigned 1 Mapping.

1 Device is stored in 1 InfluxDB Measurement. 
The LPP data is stored in multiple fields of the Measurement, tagged by channel. For example, BME280 on channel 1 produce 3 metrics stored in 3 fields : temperature, humidity and pressure. DHT22 on channel 2 produce 2 metrics : temperature and humidity. Each metric is stored in a field.
1 Tag in an InfluxDB index. It should be used to qualify the device by describing for example location, type, ...
Tags apply from a starting point in time and cannot be modify on already stored data.
1 Mapping permit to change the default channel and data types values. For example, map channel="1" to channel="BME280" ; Channel 1 type default name "Temperature" to "Temp in °C" for example.

Mapping is represented like this:
{
    "channelId": {
        "name": "newName",
        "fields": {
            "fieldId": "newFieldName"
        }
    }
}

## tao_api


## Flask-SQLAlchemy integration
For SQLAlchemy dev you may need to install pylint plugins :
``` bash
pip3 install pylint-flask
pip3 install pylint-flask-sqlalchemy
```