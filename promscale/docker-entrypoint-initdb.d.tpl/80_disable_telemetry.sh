#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$TIMESCALEDB_ADMIN_USER" --dbname "$TIMESCALEDB_ADMIN_DB" <<-EOSQL
    ALTER SYSTEM SET timescaledb.telemetry_level=off
EOSQL
