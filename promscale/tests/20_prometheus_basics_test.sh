#! /bin/bash -e
scriptDir="$(dirname $(readlink -f $0))"

source $scriptDir/../../scripts/projectEnv.sh
source $SCRIPTS_DIR/unitTestsFramework.sh

### Some queries
now="$(date +%s)"

prometheusIp=$( $SCRIPTS_DIR/listServiceIps.sh promscale prometheus prometheus | awk '{print $1}' | head -1 )
prometheusQueryUrl="http://$prometheusIp:9090/api/v1/query"
basicAuth="-u $INFLUXDB_ADMIN_USER:$INFLUXDB_ADMIN_PASSWORD"
resultTempFile=$( mkTempFile )
rmOnExit "$resultTempFile"


#assertHttpStatus 'Check anonymous user cannot create a bucket' POST "$queryApiUrl" "401" --data-urlencode "q=CREATE DATABASE \"$testDb\""

## Query for latest data
assertHttpStatus "Query for latest data" POST "$prometheusQueryUrl?query=up" "$HTTP_OK"

# up metric do not exists on startup no need to check value
#assertHttpResponseIs "Check latest data" POST "$prometheusQueryUrl?query=up&time=$now" '{"status":"success","data":{"resultType":"vector","result":[{"metric":{"__name__":"up","instance":"localhost:9090","job":"prometheus"},"value":['$now',"1"]}]}}'

## Drop metric
## FIXME

assertNoError
