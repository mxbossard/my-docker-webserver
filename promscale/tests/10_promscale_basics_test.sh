#! /bin/bash -e
scriptDir="$(dirname $(readlink -f $0))"

source $scriptDir/../../scripts/projectEnv.sh
source $SCRIPTS_DIR/unitTestsFramework.sh

### Some queries
now="$(date +%s)"

metricName="integrationtest_promscale_write"
metricValue="$now"
testData='{"labels":{"__name__":"'$metricName'"},"samples":[['$now'000, '$metricValue']]}'

promscaleIp=$( $SCRIPTS_DIR/listServiceIps.sh promscale promscale promscale | awk '{print $1}' | head -1 )
promscaleWriteUrl="http://$promscaleIp:9201/write"
promscaleQueryUrl="http://$promscaleIp:9201/api/v1/query"

prometheusIp=$( $SCRIPTS_DIR/listServiceIps.sh promscale prometheus prometheus | awk '{print $1}' | head -1 )
prometheusQueryUrl="http://$prometheusIp:9090/api/v1/query"

basicAuth="-u $INFLUXDB_ADMIN_USER:$INFLUXDB_ADMIN_PASSWORD"
resultTempFile=$( mkTempFile )
rmOnExit "$resultTempFile"


#assertHttpStatus 'Check anonymous user cannot create a bucket' POST "$queryApiUrl" "401" --data-urlencode "q=CREATE DATABASE \"$testDb\""

## Push test data
assertHttpStatus 'Push test data in promscale' POST "$promscaleWriteUrl?" "200" -H 'Content-Type: application/json' --data "$testData"

## Query for latest data
assertHttpStatus "Query for data in promscale" POST "$promscaleQueryUrl?query=$metricName" "$HTTP_OK"

assertHttpResponseIs "Check data value in promscale" POST "$promscaleQueryUrl?query=$metricName&time=$now" '{"status":"success","data":{"resultType":"vector","result":[{"metric":{"__name__":"'$metricName'"},"value":['$now',"'$metricValue'"]}]}}\n'



assertHttpStatus "Query for data in prometheus" POST "$prometheusQueryUrl?query=$metricName" "$HTTP_OK"

assertHttpResponseIs "Check data value in prometheus" POST "$prometheusQueryUrl?query=$metricName&time=$now" '{"status":"success","data":{"resultType":"vector","result":[{"metric":{"__name__":"'$metricName'"},"value":['$now',"'$metricValue'"]}]}}'


## Drop metric
## FIXME

assertNoError
