#! /bin/sh
scriptDir="$( readlink -f $( dirname $0 ) )"

outfluxImage="mxbossard/outflux"

if [ -z "$( docker images -q $outfluxImage )" ]; then
	cd $scriptDir
	docker build -t "$outfluxImage" .
fi

docker run --rm --network host "$outfluxImage" "$@"

