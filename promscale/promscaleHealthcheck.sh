#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

ips=$( $SCRIPTS_DIR/listServiceIps.sh 'promscale' 'promscale' 'promscale' | awk '{print $1}' ) 
test -z "$ips" && >&2 echo "Unable to find an IP, container may not be running !" && exit 1 || true

$SCRIPTS_DIR/loopExec.sh "curl -X GET -f -sS http://{}:9201/api/v1/query?query=device_foombd" $ips

ips=$( $SCRIPTS_DIR/listServiceIps.sh 'promscale' 'prometheus' 'prometheus' | awk '{print $1}' ) 
test -z "$ips" && >&2 echo "Unable to find an IP, container may not be running !" && exit 1 || true

$SCRIPTS_DIR/loopExec.sh "curl -X GET -f -sS http://{}:9090/api/v1/query?query=device_foombd" $ips

