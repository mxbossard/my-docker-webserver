#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
source $SCRIPT_DIR/scripts/scriptFramework.sh

upArgs=""
dontTouchDeps=false
while [ -n "$1" ]
do
        if echo " --force -f --quiet -q " | grep " $1 " > /dev/null
        then
                upArgs="$upArgs $1"
		shift
        elif echo " --build -b --no-build -n --pull -p --trust -t --strict -s " | grep " $1 " > /dev/null
        then
                recursiveUpArgs="$recursiveUpArgs $1"
		shift
	elif [ "$1" == "--one" -o "$1" == "-1" ]
	then
		dontTouchDeps=true
		shift
	else
		break
        fi
done

cd $SCRIPT_DIR

projects="$@"
test -n "$projects" || die "You must supply at least one project to up !"

if [ "$projects" == "all" ]
then
	INPUT_PROJECTS="$( $SCRIPT_DIR/scripts/listProjects.sh )"
	upArgs="$upArgs --quiet"
else
	INPUT_PROJECTS="$( $SCRIPT_DIR/scripts/listProjects.sh $@ )"
fi

healthyProjects=":"
notHealthyProjects=":"
tempErrorFile=$( mkTempFile "up-errors" )
tempSuccessFile=$( mkTempFile "up-success" )
rmOnExit "$tempErrorFile" "$tempSuccessFile"

upProjectAlone() {
	local project="$1"
	shift
	local args="$@"
	
	cmd="$SCRIPT_DIR/$project/up.sh $args"
	debug "Running up script: [$cmd] ..."
	rc=0
	cmdOutTempFile="$( mkTempFile "cmd.out" )"
	$cmd | tee "$cmdOutTempFile" || rc=$?
	if [ $rc -eq 0 ]; then
		cat "$cmdOutTempFile" >> "$tempSuccessFile"
	else
		cat "$cmdOutTempFile" >> "$tempErrorFile"
	fi
	rm -- "$cmdOutTempFile"
	return $rc
}

# Up a project with it's dependencies.
# Try to up all dependencies even if one fail.
# If one dependency fail, do not try to up project.
upProjectWithDeps() {
	local project="$1"
	shift
	local args="$@"

	if [ -n "$project" ]
	then
		# If project already flagged not healthy, skip it.
		if echo "$notHealthyProjects" | grep ":$project:" > /dev/null 
		then
			debug "Project: [$project] already flagged not healthy !"
			return 1
		fi

		local depsFailure=""
		local projectDeps="$( $SCRIPT_DIR/scripts/projectDependencies.sh $project )"
		if [ -n "$projectDeps" ]
		then
			for dep in $projectDeps
			do
				# Recursive call on dep
				debug "Recursive call on dep: [$dep] ..."
				if upProjectWithDeps "$dep" --quiet $recursiveUpArgs ; then
					debug "up on dependency: $dep succeed."
				else
					debug "up on dependency: $dep failed !"
					depsFailure="$depsFailure$dep "
				fi
				debug "depsFailure: [$depsFailure]"
			done
		fi

		if [ -n "$depsFailure" ]
		then
			alertMsg "Project: [$PURPLE$project$NC] was not up due to failed dependency: [ $YELLOW$depsFailure$NC] !" | tee -a $tempErrorFile
			return 1
		fi

		# Up project with params if no missing dependency and not alreay healthy
		if echo "$healthyProjects" | grep -v ":$project:" > /dev/null
		then 
			if upProjectAlone "$project" $args
			then
				healthyProjects="$healthyProjects$project:"
			else
				notHealthyProjects="$notHealthyProjects$project:"
				return 1
			fi
		else
			debug "Project: [$project] is already up."
		fi

	fi



}

# Up depdendency before projects.
# If a dependency cannot be upped stop upping project chain, but continue upping other projects

for project in $INPUT_PROJECTS
do
	if $dontTouchDeps; then
		upProjectAlone "$project" $upArgs $recursiveUpArgs || true
	else
		upProjectWithDeps "$project" $upArgs $recursiveUpArgs || true
		#debug "healthyProjects: $healthyProjects"
	fi
done

errOut ""
cyanMsg "----- Script $0 Report: -----"
cat "$tempSuccessFile"

if [ -n "$( cat $tempErrorFile )" ]
then
	alertMsg "Encoutered some errors :"
	cat "$tempErrorFile"
fi

