#! /bin/sh
SCRIPT_DIR="$( dirname $( readlink -f $0 ) )"

ctName="${1:-mdwsid}"
imageName="mdwsid"

docker build -t "$imageName" "$SCRIPT_DIR"

docker rm -v -f "$ctName" || true

#docker run --rm -d --name "$ctName" --privileged -e DEBUG -e TRACE -e INSECURED_ENV=true -v /var/lib/docker/overlay2:/var/lib/docker/overlay2 -v /var/lib/docker/image:/var/lib/docker/image -v "$SCRIPT_DIR/..":/root/gitRepo:ro "$imageName" || true
#docker run --rm -d --name "$ctName" --privileged -e DEBUG -e TRACE -e INSECURED_ENV=true -v /var/lib/docker/overlay2:/var/lib/docker/overlay2 -v "$SCRIPT_DIR/..":/root/gitRepo:ro "$imageName" || true
docker run --rm -d --name "$ctName" --privileged -e DEBUG -e TRACE -e INSECURED_ENV=true -v "$SCRIPT_DIR/..":/root/gitRepo:ro "$imageName" || true

# Wait for container to be up
maxAttempt=10
k=0
while [ "$maxAttempt" -gt "$k" ] && ! docker exec -i mdwsid docker info > /dev/null 2>&1 ; do
        sleep 1
        k=$(( k + 1 ))
done

test "$maxAttempt" -gt "$k" || >&2 echo "mdwsid container failed to start !"

