#! /bin/sh
SCRIPT_DIR="$( dirname $( readlink -f $0 ) )"

# Test deployment in integ env : up project building images from git reference.

usage() {
	>&2 echo "usage: $0 <gitRef> [upArgs]"
	exit 1
}

GIT_REPO_PATH="file:///root/gitRepo"
GIT_BRANCH="$1"
test -n "$GIT_BRANCH" || usage
shift

args="${@:-all}"

# Start MyDowkerWebServer container
ctName="integ-mdwsid"
$SCRIPT_DIR/startMdwsId.sh "$ctName"

docker exec -i $ctName git clone -b ${GIT_BRANCH} --depth 1 --recurse-submodules --shallow-submodules ${GIT_REPO_PATH} /home/mdws
export ENV_NAME="mdwsid"
docker exec -i $ctName bash -c "echo ${ENV_NAME} > /home/mdws/name.txt"
#docker exec -it -e COLUMNS="`tput cols`" -e LINES="`tput lines`" $ctName ./up.sh --strict $args
docker exec -it $ctName ./up.sh --strict $args

