#! /bin/sh
SCRIPT_DIR="$( dirname $( readlink -f $0 ) )"

# Test deployment in test env : up project building images from dev source project dir.

usage() {
	>&2 echo "usage: $0 [upArgs]"
	exit 1
}

args="${@:-all}"

# Start MyDowkerWebServer container
ctName="test-mdwsid"
$SCRIPT_DIR/startMdwsId.sh "$ctName"

docker exec -i $ctName cp -a /root/gitRepo/. /home/mdws
export ENV_NAME="mdwsid"
docker exec -i $ctName bash -c "echo ${ENV_NAME} > /home/mdws/name.txt"
#docker exec -it -e COLUMNS="`tput cols`" -e LINES="`tput lines`" $ctName ./up.sh --strict $args
docker exec -it $ctName ./up.sh --strict $args

