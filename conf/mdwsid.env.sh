# Override default_env.sh file for dev env.

# Proxy config
PROXY_LOCAL_IP="127.0.0.1"
ROOT_DOMAIN_NAME="mydockerweb.local"
ADMIN_EMAIL="foo@example.local"
DISABLE_HTTPS="false"
DISABLE_LETS_ENCRYPT="true"
DISABLE_REDIRECT_HTTP="true"

# Healthcheck config
HEALTHCHECK_INITIAL_DELAY_SEC=1
HEALTHCHECK_RETRIES=5
HEALTHCHECK_RETRIES_INTERVAL_SEC=1

LOG_LEVEL=debug
