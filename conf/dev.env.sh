# Override default_env.sh file for dev env.

# Proxy config
PROXY_LOCAL_IP="192.168.33.10"
ROOT_DOMAIN_NAME="mydockerweb.local"
ADMIN_EMAIL="foo@example.local"
HTTP_PROTOCOL="http"
DISABLE_HTTPS="false"
DISABLE_LETS_ENCRYPT="true"
DISABLE_REDIRECT_HTTP="true"

# Healthcheck config
HEALTHCHECK_INITIAL_DELAY_SEC=1
HEALTHCHECK_RETRIES=5
HEALTHCHECK_RETRIES_INTERVAL_SEC=1

LOG_LEVEL=debug

# Better to use this variable on launch (on up)
#INFLUXDB_BACKUP_TO_RESTORE_FILEPATH="./backups/backup_influxdb_2021-06-08.tgz"
