
# Port shift for this env.
PORT_SHIFT=0

# System config
PUBLIC_IFACE="$( basename $( ls -d1 /sys/class/net/e[tn]* | head -1 ) )"

# Proxy config
PROXY_LOCAL_IP="127.0.0.1"
ROOT_DOMAIN_NAME="mby.fr"
ADMIN_EMAIL="foo@example.local"
HTTP_PROTOCOL="https"
DISABLE_HTTPS="false"
DISABLE_LETS_ENCRYPT="false"
DISABLE_REDIRECT_HTTP="false"
VIRTUAL_GROUP="$ENV"

# Projects config
DOCKER_SHARED_NETWORKS="http-proxy@nginx-proxy influxdb@influxdb mqtt@mqtt promscale@promscale prometheus@promscale"
MAINTAINER_REPO="mxbossard"
MAINTAINER_IMAGES_FILTER="$MAINTAINER_REPO/*"

PYTHON_IMAGE_TAG=3.8-alpine

IOT_DATASTORE_INFLUXDB_DB="iot_datastore"
IOT_DATASTORE_INFLUXDB_RP="immediate_term"

IOT_PROM_DB="iot_datastore"

# managedCurl config
MANAGED_CURL_MAX_TIME=10
MANAGED_CURL_CONNECT_TIMEOUT=1

# Healthcheck config
HEALTHCHECK_INITIAL_DELAY_SEC=2
HEALTHCHECK_INTERVAL_SEC=30 # not used
HEALTHCHECK_TIMEOUT_SEC=5
HEALTHCHECK_RETRIES=5
HEALTHCHECK_RETRIES_INTERVAL_SEC=2
HEALTHCHECK_SCRIPTS="defaultHealthcheck.sh"

# Integration Tests config (tests directories)
INTEGRATION_TEST_TIMEOUT_SEC=20

# SHARED FIXED PORTS
HTTP_PORT=80
HTTPS_PORT=443

# SHARED SHIFTING PORTS
SHIFTING_PORT_MQTT=1883
SHIFTING_PORT_TIMESCALEDB=5432
SHIFTING_PORT_PROMSCALE=9201
SHIFTING_PORT_PROMETHEUS=9090

LOG_LEVEL=warn
