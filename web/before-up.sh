#! /bin/sh -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

echo "Remove public dir content ..."
rm -rf -- $SCRIPT_DIR/public/*
