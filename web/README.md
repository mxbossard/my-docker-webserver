# Web project

## Hugo hugo-theme-learn management

Add a chapter:
``` bash
docker run --rm -it -v $(pwd)/src/web:/src klakegg/hugo:0.80.0 new --kind chapter CHAPTER_NAME/_index.md
```

Add a page:
``` bash
docker run --rm -it -v $(pwd)/src/web:/src klakegg/hugo:0.80.0 new CHAPTER_NAME/PAGE.md
```
