#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

ctNames=$( $SCRIPTS_DIR/listServiceIps.sh web hugo | awk '{print $2}' ) 
test -z "$ctNames" && >&2 echo "Unable to find an IP, container may not be running !" && exit 1 || true

# Check index.html generation
for name in $ctNames
do 
	docker exec $name ls -1 /public/index.html
done
