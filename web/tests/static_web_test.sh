#! /bin/bash -e
scriptDir="$(dirname $(readlink -f $0))"
source $scriptDir/../../scripts/projectEnv.sh
source $SCRIPTS_DIR/unitTestsFramework.sh

BASE_URL="http://$VIRTUAL_HOST"
CURL_ARGS=""

# Check for static files presence
i=0
for staticFile in $( ls -1 "$scriptDir/../html" | head )
do
	i=$(( i + 1 ))
	assertHttpStatus "GET static file #$i" GET "$BASE_URL/$staticFile" "$HTTP_OK" $CURL_ARGS
done

# Check for 404 on not existing file
assertHttpStatus 'GET not existing file' GET "$BASE_URL/notExistingFile" "$HTTP_NOT_FOUND" $CURL_ARGS

# Check for nginx token without version
assertHttpResponseMatch 'Check nginx server tokens off' GET "$BASE_URL/notExistingFile" "^<hr><center>nginx</center>.*$" $CURL_ARGS

# Check for ROOT_DOMAIN_NAME URL on main page
assertHttpResponseMatch 'Check for ROOT_DOMAIN_NAME in homepage /index.html' GET "$BASE_URL/index.html" '.*href="https?://[^.]+\.'$ROOT_DOMAIN_NAME'.*' $CURL_ARGS
assertHttpResponseMatch 'Check for ROOT_DOMAIN_NAME in homepage /' GET "$BASE_URL/" '.*href="https?://[^.]+\.'$ROOT_DOMAIN_NAME'.*' $CURL_ARGS
assertHttpResponseMatch 'Check for ROOT_DOMAIN_NAME in homepage' GET "$BASE_URL" '.*href="https?://[^.]+\.'$ROOT_DOMAIN_NAME'.*' $CURL_ARGS

assertHttpResponseMatch 'Check for jitsi URL in homepage' GET "$BASE_URL" '.*href="https?://jitsi\.'$ROOT_DOMAIN_NAME'.*' $CURL_ARGS

assertNoError
