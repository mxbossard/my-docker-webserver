# mby.fr domains

## Applications disponibles
Liste des applications disponibles.

- [Jitsi Meet](https://jitsi.{{% param "rootDomainName" %}}/)
- [Mumble](https://mumble.{{% param "rootDomainName" %}}/)
- [Grafana](https://grafana.{{% param "rootDomainName" %}}/)

