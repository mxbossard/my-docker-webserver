#! /bin/bash -e
scriptDir="$(dirname $(readlink -f $0))"
source $scriptDir/../../scripts/projectEnv.sh
source $SCRIPTS_DIR/unitTestsFramework.sh

BASE_URL="http://$PROXY_LOCAL_IP"
CURL_ARGS="-H'Host: unknown.host'"

# Check for 503 on unknown host
assertHttpStatus 'GET / on unknown Host' GET "$BASE_URL/" "503" "$CURL_ARGS"

assertNoError
