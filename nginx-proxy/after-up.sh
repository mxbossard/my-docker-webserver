#! /bin/sh -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

if [ "$UFW_ENABLE" = "true" ]; then
	sudo ufw route allow in on $PUBLIC_IFACE to any port 80 >&2
	sudo ufw route allow in on $PUBLIC_IFACE to any port 443 >&2
fi
