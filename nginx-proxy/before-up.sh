#! /bin/sh -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

if $DISABLE_LETS_ENCRYPT
then 
	export SSL_CERTS_VOLUME="$SCRIPT_DIR/dev/certs"
	# Use dummy duplicated ssl certs
	rm -rf -- $SCRIPT_DIR/dev/certs
	mkdir $SCRIPT_DIR/dev/certs
	cp -f $SCRIPT_DIR/dev/default.* $SCRIPT_DIR/dev/certs
	cd $SCRIPT_DIR/dev/certs
	ln -sf default.crt $ROOT_DOMAIN_NAME.crt
	ln -sf default.key $ROOT_DOMAIN_NAME.key
fi

