#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

nginxIps=$( $SCRIPTS_DIR/listServiceIps.sh nginx-proxy nginx-proxy 'http-proxy' | awk '{print $1}' ) 
nginxNames=$( $SCRIPTS_DIR/listServiceIps.sh nginx-proxy nginx-proxy | awk '{print $2}' ) 
test -z "$nginxIps" && >&2 echo "Unable to find an IP, container may not be running !" && exit 1 || true

# Check nginx config
$SCRIPTS_DIR/loopExec.sh "docker exec {} nginx -t" $nginxNames

# 503 Errors are OK
$SCRIPTS_DIR/loopExec.sh "curl -sS http://{}:80" $nginxIps

