#! /bin/sh

DEFAULT_HOST="mydockerweb.local"
MQTT_HOST=$DEFAULT_HOST
MQTT_TOPIC="public/#"
IOT_DATASTORE_URL=http://iot.$DEFAULT_HOST/v1/datastore

params="${@:-binary-lpp-mqtt_to_iot-datastore_daemon.py $MQTT_HOST 1883 $MQTT_TOPIC $IOT_DATASTORE_URL}"

echo Using params: [$params]

#curl -k -X POST "$INFLUXDB_URL/query" --data-urlencode "q=CREATE DATABASE $INFLUXDB_DB_NAME"

docker build -t mqtt_to_iot-datastore:dev .

export LOG_LEVEL=debug
docker run -it --rm --name test-mqtt-daemon.sh -e LOG_LEVEL --network=host -v "$PWD":/usr/src/myapp -w /usr/src/myapp --entrypoint=python mqtt_to_iot-datastore:dev $params
