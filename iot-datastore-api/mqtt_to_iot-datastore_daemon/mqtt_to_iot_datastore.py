import sys
import os
import re
import datetime
import requests
import time
import logging
import paho.mqtt.client as mqtt

MQTT_HOSTNAME = sys.argv[1]
MQTT_PORT = int(sys.argv[2])
SUBSCRIBED_TOPIC = sys.argv[3]
IOT_DATASTORE_API_URL = sys.argv[4]

logLevel = os.environ.get('LOG_LEVEL', 'INFO').upper()
logging.basicConfig(level=logLevel)
__log = logging.getLogger(__name__)

__log.info('Launching MQTT to IoT datastore daemon [MQTT: %s:%d (subscribing on %s)] => [IoT datastore API: (%s)]', MQTT_HOSTNAME, MQTT_PORT, SUBSCRIBED_TOPIC, IOT_DATASTORE_API_URL)
__log.info('Logging level: [%s].', logLevel)

def postDataPayload(deviceId, dataPayload):
    __log.debug("postDataPayload() => dataPayload: [%s] to device: [%s]", str(dataPayload), deviceId)
    res = requests.post(f'{IOT_DATASTORE_API_URL}/data/{deviceId}', json=dataPayload)
    if res.status_code == 200:
        __log.info("dataPayload: [%s] posted for device: [%s].", str(dataPayload), deviceId)
    elif res.status_code == 501:
        __log.warning("dataPayload: [%s] for device: [%s] could not be proccessed by IoT Datastore API !", str(dataPayload), deviceId)
    else:
        __log.debug("IoT storage API response status: [%s] with reason: [%s] and body: [%s].", res.status_code, res.reason, res.content)
        res.raise_for_status()

def postBinaryLpp(deviceId, lpp):
    __log.debug("postBinaryLpp() => binaryLpp: [%s] to device: [%s]", lpp.hex(), deviceId)
    res = requests.post(f'{IOT_DATASTORE_API_URL}/lpp/{deviceId}', headers={'Content-Type': 'application/octet-stream'}, data=lpp)
    if res.status_code == 200:
        __log.info("lpp: [%s] posted for device: [%s].", lpp.hex(), deviceId)
    elif res.status_code == 501:
        __log.warning("lpp: [%s] for device: [%s] could not be proccessed by IoT Datastore API !.", lpp.hex(), deviceId)
    else:
        __log.debug("IoT storage API response status: [%s] and reason: [%s]", res.status_code, res.reason)
        res.raise_for_status()

def onMqttLog(client, userdata, level, buf):
    if level <= mqtt.MQTT_LOG_WARNING:
        __log.info("log: [%s] %s", level, buf)

# The callback for when the client receives a CONNACK response from the server.
def onMqttConnect(client, userdata, flags, rc):
    if rc > 0:
        raise Exception('Exception in MQTT connection with code: {rc}')
    
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.message_callback_add(SUBSCRIBED_TOPIC, onMqttSubscribedMessage)
    client.subscribe(SUBSCRIBED_TOPIC, qos=2)

def removePrefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text  # or whatever


def onMqttSubscribedMessage(client, userdata, msg):
    raise NotImplementedError()

# The callback for when a PUBLISH message is received from the server.
def onMqttMessage(client, userdata, msg):
    __log.debug("Published MQTT message on topic: [%s] with payload: [%s], raw message: [%s]", msg.topic, str(msg.payload), str(msg))
    return

def buildMqttClient(clientId):
    # clean_session=True is needed to be sure we don't loose qos 1 and 2 messages before stored in influxdb. 
    # But with clean_session=True a qos=2 message can be received twice.
    client = mqtt.Client(client_id=clientId, clean_session=False, userdata=None, protocol=mqtt.MQTTv311, transport='tcp')
    client.enable_logger(logger=None)
    client.on_connect = onMqttConnect
    client.on_message = onMqttMessage # Default on_message callback used when no other message_callback_add trapped.
    client.on_log = onMqttLog
    __log.info("MQTT client built.")
    return client

def connectMqttClient(mqttClient):
    __log.info(f'Attempt connection to MQTT broker {MQTT_HOSTNAME}:{MQTT_PORT} ...')
    mqttClient.connect(MQTT_HOSTNAME, port=MQTT_PORT)
    __log.info(f'Connected to MQTT broker {MQTT_HOSTNAME}:{MQTT_PORT} .')
    return mqttClient


def startDaemon(mqttClientId):
    while True:
        try:
            mqttClient = buildMqttClient(mqttClientId)
            connectMqttClient(mqttClient)
            # Blocking call that processes network traffic, dispatches callbacks and handles reconnecting.
            mqttClient.loop_forever(retry_first_connection=False)
                
        except Exception as e:
            try:
                mqttClient.disconnect()
            except Exception:
                pass
            __log.error(f'Exception raised in daemon: %s' % str(e))
            time.sleep(5)
        except KeyboardInterrupt:
            try:
                mqttClient.disconnect()
            except Exception:
                pass
            sys.exit(0)

