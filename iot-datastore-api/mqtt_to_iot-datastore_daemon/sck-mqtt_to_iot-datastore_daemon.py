import re
import pickle 
from cayennelpp import LppFrame
from datetime import datetime

import mqtt_to_iot_datastore
from mqtt_to_iot_datastore import __log, SUBSCRIBED_TOPIC

MQTT_CLIENT_ID = 'sck-mqtt_to_iot-datastore_daemon'

def onMqttSubscribedMessage(client, userdata, msg):
    # Do not process retained message.
    if msg.retain:
        return

    sckPayload = msg.payload.decode('ascii')
    __log.debug("Received MQTT message on subscribed topic: [%s] with payload: [%s], raw message: [%s]", msg.topic, sckPayload, str(msg))

    # SCK topic example : device/sck/DEVICE_ID/readings/raw 
    if not msg.topic.endswith('/readings/raw'):
        # If topic does not ends with /readings/raw ignore data
        return

    prefixToStrip = re.sub(r'[^/]*$', '', SUBSCRIBED_TOPIC).replace('#', '')
    deviceId = mqtt_to_iot_datastore.removePrefix(msg.topic, prefixToStrip).replace('/readings/raw', '').replace('/', '.')

    payloadDict = {}
    for item in sckPayload.replace('{', '').replace('}', '').split(','):
        keyAndVal = item.split(':')
        if not keyAndVal or len(keyAndVal) < 2:
            __log.error('Received malformed payload: [%s] for sck topic: %s', keyAndVal, msg.topic)
            return

        try:
            key = int(keyAndVal[0])
        except:
            key = str(keyAndVal[0])

        if len(keyAndVal) == 2:
            val = keyAndVal[1]
        else:
            val = ':'.join(keyAndVal[1:])

        if key == None or val == None:
            __log.error('Received malformed payload: [%s: %s] for sck topic: %s', key, val, msg.topic)
            return
        payloadDict[key] = val


    if not payloadDict:
        return

    # Build IoT data payload
    dataPayload = {"uid": deviceId, "channels": {}}

    #lppFrame = LppFrame()
    for key, value in payloadDict.items():
        if key == 't':
            # Sample time 2021-04-04T20:26:14Z
            sampleTime = datetime.fromisoformat(value.replace("Z", "+00:00"))
            #lppFrame.add_unix_time(0, sampleTime.timestamp())
            dataPayload["time"] = int(sampleTime.timestamp())

        elif key == 10 :
            # Battery level (%) between 0-100
            #lppFrame.add_analog_input(key, float(value) / 100) # % conversion
            dataPayload["channels"][str(key)] = {"percentage": float(value)}

        elif key == 14 :
            # Luminosity (Lux)
            #lppFrame.add_luminosity(key, float(value))
            dataPayload["channels"][str(key)] = {"illuminance": float(value)}

        elif key == 53 :
            # Sound volume (dbA)
            #lppFrame.add_analog_input(key, float(value))
            dataPayload["channels"][str(key)] = {"sound_level": float(value)}

        elif key == 55 :
            # Temperature (°C)
            #lppFrame.add_temperature(key, float(value))
            dataPayload["channels"][str(key)] = {"temperature": float(value)}

        elif key == 56 :
            # Humidity (%) between 0-100
            #lppFrame.add_analog_input(key, float(value))
            dataPayload["channels"][str(key)] = {"relative_humidity": float(value)}

        elif key == 58 :
            # Pressure (kPa)
            #lppFrame.add_pressure(key, float(value) * 10) # hPa conversion
            dataPayload["channels"][str(key)] = {"atmospheric_pressure": float(value) * 10}

        elif key == 87 or key == 88 or key == 89 :
            dataPayload["channels"][str(key)] = {"concentration": float(value)}

        elif key == 112 :
            # eCO2_gas_CCS811 (ppm)
            #lppFrame.add_analog_input(key, float(value) / 1000)
            dataPayload["channels"][str(key)] = {"ppm": float(value)}

        elif key == 113 :
            # VOC_gas_CCS811 (ppb)
            #lppFrame.add_analog_input(key, float(value) / 1000)
            dataPayload["channels"][str(key)] = {"ppb": float(value)}

        else:
            # Other data (for futur ?)
            #lppFrame.add_analog_input(key, float(value))
            dataPayload["channels"][str(key)] = {"generic": float(value)}

    #if len(lppFrame.bytes()) > 0:
    #    binaryLpp = lppFrame.bytes()
    #    try:
    #        mqtt_to_iot_datastore.postBinaryLpp(deviceId, binaryLpp)
    #    except:
    #        __log.error("Unable to process message !", exc_info=True)

    try:
        mqtt_to_iot_datastore.postDataPayload(deviceId, dataPayload)
    except:
        __log.error("Unable to process message !", exc_info=True)

# Override onMqttSubscribedMessage function
mqtt_to_iot_datastore.onMqttSubscribedMessage = onMqttSubscribedMessage

if __name__ == '__main__':
    mqtt_to_iot_datastore.startDaemon(MQTT_CLIENT_ID)

