import re

import mqtt_to_iot_datastore
from mqtt_to_iot_datastore import __log, SUBSCRIBED_TOPIC

MQTT_CLIENT_ID = 'binary-lpp-mqtt_to_iot-datastore_daemon'

def onMqttSubscribedMessage(client, userdata, msg):
    # Do not process retained message.
    if msg.retain:
        return

    __log.debug("Received MQTT message on subscribed topic: [%s] with payload: [%s], raw message: [%s]", msg.topic, str(msg.payload), str(msg))
    prefixToStrip = re.sub(r'[^/]*$', '', SUBSCRIBED_TOPIC).replace('#', '').replace('/', '.') #
    deviceId = mqtt_to_iot_datastore.removePrefix(msg.topic.replace('/', '.'), prefixToStrip)
    binaryLpp = msg.payload
    try:
        mqtt_to_iot_datastore.postBinaryLpp(deviceId, binaryLpp)
    except:
        __log.error("Unable to process message !", exc_info=True)

# Override onMqttSubscribedMessage function
mqtt_to_iot_datastore.onMqttSubscribedMessage = onMqttSubscribedMessage

if __name__ == '__main__':
    mqtt_to_iot_datastore.startDaemon(MQTT_CLIENT_ID)

