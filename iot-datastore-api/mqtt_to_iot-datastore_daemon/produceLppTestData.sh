#! /bin/bash -e
scriptDir="$( readlink -f $( dirname $0 ) )"

deviceUid="$1"
lppChannel="$2"
destHost="${3:-mydockerweb.local}"
iotApiUrl="http://iot.$destHost/api/v1"
mqttHost="$destHost"
mqttPort="1883"

usage() {
	>&2 echo "$1"
	>&2 echo "usage: $0 <deviceUid> <LppChannel> [destHost]"
	exit 1
}

test -n "$deviceUid" || usage "Missing deviceUid param !"
test -n "$lppChannel" || usage "Missing LppChannel param !"

hexToBytes() {
        hex="$1"
        #echo -ne "$( echo $hex | sed -e 's/../\\x&/g' )"
        echo -n "$hex" | perl -e 'print pack "H*", <STDIN>' 2> /dev/null
}

# Create device in IoT API
curl -XPOST "$iotApiUrl/datastore/devices/$deviceUid" || true

# Publish LPP messages
if $DEBUG 2> /dev/null; then
	msgProducerArgs="-v"
       	mosquittoArgs="-d"
else
	msgProducerArgs=""
      	mosquittoArgs=""
fi

msgProducerCmd="$scriptDir/msg_producer.py $msgProducerArgs"
mqttTopic="public/$deviceUid"
$msgProducerCmd -l "$lppChannel" --temperature | $msgProducerCmd --chain -l "$((lppChannel+1))" --gps | $msgProducerCmd --chain -l "$((lppChannel+2))" --gyrometer | $msgProducerCmd --chain -l "$((lppChannel+3))" --humidity | while read lpp; do
	hexToBytes "$lpp" | mosquitto_pub -h "$mqttHost" -p "$mqttPort" -t "$mqttTopic" -s $args
	>&2 echo "Pushed hex LPP: [$lpp] to MQTT on topic: [$mqttTopic] in binary format."
done

