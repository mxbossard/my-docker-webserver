import sys
import os
import datetime
import requests
import time
import paho.mqtt.client as mqtt
#from influxdb_client import InfluxDBClient, Point
#from influxdb_client.client.write_api import SYNCHRONOUS
import iot_datastore.iot_service as IotService


# IotService.storeLppByDeviceUid(deviceId, lpp, source)


MEASUREMENT_DEFAULT_NAME = 'mqtt-data'
MEASUREMENT_SOURCE = 'mqtt_daemon'

MQTT_HOSTNAME = sys.argv[1]
MQTT_PORT = 1883
MQTT_CLIENT_ID = 'mqtt-to-influxdb-daemon'

MSG_TOPIC = sys.argv[2]

INFLUX_URL = sys.argv[3]
#INFLUX_USER = "root"
#INFLUX_PASSWORD = "root"
INFLUX_USER = ''
INFLUX_PASSWORD = ''
INFLUX_DB_NAME = sys.argv[4]

print('Launching MQTT to InfluxDB daemon [MQTT: %s:%d (%s)] => [InfluxDB %s (%s)]' % (MQTT_HOSTNAME, MQTT_PORT, MSG_TOPIC, INFLUX_URL, INFLUX_DB_NAME))

write_api = None
query_api = None

# measurement: "nom", tags: map, time: "2009-11-10T23:00:00Z", fields: map
def publishData(measurement, tags, time, fields):
    p = Point(measurement).time(time)
    #print("Will plublish %s" % (str(p)))

    for key, value in tags.items():
        p.tag(key, value)
        
    for key, value in fields.items():
        p.field(key, value)

    write_api.write(bucket=INFLUX_DB_NAME, record=p)
    #print("Published data point in influxdb.")

def processMessage(topic, payload):
    # First case : payload contain raw data
    time = datetime.datetime.utcnow()
    formattedTime = time.strftime('%Y-%m-%dT%H:%M:%S.%fZ')

    #print("formattedTime: ", formattedTime)

    #measurement = topic.replace("/", ".")
    measurement = MEASUREMENT_DEFAULT_NAME
    tags = { 'source': MEASUREMENT_SOURCE, 'topic': topic , 'qualifier': 'raw_data' }
    payloadValue = float(payload)
    fields = { 'value': payloadValue }
    
    publishData(measurement, tags, formattedTime, fields)

def onMqttLog(client, userdata, level, buf):
    if level <= mqtt.MQTT_LOG_WARNING:
        print("log: [%s] %s" % (level, buf))

# The callback for when the client receives a CONNACK response from the server.
def onMqttConnect(client, userdata, flags, rc):
    if rc > 0:
        raise Exception('Exception in MQTT connection with code: {rc}')
    
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.message_callback_add(MSG_TOPIC, onMqttSubscribedMessage)
    client.subscribe(MSG_TOPIC, qos=2)

def onMqttSubscribedMessage(client, userdata, msg):
    #print("DEBUG: Received MQTT message on topic: [", msg.topic, "] with payload: [", str(msg.payload), "], raw message: [", msg, "]")
    processMessage(msg.topic, msg.payload)

# The callback for when a PUBLISH message is received from the server.
def onMqttMessage(client, userdata, msg):
    #print("DEBUG: Received MQTT message on topic: [", msg.topic, "] with payload: [", str(msg.payload), "], raw message: [", msg, "]")
    return

def buildMqttClient():
    # clean_session=True is needed to be sure we don't loose qos 1 and 2 messages before stored in influxdb. 
    # But with clean_session=True a qos=2 message can be received twice.
    client = mqtt.Client(client_id=MQTT_CLIENT_ID, clean_session=False, userdata=None, protocol=mqtt.MQTTv311, transport='tcp')
    client.enable_logger(logger=None)
    client.on_connect = onMqttConnect
    client.on_message = onMqttMessage # Default on_message callback used when no other message_callback_add trapped.
    client.on_log = onMqttLog
    return client

def connectMqttClient(mqttClient):
    print(f'Attempt connection to MQTT broker {MQTT_HOSTNAME}:{MQTT_PORT} ...')
    mqttClient.connect(MQTT_HOSTNAME, port=MQTT_PORT)
    print(f'Connected to MQTT broker {MQTT_HOSTNAME}:{MQTT_PORT} .')
    return mqttClient

def buildInfluxDbClient():
    influxClient = InfluxDBClient(url=INFLUX_URL, token="", org="testOrg", timeout=3000, enable_gzip=True)

    return influxClient

def createInluxDbBucket():
    # Create DB if it does not exists
    print(f'Attempt creation of InfluxDB bucket {INFLUX_DB_NAME} ...')
    createDbQuery = f'CREATE DATABASE "{INFLUX_DB_NAME}";'
    res = requests.post(f'{INFLUX_URL}/query', data={'q': createDbQuery})
    #print(res.status_code, res.reason)
    res.raise_for_status()


while True:
    try:
        createInluxDbBucket()

        influxClient = buildInfluxDbClient()
        write_api = influxClient.write_api(write_options=SYNCHRONOUS)
        query_api = influxClient.query_api()

        mqttClient = buildMqttClient()
        connectMqttClient(mqttClient)
        # Blocking call that processes network traffic, dispatches callbacks and handles reconnecting.
        mqttClient.loop_forever(retry_first_connection=False)
            
    except Exception as e:
        try:
            mqttClient.disconnect()
        except Exception:
            pass
        print(f'Exception raised in daemon: %s' % str(e))
        time.sleep(5)
    except KeyboardInterrupt:
        try:
            mqttClient.disconnect()
        except Exception:
            pass
        sys.exit(0)

