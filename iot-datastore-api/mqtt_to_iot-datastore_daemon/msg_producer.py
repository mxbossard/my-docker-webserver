#! /usr/bin/python3

# need packages: cayenneLPP

#from __future__ import print_function
import os
import random
import time
import sys
import argparse
import logging

_info = False
_debug = False

def rawDataArray(values):
    for val in values[:-1 or None]:
        # All values except last one
        rawData(val, lineFeed=False)
    # Last value
    rawData(values[-1])

def rawData(value, lineFeed=True):
    if isinstance(value, (bytes, bytearray)):
        #sys.stderr.write(f'Outputing binary: [{value.hex()}] (hex format displayed only).\n')
        with os.fdopen(sys.stdout.fileno(), "wb", closefd=False) as stdout:
            stdout.write(value)
            if lineFeed: 
                stdout.write(b'\n')
    else:
        #sys.stderr.write(f'Outputing: [{value}].\n')
        #print(str(value))
        sys.stdout.write(str(value))
        if lineFeed: 
            sys.stdout.write('\n')
        else:
            sys.stdout.write(' ')
    sys.stdout.flush()

def rawData2(values, separator=' '):
    global _debug
    if _debug: eprint(f'Outputing: [{values}] with separator: [{separator}].')
    for val in values[:-1 or None]:
        # All values except last one
        sys.stdout.write(str(val).strip())
        sys.stdout.write(separator)
    # Last value
    sys.stdout.write(str(values[-1]).strip())
    sys.stdout.write('\n')
    sys.stdout.flush()

def __readFromStdIn():
    return sys.stdin.readline()

def cayenneLppData(lppChannel, lppType, value):
    # Without cayennelpp
    dataType = 3 # Analog output
    val_i = int(value * 100)
    val_i = __to_unsigned(val_i)
    lpp = bytearray([lppChannel, dataType]) + __to_bytes(val_i, 2)
    return lpp

def cayenneLppData2(lppChannel, lppType, value):
    data = None
    if lppType == 103:
        data = lpp_temperature_to_bytes(value)
    if lppType == 104:
        data = lpp_humidity_to_bytes(value)
    if lppType == 134:
        data = lpp_gyro_to_bytes(value)
    if lppType == 136:
        data = lpp_gps_to_bytes(value)

    lpp = bytearray([lppChannel, lppType]) + data
    return [lpp.hex()]

def produceUniformRandomData(a, b):
    return random.uniform(a, b)

def produceGaussianRandomData(a, b, mu, sigma):
    val = None
    while (not val or val < 0 or val > 1):
        adaptedSigma = sigma / abs(b - a)
        val = random.gauss(mu, sigma)
        if _debug: eprint('random gaussian val: [%f] for sigma: [%f] and mu: [%f]' % (val, sigma, mu))
        #val = (val + 1)/2

    return val * (b - a) + a

def produceContinuedRandomData(a, b, continuitySigma, latestContinuedValue):
    if not latestContinuedValue:
        latestContinuedValue = produceUniformRandomData(a, b)
    
    mu = (latestContinuedValue - a) / (b - a)
    val = produceGaussianRandomData(a, b, mu, continuitySigma)
    return val

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)
    sys.stderr.flush()

def parseArgs():
    parser = argparse.ArgumentParser()
    # DISTRIBUTION params
    distributionGroup = parser.add_mutually_exclusive_group()
    distributionGroup.add_argument('-u', help='Uniform random distribution', dest='uniformRandom', action='store_true')
    distributionGroup.add_argument('-g', help='Gaussian random distribution (sigma is adapted from [0, 1] range)', nargs='?', type=float, dest='gaussianSigma', const=0.001)
    distributionGroup.add_argument('-c', help='"Continue" random distribution (sigma is adapted from [0, 1] range) (default)', nargs='?', type=float, dest='continueSigma', const=0.001)
    # RANGE params
    parser.add_argument('-a', help='Minimum data range (0 by default)', action='append', type=float, dest='minRange', default=None)
    parser.add_argument('-b', help='Maximum data range (42 by default)', action='append', type=float, dest='maxRange', default=None)
    parser.add_argument('--boundaries', help='Minimum and Maximum data range ([0, 42] by default)', nargs=2, action='append', type=float, dest='rangeBoundaries', default=None)
    # CONTROLL params
    parser.add_argument('-s', help='Random seed to be able to reproduce data series', dest='randomSeed', default=None)
    parser.add_argument('-d', help='Data dimension (1 by default)', type=int, dest='dimension', default=1)
    parser.add_argument('-v', help='Verbose (debugging)', action='count', dest='verbose', default=0)
    controllGroup = parser.add_mutually_exclusive_group()
    controllGroup.add_argument('-p', help='Period between samples in seconds (1s by default)', type=float, dest='period', default=1)
    controllGroup.add_argument('--chain', help='Chain input data. Publish a new sample every time it received data on input.', dest='chainInput', action='store_true')
    # PROCESSOR params
    processorGroup = parser.add_mutually_exclusive_group()
    processorGroup.add_argument('-i', help='Integer cast processor (default)', dest='intProcessor', action='store_true')
    processorGroup.add_argument('-f', help='Float cast processor', dest='floatProcessor', action='store_true')
    processorGroup.add_argument('-l', '--lpp', help='LPP processor', nargs='?', type=int, dest='lppChannel', const=1)
    # FORMATTER params
    formatGroup = parser.add_mutually_exclusive_group()
    formatGroup.add_argument('-D', help='Decimal text formatter (default)', dest='decimalFormatter', action='store_true')
    formatGroup.add_argument('-X', help='Hexadecimal text formatter', dest='hexadecimalFormatter', action='store_true')
    formatGroup.add_argument('-B', help='Bytes formatter', dest='bytesFormatter', action='store_true')
    # PROFILE params
    profileGroup = parser.add_mutually_exclusive_group()
    profileGroup.add_argument('--temperature', help='Temperature data profile', dest='dataProfile', action='store_const', const=103)
    profileGroup.add_argument('--humidity', help='Humidity data profile', dest='dataProfile', action='store_const', const=104)
    profileGroup.add_argument('--gps', help='GPS data profile', dest='dataProfile', action='store_const', const=136)
    profileGroup.add_argument('--gyrometer', help='Gyrometer data profile', dest='dataProfile', action='store_const', const=134)

    args = parser.parse_args()
    return args

def main(argv):
    global _info, _debug

    args = parseArgs()

    if args.minRange:
        numericMin = args.minRange
    else:
        numericMin = [0]
    if args.maxRange:
        numericMax = args.maxRange
    else:
        numericMax = [42]
    seed = args.randomSeed
    random.seed(seed)
    periodInSec = args.period
    dimension = args.dimension
    if args.gaussianSigma:
        gaussianSigma = args.gaussianSigma
    elif args.continueSigma:
        gaussianSigma = args.continueSigma
    else:
        gaussianSigma = 0.001
    dataProfile = args.dataProfile
    processor = None
    outputFormater = None
    dataSeparator = ' '
    randomFunc = None
    chainInput = args.chainInput
    _info = (args.verbose >= 1)
    _debug = (args.verbose >= 2)

    if _debug: eprint(args)

    # DISTRIBUTION
    if args.uniformRandom:
        randomFunc = lambda a, b, previous : produceUniformRandomData(a, b)
    elif args.gaussianSigma:
        mu = 0.5
        randomFunc = lambda a, b, previous : produceGaussianRandomData(a, b, mu, gaussianSigma)
    else:
        # Default distribution
        randomFunc = lambda a, b, previous : produceContinuedRandomData(a, b, gaussianSigma, previous)

    # PROCESSOR
    if args.floatProcessor:
        processor = lambda values, x : [ float(v) for v in values ]
    elif args.lppChannel:
        dimension = 1 # Force dimension 1
        dataSeparator = ''
        if not dataProfile: dataProfile = 103 # Temperature profile by default
        processor = lambda values, dataProfile : cayenneLppData2(args.lppChannel, dataProfile, tuple(values))
    else:
        # Default processor
        processor = lambda values, x : [ int(v) for v in values ]

    # FORMATTER
    if args.hexadecimalFormatter:
        raise NotImplementedError('Hexadecimal formatter is not implemented yet !')
    elif args.bytesFormatter:
        raise NotImplementedError('Bytes formatter is not implemented yet !')
    else:
        # Default formatter
        outputFormater = lambda x : x

    # PROFILE
    lppTransform = None
    dataProfileLabel = 'not profiled'
    if dataProfile == 103:
        # Temperature profile: 0.1° signed 2 bytes
        dataProfileLabel = 'Temperature'
        dimension = 1
        numericMin = [-18]
        numericMax = [42]
        lppTransform = lambda data : lpp_temperature_to_bytes(data)

    elif dataProfile == 104:
        # Humidity profile: 0.5% unsigned 1 byte
        dataProfileLabel = 'Humidity'
        dimension = 1
        numericMin = [0]
        numericMax = [100]
        lppTransform = lambda data : lpp_humidity_to_bytes(data)

    elif dataProfile == 136:
        # GPS profile: 2x 0.0001° signed 4 bytes + 1x 0.01m signed 2 bytes
        dataProfileLabel = 'GPS'
        gaussianSigma = 0.001
        dimension = 3
        # Arround Orleans
        numericMin = [47.8976, 1.8938, 100]
        numericMax = [47.9078, 1.9191, 140]
        lppTransform = lambda data : lpp_gps_to_bytes(data)

    elif dataProfile == 134:
        # Gyrometer profile: 3x 0.01°/s signed 2 bytes
        dataProfileLabel = 'Gyrometer'
        dimension = 3
        numericMin = [-10, -10, -10]
        numericMax = [10, 10, 10]
        lppTransform = lambda data : lpp_gyro_to_bytes(data)


    previousValues = [None] * dimension
    while 1:
        values = [None] * dimension 
        for i in range(dimension):
            minVal = numericMin[min(i, len(numericMin) - 1)]
            maxVal = numericMax[min(i, len(numericMax) - 1)]
            previousVal = previousValues[i]
            values[i] = randomFunc(minVal, maxVal, previousVal)
            if _info: eprint(f'Produced %s value: [%f <= %f <= %f]' % (dataProfileLabel, minVal, values[i], maxVal))

        processedValues = processor(values, dataProfile)
        formattedValues = outputFormater(processedValues)
       
        if chainInput:
            # Block until one line of input is read
            if _info: eprint('Waiting for stdin data ...')
            dataInput = __readFromStdIn()
            if not dataInput:
                # Input is closed
                sys.exit(0) 
            rawData2([dataInput] + formattedValues, separator=dataSeparator)
            # Do not wait
        else:
            rawData2(formattedValues, separator=dataSeparator)
            # Wait for periodInSec
            time.sleep(periodInSec)
            
        previousValues = values


### Code duplication from https://github.com/smlng/pycayennelpp/blob/master/cayennelpp/lpp_type.py

def __assert_data_tuple(data, num):
    if not isinstance(data, tuple):
        data = (data,)
    if not len(data) == num:
        raise AssertionError()
    return data

def __to_bytes(val, buflen):
    logging.debug("  in:    value = %d", val)
    buf = bytearray(buflen)
    for i in range(buflen):
        shift = (buflen - i - 1) * 8
        buf[i] = (val >> shift) & 0xff
    logging.debug("  out:   bytes = %s, length = %d", buf, len(buf))
    return buf


def __to_signed(val, bits):
    """
    internal function to convert a unsigned integer to signed
    of given bits length
    """
    logging.debug("  in:    value = %d", val)
    mask = 0x00
    for i in range(int(bits / 8)):
        mask |= 0xff << (i * 8)
    if val >= (1 << (bits - 1)):
        val = -1 - (val ^ mask)
    logging.debug("  out:   value = %d", val)
    return val


def __to_s16(val):
    return __to_signed(val, 16)


def __to_s24(val):
    return __to_signed(val, 24)


def __to_unsigned(val):
    """
    convert signed (2 complement) value to unsigned
    """
    if val < 0:
        val = ~(-val - 1)
    return val

def lpp_temperature_to_bytes(data):
    """
    Encode temperature sensor data into CayenneLPP,
    and return as a byte buffer
    """
    logging.debug("lpp_temperature_to_bytes")
    data = __assert_data_tuple(data, 1)
    val = data[0]
    logging.debug("  in:    value = %f", val)
    val_i = int(val * 10)
    logging.debug("  in:    value = %d", val_i)
    val_i = __to_unsigned(val_i)
    return __to_bytes(val_i, 2)

def lpp_humidity_to_bytes(data):
    """
    Encode humidity sensor data into CayenneLPP,
    and return as a byte buffer
    """
    logging.debug("lpp_humidity_to_bytes")
    data = __assert_data_tuple(data, 1)
    val = data[0]
    logging.debug("  in:    value = %f", val)
    val_i = int(val * 2)
    if val_i < 0:
        raise ValueError("Humidity sensor values must be positive!")
    return __to_bytes(val_i, 1)

def lpp_accel_to_bytes(data):
    """
    Encode accelerometer sensor data into CayenneLPP,
    and return as a byte buffer
    """
    logging.debug("lpp_accel_to_bytes")
    data = __assert_data_tuple(data, 3)
    val_x = data[0]
    val_y = data[1]
    val_z = data[2]
    logging.debug("  in:    x = %f, y = %f, z = %f", val_x, val_y, val_z)
    val_xi = int(val_x * 1000)
    val_yi = int(val_y * 1000)
    val_zi = int(val_z * 1000)
    logging.debug("  in:    x = %d, y = %d, z = %d", val_xi, val_yi, val_zi)
    val_xi = __to_unsigned(val_xi)
    val_yi = __to_unsigned(val_yi)
    val_zi = __to_unsigned(val_zi)
    logging.debug("  in:    x = %d, y = %d, z = %d", val_xi, val_yi, val_zi)
    buf = bytearray([0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
    buf[0:2] = __to_bytes(val_xi, 2)
    buf[2:4] = __to_bytes(val_yi, 2)
    buf[4:6] = __to_bytes(val_zi, 2)
    logging.debug("  out:   bytes = %s, length = %d", buf, len(buf))
    return buf

def lpp_gyro_to_bytes(data):
    """
    Encode gyrometer sensor data into CayenneLPP,
    and return as a byte buffer
    """
    logging.debug("lpp_gyro_to_bytes")
    data = __assert_data_tuple(data, 3)
    val_x = data[0]
    val_y = data[1]
    val_z = data[2]
    logging.debug("  in:    x = %f, y = %f, z = %f", val_x, val_y, val_z)
    val_xi = int(val_x * 100)
    val_yi = int(val_y * 100)
    val_zi = int(val_z * 100)
    logging.debug("  in:    x = %d, y = %d, z = %d", val_xi, val_yi, val_zi)
    val_xi = __to_unsigned(val_xi)
    val_yi = __to_unsigned(val_yi)
    val_zi = __to_unsigned(val_zi)
    logging.debug("  in:    x = %d, y = %d, z = %d", val_xi, val_yi, val_zi)
    buf = bytearray([0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
    buf[0:2] = __to_bytes(val_xi, 2)
    buf[2:4] = __to_bytes(val_yi, 2)
    buf[4:6] = __to_bytes(val_zi, 2)
    logging.debug("  out:   bytes = %s, length = %d", buf, len(buf))
    return buf

def lpp_gps_to_bytes(data):
    """
    Encode GPS data into CayenneLPP,
    and return as a byte buffer
    """
    logging.debug("lpp_gps_to_bytes")
    data = __assert_data_tuple(data, 3)
    lat = data[0]
    lon = data[1]
    alt = data[2]
    logging.debug("  in:    latitude = %f, longitude = %f, altitude = %f",
                  lat, lon, alt)

    lat_i = int(lat * 10000)
    lon_i = int(lon * 10000)
    alt_i = int(alt * 100)
    logging.debug("  in:    latitude = %d, longitude = %d, altitude = %d",
                  lat_i, lon_i, alt_i)
    lat_i = __to_unsigned(lat_i)
    lon_i = __to_unsigned(lon_i)
    alt_i = __to_unsigned(alt_i)
    logging.debug("  in:    latitude = %d, longitude = %d, altitude = %d",
                  lat_i, lon_i, alt_i)
    buf = bytearray([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
    buf[0:3] = __to_bytes(lat_i, 3)
    buf[3:6] = __to_bytes(lon_i, 3)
    buf[6:9] = __to_bytes(alt_i, 3)
    logging.debug("  out:   bytes = %s, length = %d", buf, len(buf))
    return buf

try:
    if __name__ == "__main__":
        main(sys.argv[1:])
except KeyboardInterrupt:
    sys.exit(0)
