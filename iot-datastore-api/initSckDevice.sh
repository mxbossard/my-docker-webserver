#! /bin/bash -e
scriptDir="$( readlink -f $( dirname $0 ) )"

usage() {
	>&2 echo "usage: $0 <DEVICE_UID> [API_DOMAIN_NAME]"
	exit 1
}

source $scriptDir/../scripts/projectEnv.sh

DEVICE_UID="$1"
API_BASE_URL="${2:-$HTTP_PROTOCOL://$VIRTUAL_HOST}"
DATASTORE_URL="$API_BASE_URL$API_CONTEXT_ROOT/v1/datastore"

test -n "$DEVICE_UID" || usage

. "$scriptDir/sckDeviceConfig.sh"

curl -X POST -H 'Content-Type: application/json' --data "$sckDeviceConfig" -- "$DATASTORE_URL/devices"
curl -X POST -H 'Content-Type: application/json' --data "$sckTags" -- "$DATASTORE_URL/devices/$DEVICE_UID/tags"

curl -sSf -X GET -- "$DATASTORE_URL/devices/$DEVICE_UID"
