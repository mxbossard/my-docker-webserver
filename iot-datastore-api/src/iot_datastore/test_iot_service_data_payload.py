from datetime import datetime, timezone
from unittest.mock import call, ANY
import copy
import base64
import pytest
from pytest_mock import MockerFixture
import tempfile
import flask
from cayennelpp import LppFrame
import iot_datastore.iot_service as IotService
import iot_datastore.db_models as DbModels
from iot_datastore.exceptions import NotProcessablePayload
from iot_datastore.test_iot_service_commons import *
from iot_datastore import iot_tags


def testStoreDataPayloadWithoutMapping(mocker: MockerFixture) -> None:
    mockedInfluxDbService_storeMeasurement = mocker.patch('iot_datastore.influxdb_service.storeMeasurement')
    mockedPromscaleService_storeMeasurement = mocker.patch('iot_datastore.promscale_service.storeMeasurement')
    mockedNowDatetime = datetime.utcfromtimestamp(1000000)
    mocker.patch('iot_datastore.helper.helper.getUtcnowDatetime').return_value = mockedNowDatetime

    deviceUid = 'y42'
    deviceName = 'foo'
    deviceLocation = 'bar'
    device = DbModels.Device(deviceUid, deviceName, deviceLocation)

    device.addDeviceTag('name', 'temp_sensor_test')
    device.addDeviceTag('version', '1.0')
    device.addDeviceTag('foo', 'baz')

    def findDeviceByIdMethod(uid):
        return device
    mocker.patch('iot_datastore.iot_service.findDeviceById', side_effect=findDeviceByIdMethod)

    dq0 = DbModels.DataQuantity('voltage', [3.5])
    dc0 = DbModels.DataChannel(0, [dq0])
    dq0 = DbModels.DataQuantity('temperature', [17.2])
    dq1 = DbModels.DataQuantity('humidity', [0.53])
    dc1 = DbModels.DataChannel(1, [dq0, dq1])
    dq0 = DbModels.DataQuantity('temperature', [18.1])
    dq1 = DbModels.DataQuantity('humidity', [0.58])
    dq2 = DbModels.DataQuantity('pressure', [1015])
    dc2 = DbModels.DataChannel(2, [dq0, dq1, dq2])
    dq0 = DbModels.DataQuantity('digital_input', [3])
    dc100 = DbModels.DataChannel(100, [dq0])
    dq0 = DbModels.DataQuantity('digital_input', [2])
    dc101 = DbModels.DataChannel(101, [dq0])
    dq0 = DbModels.DataQuantity('digital_input', [3])
    dc102 = DbModels.DataChannel(102, [dq0])
    dq0 = DbModels.DataQuantity('digital_input', [0])
    dc200 = DbModels.DataChannel(200, [dq0])
    dq0 = DbModels.DataQuantity('digital_input', [1])
    dc201 = DbModels.DataChannel(201, [dq0])
    dq0 = DbModels.DataQuantity('digital_input', [0])
    dc202 = DbModels.DataChannel(202, [dq0])
    gp = DbModels.DataPayload([dc0, dc1, dc2, dc100, dc101, dc102, dc200, dc201, dc202])

    IotService.storeDataPayload(device.uid, gp, 'UnitTest', 'UnitTest')

    extraTags = {'name': 'temp_sensor_test', 'version': '1.0', 'foo': 'baz'}
    expectedSource = 'data_payload'
    chan0Tags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 0, None, 'voltage', 'V', expectedSource, 'avg', None, 'value', extraTags)
    chan1aTags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 1, None, 'temperature', '°C', expectedSource, 'avg', None, 'value', extraTags)
    chan1bTags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 1, None, 'humidity', '%', expectedSource, 'avg', None, 'value', extraTags)
    chan2aTags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 2, None, 'temperature', '°C', expectedSource, 'avg', None, 'value', extraTags)
    chan2bTags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 2, None, 'humidity', '%', expectedSource, 'avg', None, 'value', extraTags)
    chan2cTags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 2, None, 'pressure', 'hPa', expectedSource, 'avg', None, 'value', extraTags)
    chan100Tags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 100, None, 'digital_input', None, expectedSource, 'avg', None, 'value', extraTags)
    chan101Tags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 101, None, 'digital_input', None, expectedSource, 'avg', None, 'value', extraTags)
    chan102Tags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 102, None, 'digital_input', None, expectedSource, 'avg', None, 'value', extraTags)
    chan200Tags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 200, None, 'digital_input', None, expectedSource, 'avg', None, 'value', extraTags)
    chan201Tags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 201, None, 'digital_input', None, expectedSource, 'avg', None, 'value', extraTags)
    chan202Tags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 202, None, 'digital_input', None, expectedSource, 'avg', None, 'value', extraTags)

    chan0Fields = {'value': 3.5}
    chan1aFields = {'value': 17.2}
    chan1bFields = {'value': 0.53}
    chan2aFields = {'value': 18.1}
    chan2bFields = {'value': 0.58}
    chan2cFields = {'value': 1015}
    chan100Fields = {'value': 3}
    chan101Fields = {'value': 2}
    chan102Fields = {'value': 3}
    chan200Fields = {'value': 0}
    chan201Fields = {'value': 1}
    chan202Fields = {'value': 0}
    
    mockedCalls = [
        call(buildMeasurementNameFromTags(chan0Tags), chan0Tags, mockedNowDatetime, chan0Fields),
        call(buildMeasurementNameFromTags(chan1aTags), chan1aTags, mockedNowDatetime, chan1aFields),
        call(buildMeasurementNameFromTags(chan1bTags), chan1bTags, mockedNowDatetime, chan1bFields),
        call(buildMeasurementNameFromTags(chan2aTags), chan2aTags, mockedNowDatetime, chan2aFields),
        call(buildMeasurementNameFromTags(chan2bTags), chan2bTags, mockedNowDatetime, chan2bFields),
        call(buildMeasurementNameFromTags(chan2cTags), chan2cTags, mockedNowDatetime, chan2cFields),
        call(buildMeasurementNameFromTags(chan100Tags), chan100Tags, mockedNowDatetime, chan100Fields),
        call(buildMeasurementNameFromTags(chan101Tags), chan101Tags, mockedNowDatetime, chan101Fields),
        call(buildMeasurementNameFromTags(chan102Tags), chan102Tags, mockedNowDatetime, chan102Fields),
        call(buildMeasurementNameFromTags(chan200Tags), chan200Tags, mockedNowDatetime, chan200Fields),
        call(buildMeasurementNameFromTags(chan201Tags), chan201Tags, mockedNowDatetime, chan201Fields),
        call(buildMeasurementNameFromTags(chan202Tags), chan202Tags, mockedNowDatetime, chan202Fields)
    ]

    for k, called in enumerate(mockedInfluxDbService_storeMeasurement.call_args_list):
        assert called.args[0] == mockedCalls[k].args[0], 'storeMeasurement called with bad deviceName !'
        assert called.args[1] == mockedCalls[k].args[1], 'storeMeasurement called with bad tags !'
        assert called.args[2] == mockedCalls[k].args[2], 'storeMeasurement called with bad time !'
        assert called.args[3] == mockedCalls[k].args[3], 'storeMeasurement called with bad fields !'


def testStoreLppFrameByDeviceWithFullMapping(mocker: MockerFixture) -> None:
    mockedInfluxDbService_storeMeasurement = mocker.patch('iot_datastore.influxdb_service.storeMeasurement')
    mockedPromscaleService_storeMeasurement = mocker.patch('iot_datastore.promscale_service.storeMeasurement')
    mockedNowDatetime = datetime.utcfromtimestamp(1000000)
    mocker.patch('iot_datastore.helper.helper.getUtcnowDatetime').return_value = mockedNowDatetime

    deviceUid = 'x42'
    deviceName = 'foo'
    deviceLocation = 'bar'
    device = DbModels.Device(deviceUid, deviceName, deviceLocation)

    def findDeviceByIdMethod(uid):
        return device
    mocker.patch('iot_datastore.iot_service.findDeviceById', side_effect=findDeviceByIdMethod)

    device.addDeviceTag('name', 'temp_sensor_test')
    device.addDeviceTag('version', '1.0')
    device.addDeviceTag('foo', 'baz')

    device.addChannelTag(0, '_channelName', 'chan0')
    device.addQuantityTag(0, 'voltage', '_label', 'Battery voltage')

    device.addChannelTag(1, '_channelName', 'dht22_sensor')
    device.addQuantityTag(1, 'temperature', '_label', 'Temp in °C')
    device.addQuantityTag(1, 'humidity', '_label', 'Humidity')

    device.addChannelTag(2, '_channelName', 'bme280_sensor')
    device.addQuantityTag(2, 'temperature', '_label', 'Temp in °F')
    device.addQuantityTag(2, 'humidity', '_label', 'Humidity')
    device.addQuantityTag(2, 'pressure', '_label', 'Pressure')

    device.addChannelTag(100, '_channelName', 'device_probings')
    device.addQuantityTag(100, 'digital_input', '_aggregationMethod', 'sum')
    device.addQuantityTag(100, 'digital_input', '_label', 'Probing count')
    device.addQuantityTag(100, 'digital_input', 'probing', 'probing')
    device.addChannelTag(101, '_channelName', 'dht22_probings')
    device.addQuantityTag(101, 'digital_input', '_aggregationMethod', 'sum')
    device.addQuantityTag(101, 'digital_input', '_label', 'Probing count')
    device.addQuantityTag(101, 'digital_input', 'probing', 'probing')
    device.addChannelTag(102, '_channelName', 'bme280_probings')
    device.addQuantityTag(102, 'digital_input', '_aggregationMethod', 'sum')
    device.addQuantityTag(102, 'digital_input', '_label', 'Probing count')
    device.addQuantityTag(102, 'digital_input', 'probing', 'probing')

    device.addChannelTag(200, '_channelName', 'device_errors')
    device.addQuantityTag(200, 'digital_input', '_aggregationMethod', 'sum')
    device.addQuantityTag(200, 'digital_input', '_label', 'Error count')
    device.addQuantityTag(200, 'digital_input', 'error', 'error')
    device.addChannelTag(201, '_channelName', 'dht22_errors')
    device.addQuantityTag(201, 'digital_input', '_aggregationMethod', 'sum')
    device.addQuantityTag(201, 'digital_input', '_label', 'Error count')
    device.addQuantityTag(201, 'digital_input', 'error', 'error')
    device.addChannelTag(202, '_channelName', 'bme280_errors')
    device.addQuantityTag(202, 'digital_input', '_aggregationMethod', 'sum')
    device.addQuantityTag(202, 'digital_input', '_label', 'Error count')
    device.addQuantityTag(202, 'digital_input', 'error', 'error')

    dq0 = DbModels.DataQuantity('voltage', [3.5])
    dc0 = DbModels.DataChannel(0, [dq0])
    dq0 = DbModels.DataQuantity('temperature', [17.2])
    dq1 = DbModels.DataQuantity('humidity', [0.53])
    dc1 = DbModels.DataChannel(1, [dq0, dq1])
    dq0 = DbModels.DataQuantity('temperature', [18.1])
    dq1 = DbModels.DataQuantity('humidity', [0.58])
    dq2 = DbModels.DataQuantity('pressure', [1015])
    dc2 = DbModels.DataChannel(2, [dq0, dq1, dq2])
    dq0 = DbModels.DataQuantity('digital_input', [3])
    dc100 = DbModels.DataChannel(100, [dq0])
    dq0 = DbModels.DataQuantity('digital_input', [2])
    dc101 = DbModels.DataChannel(101, [dq0])
    dq0 = DbModels.DataQuantity('digital_input', [3])
    dc102 = DbModels.DataChannel(102, [dq0])
    dq0 = DbModels.DataQuantity('digital_input', [0])
    dc200 = DbModels.DataChannel(200, [dq0])
    dq0 = DbModels.DataQuantity('digital_input', [1])
    dc201 = DbModels.DataChannel(201, [dq0])
    dq0 = DbModels.DataQuantity('digital_input', [0])
    dc202 = DbModels.DataChannel(202, [dq0])
    gp = DbModels.DataPayload([dc0, dc1, dc2, dc100, dc101, dc102, dc200, dc201, dc202])

    IotService.storeDataPayload(device.uid, gp, 'UnitTest', 'UnitTest')

    extraTags = {'name': 'temp_sensor_test', 'version': '1.0', 'foo': 'baz'}
    probingTags = {**extraTags, **{'probing': 'probing'}}
    errorTags = {**extraTags, **{'error': 'error'}}
    expectedSource = 'data_payload'
    chan0Tags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 0, 'chan0', 'voltage', 'V', expectedSource, 'avg', 'Battery voltage', 'value', extraTags)
    chan1aTags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 1, 'dht22_sensor', 'temperature', '°C', expectedSource, 'avg', 'Temp in °C', 'value', extraTags)
    chan1bTags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 1, 'dht22_sensor', 'humidity', '%', expectedSource, 'avg', 'Humidity', 'value', extraTags)
    chan2aTags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 2, 'bme280_sensor', 'temperature', '°C', expectedSource, 'avg', 'Temp in °F', 'value', extraTags)
    chan2bTags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 2, 'bme280_sensor', 'humidity', '%', expectedSource, 'avg', 'Humidity', 'value', extraTags)
    chan2cTags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 2, 'bme280_sensor', 'pressure', 'hPa', expectedSource, 'avg', 'Pressure', 'value', extraTags)
    chan100Tags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 100, 'device_probings', 'digital_input', None, expectedSource, 'sum', 'Probing count', 'value', probingTags)
    chan101Tags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 101, 'dht22_probings', 'digital_input', None, expectedSource, 'sum', 'Probing count', 'value', probingTags)
    chan102Tags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 102, 'bme280_probings', 'digital_input', None, expectedSource, 'sum', 'Probing count', 'value', probingTags)
    chan200Tags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 200, 'device_errors', 'digital_input', None, expectedSource, 'sum', 'Error count', 'value', errorTags)
    chan201Tags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 201, 'dht22_errors', 'digital_input', None, expectedSource, 'sum', 'Error count', 'value', errorTags)
    chan202Tags = iot_tags.buildDataPayloadTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 202, 'bme280_errors', 'digital_input', None, expectedSource, 'sum', 'Error count', 'value', errorTags)

    chan0Fields = {'value': 3.5}
    chan1aFields = {'value': 17.2}
    chan1bFields = {'value': 0.53}
    chan2aFields = {'value': 18.1}
    chan2bFields = {'value': 0.58}
    chan2cFields = {'value': 1015}
    chan100Fields = {'value': 3}
    chan101Fields = {'value': 2}
    chan102Fields = {'value': 3}
    chan200Fields = {'value': 0}
    chan201Fields = {'value': 1}
    chan202Fields = {'value': 0}
    
    mockedCalls = [
        call(buildMeasurementNameFromTags(chan0Tags), chan0Tags, mockedNowDatetime, chan0Fields),
        call(buildMeasurementNameFromTags(chan1aTags), chan1aTags, mockedNowDatetime, chan1aFields),
        call(buildMeasurementNameFromTags(chan1bTags), chan1bTags, mockedNowDatetime, chan1bFields),
        call(buildMeasurementNameFromTags(chan2aTags), chan2aTags, mockedNowDatetime, chan2aFields),
        call(buildMeasurementNameFromTags(chan2bTags), chan2bTags, mockedNowDatetime, chan2bFields),
        call(buildMeasurementNameFromTags(chan2cTags), chan2cTags, mockedNowDatetime, chan2cFields),
        call(buildMeasurementNameFromTags(chan100Tags), chan100Tags, mockedNowDatetime, chan100Fields),
        call(buildMeasurementNameFromTags(chan101Tags), chan101Tags, mockedNowDatetime, chan101Fields),
        call(buildMeasurementNameFromTags(chan102Tags), chan102Tags, mockedNowDatetime, chan102Fields),
        call(buildMeasurementNameFromTags(chan200Tags), chan200Tags, mockedNowDatetime, chan200Fields),
        call(buildMeasurementNameFromTags(chan201Tags), chan201Tags, mockedNowDatetime, chan201Fields),
        call(buildMeasurementNameFromTags(chan202Tags), chan202Tags, mockedNowDatetime, chan202Fields)
    ]

    for k, called in enumerate(mockedInfluxDbService_storeMeasurement.call_args_list):
        assert called.args[0] == mockedCalls[k].args[0], 'storeMeasurement called with bad deviceName !'
        assert called.args[1] == mockedCalls[k].args[1], 'storeMeasurement called with bad tags !'
        assert called.args[2] == mockedCalls[k].args[2], 'storeMeasurement called with bad time !'
        assert called.args[3] == mockedCalls[k].args[3], 'storeMeasurement called with bad fields !'
