import logging
import base64
import re
from datetime import datetime, timedelta
from typing import Mapping
from cayennelpp import LppFrame, LppData
#from cayennelpp.lpp_type import LppType
#from sqlalchemy.ext.declarative import DeclarativeMeta
import iot_datastore.quantities_config as config
import iot_datastore.db_models as DbModels
import iot_datastore.influxdb_service as InfluxDbService
import iot_datastore.promscale_service as PromscaleService
import iot_datastore.helper.helper as helper
import iot_datastore.helper.encoding as EncodingHelper
from iot_datastore.exceptions import NotProcessablePayload, NotValidDataTime
from iot_datastore import iot_tags

__log = logging.getLogger(__name__)


def initDb(app, dbUrl, debug=True):
    app.config['SQLALCHEMY_BINDS'][DbModels.IOT_STORAGE_BIND] = dbUrl

    try:
        DbModels.db.init_app(app)
    except AssertionError as e:
        if debug:
            __log.warn('Problem during DB initialization !', exc_info=e)

    with app.app_context():
        DbModels.db.create_all()

def findDeviceById(uid: str) -> DbModels.Device:
    device = DbModels.Device.query.filter_by(uid=uid).first()
    return device

def findDeviceByIdOrFail(uid: str) -> DbModels.Device:
    device = findDeviceById(uid)
    if not device:
        message = 'Device #%s does not exists in database !' % uid
        __log.warning(message)
        raise KeyError(message)
    return device

def findAllDevice():
    return DbModels.Device.query.all()

def createDevice(uid: str, name: str = None, location: str = None, maxSecondsBackInTime: int = None, maxSecondsForwardInFutur: int = None) -> DbModels.Device:
    newDevice = DbModels.Device(uid, name, location, maxSecondsBackInTime, maxSecondsForwardInFutur)
    if DbModels.addEntity(newDevice):
        return newDevice
    return None

def updateDevice(device: DbModels.Device) -> DbModels.Device:
    if DbModels.mergeEntity(device):
        return device
    return None

def deleteDevice(uid: str) -> DbModels.Device:
    device = findDeviceById(uid)
    if DbModels.deleteEntity(device):
        return device
    return None

def commitDbSession() -> None:
    DbModels.commit()


def storeDataPayload(deviceUid: str, payload: DbModels.DataPayload, ingestComponent: str, ingestMethod: str) -> None:
    now = helper.getUtcnowDatetime()
    defaultEventTime = now

    device = findDeviceByIdOrFail(deviceUid)
    for channelId in payload.getChannels():
        channelTime = payload.getTime(channelId)
        if channelTime:
            eventTime = channelTime
        else:
            eventTime = defaultEventTime
        channel = payload.getChannel(channelId)
        for quantityCode in channel.getQuantities():
            quantity = channel.getQuantity(quantityCode)
            lppExtraTags = {}
            _storeQuantity(device, channelId, quantity, lppExtraTags, ingestComponent, ingestMethod, 'data_payload', now, eventTime)

# Support LPP in base64 or bytes format. URL Base64 is also supported.
def storeLppByDeviceUid(deviceUid: str, lpp, ingestComponent: str, ingestMethod: str) -> None:
    lppFrame = None

    if isinstance(lpp, str):
        # If URLsafe base64 encoded. Reencode it in standard base64.
        if EncodingHelper.isUrlSafeBase64(lpp):
            lpp = base64.standard_b64encode(base64.urlsafe_b64decode(bytes(lpp, 'ascii'))).decode('ascii')
        elif not EncodingHelper.isBase64(lpp):
            raise NotProcessablePayload('Supplied LPP is not base64 encoded !', lpp)
        try:
            __log.debug('Trying to store string LPP: [%s] for device: [%s]', lpp, deviceUid)
            decodedLpp = base64.b64decode(lpp)
            #lppFrame = LppFrame().from_base64(lpp)
            lppFrame = LppFrame().from_bytes(decodedLpp)
        except Exception as e:
            raise NotProcessablePayload('Unable to process supplied LPP !', lpp) from None 
    elif isinstance(lpp, bytes):
        try:
            __log.debug('Trying to store binary LPP: [%s] for device: [%s]', lpp.hex(), deviceUid)
            lppFrame = LppFrame().from_bytes(lpp)
        except Exception as e:
            raise NotProcessablePayload('Unable to process supplied LPP !', lpp.hex()) from None

    _storeLppFrameByDeviceUid(deviceUid, lppFrame, ingestComponent, ingestMethod)

def _storeLppFrameByDeviceUid(deviceUid: str, lppFrame: LppFrame, ingestComponent: str, ingestMethod: str) -> None:
    storedDevice = findDeviceByIdOrFail(deviceUid)

    _storeLppFrameByDevice(storedDevice, lppFrame, ingestComponent, ingestMethod)

def _storeLppFrameByDevice(device: DbModels.Device, lppFrame: LppFrame, ingestComponent: str, ingestMethod: str) -> None:
    if not lppFrame:
        raise Exception('No LppFrame supplied !')

    if not ingestComponent:
        raise Exception('No ingestComponent supplied !')

    if not ingestMethod:
        raise Exception('No ingestMethod supplied !')

    now = helper.getUtcnowDatetime()

    lppDataIterator = iter(lppFrame.data)
    doneLooping = False

    datasByChannel = {}
    # Sort Data by channel
    while not doneLooping:
        lppData = None
        try:
            lppData = next(lppDataIterator)
            #log.info("Looping on LPP data: %s", lppData)
            channelId = lppData.channel
            channelArray = datasByChannel.get(channelId, None)
            if not channelArray:
                channelArray = []
                datasByChannel[channelId] = channelArray
            if lppData:
                channelArray.append(lppData)

        except StopIteration:
            doneLooping = True

    # Search for a default unix_timestamp in all channels
    defaultEventTime = now
    for channelId in datasByChannel.keys():
        channelDatas = datasByChannel[channelId]
        if len(channelDatas) == 1 and channelDatas[0].type.type == 133:
            # Use lone unix_timestamp channel for default measurement time for all channels
            defaultEventTime = datetime.utcfromtimestamp(channelDatas[0].value[0])
            del datasByChannel[channelId]
            break

    timeErrors = []
    for channelId in datasByChannel.keys():
        channelTime = defaultEventTime
        channelDatas = datasByChannel[channelId]
        
        # Search for a channel event time
        for lppData in channelDatas:
            if lppData.type.type == 133:
                # Use channel unix_timestamp for measurement time
                channelTime = datetime.utcfromtimestamp(lppData.value[0])
                channelDatas.remove(lppData)
                break

        for lppData in channelDatas:
            try:
                _storeLppData(device, lppData, ingestComponent, ingestMethod, now, channelTime)
            except NotValidDataTime as e:
                __log.warning('Not valid time: [%s] for device: [%s] channel: [%d]', str(e.time), device.uid, channelId)
                timeErrors.append(e)

    if len(timeErrors) > 0:
        raise timeErrors[0]


def _storeLppData(device, lppData:LppData, ingestComponent, ingestMethod, now, eventTime) -> None:
    currentChannel = lppData.channel
    lppTypeId = lppData.type.type

    ### Retrive or Build LPP quantityCode
    quantityCode = config.getLppQuantityCode(lppTypeId)
    if not quantityCode:
        # Use LPP type id as type name
        quantityCode = 'unknown_' + str(lppData.type.name)

    quantity = DbModels.DataQuantity(quantityCode, lppData.value)
    lppExtraTags = {
        iot_tags.LPP_TYPE_TAG_KEY: str(lppTypeId),
    }

    _storeQuantity(device, currentChannel, quantity, lppExtraTags, ingestComponent, ingestMethod, 'lpp', now, eventTime)

def _storeQuantity(device:DbModels.Device, channelId: int, quantity:DbModels.DataQuantity, extraTags:Mapping[str, str], 
        ingestComponent: str, ingestMethod:str, ingestFormat: str, now:datetime, eventTime:datetime) -> None:
    maxSecondsBack = timedelta(seconds=device.maxSecondsBackInTime)
    maxSecondsForward = timedelta(seconds=device.maxSecondsForwardInFutur)

    if eventTime != now:
        if eventTime < now - maxSecondsBack:
            # outdated data => reject it
            raise NotValidDataTime('Supplied time is too old !', eventTime)

        if eventTime > now + maxSecondsForward:
            # data in too far futur => reject it
            raise NotValidDataTime('Supplied time is too far in the futur !', eventTime)

    quantityCode = quantity.quantity

    ### Build tags
    measurementTags = buildMeasurementTags(device, channelId, quantityCode, ingestComponent, ingestMethod, ingestFormat, extraTags)

    ### Build fields
    fields = quantity.getFields()

    # Use double underscore to build measurment name because uid can contains simple underscore
    measurementName = buildMeasurementName(device.uid, channelId, quantityCode)
    #InfluxDbService.storeMeasurement(measurementName, measurementTags, eventTime, fields)
    PromscaleService.storeMeasurement(measurementName, measurementTags, eventTime, fields)


def buildMeasurementName(deviceUid, channelId, typeId):
    #return '%s__%s__%s' % (deviceUid, channelId, typeId)
    return 'device_%s' % (deviceUid)

def buildMeasurementTags(device: DbModels.Device, channelId: int, quantityCode: str, ingestComponent: str, ingestMethod: str, ingestFormat: str, extraTags: dict):
    deviceUid = device.uid
    deviceTags = device.getDeviceTags()
    deviceName = device.getDeviceName()
    locationTag = device.getDeviceLocation()
    quantityTag = quantityCode
    typeTag = deviceTags.get(iot_tags.DEVICE_TYPE_TAG_KEY, None)
    versionTag = deviceTags.get(iot_tags.DEVICE_VERSION_TAG_KEY, None)

    ### Build units tag
    unitsTag = device.getQuantityTagValue(channelId, quantityCode, iot_tags.DATA_UNITS_TAG_KEY)
    if not unitsTag:
        unitsTag = config.getUnitsTag(quantityCode)

    ### Build label tag
    labelTag = device.getQuantityTagValue(channelId, quantityCode, iot_tags.DATA_LABEL_TAG_KEY)

    # Aggregation method mean by default
    aggregationMethod = device.getAggregationMethod(channelId, quantityCode)
    # if not aggregationMethod:
    #     aggregationMethod = config.getDefaultAggregationMethod(quantityCode)

    channelName = device.getChannelTagValue(channelId, iot_tags.CHANNEL_NAME_TAG_KEY)

    # Add device type and version tags
    if not extraTags:
        extraTags = {}
        
    if typeTag:
        extraTags[iot_tags.DEVICE_TYPE_TAG_KEY] = typeTag
    if versionTag:
        extraTags[iot_tags.DEVICE_VERSION_TAG_KEY] = versionTag

    metaTags = iot_tags.buildDataPayloadTags(ingestComponent, ingestMethod, deviceUid, deviceName, locationTag, str(channelId), channelName, quantityTag, unitsTag, ingestFormat, aggregationMethod, labelTag, None, extraTags)

    channelTags = device.getChannelTags(channelId)
    quantityTags = device.getQuantityTags(channelId, quantityCode)
    userTags = {**deviceTags, **channelTags, **quantityTags}
    # Filter to keep only non special "_" tags.
    filteredTags = {}
    for (key, value) in userTags.items():
        if not key.startswith('_'):
            filteredTags[key] = value
    measurementTags = {**filteredTags, **metaTags}
    iot_tags.validateTagsFormat(measurementTags)
    return measurementTags
