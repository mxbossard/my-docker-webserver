from datetime import datetime, timezone, timedelta
from unittest.mock import call, ANY
import copy
import base64
import pytest
from pytest_mock import MockerFixture
import tempfile
import flask
from cayennelpp import LppFrame
import iot_datastore.iot_service as IotService
import iot_datastore.db_models as DbModels
from iot_datastore.exceptions import NotProcessablePayload, NotValidDataTime
import iot_datastore.helper.helper as helper
import logging

@pytest.fixture
def app(mocker):
    #__log.info('app fixture called')
    #mocker.patch('iot_datastore.influxdb_service.createInluxDbBucket')
    app = flask.Flask(__name__)

    db_fd, app.config['DATABASE'] = tempfile.mkstemp()
    app.config['TESTING'] = True
    app.config['SQLALCHEMY_BINDS'] = {}
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_ECHO'] = False
    app.config['BUNDLE_ERRORS'] = True

    with app.app_context():
        IotService.initDb(app, 'sqlite:///' + app.config['DATABASE'], False)

    yield app

MOCKED_NOW_IN_SEC = 1000000
MOCKED_NOW = datetime.utcfromtimestamp(MOCKED_NOW_IN_SEC)

MAX_SECONDS_BACK = 180
MAX_SECONDS_FORWARD = 60
DEVICE_1 = DbModels.Device('xxx1', 'device1', 'location1', MAX_SECONDS_BACK, MAX_SECONDS_FORWARD)
DEVICE_2 = DbModels.Device('xxx2', 'device2', 'location2', MAX_SECONDS_BACK, MAX_SECONDS_FORWARD)
DEVICE_3 = DbModels.Device('xxx3', 'device3', 'location3', MAX_SECONDS_BACK, MAX_SECONDS_FORWARD)

SIMPLE_BINARY_LPP = bytes([0x03, 0x67, 0x01, 0x10, 0x05, 0x67, 0x00, 0xff])

JUST_NOT_OUTDATED = MOCKED_NOW_IN_SEC - MAX_SECONDS_BACK + 1
JUST_OUTDATED = MOCKED_NOW_IN_SEC - MAX_SECONDS_BACK - 1
JUST_NOT_TOO_FAR_IN_FUTUR = MOCKED_NOW_IN_SEC + MAX_SECONDS_FORWARD - 1
JUST_TOO_FAR_IN_FUTUR = MOCKED_NOW_IN_SEC + MAX_SECONDS_FORWARD + 1

# Time on channel 3
JUST_NOT_OUTDATED_BINARY_LPP = bytes([0x03, 0x67, 0x01, 0x10, 0x05, 0x67, 0x00, 0xff, 0x03, 0x85]) + JUST_NOT_OUTDATED.to_bytes(4, 'big')
JUST_OUTDATED_BINARY_LPP = bytes([0x03, 0x67, 0x01, 0x10, 0x05, 0x67, 0x00, 0xff, 0x03, 0x85]) + JUST_OUTDATED.to_bytes(4, 'big')
# Time on channel 5
JUST_NOT_TOO_FAR_IN_FUTUR_BINARY_LPP = bytes([0x03, 0x67, 0x01, 0x10, 0x05, 0x67, 0x00, 0xff, 0x05, 0x85]) + JUST_NOT_TOO_FAR_IN_FUTUR.to_bytes(4, 'big')
JUST_TOO_FAR_IN_FUTUR_BINARY_LPP = bytes([0x03, 0x67, 0x01, 0x10, 0x05, 0x67, 0x00, 0xff, 0x05, 0x85]) + JUST_TOO_FAR_IN_FUTUR.to_bytes(4, 'big')

# Time on channel 7 (default time)
DEFAULT_TIME = MOCKED_NOW_IN_SEC - int(MAX_SECONDS_BACK / 2)
DEFAULT_TIME_BINARY_LPP = bytes([0x03, 0x67, 0x01, 0x10, 0x05, 0x67, 0x00, 0xff, 0x07, 0x85]) + DEFAULT_TIME.to_bytes(4, 'big')

# Time on channel 3 + 7 (default time)
OVERRIDEN_TIME_BINARY_LPP = bytes([0x03, 0x67, 0x01, 0x10, 0x05, 0x67, 0x00, 0xff, 0x07, 0x85]) + DEFAULT_TIME.to_bytes(4, 'big') + bytes([0x03, 0x85]) + JUST_NOT_OUTDATED.to_bytes(4, 'big')


def buildMeasurementNameFromTags(tags):
    return IotService.buildMeasurementName(tags['_deviceUid'], tags['_channel'], tags['_lppType'])


def initStoreLppByDeviceUidMock(mocker, findDeviceByIdMethod, storeMeasurementMethod):
    findDeviceByIdMock = mocker.patch('iot_datastore.iot_service.findDeviceById', side_effect=findDeviceByIdMethod)
    #storeMeasurementMock = mocker.patch('iot_datastore.influxdb_service.storeMeasurement', side_effect=storeMeasurementMethod)
    mockedPromscaleService_storeMeasurement = mocker.patch('iot_datastore.promscale_service.storeMeasurement')
    return (findDeviceByIdMock, mockedPromscaleService_storeMeasurement)
    
def testStoreJustNotOutdatedLpp(mocker):
    mocker.patch('iot_datastore.helper.helper.getUtcnowDatetime').return_value = MOCKED_NOW
    deviceUid = DEVICE_1.uid
    lpp = JUST_NOT_OUTDATED_BINARY_LPP
    ingestComponent = "ingest1"
    ingestMethod = 'method1'

    def findDeviceById(id):
        return copy.deepcopy(DEVICE_1)
    def storeMeasurement(name, tags, time, fields):
        if name == IotService.buildMeasurementName(DEVICE_1.uid, 3, 'temperature'):
            assert time == datetime.utcfromtimestamp(JUST_NOT_OUTDATED), 'Event time should be just not outdated !'
            assert fields == {'value': 27.2}, 'Fields are Erroneous !'
        elif name == IotService.buildMeasurementName(DEVICE_1.uid, 5, 'temperature'):
            assert time == MOCKED_NOW, 'Event time should be now !'
            assert fields == {'value': 25.5}, 'Fields are Erroneous !'
        else:
            assert False, 'Bad Device UID !'

    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, storeMeasurement)

    IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)

    storeMeasurementMock.assert_called()
    assert storeMeasurementMock.call_count == 2, 'Bad LPP count processed !'

def testStoreJustOutdatedLpp(mocker):
    mocker.patch('iot_datastore.helper.helper.getUtcnowDatetime').return_value = MOCKED_NOW

    deviceUid = DEVICE_1.uid
    lpp = JUST_OUTDATED_BINARY_LPP
    ingestComponent = "ingest1"
    ingestMethod = 'method1'

    def findDeviceById(id):
        return copy.deepcopy(DEVICE_1)
    def storeMeasurement(name, tags, time, fields):
        if name == IotService.buildMeasurementName(DEVICE_1.uid, 5, 'temperature'):
            assert time == MOCKED_NOW, 'Event time should be now !'
            assert fields == {'value': 25.5}, 'Fields are Erroneous !'
        else:
            assert False, 'Bad Device UID !'

    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, storeMeasurement)

    with pytest.raises(NotValidDataTime):
        IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)

    storeMeasurementMock.assert_called_once()

def testStoreJustNotTooFarInFuturLpp(mocker):
    mocker.patch('iot_datastore.helper.helper.getUtcnowDatetime').return_value = MOCKED_NOW

    deviceUid = DEVICE_1.uid
    lpp = JUST_NOT_TOO_FAR_IN_FUTUR_BINARY_LPP
    ingestComponent = "ingest1"
    ingestMethod = 'method1'

    def findDeviceById(id):
        return copy.deepcopy(DEVICE_1)
    def storeMeasurement(name, tags, time, fields):
        if name == IotService.buildMeasurementName(DEVICE_1.uid, 3, 'temperature'):
            assert time == MOCKED_NOW, 'Event time should be now !'
            assert fields == {'value': 27.2}, 'Fields are Erroneous !'
        elif name == IotService.buildMeasurementName(DEVICE_1.uid, 5, 'temperature'):
            assert time == datetime.utcfromtimestamp(JUST_NOT_TOO_FAR_IN_FUTUR), 'Event time should be too far in futur !'
            assert fields == {'value': 25.5}, 'Fields are Erroneous !'
        else:
            assert False, 'Bad Device UID !'

    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, storeMeasurement)

    IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)

    storeMeasurementMock.assert_called()
    assert storeMeasurementMock.call_count == 2, 'Bad LPP count processed !'

def testStoreJustTooFarInFuturLpp(mocker):
    mocker.patch('iot_datastore.helper.helper.getUtcnowDatetime').return_value = MOCKED_NOW

    deviceUid = DEVICE_1.uid
    lpp = JUST_TOO_FAR_IN_FUTUR_BINARY_LPP
    ingestComponent = "ingest1"
    ingestMethod = 'method1'

    def findDeviceById(id):
        return copy.deepcopy(DEVICE_1)
    def storeMeasurement(name, tags, time, fields):
        if name == IotService.buildMeasurementName(DEVICE_1.uid, 3, 'temperature'):
            assert time == MOCKED_NOW, 'Event time should be now !'
            assert fields == {'value': 27.2}, 'Fields are Erroneous !'
        else:
            assert False, 'Bad Device UID !'

    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, storeMeasurement)

    with pytest.raises(NotValidDataTime):
        IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)

    storeMeasurementMock.assert_called_once()

def testStoreDefaultTimeLpp(mocker):
    mocker.patch('iot_datastore.helper.helper.getUtcnowDatetime').return_value = MOCKED_NOW

    deviceUid = DEVICE_1.uid
    lpp = DEFAULT_TIME_BINARY_LPP
    ingestComponent = "ingest1"
    ingestMethod = 'method1'

    def findDeviceById(id):
        return copy.deepcopy(DEVICE_1)
    def storeMeasurement(name, tags, time, fields):
        if name == IotService.buildMeasurementName(DEVICE_1.uid, 3, 'temperature'):
            assert time == datetime.utcfromtimestamp(DEFAULT_TIME), 'Event time should be default submited time !'
            assert fields == {'value': 27.2}, 'Fields are Erroneous !'
        elif name == IotService.buildMeasurementName(DEVICE_1.uid, 5, 'temperature'):
            assert time == datetime.utcfromtimestamp(DEFAULT_TIME), 'Event time should be default submited time !'
            assert fields == {'value': 25.5}, 'Fields are Erroneous !'
        else:
            assert False, 'Bad Device UID !'

    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, storeMeasurement)

    IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)

    storeMeasurementMock.assert_called()
    assert storeMeasurementMock.call_count == 2, 'Bad LPP count processed !'

def testStoreOverridenTimeLpp(mocker):
    mocker.patch('iot_datastore.helper.helper.getUtcnowDatetime').return_value = MOCKED_NOW

    deviceUid = DEVICE_1.uid
    lpp = OVERRIDEN_TIME_BINARY_LPP
    ingestComponent = "ingest1"
    ingestMethod = 'method1'

    def findDeviceById(id):
        return copy.deepcopy(DEVICE_1)
    def storeMeasurement(name, tags, time, fields):
        if name == IotService.buildMeasurementName(DEVICE_1.uid, 3, 'temperature'):
            assert time == datetime.utcfromtimestamp(JUST_NOT_OUTDATED), 'Event time should be overriden time !'
            assert fields == {'value': 27.2}, 'Fields are Erroneous !'
        elif name == IotService.buildMeasurementName(DEVICE_1.uid, 5, 'temperature'):
            assert time == datetime.utcfromtimestamp(DEFAULT_TIME), 'Event time should be default submited time !'
            assert fields == {'value': 25.5}, 'Fields are Erroneous !'
        else:
            assert False, 'Bad Device UID !'

    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, storeMeasurement)

    IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)

    storeMeasurementMock.assert_called()
    assert storeMeasurementMock.call_count == 2, 'Bad LPP count processed !'
