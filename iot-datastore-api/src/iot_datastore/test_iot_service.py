from datetime import datetime, timezone
from unittest.mock import call, ANY
import copy
import base64
from iot_datastore import iot_tags
import pytest
from pytest_mock import MockerFixture
import tempfile
import flask
from cayennelpp import LppFrame
import iot_datastore.iot_service as IotService
import iot_datastore.db_models as DbModels
from iot_datastore.exceptions import NotProcessablePayload
from iot_datastore.test_iot_service_commons import *


def testCreateDevice1(app):
    with app.app_context():
        IotService.createDevice(DEVICE_1.uid)
        devices = DbModels.Device.query.all()
        assert 1 == len(devices), 'Device 1 should be created !'
        assert DEVICE_1.uid == devices[0].uid, 'Wrong device 1 uid !'
        assert None == devices[0].name, 'Wrong device 1 name !'
        assert None == devices[0].location, 'Wrong device 1 location !'

        IotService.createDevice(DEVICE_2.uid, DEVICE_2.name)
        devices = DbModels.Device.query.all()
        assert 2 == len(devices), 'Device 2 should be created !'
        assert DEVICE_2.uid == devices[1].uid, 'Wrong device 2 uid !'
        assert DEVICE_2.name == devices[1].name, 'Wrong device 2 name !'
        assert None == devices[1].location, 'Wrong device 2 location !'

        IotService.createDevice(DEVICE_3.uid, DEVICE_3.name, DEVICE_3.location)
        devices = DbModels.Device.query.all()
        assert 3 == len(devices), 'Device 3 should be created !'
        assert DEVICE_3.uid == devices[2].uid, 'Wrong device 3 uid !'
        assert DEVICE_3.name == devices[2].name, 'Wrong device 3 name !'
        assert DEVICE_3.location == devices[2].location, 'Wrong device 3 location !'

        #with pytest.raises(Exception):
        IotService.createDevice(DEVICE_1.uid, DEVICE_1.name, DEVICE_1.location)
        devices = DbModels.Device.query.all()
        assert 3 == len(devices), 'Device 1 should not be created twice !'
        assert DEVICE_1.uid == devices[0].uid, 'Wrong device 1 uid !'
        assert None == devices[0].name, 'Wrong device 1 name !'
        assert None == devices[0].location, 'Wrong device 1 location !'
        
def testCreateDevice2(app):
    with app.app_context():
        devices = IotService.findAllDevice()
        assert [] == devices, 'Should return an empty list !'

        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.commit()
        devices = IotService.findAllDevice()
        assert 1 == len(devices), 'Device 1 should be listed !'

        DbModels.db.session.add(copy.deepcopy(DEVICE_2))
        DbModels.db.session.commit()
        devices = IotService.findAllDevice()
        assert 2 == len(devices), 'Device 2 should be listed !'

        DbModels.db.session.add(copy.deepcopy(DEVICE_3))
        DbModels.db.session.commit()
        devices = IotService.findAllDevice()
        assert 3 == len(devices), 'Device 3 should be listed !'

        
def testFindDeviceById(app):
    with app.app_context():
        device = IotService.findDeviceById(DEVICE_1.uid)
        assert None == device, 'No Device should be found !'

        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.commit()
        device = IotService.findDeviceById(DEVICE_1.uid)
        assert DEVICE_1.uid == device.uid, 'Device 1 uid is bad !'
        assert DEVICE_1.name == device.name, 'Device 1 name is bad !'
        assert DEVICE_1.location == device.location, 'Device 1 uid is bad !'

        DbModels.db.session.add(copy.deepcopy(DEVICE_2))
        DbModels.db.session.commit()
        device = IotService.findDeviceById(DEVICE_1.uid)
        assert DEVICE_1.uid == device.uid, 'Device 1 uid is bad !'
        assert DEVICE_1.name == device.name, 'Device 1 name is bad !'
        assert DEVICE_1.location == device.location, 'Device 1 uid is bad !'
        device = IotService.findDeviceById(DEVICE_2.uid)
        assert DEVICE_2.uid == device.uid, 'Device 2 uid is bad !'
        assert DEVICE_2.name == device.name, 'Device 2 name is bad !'
        assert DEVICE_2.location == device.location, 'Device 2 uid is bad !'

        DbModels.db.session.add(copy.deepcopy(DEVICE_3))
        DbModels.db.session.commit()
        device = IotService.findDeviceById(DEVICE_3.uid)
        assert DEVICE_3.uid == device.uid, 'Device 3 uid is bad !'
        assert DEVICE_3.name == device.name, 'Device 3 name is bad !'
        assert DEVICE_3.location == device.location, 'Device 3 uid is bad !'
        device = IotService.findDeviceById(DEVICE_1.uid)
        assert DEVICE_1.uid == device.uid, 'Device 1 uid is bad !'
        assert DEVICE_1.name == device.name, 'Device 1 name is bad !'
        assert DEVICE_1.location == device.location, 'Device 1 uid is bad !'
        device = IotService.findDeviceById(DEVICE_2.uid)
        assert DEVICE_2.uid == device.uid, 'Device 2 uid is bad !'
        assert DEVICE_2.name == device.name, 'Device 2 name is bad !'
        assert DEVICE_2.location == device.location, 'Device 2 uid is bad !'

def testUpdateDevice(app):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.add(copy.deepcopy(DEVICE_2))
        DbModels.db.session.add(copy.deepcopy(DEVICE_3))
        DbModels.db.session.commit()

        device2 = DbModels.Device.query.filter_by(uid=DEVICE_2.uid).first()
        assert DEVICE_2.name == device2.name, 'Bad device retrieved !'

        device2Modified = copy.deepcopy(DEVICE_2)
        newDevice2name = 'newNameForDevice2'
        device2Modified.name = newDevice2name
        updated = IotService.updateDevice(device2Modified)
        assert updated == device2Modified, 'Returned device is not updated !'

        device2 = DbModels.Device.query.filter_by(uid=DEVICE_2.uid).first()
        assert newDevice2name == device2.name, 'Modification was not stored in DB !'

def testDeleteDevice(app):
    with app.app_context():
        # with pytest.raises(Exception):
        deleted = IotService.deleteDevice(DEVICE_1.uid)
        assert None == deleted, 'deleteDevice() should return None !'

        devices = DbModels.Device.query.all()
        assert [] == devices, 'Should return an empty list !'

        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.add(copy.deepcopy(DEVICE_2))
        DbModels.db.session.add(copy.deepcopy(DEVICE_3))
        DbModels.db.session.commit()

        devices = DbModels.Device.query.all()
        assert 3 == len(devices), 'Missing some devices in BD !'
        
        # with pytest.raises(Exception):
        deleted = IotService.deleteDevice('notExistingUid')
        assert None == deleted, 'deleteDevice() should return None !'
        devices = DbModels.Device.query.all()
        assert 3 == len(devices), 'No device should be deleted !'
        
        deleted = IotService.deleteDevice(DEVICE_2.uid)
        assert DEVICE_2 == deleted, 'deleteDevice() should return the deleted device !'
        devices = DbModels.Device.query.all()
        assert 2 == len(devices), 'Device 2 should be deleted !'
        assert DEVICE_1.uid == devices[0].uid, 'Bad Device remaining !'
        assert DEVICE_3.uid == devices[1].uid, 'Bad Device remaining !'

def test_storeQuantity(mocker: MockerFixture) -> None:
    #mockedInfluxDbService_storeMeasurement = mocker.patch('iot_datastore.influxdb_service.storeMeasurement')
    mockedPromscaleService_storeMeasurement = mocker.patch('iot_datastore.promscale_service.storeMeasurement')
    mockedNowDatetime = datetime.utcfromtimestamp(1000000)
    mocker.patch('iot_datastore.helper.helper.getUtcnowDatetime').return_value = mockedNowDatetime

    deviceUid = 'y42'
    deviceName = 'foo'
    deviceLocation = 'bar'
    device = DbModels.Device(deviceUid, deviceName, deviceLocation)

    device.addDeviceTag('name', 'temp_sensor_test')
    device.addDeviceTag('version', '1.0')
    device.addDeviceTag('foo', 'baz')

    quantityCode = 'voltage'
    dq0 = DbModels.DataQuantity(quantityCode, [3.5])
    ingestComponent = 'comp1'
    ingestFormat = 'testFormat'
    extraTags = {'extra': 'yes'}
    ingestMethod = 'unittest'
    now = mockedNowDatetime
    eventTime = now

    channelId = 42
    IotService._storeQuantity(device, channelId, dq0, extraTags, ingestComponent, ingestMethod, ingestFormat, now, eventTime)

    #expectedMeasurmentName = '%s__%s__%s' % (deviceUid, channelId, quantityCode)
    expectedMeasurmentName = 'device_%s' % (deviceUid)
    expectedTags = {
        iot_tags.SOURCE_TAG_KEY: iot_tags.SOURCE_TAG_VALUE,
        iot_tags.PRODUCER_TAG_KEY: iot_tags.ANONYOUS_PRODUCER_TAG_VALUE,
        iot_tags.INGEST_COMPONENT_TAG_KEY: ingestComponent,
        iot_tags.DATA_QUANTITY_TAG_KEY: quantityCode,
        iot_tags.CHANNEL_TAG_KEY: str(channelId),
        iot_tags.INGEST_METHOD_TAG_KEY: ingestMethod,
        iot_tags.DEVICE_UID_TAG_KEY: deviceUid,
        iot_tags.DEVICE_NAME_TAG_KEY: deviceName,
        iot_tags.DEVICE_LOCATION_TAG_KEY: deviceLocation,
        iot_tags.INGEST_FORMAT_TAG_KEY: ingestFormat,
        iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY: 'avg',
        iot_tags.DATA_UNITS_TAG_KEY: 'V',
        'extra': 'yes',
        'name': 'temp_sensor_test',
        'version': '1.0',
        'foo': 'baz',
    }
    expectedFields = {'value': 3.5}
    assert mockedPromscaleService_storeMeasurement.call_args_list == [call(expectedMeasurmentName, expectedTags, eventTime, expectedFields)]
