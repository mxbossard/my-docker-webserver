import os
import json
from datetime import datetime
import requests
from iot_datastore.db_models import DataPayload
import iot_datastore.promscale_service as prom

def test_buildPromscalePayload():
    name = 'foombd'
    tags = {'quantity': 'gps_location', 'channel': '12'}
    timeEpoch = 4242
    time = datetime.utcfromtimestamp(timeEpoch)
    fieldName = 'lat'
    value = '42.3'
    expedPayload = {
        "labels": {**tags, "__name__": name, '_field': fieldName},
        "samples": [
            [int(timeEpoch * 1000), value]
        ]
    }

    assert expedPayload == prom._buildPromscalePayload(name, tags, time, fieldName, value)

def testStoreMeasurement(mocker):
    name = 'foombd'
    tags = {'quantity': 'gps_location', 'channel': '12'}
    timeEpoch = 4242
    time = datetime.utcfromtimestamp(timeEpoch)
    fields = {'lat': 42.3, 'lon': 43.2, 'alt': 173}

    expedLatPayload = {
        "labels": {**tags, "__name__": name, '_field': 'lat'},
        "samples": [
            [int(timeEpoch * 1000), 42.3]
        ]
    }
    expedLonPayload = {
        "labels": {**tags, "__name__": name, '_field': 'lon'},
        "samples": [
            [int(timeEpoch * 1000), 43.2]
        ]
    }
    expedAltPayload = {
        "labels": {**tags, "__name__": name, '_field': 'alt'},
        "samples": [
            [int(timeEpoch * 1000), 173]
        ]
    }

    mock = mocker.patch('iot_datastore.promscale_service._writeUniqPayloadAsJson')
    prom.storeMeasurement(name, tags, time, fields)

    assert 3 == mock.call_count
    mock.assert_any_call(expedLatPayload)
    mock.assert_any_call(expedLonPayload)
    mock.assert_any_call(expedAltPayload)

def testStoreMeasurements(mocker):
    name1 = 'foombd'
    tags1 = {'quantity': 'gps_location', 'channel': '12'}
    timeEpoch1 = 4242
    time1 = datetime.utcfromtimestamp(timeEpoch1)
    fields1 = {'lat': 42.3, 'lon': 43.2, 'alt': 173}

    name2 = 'rokjjm'
    tags2 = {'quantity': 'temperature', 'channel': '17'}
    timeEpoch2 = 7575
    time2 = datetime.utcfromtimestamp(timeEpoch2)
    fields2 = {'value': 17.42}

    name3 = 'truc'
    tags3 = {'quantity': 'humidity', 'channel': '57'}
    timeEpoch3 = 2424
    time3 = datetime.utcfromtimestamp(timeEpoch3)
    fields3 = {'value0': 42.03}

    expedLatPayload = {
        "labels": {**tags1, "__name__": name1, '_field': 'lat'},
        "samples": [
            [int(timeEpoch1 * 1000), 42.3]
        ]
    }
    expedLonPayload = {
        "labels": {**tags1, "__name__": name1, '_field': 'lon'},
        "samples": [
            [int(timeEpoch1 * 1000), 43.2]
        ]
    }
    expedAltPayload = {
        "labels": {**tags1, "__name__": name1, '_field': 'alt'},
        "samples": [
            [int(timeEpoch1 * 1000), 173]
        ]
    }

    expedTempPayload = {
        "labels": {**tags2, "__name__": name2, '_field': 'value'},
        "samples": [
            [int(timeEpoch2 * 1000), 17.42]
        ]
    }
    expedHumPayload = {
        "labels": {**tags3, "__name__": name3, '_field': 'value0'},
        "samples": [
            [int(timeEpoch3 * 1000), 42.03]
        ]
    }

    mock = mocker.patch('iot_datastore.promscale_service._writePayloadsAsJson')
    prom.storeMeasurements([
        [name1, tags1, time1, fields1],
        [name2, tags2, time2, fields2],
        [name3, tags3, time3, fields3],
    ])

    assert 1 == mock.call_count
    mock.assert_any_call([expedLatPayload, expedLonPayload, expedAltPayload, expedTempPayload, expedHumPayload])


# measurement: "name", tags: map, time: "2009-11-10T23:00:00Z", fields: map
def storeMeasurements(measurements: dict):
    payloads = []
    for measurement in measurements:
        for (fieldName, value) in measurement['fields'].items():
            p = _buildPromscalePayload(measurement['name'], measurement['tags'], measurement['time'], fieldName, value)
            payloads.append(p)
            
    _writePayloadsAsJson(payloads)
