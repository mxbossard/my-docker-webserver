import base64
import iot_datastore.helper.encoding as EncodingHelper

PLAIN_STR_1 = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
BASE64_STR_1 = 'PDw/Pz8+Pg=='
BASE64_BYTES_1 = bytes(BASE64_STR_1, 'ascii')

BASE64URL_STR_1 = 'PDw_Pz8-Pg=='
BASE64URL_BYTES_1 = bytes(BASE64URL_STR_1, 'ascii')

BAD_BASE64_STR_1 = 'TWFuIGlzIGRpc3R*='
BAD_BASE64_BYTES_1 = bytes(BAD_BASE64_STR_1, 'ascii')

def testIsBase64Empty():
    assert not EncodingHelper.isBase64(None)
    assert not EncodingHelper.isBase64({})
    assert not EncodingHelper.isBase64('')

def testIsBase64String():
    assert EncodingHelper.isBase64(base64.standard_b64encode(bytes(BASE64_STR_1, 'ascii')).decode('ascii'))
    assert EncodingHelper.isBase64(BASE64_STR_1)
    assert not EncodingHelper.isBase64(BASE64URL_STR_1)
    assert not EncodingHelper.isBase64(BAD_BASE64_STR_1)

def testIsBase64Bytes():
    assert EncodingHelper.isBase64(base64.standard_b64encode(bytes(BASE64_STR_1, 'ascii')))
    assert EncodingHelper.isBase64(BASE64_BYTES_1)
    assert not EncodingHelper.isBase64(BASE64URL_BYTES_1)
    assert not EncodingHelper.isBase64(BAD_BASE64_BYTES_1)

def testIsUrlSafeBase64Empty():
    assert not EncodingHelper.isUrlSafeBase64(None)
    assert not EncodingHelper.isUrlSafeBase64({})
    assert not EncodingHelper.isUrlSafeBase64('')

def testIsUrlSafeBase64String():
    assert EncodingHelper.isUrlSafeBase64(base64.urlsafe_b64encode(bytes(BASE64_STR_1, 'ascii')).decode('ascii'))
    assert EncodingHelper.isUrlSafeBase64(BASE64URL_STR_1)
    assert not EncodingHelper.isUrlSafeBase64(BASE64_STR_1)
    assert not EncodingHelper.isUrlSafeBase64(BAD_BASE64_STR_1)

def testIsUrlSafeBase64Bytes():
    assert EncodingHelper.isUrlSafeBase64(base64.urlsafe_b64encode(bytes(BASE64_STR_1, 'ascii')))
    assert EncodingHelper.isUrlSafeBase64(BASE64URL_BYTES_1)
    assert not EncodingHelper.isUrlSafeBase64(BASE64_BYTES_1)
    assert not EncodingHelper.isUrlSafeBase64(BAD_BASE64_BYTES_1)