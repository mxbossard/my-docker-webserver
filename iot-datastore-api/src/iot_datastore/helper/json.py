import datetime
import logging
from functools import wraps
from flask.json import JSONEncoder
from flask import request


__log = logging.getLogger(__name__)

class CustomJSONEncoder(JSONEncoder):
    "Add support for serializing timedeltas"

    def default(self, o):
        if type(o) == datetime.timedelta:
            return str(o)
        elif type(o) == datetime.datetime:
            return o.isoformat()
        else:
            return super().default(o)

def required_params(required, optional=None):
    def decorator(fn):

        @wraps(fn)
        def wrapper(*args, **kwargs):
            #__log.info('Validating JSON wrapper ...')
            #__log.info('Content-Type: %s', request.headers.get('Content-Type'))
            request.validated_json = None

            if request.is_json:
                #__log.info('Validating JSON data ...')
                _json = request.get_json()
                missing = [r for r in required.keys()
                        if r not in _json]
                if missing:
                    response = {
                        "status": "error",
                        "message": "Request JSON is missing some required params",
                        "missing": missing
                    }
                    __log.warning('Missing JSON params: %s', missing)
                    return response, 400
                
                if optional:
                    requiredPlusOptional = {**optional, **required}
                else:
                    requiredPlusOptional = required

                wrong_types = [r for r in requiredPlusOptional.keys()
                            if r in _json and _json[r] and not isinstance(_json[r], requiredPlusOptional[r])]
                if wrong_types:
                    response = {
                        "status": "error",
                        "message": "Data types in the request JSON doesn't match the required format",
                        "param_types": {k: str(v) for k, v in requiredPlusOptional.items()}
                    }
                    __log.warning('JSON params with wrong types: %s', wrong_types)
                    return response, 400

                #__log.info('JSON params are valid')
                request.validated_json = _json
            return fn(*args, **kwargs)
        return wrapper
    return decorator

