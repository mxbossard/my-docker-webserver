import base64
import logging

def isBase64(sb):
    if not sb: return False
    try:
        if isinstance(sb, str):
            # If there's any unicode here, an exception will be thrown and the function will return false
            sb_bytes = bytes(sb, 'ascii')
        elif isinstance(sb, bytes):
            sb_bytes = sb
        else:
            raise ValueError("Argument must be string or bytes")
        return base64.standard_b64encode(base64.standard_b64decode(sb_bytes)) == sb_bytes
    except Exception as e:
        #raise e
        return False

def isUrlSafeBase64(sb):
    if not sb: return False
    try:
        if isinstance(sb, str):
            # If there's any unicode here, an exception will be thrown and the function will return false
            sb_bytes = bytes(sb, 'ascii')
        elif isinstance(sb, bytes):
            sb_bytes = sb
        else:
            raise ValueError("Argument must be string or bytes")
        return base64.urlsafe_b64encode(base64.urlsafe_b64decode(sb_bytes)) == sb_bytes
    except Exception as e:
        #raise e
        return False