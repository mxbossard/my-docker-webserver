import base64
from datetime import datetime

from flask.json import jsonify
from iot_datastore.db_models import Device, MAX_SECONDS_BACK_IN_TIME, MAX_SECONDS_FORWARD_IN_FUTUR
import iot_datastore.helper.json as JsonHelper

def testJsonEncoder():
    time = datetime.utcfromtimestamp(12)
    foo = {
        "int": 1,
        "float": 1.2,
        "string": "foo",
        "bool": True,
        "null": None,
        "datetime": time
    }

    expectedResult = '{"int": 1, "float": 1.2, "string": "foo", "bool": true, "null": null, "datetime": "%s"}' % time.isoformat()
    assert expectedResult == JsonHelper.CustomJSONEncoder().encode(foo)

def ignoredtestJsonEncoderWithHiddenFields():
    foo = {
        "int": 1,
        "float": 1.2,
        "string": "foo",
        "bool": True,
        "null": None,
        "__hiddenParam": "bar"
    }

    expectedResult = '{"int": 1, "float": 1.2, "string": "foo", "bool": true, "null": null}'
    assert expectedResult == JsonHelper.CustomJSONEncoder().encode(foo)

def ignoredtestJsonEncoderWithDevice():
    device = Device("foo", "bar", "baz")
    expectedResult = '{"uid": "foo", "name": "bar", "location": "baz", "maxSecondsBackInTime": %d, "maxSecondsForwardInFutur": %d}' % (MAX_SECONDS_BACK_IN_TIME, MAX_SECONDS_FORWARD_IN_FUTUR)
    assert expectedResult == JsonHelper.CustomJSONEncoder().encode(device)