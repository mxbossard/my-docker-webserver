import pytest
import iot_datastore.db_models as DbModels
from iot_datastore.exceptions import NotSupportedUid
from iot_datastore import iot_tags

def testBuildUnknownDevice():
    device = DbModels.Device('42')
    assert device.uid == '42'
    assert device.name == None
    assert device.location == None
    assert device.getDeviceName() == '42'
    assert device.maxSecondsBackInTime == DbModels.MAX_SECONDS_BACK_IN_TIME
    assert device.maxSecondsForwardInFutur == DbModels.MAX_SECONDS_FORWARD_IN_FUTUR
    assert {} == device.getDeviceTags(), 'Device tags dict should be empty !'

def testBuildGoodUidDevice():
    DbModels.Device('42_a', 'foo', 'bar')
    DbModels.Device('42_a_b', 'foo', 'bar')
    DbModels.Device('42_a_b_57_z', 'foo', 'bar')

def testBuildBadUidDevice():
    with pytest.raises(NotSupportedUid):
        DbModels.Device('', 'foo', 'bar')

    with pytest.raises(NotSupportedUid):
        DbModels.Device('42+', 'foo', 'bar')

    with pytest.raises(NotSupportedUid):
        DbModels.Device('42 a', 'foo', 'bar')

    with pytest.raises(NotSupportedUid):
        DbModels.Device('42-a', 'foo', 'bar')

    with pytest.raises(NotSupportedUid):
        DbModels.Device('42__a', 'foo', 'bar')

    with pytest.raises(NotSupportedUid):
        DbModels.Device('42__', 'foo', 'bar')

    with pytest.raises(NotSupportedUid):
        DbModels.Device('__abc', 'foo', 'bar')

    with pytest.raises(NotSupportedUid):
        DbModels.Device('_', 'foo', 'bar')

    with pytest.raises(NotSupportedUid):
        DbModels.Device('_a', 'foo', 'bar')

    with pytest.raises(NotSupportedUid):
        DbModels.Device('a_', 'foo', 'bar')

    with pytest.raises(NotSupportedUid):
        DbModels.Device('__', 'foo', 'bar')

    with pytest.raises(NotSupportedUid):
        DbModels.Device('a__', 'foo', 'bar')

    with pytest.raises(NotSupportedUid):
        DbModels.Device('__a', 'foo', 'bar')

    with pytest.raises(NotSupportedUid):
        DbModels.Device('42+a', 'foo', 'bar')

    with pytest.raises(NotSupportedUid):
        DbModels.Device('42~a', 'foo', 'bar')

    with pytest.raises(NotSupportedUid):
        DbModels.Device('42_a_b_57_Z', 'foo', 'bar')

def testBuildNamedDevice():
    device = DbModels.Device('42', 'foo')
    assert device.uid == '42'
    assert device.name == 'foo'
    assert device.location == None
    assert device.maxSecondsBackInTime == DbModels.MAX_SECONDS_BACK_IN_TIME
    assert device.maxSecondsForwardInFutur == DbModels.MAX_SECONDS_FORWARD_IN_FUTUR
    assert {iot_tags.DEVICE_NAME_TAG_KEY: 'foo'} == device.getDeviceTags(), 'Device tags dict should be minimal !'
    assert device.getDeviceName() == 'foo'
    assert device.getDeviceLocation() == None

def testBuildLocatedDevice():
    device = DbModels.Device('42', None, 'bar')
    assert device.uid == '42'
    assert device.name == None
    assert device.location == 'bar'
    assert {iot_tags.DEVICE_LOCATION_TAG_KEY: 'bar'} == device.getDeviceTags(), 'Device tags dict should be minimal !'
    assert device.getDeviceName() == '42'
    assert device.getDeviceLocation() == 'bar'

def testBuildDeviceWithTimings():
    device = DbModels.Device('42', 'foo', 'bar', 100, 200)
    assert device.uid == '42'
    assert device.name == 'foo'
    assert device.location == 'bar'
    assert device.maxSecondsBackInTime == 100
    assert device.maxSecondsForwardInFutur == 200
    assert {iot_tags.DEVICE_NAME_TAG_KEY: 'foo', iot_tags.DEVICE_LOCATION_TAG_KEY: 'bar'} == device.getDeviceTags(), 'Device tags dict should be minimal !'
    assert device.getDeviceName() == 'foo'
    assert device.getDeviceLocation() == 'bar'

def testEmptyDeviceTags():
    device = DbModels.Device('42', 'foo', 'bar')

    assert {iot_tags.DEVICE_NAME_TAG_KEY: 'foo', iot_tags.DEVICE_LOCATION_TAG_KEY: 'bar'} == device.getDeviceTags(), 'Device tags dict should be minimal !'
    assert {} == device.getChannelTags(1), 'Device tags dict should be empty !'
    assert {} == device.getQuantityTags(1, "temperature"), 'Device tags dict should be empty !'

def testDeviceUpdates():
    device = DbModels.Device('42', 'foo', 'bar')

    assert device.uid == '42'
    assert device.name == 'foo'
    assert device.location == 'bar'
    assert device.maxSecondsBackInTime == DbModels.MAX_SECONDS_BACK_IN_TIME
    assert device.maxSecondsForwardInFutur == DbModels.MAX_SECONDS_FORWARD_IN_FUTUR
    assert device.getDeviceName() == 'foo'
    assert {iot_tags.DEVICE_NAME_TAG_KEY: 'foo', iot_tags.DEVICE_LOCATION_TAG_KEY: 'bar'} == device.getDeviceTags(), 'Device tags dict should be minimal !'
    assert {} == device.getChannelTags(1), 'Device tags dict should be empty !'
    assert {} == device.getQuantityTags(1, "temperature"), 'Device tags dict should be empty !'

    newName = "newName"
    newLocation = "newLocation"
    newMaxSecondsBack = 42
    device.name = newName
    device.location = newLocation
    device.maxSecondsBackInTime = newMaxSecondsBack

    assert device.uid == '42'
    assert device.name == newName
    assert device.location == newLocation
    assert device.maxSecondsBackInTime == newMaxSecondsBack
    assert device.maxSecondsForwardInFutur == DbModels.MAX_SECONDS_FORWARD_IN_FUTUR
    assert device.getDeviceName() == newName
    assert {iot_tags.DEVICE_NAME_TAG_KEY: newName, iot_tags.DEVICE_LOCATION_TAG_KEY: newLocation} == device.getDeviceTags(), 'Device tags dict should be minimal !'
    assert {} == device.getChannelTags(1), 'Device tags dict should be empty !'
    assert {} == device.getQuantityTags(1, "temperature"), 'Device tags dict should be empty !'

def testDeviceTags():
    deviceTags = {iot_tags.DEVICE_NAME_TAG_KEY: 'temp_sensor_test', 'version': '1.0', 'foo': 'baz'}
    device = DbModels.Device('42', 'foo', 'bar')

    for i, (key, value) in enumerate(deviceTags.items()):
        device.addDeviceTag(key, value)

    expectedDeviceTags = {iot_tags.DEVICE_LOCATION_TAG_KEY: 'bar', **deviceTags}
    assert expectedDeviceTags == device.getDeviceTags(), 'Bad Device tags dict !'
    assert {} == device.getChannelTags(1), 'Channel tags dict should be empty !'
    assert {} == device.getQuantityTags(1, "temperature"), 'Quantity tags dict should be empty !'

    expectedAllTags = {
        DbModels.DEVICE_NS: expectedDeviceTags
    }

    assert expectedAllTags == device.getAllTags()

    assert None == device.getLabel(1, "temperature")
    assert 'avg' == device.getAggregationMethod(1, "temperature")
    assert '°C' == device.getUnits(1, "temperature")
    assert None == device.getLabel(1, "unknown")
    assert 'avg' == device.getAggregationMethod(1, "unknown")
    assert None == device.getUnits(1, "unknown")

def testBadFormatTags1():
    key = '__foo'
    value = 'bar'
    device = DbModels.Device('42', 'foo', 'bar')

    with pytest.raises(ValueError):
        device.addDeviceTag(key, value)
    with pytest.raises(ValueError):
        device.addChannelTag(0, key, value)
    with pytest.raises(ValueError):
        device.addDefaultQuantityTag('q', key, value)
    with pytest.raises(ValueError):
        device.addQuantityTag('bar', 0, key, value)

def testBadFormatTags2():
    key = '__f~oo'
    value = 'bar'
    device = DbModels.Device('42', 'foo', 'bar')

    with pytest.raises(ValueError):
        device.addDeviceTag(key, value)
    with pytest.raises(ValueError):
        device.addChannelTag(0, key, value)
    with pytest.raises(ValueError):
        device.addDefaultQuantityTag('q', key, value)
    with pytest.raises(ValueError):
        device.addQuantityTag('bar', 0, key, value)

def testBadFormatTags3():
    key = '_foo'
    value = 'bar@'
    device = DbModels.Device('42', 'foo', 'bar')

    with pytest.raises(ValueError):
        device.addDeviceTag(key, value)
    with pytest.raises(ValueError):
        device.addChannelTag(0, key, value)
    with pytest.raises(ValueError):
        device.addDefaultQuantityTag('q', key, value)
    with pytest.raises(ValueError):
        device.addQuantityTag('bar', 0, key, value)

def testBadChannelFormat():
    key = '_foo'
    value = 'bar'
    device = DbModels.Device('42', 'foo', 'bar')

    with pytest.raises(TypeError):
        device.addChannelTag("a", key, value)
    with pytest.raises(TypeError):
        device.addQuantityTag('bar', "a", key, value)

def testBadQuantityFormat():
    key = '_foo'
    value = 'bar'
    device = DbModels.Device('42', 'foo', 'bar')

    with pytest.raises(ValueError):
        device.addDefaultQuantityTag('__q', key, value)
    with pytest.raises(ValueError):
        device.addQuantityTag(0, '__q', key, value)


def testChannelTags():
    deviceTags = {iot_tags.DEVICE_NAME_TAG_KEY: 'temp_sensor_test', 'version': '1.0', 'foo': 'baz'}
    channel1Tags = {iot_tags.CHANNEL_NAME_TAG_KEY: 'baz1', 'version': '2.0'}
    channel2Tags = {iot_tags.CHANNEL_NAME_TAG_KEY: 'baz2', 'version': '2.0'}
    device = DbModels.Device('42', 'foo', 'bar')

    for i, (key, value) in enumerate(deviceTags.items()):
        device.addDeviceTag(key, value)

    for i, (key, value) in enumerate(channel1Tags.items()):
        device.addChannelTag(1, key, value)

    for i, (key, value) in enumerate(channel2Tags.items()):
        device.addChannelTag(2, key, value)

    expectedDeviceTags = {iot_tags.DEVICE_LOCATION_TAG_KEY: 'bar', **deviceTags}
    assert expectedDeviceTags == device.getDeviceTags(), 'Bad device tags dict !'
    assert channel1Tags == device.getChannelTags(1), 'Bad channel 1 tags dict !'
    assert {} == device.getQuantityTags(1, "temperature"), 'Bad quantity tags dict should be empty !'
    assert channel1Tags == device.getChannelTags(1), 'Bad channel 2 tags dict !'
    assert {} == device.getQuantityTags(1, "humidity"), 'Bad quantity tags dict should be empty !'

    expectedAllTags = {
        DbModels.DEVICE_NS: expectedDeviceTags,
        DbModels.CHANNELS_NS: {
            "1": channel1Tags,
            "2": channel2Tags
        }
    }

    assert expectedAllTags == device.getAllTags()

def testDefaultQuantityTags():
    deviceTags = {iot_tags.DEVICE_NAME_TAG_KEY: 'temp_sensor_test', 'version': '1.0', 'foo': 'baz'}
    temperatureTags = {iot_tags.CHANNEL_NAME_TAG_KEY: 'temp', iot_tags.DATA_UNITS_TAG_KEY: '°C', 'version': '3.0'}
    humidityTags = {iot_tags.CHANNEL_NAME_TAG_KEY: 'hum', iot_tags.DATA_UNITS_TAG_KEY: 'per', 'version': '3.0'}
    device = DbModels.Device('42', 'foo', 'bar')

    for i, (key, value) in enumerate(deviceTags.items()):
        device.addDeviceTag(key, value)

    for i, (key, value) in enumerate(temperatureTags.items()):
        device.addDefaultQuantityTag("temperature", key, value)

    for i, (key, value) in enumerate(humidityTags.items()):
        device.addDefaultQuantityTag("humidity", key, value)

    expectedDeviceTags = {iot_tags.DEVICE_LOCATION_TAG_KEY: 'bar', **deviceTags}
    assert expectedDeviceTags == device.getDeviceTags(), 'Bad device tags dict !'
    assert {} == device.getChannelTags(1), 'Bad channel 1 tags dict !'
    assert temperatureTags == device.getQuantityTags(1, "temperature"), 'Bad quantity tags dict  !'
    assert humidityTags == device.getQuantityTags(1, "humidity"), 'Bad quantity tags dict !'
    assert {} == device.getChannelTags(2), 'Bad channel 1 tags dict !'
    assert temperatureTags == device.getQuantityTags(2, "temperature"), 'Bad quantity tags dict !'
    assert humidityTags == device.getQuantityTags(2, "humidity"), 'Bad quantity tags dict !'
    
    expectedAllTags = {
        DbModels.DEVICE_NS: expectedDeviceTags,
        DbModels.DEFAULT_QUANTITIES_NS: {
            "temperature": temperatureTags,
            "humidity": humidityTags
        }
    }

    assert expectedAllTags == device.getAllTags()

def testQuantityTags():
    deviceTags = {iot_tags.DEVICE_NAME_TAG_KEY: 'temp_sensor_test', 'version': '1.0', 'foo': 'baz'}
    temperatureTags = {iot_tags.CHANNEL_NAME_TAG_KEY: 'temp', iot_tags.DATA_UNITS_TAG_KEY: '°C', 'version': '3.0'}
    humidityTags = {iot_tags.CHANNEL_NAME_TAG_KEY: 'hum', iot_tags.DATA_LABEL_TAG_KEY: 'Humidity', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY: 'last', iot_tags.DATA_UNITS_TAG_KEY: 'percent', 'version': '3.0'}
    device = DbModels.Device('42', 'foo', 'bar')

    for i, (key, value) in enumerate(deviceTags.items()):
        device.addDeviceTag(key, value)

    for i, (key, value) in enumerate(temperatureTags.items()):
        device.addQuantityTag(1, "temperature", key, value)

    for i, (key, value) in enumerate(humidityTags.items()):
        device.addQuantityTag(2, "relative_humidity", key, value)

    expectedDeviceTags = {iot_tags.DEVICE_LOCATION_TAG_KEY: 'bar', **deviceTags}
    assert expectedDeviceTags == device.getDeviceTags(), 'Bad device tags dict !'
    assert {} == device.getChannelTags(1), 'Bad channel 1 tags dict !'
    assert temperatureTags == device.getQuantityTags(1, "temperature"), 'Bad quantity tags dict !'
    assert {} == device.getQuantityTags(1, "relative_humidity"), 'Bad quantity tags dict !'
    assert {} == device.getChannelTags(2), 'Bad channel 1 tags dict !'
    assert {} == device.getQuantityTags(2, "temperature"), 'Bad quantity tags dict !'
    assert humidityTags == device.getQuantityTags(2, "relative_humidity"), 'Bad quantity tags dict !'
    
    expectedAllTags = {
        DbModels.DEVICE_NS: expectedDeviceTags,
        DbModels.QUANTITIES_BY_CHANNEL_NS: {
            "1": { "temperature": temperatureTags },
            "2": { "relative_humidity": humidityTags }
        }
    }

    assert expectedAllTags == device.getAllTags()

    assert None == device.getLabel(1, "temperature")
    assert "avg" == device.getAggregationMethod(1, "temperature")
    assert "°C" == device.getUnits(1, "temperature")

    assert None == device.getLabel(1, "relative_humidity")
    assert "Humidity" == device.getLabel(2, "relative_humidity")
    assert "avg" == device.getAggregationMethod(1, "relative_humidity")
    assert "last" == device.getAggregationMethod(2, "relative_humidity")
    assert "%" == device.getUnits(1, "relative_humidity")
    assert "percent" == device.getUnits(2, "relative_humidity")

def testQuantityOverrideTags():
    deviceTags = {iot_tags.DEVICE_NAME_TAG_KEY: 'temp_sensor_test', 'version': '1.0', 'foo': 'baz'}
    temperatureTags = {iot_tags.CHANNEL_NAME_TAG_KEY: 'temp', iot_tags.DATA_UNITS_TAG_KEY: '°C', 'version': '3.0', iot_tags.DATA_UNITS_TAG_KEY: '°C'}
    humidityTags = {iot_tags.CHANNEL_NAME_TAG_KEY: 'hum', iot_tags.DATA_UNITS_TAG_KEY: 'per', 'version': '3.0'}
    chan1TemperatureTags = {iot_tags.DATA_LABEL_TAG_KEY: "american_sensor", iot_tags.DATA_UNITS_TAG_KEY: '°F', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY: 'last'}
    device = DbModels.Device('42', 'foo', 'bar')

    for i, (key, value) in enumerate(deviceTags.items()):
        device.addDeviceTag(key, value)

    for i, (key, value) in enumerate(temperatureTags.items()):
        device.addDefaultQuantityTag("temperature", key, value)

    for i, (key, value) in enumerate(humidityTags.items()):
        device.addDefaultQuantityTag("humidity", key, value)
    
    for i, (key, value) in enumerate(chan1TemperatureTags.items()):
        device.addQuantityTag(1, "temperature", key, value)

    expectedDeviceTags = {iot_tags.DEVICE_LOCATION_TAG_KEY: 'bar', **deviceTags}
    expectedChan1TemperatureTags = {iot_tags.CHANNEL_NAME_TAG_KEY: 'temp', iot_tags.DATA_LABEL_TAG_KEY: 'american_sensor', iot_tags.DATA_UNITS_TAG_KEY: '°F', 'version': '3.0', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY: 'last'}
    assert expectedDeviceTags == device.getDeviceTags(), 'Bad device tags dict !'
    assert {} == device.getChannelTags(1), 'Bad channel 1 tags dict !'
    assert expectedChan1TemperatureTags == device.getQuantityTags(1, "temperature"), 'Bad quantity tags dict !'
    assert humidityTags == device.getQuantityTags(1, "humidity"), 'Bad quantity tags dict !'
    assert {} == device.getChannelTags(2), 'Bad channel 1 tags dict !'
    assert temperatureTags == device.getQuantityTags(2, "temperature"), 'Bad quantity tags dict !'
    assert humidityTags == device.getQuantityTags(2, "humidity"), 'Bad quantity tags dict !'

    expectedAllTags = {
        DbModels.DEVICE_NS: expectedDeviceTags,
        DbModels.DEFAULT_QUANTITIES_NS: {
            "temperature": temperatureTags,
            "humidity": humidityTags
        },        
        DbModels.QUANTITIES_BY_CHANNEL_NS: {
            "1": { "temperature": chan1TemperatureTags },
        }
    }

    assert expectedAllTags == device.getAllTags()

    assert "american_sensor" == device.getLabel(1, "temperature")
    assert None == device.getLabel(2, "temperature")
    assert "last" == device.getAggregationMethod(1, "temperature")
    assert "avg" == device.getAggregationMethod(2, "temperature")
    assert "°F" == device.getUnits(1, "temperature")
    assert "°C" == device.getUnits(2, "temperature")

    assert None == device.getLabel(1, "humidity")
    assert None == device.getLabel(2, "humidity")
    assert "avg" == device.getAggregationMethod(1, "humidity")
    assert "avg" == device.getAggregationMethod(2, "humidity")
    assert "per" == device.getUnits(1, "humidity")
    assert "per" == device.getUnits(2, "humidity")


def testLppChannelMapping():
    device: DbModels.Device = DbModels.Device('42', 'foo', 'bar')

    device.addDeviceTag('name', 'temp_sensor_test')
    device.addDeviceTag('version', '1.0')
    device.addDeviceTag('foo', 'baz')
    
    device.addChannelTag(0, iot_tags.CHANNEL_NAME_TAG_KEY, 'device')
    device.addDefaultQuantityTag('voltage', iot_tags.DATA_LABEL_TAG_KEY, 'Battery voltage')

    device.addChannelTag(1, iot_tags.CHANNEL_NAME_TAG_KEY, 'dht22_sensor')
    device.addDefaultQuantityTag('temperature', iot_tags.DATA_LABEL_TAG_KEY, 'Temp in °C')
    device.addDefaultQuantityTag('humidity', iot_tags.DATA_LABEL_TAG_KEY, 'Humidity')

    device.addChannelTag(2, iot_tags.CHANNEL_NAME_TAG_KEY, 'bme280_sensor')
    device.addQuantityTag(2, 'temperature', iot_tags.DATA_LABEL_TAG_KEY, 'Temp in °F')
    device.addDefaultQuantityTag('pressure', iot_tags.DATA_LABEL_TAG_KEY, 'Pressure')

    device.addDefaultQuantityTag('digital_output', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY, 'sum')
    device.addChannelTag(100, iot_tags.CHANNEL_NAME_TAG_KEY, 'device_probings')
    device.addQuantityTag(100, 'digital_output', iot_tags.DATA_LABEL_TAG_KEY, 'Probing count')
    device.addChannelTag(100, 'probing', 'probing')
    device.addChannelTag(101, iot_tags.CHANNEL_NAME_TAG_KEY, 'dht22_probings')
    device.addQuantityTag(101, 'digital_output', iot_tags.DATA_LABEL_TAG_KEY, 'Probing count')
    device.addChannelTag(101, 'probing', 'probing')
    device.addChannelTag(102, iot_tags.CHANNEL_NAME_TAG_KEY, 'bme280_probings')
    device.addQuantityTag(102, 'digital_output', iot_tags.DATA_LABEL_TAG_KEY, 'Probing count')
    device.addChannelTag(102, 'probing', 'probing')

    device.addChannelTag(200, iot_tags.CHANNEL_NAME_TAG_KEY, 'device_errors')
    device.addQuantityTag(200, 'digital_output', iot_tags.DATA_LABEL_TAG_KEY, 'Error count')
    device.addChannelTag(200, 'error', 'error')
    device.addChannelTag(201, iot_tags.CHANNEL_NAME_TAG_KEY, 'dht22_errors')
    device.addQuantityTag(201, 'digital_output', iot_tags.DATA_LABEL_TAG_KEY, 'Error count')
    device.addChannelTag(201, 'error', 'error')
    device.addChannelTag(202, iot_tags.CHANNEL_NAME_TAG_KEY, 'bme280_errors')
    device.addQuantityTag(202, 'digital_output', iot_tags.DATA_LABEL_TAG_KEY, 'Error count')
    device.addChannelTag(202, 'error', 'error')

    assert device.getDeviceTagValue('bar') == None, 'Device bar tag should be absent !'
    assert device.getDeviceTagValue('name') == 'temp_sensor_test', 'Device name tag is bad !'
    assert device.getDeviceTagValue('version') == '1.0', 'Device version tag is bad !'
    assert device.getDeviceTagValue('foo') == 'baz', 'Device foo tag is bad !'

    device.addDeviceTag('foo', 'bal')
    assert device.getDeviceTagValue('foo') == 'bal', 'Device foo tag should be changed !'

    device.removeDeviceTag('bar') # Should be possible to remove an inexisting tag
    device.removeDeviceTag('foo')
    assert device.getDeviceTagValue('foo') == None, 'Device foo tag should be removed !'

    device.addDeviceTag('foo', 'bam')
    assert device.getDeviceTagValue('foo') == 'bam', 'Device foo tag should be changed !'

    assert device.getChannelTagValue(0, iot_tags.CHANNEL_NAME_TAG_KEY) == 'device', 'Bad channel 0 name !'
    assert device.getChannelTagValue(1, iot_tags.CHANNEL_NAME_TAG_KEY) == 'dht22_sensor', 'Bad channel 1 name !'
    assert device.getChannelTagValue(2, iot_tags.CHANNEL_NAME_TAG_KEY) == 'bme280_sensor', 'Bad channel 2 name !'
    assert device.getChannelTagValue(3, iot_tags.CHANNEL_NAME_TAG_KEY) == None, 'Bad channel 3 name !'
    assert device.getChannelTagValue(100, iot_tags.CHANNEL_NAME_TAG_KEY) == 'device_probings', 'Bad channel 100 name !'
    assert device.getChannelTagValue(101, iot_tags.CHANNEL_NAME_TAG_KEY) == 'dht22_probings', 'Bad channel 101 name !'
    assert device.getChannelTagValue(102, iot_tags.CHANNEL_NAME_TAG_KEY) == 'bme280_probings', 'Bad channel 102 name !'
    assert device.getChannelTagValue(200, iot_tags.CHANNEL_NAME_TAG_KEY) == 'device_errors', 'Bad channel 200 name !'
    assert device.getChannelTagValue(201, iot_tags.CHANNEL_NAME_TAG_KEY) == 'dht22_errors', 'Bad channel 201 name !'
    assert device.getChannelTagValue(202, iot_tags.CHANNEL_NAME_TAG_KEY) == 'bme280_errors', 'Bad channel 202 name !'

    assert device.getQuantityTagValue(0, 'temperature', iot_tags.DATA_LABEL_TAG_KEY) == 'Temp in °C', 'Bad type 103 name for channel 0 !'
    assert device.getQuantityTagValue(0, 'voltage', iot_tags.DATA_LABEL_TAG_KEY) == 'Battery voltage', 'Bad type 116 name for channel 0 !'
    assert device.getQuantityTagValue(1, 'temperature', iot_tags.DATA_LABEL_TAG_KEY) == 'Temp in °C', 'Bad type 103 name for channel 1 !'
    assert device.getQuantityTagValue(1, 'humidity', iot_tags.DATA_LABEL_TAG_KEY) == 'Humidity', 'Bad type 104 name for channel 1 !'
    assert device.getQuantityTagValue(2, 'temperature', iot_tags.DATA_LABEL_TAG_KEY) == 'Temp in °F', 'Bad type 103 name for channel 2 !'
    assert device.getQuantityTagValue(2, 'humidity', iot_tags.DATA_LABEL_TAG_KEY) == 'Humidity', 'Bad type 103 name for channel 2 !'
    assert device.getQuantityTagValue(2, 'pressure', iot_tags.DATA_LABEL_TAG_KEY) == 'Pressure', 'Bad type 115 name for channel 2 !'
    assert device.getQuantityTagValue(3, 'temperature', iot_tags.DATA_LABEL_TAG_KEY) == 'Temp in °C', 'Bad type 103 name for channel !'
    assert device.getQuantityTagValue(100, 'digital_output', iot_tags.DATA_LABEL_TAG_KEY) == 'Probing count', 'Bad type 1 name for channel 100 !'
    assert device.getQuantityTagValue(101, 'digital_output', iot_tags.DATA_LABEL_TAG_KEY) == 'Probing count', 'Bad type 1 name for channel 101 !'
    assert device.getQuantityTagValue(102, 'digital_output', iot_tags.DATA_LABEL_TAG_KEY) == 'Probing count', 'Bad type 1 name for channel 102 !'
    assert device.getQuantityTagValue(200, 'digital_output', iot_tags.DATA_LABEL_TAG_KEY) == 'Error count', 'Bad type 1 name for channel 200 !'
    assert device.getQuantityTagValue(201, 'digital_output', iot_tags.DATA_LABEL_TAG_KEY) == 'Error count', 'Bad type 1 name for channel 201 !'
    assert device.getQuantityTagValue(202, 'digital_output', iot_tags.DATA_LABEL_TAG_KEY) == 'Error count', 'Bad type 1 name for channel 202 !'

    assert device.getQuantityTagValue(0, 'temperature', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY) == None, 'Bad type 103 aggregation method for channel 0 !'
    assert device.getQuantityTagValue(0, 'voltage', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY) == None, 'Bad type 116 aggregation method for channel 0 !'
    assert device.getQuantityTagValue(1, 'temperature', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY) == None, 'Bad type 103 aggregation method for channel 1 !'
    assert device.getQuantityTagValue(1, 'humidity', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY) == None, 'Bad type 104 aggregation method for channel 1 !'
    assert device.getQuantityTagValue(2, 'temperature', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY) == None, 'Bad type 103 aggregation method for channel 2 !'
    assert device.getQuantityTagValue(2, 'humidity', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY) == None, 'Bad type 103 aggregation method for channel 2 !'
    assert device.getQuantityTagValue(2, 'pressure', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY) == None, 'Bad type 115 aggregation method for channel 2 !'
    assert device.getQuantityTagValue(3, 'temperature', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY) == None, 'Bad type 103 aggregation method for channel !'
    assert device.getQuantityTagValue(100, 'digital_output', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY) == 'sum', 'Bad type 1 aggregation method for channel 100 !'
    assert device.getQuantityTagValue(100, 'analog_input', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY) == None, 'Bad type 2 aggregation method for channel 100 !'
    assert device.getQuantityTagValue(101, 'digital_output', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY) == 'sum', 'Bad type 1 aggregation method for channel 101 !'
    assert device.getQuantityTagValue(102, 'digital_output', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY) == 'sum', 'Bad type 1 aggregation method for channel 102 !'
    assert device.getQuantityTagValue(200, 'digital_output', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY) == 'sum', 'Bad type 1 aggregation method for channel 200 !'
    assert device.getQuantityTagValue(201, 'digital_output', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY) == 'sum', 'Bad type 1 aggregation method for channel 201 !'
    assert device.getQuantityTagValue(202, 'digital_output', iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY) == 'sum', 'Bad type 1 aggregation method for channel 202 !'


    assert device.getChannelTagValue(0, 'version') == None, 'Bad version tag value for channel 0 !'
    assert device.getChannelTagValue(0, 'foo') == None, 'Bad foo tag value for channel 0 !'
    assert device.getChannelTagValue(1, 'foo') == None, 'Bad foo tag value for channel 1 !'
    assert device.getChannelTagValue(2, 'foo') == None, 'Bad foo tag value for channel 2 !'
    assert device.getChannelTagValue(100, 'foo') == None, 'Bad foo tag value for channel 100 !'
    assert device.getChannelTagValue(100, 'probing') == 'probing', 'Bad probing tag value for channel 100 !'
    assert device.getChannelTagValue(101, 'foo') == None, 'Bad foo tag value for channel 101 !'
    assert device.getChannelTagValue(101, 'probing') == 'probing', 'Bad probing tag value for channel 101 !'
    assert device.getChannelTagValue(102, 'foo') == None, 'Bad foo tag value for channel 102 !'
    assert device.getChannelTagValue(102, 'probing') == 'probing', 'Bad probing tag value for channel 102 !'
    assert device.getChannelTagValue(200, 'foo') == None, 'Bad foo tag value for channel 200 !'
    assert device.getChannelTagValue(200, 'error') == 'error', 'Bad error tag value for channel 200 !'
    assert device.getChannelTagValue(201, 'foo') == None, 'Bad foo tag value for channel 201 !'
    assert device.getChannelTagValue(201, 'error') == 'error', 'Bad error tag value for channel 201 !'
    assert device.getChannelTagValue(202, 'foo') == None, 'Bad foo tag value for channel 202 !'
    assert device.getChannelTagValue(202, 'error') == 'error', 'Bad error tag value for channel 202 !'

    expectedAllTags = {
        DbModels.DEVICE_NS: {
            iot_tags.DEVICE_NAME_TAG_KEY: 'foo',
            iot_tags.DEVICE_LOCATION_TAG_KEY: 'bar',
            "name": "temp_sensor_test",
            "version": "1.0",
            "foo": "bam"
        },
        DbModels.CHANNELS_NS: {
            "0": {
                iot_tags.CHANNEL_NAME_TAG_KEY: "device",
            },
            "1": {
                iot_tags.CHANNEL_NAME_TAG_KEY: "dht22_sensor",
            },
            "2": {
                iot_tags.CHANNEL_NAME_TAG_KEY: "bme280_sensor",
            },
            "100": {
                iot_tags.CHANNEL_NAME_TAG_KEY: "device_probings",
                "probing": "probing",
            },
            "101": {
                iot_tags.CHANNEL_NAME_TAG_KEY: "dht22_probings",
                "probing": "probing",
            },
            "102": {
                iot_tags.CHANNEL_NAME_TAG_KEY: "bme280_probings",
                "probing": "probing",
            },
            "200": {
                iot_tags.CHANNEL_NAME_TAG_KEY: "device_errors",
                "error": "error",
            },
            "201": {
                iot_tags.CHANNEL_NAME_TAG_KEY: "dht22_errors",
                "error": "error",
            },
            "202": {
                iot_tags.CHANNEL_NAME_TAG_KEY: "bme280_errors",
                "error": "error",
            },
        },
        DbModels.DEFAULT_QUANTITIES_NS: {
            "digital_output": {iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY: "sum"},
            "temperature": {iot_tags.DATA_LABEL_TAG_KEY: "Temp in °C"},
            "humidity": {iot_tags.DATA_LABEL_TAG_KEY: "Humidity"},
            "pressure": {iot_tags.DATA_LABEL_TAG_KEY: "Pressure"},
            "voltage": {iot_tags.DATA_LABEL_TAG_KEY: "Battery voltage"},
        },        
        DbModels.QUANTITIES_BY_CHANNEL_NS: {
            "2": { "temperature": {iot_tags.DATA_LABEL_TAG_KEY: "Temp in °F"} },
            "100": { "digital_output": {iot_tags.DATA_LABEL_TAG_KEY: "Probing count"} },
            "101": { "digital_output": {iot_tags.DATA_LABEL_TAG_KEY: "Probing count"} },
            "102": { "digital_output": {iot_tags.DATA_LABEL_TAG_KEY: "Probing count"} },
            "200": { "digital_output": {iot_tags.DATA_LABEL_TAG_KEY: "Error count"} },
            "201": { "digital_output": {iot_tags.DATA_LABEL_TAG_KEY: "Error count"} },
            "202": { "digital_output": {iot_tags.DATA_LABEL_TAG_KEY: "Error count"} },
        }
    }

    assert expectedAllTags == device.getAllTags()

    assert "Probing count" == device.getLabel(101, "digital_output")
    assert "sum" == device.getAggregationMethod(101, "digital_output")
    assert "°C" == device.getUnits(1, "temperature")
