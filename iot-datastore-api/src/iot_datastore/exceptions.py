
class NotProcessablePayload(Exception):
    def __init__(self, message, payload):
        payloadStr = str(payload)
        self.payload = payload
        self.err = f"Unable to process payload: [{payloadStr}] with message: {message}"
        super().__init__(self.err)

class NotSupportedUid(Exception):
    def __init__(self, uid):
        self.err = f"Supplied uid: [{uid}] is not supported"
        super().__init__(self.err)

class NotValidDataTime(Exception):
    def __init__(self, message, time):
        self.time = time
        self.err = f"Supplied time: [{time}] is not valid. Message: {message}"
        super().__init__(self.err)

