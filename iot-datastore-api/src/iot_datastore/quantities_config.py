from typing import Mapping

quantityCodeByLppTypeMap: Mapping[int, str] = {
    0: 'digital_input',
    1: 'digital_output',
    2: 'analog_input',
    3: 'analog_output',
    100: 'generic',
    101: 'illuminance',
    102: 'presence',
    103: 'temperature',
    104: 'relative_humidity',
    113: 'acceleration',
    115: 'atmospheric_pressure',
    116: 'voltage',
    117: 'current',
    118: 'frequency',
    120: 'percentage',
    121: 'altitude',
    122: 'load',
    128: 'power',
    130: 'distance',
    131: 'energy',
    132: 'direction',
    133: 'unix_timestamp',
    134: 'gyrometer',
    136: 'gps_location',
}

unitsTagByQuantityCodeMap: Mapping[str, str] = {
    'illuminance': 'lux',
    'temperature': '°C',
    'relative_humidity': '%',
    'acceleration': 'G',
    'atmospheric_pressure': 'hPa',
    'voltage': 'V',
    'current': 'A',
    'frequency': 'Hz',
    'percentage': '%',
    'altitude': 'm',
    'load': 'g',
    'power': 'W',
    'distance': 'm',
    'energy': 'J',
    'direction': '°',
    'gyrometer': '°/s',
    'sound_level': 'dbA',
    #'gps_location': 'm',
}

fieldNameOverrideByQuantityCodeMap: Mapping[str, str] = {
    'acceleration': ['x', 'y', 'z'],
    'gyrometer': ['x', 'y', 'z'],
    'gps_location': ['latitude', 'longitude', 'altitude'],
}

defaultAggregationMethodByQuantityCodeMap: Mapping[str, str] = {
    'acceleration': 'last',
    'gyrometer': 'last',
    'gps_location': 'last',
}

def getLppQuantityCode(lppType: int) -> str:
    return quantityCodeByLppTypeMap.get(lppType, None)

def getUnitsTag(quantityCode: str) -> str:
    return unitsTagByQuantityCodeMap.get(quantityCode, None)

def getFieldNames(quantityCode: str, values: list) -> list:
    if not quantityCode:
        raise ValueError('No quantityCode supplied')
    if not values or len(values) == 0:
        raise ValueError('No values supplied !')

    dimension = len(values)
    fieldNames = fieldNameOverrideByQuantityCodeMap.get(quantityCode, None)
    if fieldNames:
        if dimension != len(fieldNames):
            raise ValueError('Supplied values: [%s] for quantity: [%s] do not match expected format: [%s]' % (str(values), quantityCode, str(fieldNames)))
    else:
        if dimension == 1:
            fieldNames = ['value']
        elif dimension > 10:
            raise NotImplementedError('Supplied multidimensional quantity: [%s] of size: [%d] greater than max currently supported (10)' % (quantityCode, dimension))
        else:
            fieldNames = ['value%d' % i for i in range(dimension)]

    return fieldNames

def getDefaultAggregationMethod(quantityCode: str) -> str:
    return defaultAggregationMethodByQuantityCodeMap.get(quantityCode, 'avg')
