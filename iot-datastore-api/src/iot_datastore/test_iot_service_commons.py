from datetime import datetime, timezone
from unittest.mock import call, ANY
import base64
from iot_datastore import iot_tags
import pytest
from pytest_mock import MockerFixture
import tempfile
import flask
from cayennelpp import LppFrame
import iot_datastore.iot_service as IotService
import iot_datastore.db_models as DbModels
from iot_datastore.exceptions import NotProcessablePayload

@pytest.fixture
def app(mocker):
    #__log.info('app fixture called')
    #mocker.patch('iot_datastore.influxdb_service.createInluxDbBucket')
    app = flask.Flask(__name__)

    db_fd, app.config['DATABASE'] = tempfile.mkstemp()
    app.config['TESTING'] = True
    app.config['SQLALCHEMY_BINDS'] = {}
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_ECHO'] = False
    app.config['BUNDLE_ERRORS'] = True

    with app.app_context():
        IotService.initDb(app, 'sqlite:///' + app.config['DATABASE'], False)

    yield app

MOCKED_NOW_IN_SEC = 1000000
MOCKED_NOW = datetime.utcfromtimestamp(MOCKED_NOW_IN_SEC)

MAX_SECONDS_BACK = 180
MAX_SECONDS_FORWARD = 60

DEVICE_1 = DbModels.Device('xxx1', 'device1', 'location1')
DEVICE_2 = DbModels.Device('xxx2', 'device2', 'location2')
DEVICE_3 = DbModels.Device('xxx3', 'device3', 'location3')

SIMPLE_BINARY_LPP_1 = bytes([0x03, 0x67, 0x01, 0x10])
SIMPLE_BASE64_LPP_1 = base64.standard_b64encode(SIMPLE_BINARY_LPP_1).decode("ascii")
SIMPLE_BINARY_LPP_2 = bytes([0x05, 0x67, 0xff, 0xff]) 
SIMPLE_BASE64_LPP_2 = base64.standard_b64encode(SIMPLE_BINARY_LPP_2).decode("ascii") 
SIMPLE_URL_BASE64_LPP_2 = base64.urlsafe_b64encode(SIMPLE_BINARY_LPP_2).decode("ascii") 
SIMPLE_BINARY_LPP_3 = bytes([0x03, 0x67, 0x01, 0x10, 0x05, 0x67, 0x00, 0xff])

ACCELEROMETER_BINARY_LPP_1 = bytes([0x06, 0x71, 0x04, 0xD2, 0xFB, 0x2E, 0x00, 0x00]) # Channel 6 x=1.234G ; y=-1.234G ; z=0G
ACCELEROMETER_BASE64_LPP_1 = base64.standard_b64encode(ACCELEROMETER_BINARY_LPP_1).decode("ascii")
GYROMETER_BINARY_LPP_1 = bytes([0x03, 0x86, 0x00, 0x34, 0xfe, 0xb6, 0xff, 0xfe]) # Channel 3 x=0.52°/s ; y=-3.3°/s ; z=-0.2°/s
GYROMETER_URL_BASE64_LPP_1 = base64.urlsafe_b64encode(GYROMETER_BINARY_LPP_1).decode("ascii") 
GPS_BINARY_LPP_1 = bytes([0x01, 0x88, 0x06, 0x76, 0x5f, 0xf2, 0x96, 0x0a, 0x00, 0x03, 0xe8]) # Channel 1 latitude=42.3519 ; longitude=-87.9094 ; altitude=10

ERRONEOUS_BASE64_LPP_1 = base64.standard_b64encode(bytes([0x03, 0x67, 0x01])).decode("ascii")
ERRONEOUS_URL_BASE64_LPP_1 = base64.urlsafe_b64encode(bytes([0x03, 0x67, 0x01])).decode("ascii")
NOT_BASE64_LPP_1 = '03670110056700FFz'


JUST_NOT_OUTDATED = MOCKED_NOW_IN_SEC - MAX_SECONDS_BACK + 1
JUST_OUTDATED = MOCKED_NOW_IN_SEC - MAX_SECONDS_BACK - 1
JUST_NOT_TOO_FAR_IN_FUTUR = MOCKED_NOW_IN_SEC + MAX_SECONDS_FORWARD - 1
JUST_TOO_FAR_IN_FUTUR = MOCKED_NOW_IN_SEC + MAX_SECONDS_FORWARD + 1

# Time on channel 3
JUST_NOT_OUTDATED_BINARY_LPP = bytes([0x03, 0x67, 0x01, 0x10, 0x05, 0x67, 0x00, 0xff, 0x03, 0x85]) + JUST_NOT_OUTDATED.to_bytes(4, 'big')
JUST_OUTDATED_BINARY_LPP = bytes([0x03, 0x67, 0x01, 0x10, 0x05, 0x67, 0x00, 0xff, 0x03, 0x85]) + JUST_OUTDATED.to_bytes(4, 'big')
# Time on channel 5
JUST_NOT_TOO_FAR_IN_FUTUR_BINARY_LPP = bytes([0x03, 0x67, 0x01, 0x10, 0x05, 0x67, 0x00, 0xff, 0x05, 0x85]) + JUST_NOT_TOO_FAR_IN_FUTUR.to_bytes(4, 'big')
JUST_TOO_FAR_IN_FUTUR_BINARY_LPP = bytes([0x03, 0x67, 0x01, 0x10, 0x05, 0x67, 0x00, 0xff, 0x05, 0x85]) + JUST_TOO_FAR_IN_FUTUR.to_bytes(4, 'big')

# Time on channel 7 (default time)
DEFAULT_TIME = MOCKED_NOW_IN_SEC - int(MAX_SECONDS_BACK / 2)
DEFAULT_TIME_BINARY_LPP = bytes([0x03, 0x67, 0x01, 0x10, 0x05, 0x67, 0x00, 0xff, 0x07, 0x85]) + DEFAULT_TIME.to_bytes(4, 'big')

# Time on channel 3 + 7 (default time)
OVERRIDEN_TIME_BINARY_LPP = bytes([0x03, 0x67, 0x01, 0x10, 0x05, 0x67, 0x00, 0xff, 0x07, 0x85]) + DEFAULT_TIME.to_bytes(4, 'big') + bytes([0x03, 0x85]) + JUST_NOT_OUTDATED.to_bytes(4, 'big')


def buildMeasurementNameFromTags(tags):
    return IotService.buildMeasurementName(tags[iot_tags.DEVICE_UID_TAG_KEY], tags[iot_tags.CHANNEL_TAG_KEY], tags[iot_tags.DATA_QUANTITY_TAG_KEY])


def initStoreLppByDeviceUidMock(mocker, findDeviceByIdMethod, storeMeasurementMethod):
    findDeviceByIdMock = mocker.patch('iot_datastore.iot_service.findDeviceById', side_effect=findDeviceByIdMethod)
    #storeMeasurementMock = mocker.patch('iot_datastore.influxdb_service.storeMeasurement', side_effect=storeMeasurementMethod)
    storeMeasurementMock = mocker.patch('iot_datastore.promscale_service.storeMeasurement', side_effect=storeMeasurementMethod)
    return (findDeviceByIdMock, storeMeasurementMock)
    