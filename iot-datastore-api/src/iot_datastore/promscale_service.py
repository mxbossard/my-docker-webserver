import os
import logging
import json
import copy
from datetime import datetime
import requests
from iot_datastore.db_models import DataPayload


__log = logging.getLogger(__name__)

promscaleUrl = os.environ.get('PROMSCALE_URL', 'PROMSCALE_URL env var not defined !')
#influxdbDbName = os.environ.get('INFLUXDB_DB_NAME')
#influxdbRpName = os.environ.get('INFLUXDB_RP_NAME', None)
#influxdbOrg = os.environ.get('INFLUXDB_ORG', 'testOrg')
#influxdbUser = os.environ.get('INFLUXDB_USER')
#influxdbPassword = os.environ.get('INFLUXDB_PASSWORD')

__write_url = promscaleUrl + '/write'

def _writeUniqPayloadAsJson(payload: dict): 
    r = requests.post(__write_url, json=payload)
    r.raise_for_status()

def _writePayloadsAsJson(payloads: list): 
    jsonPayloads = []
    for payload in payloads:
        p = json.dumps(payload)
        jsonPayloads.append(p)

    r = requests.post(__write_url, data=''.join(jsonPayloads))
    r.raise_for_status()

def _buildPromscalePayload(name: str, tags: dict, time:datetime, fieldName: str, value: float):
    formattedTime = int(time.timestamp() * 1000)
    sample = [formattedTime, value]
    # Build labels dict from tags dict
    labels = copy.deepcopy(tags)
    labels['__name__'] = name
    labels['_field'] = fieldName
    payload = {
            "labels": labels,
            "samples": [sample]
    }
    return payload

# measurement: "name", tags: map, time: "2009-11-10T23:00:00Z", fields: map
def storeMeasurement(name: str, tags: dict, time: datetime, fields: dict):
    for (fieldName, value) in fields.items():
        payload = _buildPromscalePayload(name, tags, time, fieldName, value)
        _writeUniqPayloadAsJson(payload)

    __log.info("Published data in promscale for measurment: [%s] with time: [%s].", name, time)

# measurement: "name", tags: map, time: "2009-11-10T23:00:00Z", fields: map
def storeMeasurements(measurements: list):
    payloads = []
    for measurement in measurements:
        name = measurement[0]
        tags = measurement[1]
        time = measurement[2]
        fields = measurement[3]
        for (fieldName, value) in fields.items():
            p = _buildPromscalePayload(name, tags, time, fieldName, value)
            payloads.append(p)

    _writePayloadsAsJson(payloads)
    __log.info("Published data in promscale.")


def reTagMeasurement(measurementName, tagKey, tagOldValue, tagNewValue, fromTime):
    raise NotImplementedError
    # For now we need to duplicate data with new tag then drop previous data
    formattedFromTime = fromTime.strftime('%Y-%m-%dT%H:%M:%S.%fZ')

    query = f'''from(bucket:{influxdbBucket})
        |> range(start: {formattedFromTime})
        |> filter(fn: (r) =>
            r._measurement == "{measurementName}" and
            r.{tagKey} == "{tagOldValue}"
        )'''

    records = __query_api.query_stream(query=query)
    for record in records:
        # Alter tag value
        record[tagKey] = tagNewValue

    write_api = __influxClient.write_api(write_options=WriteOptions(batch_size=50_000, flush_interval=10_000))
    write_api.write(bucket=influxdbBucket, record=records)
    

