from datetime import datetime, timezone
from unittest.mock import call, ANY
import copy
import base64
import pytest
from pytest_mock import MockerFixture
import tempfile
import flask
from cayennelpp import LppFrame
import iot_datastore.iot_service as IotService
import iot_datastore.db_models as DbModels
from iot_datastore.exceptions import NotProcessablePayload
from iot_datastore.test_iot_service_commons import *
from iot_datastore import iot_tags


def test_storeLppFrameByDeviceWithoutMapping(mocker: MockerFixture) -> None:
    mockedInfluxDbService_storeMeasurement = mocker.patch('iot_datastore.influxdb_service.storeMeasurement')
    mockedPromscaleService_storeMeasurement = mocker.patch('iot_datastore.promscale_service.storeMeasurement')
    mockedNowDatetime = datetime.utcfromtimestamp(1000000)
    mocker.patch('iot_datastore.helper.helper.getUtcnowDatetime').return_value = mockedNowDatetime

    deviceUid = 'y42'
    deviceName = 'foo'
    deviceLocation = 'bar'
    device = DbModels.Device(deviceUid, deviceName, deviceLocation)

    device.addDeviceTag('name', 'temp_sensor_test')
    device.addDeviceTag('version', '1.0')
    device.addDeviceTag('foo', 'baz')

    lppFrame = LppFrame()
    lppFrame.add_voltage(0, 3.5)
    lppFrame.add_temperature(1, 17.2)
    lppFrame.add_humidity(1, 0.53)
    lppFrame.add_temperature(2, 18.1)
    lppFrame.add_humidity(2, 0.58)
    lppFrame.add_pressure(2, 1015)
    lppFrame.add_digital_input(100, 3)
    lppFrame.add_digital_input(101, 2)
    lppFrame.add_digital_input(102, 3)
    lppFrame.add_digital_input(200, 0)
    lppFrame.add_digital_input(201, 1)
    lppFrame.add_digital_input(202, 0)

    IotService._storeLppFrameByDevice(device, lppFrame, 'UnitTest', 'UnitTest')

    extraTags = {'name': 'temp_sensor_test', 'version': '1.0', 'foo': 'baz'}

    chan0Tags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 0, None, 116, 'voltage', 'V', 'lpp', 'avg', None, 'value', extraTags)
    chan1aTags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 1, None, 103, 'temperature', '°C', 'lpp', 'avg', None, 'value', extraTags)
    chan1bTags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 1, None, 104, 'humidity', '%', 'lpp', 'avg', None, 'value', extraTags)
    chan2aTags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 2, None, 103, 'temperature', '°C', 'lpp', 'avg', None, 'value', extraTags)
    chan2bTags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 2, None, 104, 'humidity', '%', 'lpp', 'avg', None, 'value', extraTags)
    chan2cTags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 2, None, 115, 'pressure', 'hPa', 'lpp', 'avg', None, 'value', extraTags)
    chan100Tags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 100, None, 0, 'digital_input', None, 'lpp', 'avg', None, 'value', extraTags)
    chan101Tags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 101, None, 0, 'digital_input', None, 'lpp', 'avg', None, 'value', extraTags)
    chan102Tags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 102, None, 0, 'digital_input', None, 'lpp', 'avg', None, 'value', extraTags)
    chan200Tags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 200, None, 0, 'digital_input', None, 'lpp', 'avg', None, 'value', extraTags)
    chan201Tags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 201, None, 0, 'digital_input', None, 'lpp', 'avg', None, 'value', extraTags)
    chan202Tags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 202, None, 0, 'digital_input', None, 'lpp', 'avg', None, 'value', extraTags)

    chan0Fields = {'value': 3.5}
    chan1aFields = {'value': 17.2}
    chan1bFields = {'value': 0.53}
    chan2aFields = {'value': 18.1}
    chan2bFields = {'value': 0.58}
    chan2cFields = {'value': 1015}
    chan100Fields = {'value': 3}
    chan101Fields = {'value': 2}
    chan102Fields = {'value': 3}
    chan200Fields = {'value': 0}
    chan201Fields = {'value': 1}
    chan202Fields = {'value': 0}
    
    mockedTime = '1970-01-12T13:46:40.000000Z'
    mockedCalls = [
        call(buildMeasurementNameFromTags(chan0Tags), chan0Tags, mockedNowDatetime, chan0Fields),
        call(buildMeasurementNameFromTags(chan1aTags), chan1aTags, mockedNowDatetime, chan1aFields),
        call(buildMeasurementNameFromTags(chan1bTags), chan1bTags, mockedNowDatetime, chan1bFields),
        call(buildMeasurementNameFromTags(chan2aTags), chan2aTags, mockedNowDatetime, chan2aFields),
        call(buildMeasurementNameFromTags(chan2bTags), chan2bTags, mockedNowDatetime, chan2bFields),
        call(buildMeasurementNameFromTags(chan2cTags), chan2cTags, mockedNowDatetime, chan2cFields),
        call(buildMeasurementNameFromTags(chan100Tags), chan100Tags, mockedNowDatetime, chan100Fields),
        call(buildMeasurementNameFromTags(chan101Tags), chan101Tags, mockedNowDatetime, chan101Fields),
        call(buildMeasurementNameFromTags(chan102Tags), chan102Tags, mockedNowDatetime, chan102Fields),
        call(buildMeasurementNameFromTags(chan200Tags), chan200Tags, mockedNowDatetime, chan200Fields),
        call(buildMeasurementNameFromTags(chan201Tags), chan201Tags, mockedNowDatetime, chan201Fields),
        call(buildMeasurementNameFromTags(chan202Tags), chan202Tags, mockedNowDatetime, chan202Fields)
    ]

    for k, called in enumerate(mockedInfluxDbService_storeMeasurement.call_args_list):
        assert called.args[0] == mockedCalls[k].args[0], 'storeMeasurement called with bad deviceName !'
        assert called.args[1] == mockedCalls[k].args[1], 'storeMeasurement called with bad tags !'
        assert called.args[2] == mockedCalls[k].args[2], 'storeMeasurement called with bad time !'
        assert called.args[3] == mockedCalls[k].args[3], 'storeMeasurement called with bad fields !'


def test_storeLppFrameByDeviceWithFullMapping(mocker: MockerFixture) -> None:
    mockedInfluxDbService_storeMeasurement = mocker.patch('iot_datastore.influxdb_service.storeMeasurement')
    mockedPromscaleService_storeMeasurement = mocker.patch('iot_datastore.promscale_service.storeMeasurement')
    mockedNowDatetime = datetime.utcfromtimestamp(1000000)
    mocker.patch('iot_datastore.helper.helper.getUtcnowDatetime').return_value = mockedNowDatetime

    deviceUid = 'x42'
    deviceName = 'foo'
    deviceLocation = 'bar'
    device = DbModels.Device(deviceUid, deviceName, deviceLocation)

    device.addDeviceTag('name', 'temp_sensor_test')
    device.addDeviceTag('version', '1.0')
    device.addDeviceTag('foo', 'baz')

    device.addChannelTag(0, '_channelName', 'chan0')
    device.addQuantityTag(0, 'voltage', '_label', 'Battery voltage')

    device.addChannelTag(1, '_channelName', 'dht22_sensor')
    device.addQuantityTag(1, 'temperature', '_label', 'Temp in °C')
    device.addQuantityTag(1, 'humidity', '_label', 'Humidity')

    device.addChannelTag(2, '_channelName', 'bme280_sensor')
    device.addQuantityTag(2, 'temperature', '_label', 'Temp in °F')
    device.addQuantityTag(2, 'humidity', '_label', 'Humidity')
    device.addQuantityTag(2, 'pressure', '_label', 'Pressure')

    device.addDefaultQuantityTag('digital_input', '_aggregationMethod', 'sum')

    device.addChannelTag(100, '_channelName', 'device_probings')
    device.addQuantityTag(100, 'digital_input', '_label', 'Probing count')
    device.addQuantityTag(100, 'digital_input', 'probing', 'probing')
    device.addChannelTag(101, '_channelName', 'dht22_probings')
    device.addQuantityTag(101, 'digital_input', '_label', 'Probing count')
    device.addQuantityTag(101, 'digital_input', 'probing', 'probing')
    device.addChannelTag(102, '_channelName', 'bme280_probings')
    device.addQuantityTag(102, 'digital_input', '_label', 'Probing count')
    device.addQuantityTag(102, 'digital_input', 'probing', 'probing')

    device.addChannelTag(200, '_channelName', 'device_errors')
    device.addQuantityTag(200, 'digital_input', '_label', 'Error count')
    device.addQuantityTag(200, 'digital_input', 'error', 'error')
    device.addChannelTag(201, '_channelName', 'dht22_errors')
    device.addQuantityTag(201, 'digital_input', '_label', 'Error count')
    device.addQuantityTag(201, 'digital_input', 'error', 'error')
    device.addChannelTag(202, '_channelName', 'bme280_errors')
    device.addQuantityTag(202, 'digital_input', '_label', 'Error count')
    device.addQuantityTag(202, 'digital_input', 'error', 'error')

    lppFrame = LppFrame()
    lppFrame.add_voltage(0, 3.5)
    lppFrame.add_temperature(1, 17.2)
    lppFrame.add_humidity(1, 0.53)
    lppFrame.add_temperature(2, 18.1)
    lppFrame.add_humidity(2, 0.58)
    lppFrame.add_pressure(2, 1015)
    lppFrame.add_digital_input(100, 3)
    lppFrame.add_digital_input(101, 2)
    lppFrame.add_digital_input(102, 3)
    lppFrame.add_digital_input(200, 0)
    lppFrame.add_digital_input(201, 1)
    lppFrame.add_digital_input(202, 0)

    IotService._storeLppFrameByDevice(device, lppFrame, 'UnitTest', 'UnitTest')

    extraTags = {'name': 'temp_sensor_test', 'version': '1.0', 'foo': 'baz'}
    probingTags = {**extraTags, **{'probing': 'probing'}}
    errorTags = {**extraTags, **{'error': 'error'}}
    chan0Tags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 0, 'chan0', 116, 'voltage', 'V', 'lpp', 'avg', 'Battery voltage', 'value', extraTags)
    chan1aTags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 1, 'dht22_sensor', 103, 'temperature', '°C', 'lpp', 'avg', 'Temp in °C', 'value', extraTags)
    chan1bTags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 1, 'dht22_sensor', 104, 'humidity', '%', 'lpp', 'avg', 'Humidity', 'value', extraTags)
    chan2aTags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 2, 'bme280_sensor', 103, 'temperature', '°C', 'lpp', 'avg', 'Temp in °F', 'value', extraTags)
    chan2bTags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 2, 'bme280_sensor', 104, 'humidity', '%', 'lpp', 'avg', 'Humidity', 'value', extraTags)
    chan2cTags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 2, 'bme280_sensor', 115, 'pressure', 'hPa', 'lpp', 'avg', 'Pressure', 'value', extraTags)
    chan100Tags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 100, 'device_probings', 0, 'digital_input', None, 'lpp', 'sum', 'Probing count', 'value', probingTags)
    chan101Tags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 101, 'dht22_probings', 0, 'digital_input', None, 'lpp', 'sum', 'Probing count', 'value', probingTags)
    chan102Tags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 102, 'bme280_probings', 0, 'digital_input', None, 'lpp', 'sum', 'Probing count', 'value', probingTags)
    chan200Tags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 200, 'device_errors', 0, 'digital_input', None, 'lpp', 'sum', 'Error count', 'value', errorTags)
    chan201Tags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 201, 'dht22_errors', 0, 'digital_input', None, 'lpp', 'sum', 'Error count', 'value', errorTags)
    chan202Tags = iot_tags.buildLppTags('UnitTest', 'UnitTest', deviceUid, deviceName, deviceLocation, 202, 'bme280_errors', 0, 'digital_input', None, 'lpp', 'sum', 'Error count', 'value', errorTags)

    chan0Fields = {'value': 3.5}
    chan1aFields = {'value': 17.2}
    chan1bFields = {'value': 0.53}
    chan2aFields = {'value': 18.1}
    chan2bFields = {'value': 0.58}
    chan2cFields = {'value': 1015}
    chan100Fields = {'value': 3}
    chan101Fields = {'value': 2}
    chan102Fields = {'value': 3}
    chan200Fields = {'value': 0}
    chan201Fields = {'value': 1}
    chan202Fields = {'value': 0}
    
    mockedTime = '1970-01-12T13:46:40.000000Z'
    mockedCalls = [
        call(buildMeasurementNameFromTags(chan0Tags), chan0Tags, mockedNowDatetime, chan0Fields),
        call(buildMeasurementNameFromTags(chan1aTags), chan1aTags, mockedNowDatetime, chan1aFields),
        call(buildMeasurementNameFromTags(chan1bTags), chan1bTags, mockedNowDatetime, chan1bFields),
        call(buildMeasurementNameFromTags(chan2aTags), chan2aTags, mockedNowDatetime, chan2aFields),
        call(buildMeasurementNameFromTags(chan2bTags), chan2bTags, mockedNowDatetime, chan2bFields),
        call(buildMeasurementNameFromTags(chan2cTags), chan2cTags, mockedNowDatetime, chan2cFields),
        call(buildMeasurementNameFromTags(chan100Tags), chan100Tags, mockedNowDatetime, chan100Fields),
        call(buildMeasurementNameFromTags(chan101Tags), chan101Tags, mockedNowDatetime, chan101Fields),
        call(buildMeasurementNameFromTags(chan102Tags), chan102Tags, mockedNowDatetime, chan102Fields),
        call(buildMeasurementNameFromTags(chan200Tags), chan200Tags, mockedNowDatetime, chan200Fields),
        call(buildMeasurementNameFromTags(chan201Tags), chan201Tags, mockedNowDatetime, chan201Fields),
        call(buildMeasurementNameFromTags(chan202Tags), chan202Tags, mockedNowDatetime, chan202Fields)
    ]

    for k, called in enumerate(mockedInfluxDbService_storeMeasurement.call_args_list):
        assert called.args[0] == mockedCalls[k].args[0], 'storeMeasurement called with bad deviceName !'
        assert called.args[1] == mockedCalls[k].args[1], 'storeMeasurement called with bad tags !'
        assert called.args[2] == mockedCalls[k].args[2], 'storeMeasurement called with bad time !'
        assert called.args[3] == mockedCalls[k].args[3], 'storeMeasurement called with bad fields !'

def testStoreLppByDeviceUidWithoutUid(mocker):
    def findDeviceById(id):
        return None
    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, None)

    deviceUid = None
    lpp = SIMPLE_BASE64_LPP_1
    ingestComponent = "ingest1"
    ingestMethod = 'method1'
    with pytest.raises(KeyError):
        IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)

    storeMeasurementMock.assert_not_called()

def testStoreLppByDeviceUidWithoutIngestComponent(mocker):
    def findDeviceById(id):
        return copy.deepcopy(DEVICE_1)
    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, None)

    deviceUid = DEVICE_1.uid
    lpp = SIMPLE_BASE64_LPP_1
    ingestComponent = None
    ingestMethod = 'method1'
    with pytest.raises(Exception):
        IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)

    storeMeasurementMock.assert_not_called()

def testStoreLppByDeviceUidWithoutIngestMethod(mocker):
    def findDeviceById(id):
        return copy.deepcopy(DEVICE_1)
    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, None)

    deviceUid = DEVICE_1.uid
    lpp = SIMPLE_BASE64_LPP_1
    ingestComponent = 'source1'
    ingestMethod = None
    with pytest.raises(Exception):
        IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)

    storeMeasurementMock.assert_not_called()

def testStoreLppByDeviceUidWithoutLpp(mocker):
    def findDeviceById(id):
        return copy.deepcopy(DEVICE_1)
    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, None)

    deviceUid = DEVICE_1.uid
    lpp = None
    ingestComponent = "ingest1"
    ingestMethod = 'method1'
    with pytest.raises(Exception):
        IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)

    storeMeasurementMock.assert_not_called()

def testStoreLppByDeviceUidWithEmptyLpp(mocker):
    def findDeviceById(id):
        return copy.deepcopy(DEVICE_1)
    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, None)

    deviceUid = DEVICE_1.uid
    lpp = ''
    ingestComponent = "ingest1"
    ingestMethod = 'method1'
    with pytest.raises(Exception):
        IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)

    storeMeasurementMock.assert_not_called()

def testStoreLppByDeviceUidWithErroneousLpp(mocker):
    def findDeviceById(id):
        return copy.deepcopy(DEVICE_1)
    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, None)

    deviceUid = DEVICE_1.uid
    lpp = ERRONEOUS_BASE64_LPP_1
    ingestComponent = "ingest1"
    ingestMethod = 'method1'
    with pytest.raises(NotProcessablePayload):
        IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)

    storeMeasurementMock.assert_not_called()

def testStoreLppByDeviceUidWithNotBase64Lpp(mocker):
    def findDeviceById(id):
        return copy.deepcopy(DEVICE_1)
    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, None)

    deviceUid = DEVICE_1.uid
    lpp = NOT_BASE64_LPP_1
    ingestComponent = "ingest1"
    ingestMethod = 'method1'
    with pytest.raises(NotProcessablePayload):
        IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)

    storeMeasurementMock.assert_not_called()

def testStoreLppByDeviceUidWithBase64Lpp(mocker):
    deviceUid = DEVICE_1.uid
    lpp = SIMPLE_BASE64_LPP_1
    ingestComponent = "ingest1"
    ingestMethod = 'method1'

    def findDeviceById(id):
        return copy.deepcopy(DEVICE_1)
    def storeMeasurement(name, tags, time, fields):
        assert name == IotService.buildMeasurementName(DEVICE_1.uid, 3, 'temperature'), 'Measurement name should be the device UID !'
        assert tags == iot_tags.buildLppTags(ingestComponent, ingestMethod, deviceUid, DEVICE_1.name, DEVICE_1.location, 3, None, 103, 'temperature', '°C', 'lpp', 'avg', None, None, {}), 'Tags are Erroneous !'
        assert fields == {'value': 27.2}, 'Fields are Erroneous !'
    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, storeMeasurement)

    IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)
    storeMeasurementMock.assert_called_once()

def testStoreAccelerometerLppByDeviceUidWithBase64Lpp(mocker):
    deviceUid = DEVICE_1.uid
    lpp = ACCELEROMETER_BASE64_LPP_1
    ingestComponent = "ingest1"
    ingestMethod = 'method1'

    def findDeviceById(id):
        return copy.deepcopy(DEVICE_1)
    def storeMeasurement(name, tags, time, fields):
        assert name == IotService.buildMeasurementName(DEVICE_1.uid, 6, 'acceleration'), 'Measurement name should be the device UID !'
        assert tags == iot_tags.buildLppTags(ingestComponent, ingestMethod, deviceUid, DEVICE_1.name, DEVICE_1.location, 6, None, 113, 'acceleration', 'G', 'lpp', 'last', None, None, {}), 'Tags are Erroneous !'
        assert fields == {'x': 1.234, 'y': -1.234, 'z': 0}, 'Fields are Erroneous !'
    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, storeMeasurement)

    IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)
    storeMeasurementMock.assert_called_once()

def testStoreLppByDeviceUidWithUrlBase64Lpp(mocker):
    deviceUid = DEVICE_2.uid
    lpp = SIMPLE_URL_BASE64_LPP_2
    ingestComponent = "ingest1"
    ingestMethod = 'method1'

    assert SIMPLE_URL_BASE64_LPP_2 != SIMPLE_BASE64_LPP_2, 'Standard and URL safe Base64 LPP must differ for tests !'

    def findDeviceById(id):
        return copy.deepcopy(DEVICE_2)
    def storeMeasurement(name, tags, time, fields):
        assert name == IotService.buildMeasurementName(DEVICE_2.uid, 5, 'temperature'), 'Measurement name should be the device UID !'
        assert tags == iot_tags.buildLppTags(ingestComponent, ingestMethod, deviceUid, DEVICE_2.name, DEVICE_2.location, 5, None, 103, 'temperature', '°C', 'lpp', 'avg', None, None, {}), 'Tags are Erroneous !'
        assert fields == {'value': -0.1}, 'Fields are Erroneous !'
    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, storeMeasurement)
    IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)
    storeMeasurementMock.assert_called_once()

def testStoreGyrometerLppByDeviceUidWithUrlBase64Lpp(mocker):
    deviceUid = DEVICE_2.uid
    lpp = GYROMETER_URL_BASE64_LPP_1
    ingestComponent = "ingest1"
    ingestMethod = 'method1'

    def findDeviceById(id):
        return copy.deepcopy(DEVICE_2)
    def storeMeasurement(name, tags, time, fields):
        assert name == IotService.buildMeasurementName(DEVICE_2.uid, 3, 'gyrometer'), 'Measurement name should be the device UID !'
        assert tags == iot_tags.buildLppTags(ingestComponent, ingestMethod, deviceUid, DEVICE_2.name, DEVICE_2.location, 3, None, 134, 'gyrometer', '°/s', 'lpp', 'last', None, None, {}), 'Tags are Erroneous !'
        assert fields == {'x': 0.52, 'y': -3.3, 'z': -0.02}, 'Fields are Erroneous !'
    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, storeMeasurement)
    IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)
    storeMeasurementMock.assert_called_once()

def testStoreLppByDeviceUidWithBinaryLpp(mocker):
    deviceUid = DEVICE_2.uid
    lpp = SIMPLE_BINARY_LPP_3
    ingestComponent = "ingest1"
    ingestMethod = 'method1'

    def findDeviceById(id):
        return copy.deepcopy(DEVICE_2)

    mockedTime = '1970-01-12T13:46:40.000000Z'
    mockedNowDatetime = datetime.utcfromtimestamp(1000000)
    mockedCalls = [
        call(IotService.buildMeasurementName(DEVICE_2.uid, 3, 'temperature'), iot_tags.buildLppTags(ingestComponent, ingestMethod, deviceUid, DEVICE_2.name, DEVICE_2.location, 3, None, 103, 'temperature', '°C', 'lpp', 'avg', None, None, {}), mockedNowDatetime, {'value': 27.2}),
        call(IotService.buildMeasurementName(DEVICE_2.uid, 5, 'temperature'), iot_tags.buildLppTags(ingestComponent, ingestMethod, deviceUid, DEVICE_2.name, DEVICE_2.location, 5, None, 103, 'temperature', '°C', 'lpp', 'avg', None, None, {}), mockedNowDatetime, {'value': 25.5})
    ]

    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, None)
    mocker.patch('iot_datastore.helper.helper.getUtcnowDatetime').return_value = mockedNowDatetime

    IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)

    assert storeMeasurementMock.call_count == 2, 'storeMeasurement should be called for each channel in LPP !'

    for k, called in enumerate(storeMeasurementMock.call_args_list):
        assert called.args[0] == mockedCalls[k].args[0], 'storeMeasurement called with bad deviceName !'
        assert called.args[1] == mockedCalls[k].args[1], 'storeMeasurement called with bad tags !'
        assert called.args[2] == mockedCalls[k].args[2], 'storeMeasurement called with bad time !'
        assert called.args[3] == mockedCalls[k].args[3], 'storeMeasurement called with bad fields !'
    
def testStoreGpsLppByDeviceUidWithBinaryLpp(mocker):
    deviceUid = DEVICE_2.uid
    lpp = GPS_BINARY_LPP_1
    ingestComponent = "ingest1"
    ingestMethod = 'method1'

    def findDeviceById(id):
        return copy.deepcopy(DEVICE_2)
    def storeMeasurement(name, tags, time, fields):
        assert name == IotService.buildMeasurementName(DEVICE_2.uid, 1, 'gps_location'), 'Measurement name should be the device UID !'
        assert tags == iot_tags.buildLppTags(ingestComponent, ingestMethod, deviceUid, DEVICE_2.name, DEVICE_2.location, 1, None, 136, 'gps_location', None, 'lpp', 'last', None, None, {}), 'Tags are Erroneous !'
        assert fields == {'latitude': 42.3519, 'longitude': -87.9094, 'altitude': 10}, 'Fields are Erroneous !'
    (findDeviceByIdMock, storeMeasurementMock) = initStoreLppByDeviceUidMock(mocker, findDeviceById, storeMeasurement)
    IotService.storeLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod)
    storeMeasurementMock.assert_called_once()

