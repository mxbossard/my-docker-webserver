import re
from typing import Mapping

# Managed by the API
PRODUCER_TAG_KEY = "_producer"
ANONYOUS_PRODUCER_TAG_VALUE = "_anonymous"

SOURCE_TAG_KEY = "_source"
SOURCE_TAG_VALUE = "iot_datastore"

DEVICE_UID_TAG_KEY = "_device"

# Device channel number
CHANNEL_TAG_KEY = "_channel"

INGEST_COMPONENT_TAG_KEY = "_ingest_component"
INGEST_METHOD_TAG_KEY = "_ingest_method"
INGEST_FORMAT_TAG_KEY = "_ingest_format"

# Physical quantity
DATA_QUANTITY_TAG_KEY = "_quantity"
DATA_FIELD_TAG_KEY = "_field"
DATA_AGGREGATION_METHOD_TAG_KEY = "_aggregation_method"

LPP_TYPE_TAG_KEY = "_lpp_type"

# User defined tags
DEVICE_NAME_TAG_KEY = "device_name"
DEVICE_LOCATION_TAG_KEY = "device_location"
DEVICE_TYPE_TAG_KEY = "device_type"
DEVICE_VERSION_TAG_KEY = "device_version"

CHANNEL_NAME_TAG_KEY = "channel_name"

DATA_LABEL_TAG_KEY = "name"
DATA_UNITS_TAG_KEY = "units"



TAG_KEY_PATTERN = re.compile('^_?[a-zA-Z0-9]+([_-][a-zA-Z0-9]+)*$')
TAG_VALUE_PATTERN = re.compile('^[-_a-zA-Z0-9%°/. µ²³]+$')

def validateTagsFormat(tags:Mapping[str, str]):
    for (key, value) in tags.items():
        validateTagFormat(key, value)

def validateTagFormat(key, value):
    if not key or not isinstance(key, str):
        raise TypeError('Bad type for tag key: [%s]' % str(key))
    if not value or not isinstance(value, str):
        raise TypeError('Bad type for tag value: [%s] with key: [%s]' % (str(value), key))

    if not TAG_KEY_PATTERN.match(key):
        raise ValueError('Tag key: [%s] is badly formated !' % key)
    if not TAG_VALUE_PATTERN.match(value):
        raise ValueError('Tag value: [%s] for key: [%s] is badly formated !' % (value, key))


def buildLppTags(ingestComponent, ingestMethod, deviceUid, deviceName, deviceLocation, channelId, channelName, 
        lppType, quantityCode, units, ingestFormat, aggregMethod, label, field, extraTags):
    extraTags = { 
        LPP_TYPE_TAG_KEY: str(lppType), 
        **extraTags 
    }
    return buildDataPayloadTags(ingestComponent, ingestMethod, deviceUid, deviceName, deviceLocation, channelId, channelName, 
        quantityCode, units, ingestFormat, aggregMethod, label, field, extraTags)

def buildDataPayloadTags(ingestComponent, ingestMethod, deviceUid, deviceName, deviceLocation, channelId, channelName, 
        quantityCode, units, ingestFormat, aggregMethod, label, field, extraTags):
    tags = {
        SOURCE_TAG_KEY: SOURCE_TAG_VALUE,
        PRODUCER_TAG_KEY: ANONYOUS_PRODUCER_TAG_VALUE,
        DEVICE_UID_TAG_KEY: str(deviceUid),
        CHANNEL_TAG_KEY: str(channelId), 
        DATA_QUANTITY_TAG_KEY: quantityCode, 
        INGEST_COMPONENT_TAG_KEY: ingestComponent,
        INGEST_METHOD_TAG_KEY: ingestMethod,  
        INGEST_FORMAT_TAG_KEY: ingestFormat, 
        DATA_AGGREGATION_METHOD_TAG_KEY: aggregMethod,
    }
    if deviceName:
        tags[DEVICE_NAME_TAG_KEY] = deviceName
    if channelName:
        tags[CHANNEL_NAME_TAG_KEY] = channelName

    if units:
        tags[DATA_UNITS_TAG_KEY] = units
    if label:
        tags[DATA_LABEL_TAG_KEY] = label
    if field:
        tags[DATA_FIELD_TAG_KEY] = field

    if deviceLocation:
        tags[DEVICE_LOCATION_TAG_KEY] = deviceLocation

    if extraTags:
        tags = {**extraTags, **tags}

    return tags
