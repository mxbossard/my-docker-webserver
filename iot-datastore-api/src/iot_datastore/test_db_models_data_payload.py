from datetime import datetime, timedelta
from iot_datastore.quantities_config import getFieldNames
import pytest
import iot_datastore.db_models as DbModels
from iot_datastore.exceptions import NotSupportedUid


def testBuildDataQuantityWithUniqValue():
    quantity = 'foo'
    values = 42.3

    dq = DbModels.DataQuantity(quantity, values)
    assert quantity == dq.quantity
    assert {"value": 42.3} == dq.getFields()

def testBuildDataQuantityWithArrayValues():
    quantity = 'foo'
    values = [42.3, 17, 0, 2]

    dq = DbModels.DataQuantity(quantity, values)
    assert quantity == dq.quantity
    assert {"value0": 42.3, "value1": 17, "value2": 0, "value3": 2} == dq.getFields()

def testBuildDataQuantityWithMapValues():
    quantity = 'foo_bar'
    values = {
        'val1': 42.3,
        'val2': 17
    }

    dq = DbModels.DataQuantity(quantity, values)
    assert quantity == dq.quantity
    assert {"val1": 42.3, "val2": 17} == dq.getFields()

def testBuildGpsDataQuantityWithArrayFields():
    quantity = 'gps_location'
    values = [42.3, 17, 123]

    dq = DbModels.DataQuantity(quantity, values)
    assert quantity == dq.quantity
    assert {"latitude": 42.3, "longitude": 17, "altitude": 123} == dq.getFields()


def testBuildDataQuantityWithoutQuantity():
    with pytest.raises(ValueError):
        DbModels.DataQuantity(None, [42.3, 17])

def testBuildDataQuantityWithoutBadlyFormatedQuantity():
    values = [42.3, 17]
    with pytest.raises(ValueError):
        DbModels.DataQuantity('_', values)

    with pytest.raises(ValueError):
        DbModels.DataQuantity('_a', values)

    with pytest.raises(ValueError):
        DbModels.DataQuantity('a_', values)

    with pytest.raises(ValueError):
        DbModels.DataQuantity('a-b', values)

    with pytest.raises(ValueError):
        DbModels.DataQuantity('a~c', values)

    with pytest.raises(ValueError):
        DbModels.DataQuantity('_', values)


def testBuildDataQuantityWithoutValues():
    with pytest.raises(TypeError):
        DbModels.DataQuantity('foo', None)

def testBuildDataQuantityWithEmptyArrayValue():
    with pytest.raises(ValueError):
        DbModels.DataQuantity('foo', [])

def testBuildDataQuantityWithBadQuantityType():
    with pytest.raises(TypeError):
        DbModels.DataQuantity(42, 'test')

def testBuildDataQuantityWithBadValueType():
    with pytest.raises(TypeError):
        DbModels.DataQuantity('foo', 'test')

def testBuildDataQuantityWithBadValuesArrayType():
    with pytest.raises(TypeError):
        DbModels.DataQuantity('foo', [3, 'test', 42])

def testBuildDataQuantityWithBadValuesMapType():
    with pytest.raises(TypeError):
        DbModels.DataQuantity('foo', {'bar': 42, 'test': 'foo'})

def testBuildDataQuantityWithBadValuesNameMapType():
    with pytest.raises(TypeError):
        DbModels.DataQuantity('foo', {'bar': 42, 53: 17})

def testBuildDataChannel():
    v1 = {'dq1v1': 11, 'dq1v2': 12, 'dq1v3': 13}
    v2 = {'dq2v1': 21, 'dq2v2': 22, 'dq2v3': 23}
    v3 = {'dq3v1': 31, 'dq3v2': 32, 'dq3v3': 33}
    dq1 = DbModels.DataQuantity('q1', v1)
    dq2 = DbModels.DataQuantity('q2', v2)
    dq3 = DbModels.DataQuantity('q3', v3)

    channelId = 42
    dc = DbModels.DataChannel(channelId, [dq1, dq2, dq3])

    assert channelId == dc.channelId
    assert v1 == dc.getFields('q1')
    assert v2 == dc.getFields('q2')
    assert v3 == dc.getFields('q3')
    assert ['q1', 'q2', 'q3'] == dc.getQuantities()
    assert dq1 == dc.getQuantity('q1')
    assert dq2 == dc.getQuantity('q2')
    assert dq3 == dc.getQuantity('q3')
    assert None == dc.getTime()

    with pytest.raises(KeyError):
        dc.getFields('None')

    with pytest.raises(KeyError):
        dc.getFields(None)

    with pytest.raises(KeyError):
        dc.getFields(12)

def testBuildTimedDataChannel():
    v1 = {'dq1v1': 11, 'dq1v2': 12, 'dq1v3': 13}
    v2 = {'dq2v1': 21, 'dq2v2': 22, 'dq2v3': 23}
    v3 = {'dq3v1': 31, 'dq3v2': 32, 'dq3v3': 33}
    dq1 = DbModels.DataQuantity('q1', v1)
    dq2 = DbModels.DataQuantity('q2', v2)
    dq3 = DbModels.DataQuantity('q3', v3)

    channelId = 42
    now = datetime.now()
    dc = DbModels.DataChannel(channelId, [dq1, dq2, dq3], now)

    assert now == dc.getTime()

def testBuildDataChannelWithDuplicateQuantity():
    v1 = {'dq1v1': 11, 'dq1v2': 12, 'dq1v3': 13}
    v2 = {'dq2v1': 21, 'dq2v2': 22, 'dq2v3': 23}
    v3 = {'dq3v1': 31, 'dq3v2': 32, 'dq3v3': 33}
    dq1 = DbModels.DataQuantity('q1', v1)
    dq2 = DbModels.DataQuantity('q2', v2)
    dq3 = DbModels.DataQuantity('q2', v3)
    dq4 = DbModels.DataQuantity('q4', v3)

    channelId = 42
    with pytest.raises(ValueError):
        dc = DbModels.DataChannel(channelId, [dq1, dq2, dq3, dq4])

    # assert channelId == dc.channelId
    # assert v1 == dc.getFields('q1')
    # assert ['d1', 'q4'] == dc.getQuantities()
    # assert dq1 == dc.getQuantity('q1')
    # assert dq4 == dc.getQuantity('q4')
    # assert None == dc.getTime()

    # with pytest.raises(ValueError):
    #     dc.getFields('q2')

    # with pytest.raises(ValueError):
    #     dc.getFields('q3')

def testBuildDataChannelWithoutChannelId():
    dq1 = DbModels.DataQuantity('q1', [11, 12, 13])
    with pytest.raises(TypeError):
        DbModels.DataChannel(None, [dq1])

def testBuildDataChannelWithBadTypeChannelId():
    dq1 = DbModels.DataQuantity('q1', [11, 12, 13])
    with pytest.raises(TypeError):
        DbModels.DataChannel('1', [dq1])

def testBuildDataChannelWithEmptyQuantities():
    with pytest.raises(ValueError):
        DbModels.DataChannel(1, [])

def testBuildDataChannelWithNoneQuantities():
    dq1 = DbModels.DataQuantity('q1', [11, 12, 13])
    with pytest.raises(TypeError):
        DbModels.DataChannel(1, 'test')

def testBuildDataChannelWithBadTypeQuantities():
    dq1 = DbModels.DataQuantity('q1', [11, 12, 13])
    with pytest.raises(TypeError):
        DbModels.DataChannel(1, 'test')

def testBuildDataChannelWithNoneQuantity():
    dq1 = DbModels.DataQuantity('q1', [11, 12, 13])
    dc1 = DbModels.DataChannel(1, [dq1, None])
    assert {'value0': 11, 'value1': 12, 'value2': 13} == dc1.getQuantity('q1').getFields()

def testBuildDataChannelWithBadTypeQuantity():
    dq1 = DbModels.DataQuantity('q1', [11, 12, 13])
    with pytest.raises(TypeError):
        DbModels.DataChannel(1, [dq1, 'test'])

def testBuildDataPayload():
    v1 = {'dq1v1': 11, 'dq1v2': 12, 'dq1v3': 13}
    v2 = {'dq2v1': 21, 'dq2v2': 22, 'dq2v3': 23}
    v3 = {'dq3v1': 31, 'dq3v2': 32, 'dq3v3': 33}
    dq1 = DbModels.DataQuantity('q1', v1)
    dq2 = DbModels.DataQuantity('q2', v2)
    dq3 = DbModels.DataQuantity('q1', v3)

    channel1Id = 3
    channel2Id = 200
    dc1 = DbModels.DataChannel(channel1Id, [dq1, dq2])
    dc2 = DbModels.DataChannel(channel2Id, [dq3])

    uid = 'foo'
    gp = DbModels.DataPayload([dc1, dc2])
    assert v1 == gp.getFields(channel1Id, 'q1')
    assert v2 == gp.getFields(channel1Id, 'q2')
    assert v3 == gp.getFields(channel2Id, 'q1')

    assert None == gp.getTime(channel1Id)
    assert None == gp.getTime(channel2Id)
    assert [channel1Id, channel2Id] == gp.getChannels()
    assert dc1 == gp.getChannel(channel1Id)
    assert dc2 == gp.getChannel(channel2Id)

    with pytest.raises(KeyError):
        gp.getChannel(None)

    with pytest.raises(KeyError):
        gp.getChannel('None')

    with pytest.raises(KeyError):
        gp.getChannel(12)

    with pytest.raises(KeyError):
        gp.getFields(None, 'q1')

    with pytest.raises(KeyError):
        gp.getFields('None', 'q1')

    with pytest.raises(KeyError):
        gp.getFields(12, 'q1')

    with pytest.raises(KeyError):
        gp.getFields(channel1Id, 'None')

    with pytest.raises(KeyError):
        gp.getFields(channel1Id, 12)

    with pytest.raises(KeyError):
        gp.getFields(channel1Id, None)


def testBuildTimedDataPayload():
    v1 = {'dq1v1': 11, 'dq1v2': 12, 'dq1v3': 13}
    v2 = {'dq2v1': 21, 'dq2v2': 22, 'dq2v3': 23}
    v3 = {'dq3v1': 31, 'dq3v2': 32, 'dq3v3': 33}
    dq1 = DbModels.DataQuantity('q1', v1)
    dq2 = DbModels.DataQuantity('q2', v2)
    dq3 = DbModels.DataQuantity('q1', v3)

    channel1Id = 3
    channel2Id = 200
    channel3Id = 100
    now = datetime.now()
    yesterday = now - timedelta(days=1)
    dc1 = DbModels.DataChannel(channel1Id, [dq1, dq2])
    dc2 = DbModels.DataChannel(channel2Id, [dq2], yesterday)
    dc3 = DbModels.DataChannel(channel3Id, [dq3])

    
    gp = DbModels.DataPayload([dc1, dc2, dc3], now)

    assert now == gp.getTime(channel1Id)
    assert yesterday == gp.getTime(channel2Id)
    assert now == gp.getTime(channel3Id)

def testBuildDataPayloadWithDuplicateChannel():
    v1 = {'dq1v1': 11, 'dq1v2': 12, 'dq1v3': 13}
    v2 = {'dq2v1': 21, 'dq2v2': 22, 'dq2v3': 23}
    v3 = {'dq3v1': 31, 'dq3v2': 32, 'dq3v3': 33}
    dq1 = DbModels.DataQuantity('q1', v1)
    dq2 = DbModels.DataQuantity('q2', v2)
    dq3 = DbModels.DataQuantity('q1', v3)

    channel1Id = 3
    channel2Id = 200
    channel3Id = 100
    channel4Id = 42
    dc1 = DbModels.DataChannel(channel1Id, [dq1, dq2])
    dc2 = DbModels.DataChannel(channel2Id, [dq2])
    dc3 = DbModels.DataChannel(channel2Id, [dq3])
    dc4 = DbModels.DataChannel(channel4Id, [dq3])

    with pytest.raises(ValueError):
        gp = DbModels.DataPayload([dc1, dc2, dc3, dc4])

    # assert None == gp.getTime(channel1Id)
    # assert None == gp.getTime(channel4Id)
    # assert [channel1Id, channel4Id] == gp.getChannels()
    # assert dc1 == gp.getChannel(channel1Id)
    # assert dc4 == gp.getChannel(channel4Id)
    # assert v1 == gp.getFields(channel1Id, 'q1')
    # assert v3 == gp.getFields(channel4Id, 'q1')

    # with pytest.raises(KeyError):
    #     gp.getChannel(channel2Id)

    # with pytest.raises(KeyError):
    #     gp.getFields(channel2Id, 'q2')

def testBuildDataPayloadWithEmptyChannels():
    with pytest.raises(ValueError):
        DbModels.DataPayload([])

def testBuildDataPayloadWithNoneChannels():
    with pytest.raises(TypeError):
        DbModels.DataPayload(None)

def testBuildDataPayloadWithBadTypeChannels():
    with pytest.raises(TypeError):
        DbModels.DataPayload('channels')

def testBuildDataPayloadWithNoneChannel():
    dq1 = DbModels.DataQuantity('q1', [11, 12, 13])
    dc1 = DbModels.DataChannel(1, [dq1])
    gp = DbModels.DataPayload([dc1, None])
    assert [1] == gp.getChannels()
    assert dc1 == gp.getChannel(1)

def testBuildDataPayloadWithBadTypeChannel():
    dq1 = DbModels.DataQuantity('q1', [11, 12, 13])
    dc1 = DbModels.DataChannel(1, [dq1])
    with pytest.raises(TypeError):
        DbModels.DataPayload([dc1, 'channel'])
