import logging
import re
import os
from urllib import parse
from dataclasses import dataclass, field
from datetime import datetime
import collections
from typing import Mapping, Sequence, Union
from flask_sqlalchemy import SQLAlchemy
from flask import json
from iot_datastore.helper.logging import addLogger
from iot_datastore.helper.json import CustomJSONEncoder
from iot_datastore.exceptions import NotSupportedUid
import iot_datastore.quantities_config as qconf
from iot_datastore import iot_tags

__log = logging.getLogger(__name__)

IOT_STORAGE_BIND = 'iot_datastore'
UID_PATTERN = re.compile('^[a-z0-9]+(_[a-z0-9]+)*$')
QUANTITY_CODE_PATTERN = UID_PATTERN

MAX_SECONDS_BACK_IN_TIME = os.environ.get('MAX_SECONDS_BACK_IN_TIME', 300)
MAX_SECONDS_FORWARD_IN_FUTUR = os.environ.get('MAX_SECONDS_BACK_IN_TIME', 120)


DEVICE_NS = 'device'
CHANNELS_NS = 'channels'
DEFAULT_QUANTITIES_NS = 'defaultQuantities'
QUANTITIES_BY_CHANNEL_NS = 'byChannelQuantities'


FIELD_NAME_PATTERN = re.compile('^[a-zA-Z0-9]+([_][a-zA-Z0-9]+)*$')
QUANTITY_PATTERN = re.compile('^[a-zA-Z0-9]+([_][a-zA-Z0-9]+)*$')

def validateChannel(channel):
    if not isinstance(channel, int):
        raise TypeError('Supplied channel: [%s] not of type int!' % str(channel))
    if channel < 0:
        raise ValueError('Supplied channel: [%d] is negative !' % channel)
    if channel > 999:
        raise ValueError('Supplied channel: [%d] is greater than 999 !' % channel)

def validateQuantity(quantity):
    if not quantity or not isinstance(quantity, str):
        raise TypeError('Supplied quantity: [%s] not of type string !' % str(quantity))
    if not QUANTITY_PATTERN.match(quantity):
        raise ValueError('Quantity: [%s] is badly formated !' % quantity)

def validateFieldName(name):
    if not name or not isinstance(name, str):
        raise TypeError('Supplied field name: [%s] not of type string !' % str(name))
    if not FIELD_NAME_PATTERN.match(name):
        raise ValueError('Field name: [%s] is badly formated !' % name)

def validateFieldValue(value):
    if not (isinstance(value, float) or isinstance(value, int)):
        raise TypeError('Supplied value: [%s] not of type float nor int!' % str(value))

db = SQLAlchemy()

@dataclass
class DataQuantity():
    quantity: str
    __fields: Mapping[str, float]

    def __init__(self, quantity:str, values:Union[float, Mapping[str, float], Sequence[float]]):
        if not quantity: raise ValueError('Quantity field is empty !')
        if not QUANTITY_CODE_PATTERN.match(quantity):
            raise ValueError('Supplied quantity field: [%s] is badly formated !' % quantity)
        
        filteredFields = {}
        if isinstance(values, float) or isinstance(values, int) :
            # Uniq value
            fieldNames = qconf.getFieldNames(quantity, [values])
            filteredFields = {fieldNames[0]: values}
        elif isinstance(values, Sequence):
            # Array of value
            fieldNames = qconf.getFieldNames(quantity, values)
            for (k, f) in enumerate(fieldNames):
                if f not in filteredFields:
                    filteredFields[f] = values[k]
                else:
                    raise ValueError('Duplicate field name found for quantity: [%s] !' % (quantity))
        elif isinstance(values, Mapping):
            for (fieldName, value) in values.items():
                validateFieldName(fieldName)

                if fieldName not in filteredFields:
                    filteredFields[fieldName] = value
                else:
                    raise ValueError('Duplicate field name found for quantity: [%s] !' % (quantity))
        else:
            raise TypeError('Bad type for supplied values field: [%s] !' % str(values))

        self.quantity = quantity
        self.__fields = filteredFields

        if len(self.__fields.items()) == 0:
            raise ValueError('Values field is empty !')

        for i in self.__fields.values():
            validateFieldValue(i)

    def __eq__(self, other):
        return self.quantity == other.quantity and self.__fields == other.__fields

    def getFields(self) -> Mapping[str, float]:
        return self.__fields

@dataclass
class DataChannel():
    channelId: int
    __time: datetime
    __quantities: Mapping[str, DataQuantity]

    def __init__(self, channelId:int, quantities:Sequence[DataQuantity], time:datetime=None):
        if not isinstance(channelId, int) :
            raise TypeError('Supplied channelId: [%s] is not an int !' % str(channelId))

        if time and not isinstance(time, datetime):
            raise TypeError('Supplied time: [%s] is not a datetime !' % str(time))

        if not isinstance(quantities, Sequence):
            raise TypeError('Supplied quantities: [%s] is not a Sequence !' % str(quantities))

        filteredQuantities = {}
        for q in quantities:
            if q:
                if not isinstance(q, DataQuantity):
                    raise TypeError('Bad type for supplied quantity: [%s] ' % str(q))
                
                quantityCode = q.quantity
                if quantityCode not in filteredQuantities:
                    filteredQuantities[quantityCode] = q
                else:
                    raise ValueError('Duplicate quantity: [%s] found in channel: [%s]!' % (quantityCode, str(channelId)))
        
        self.channelId = channelId
        self.__time = time
        self.__quantities = filteredQuantities

        if len(self.__quantities) == 0:
            raise ValueError('No valid quantity supplied !')

    def __eq__(self, other):
        return self.channelId == other.channelId and self.__time == other.__time and self.__quantities == other.__quantities
        
    def getQuantities(self) -> Sequence[DataQuantity]:
        return list(self.__quantities.keys())

    def getQuantity(self, quantity) -> DataQuantity:
        return self.__quantities[quantity]

    def getFields(self, quantityCode: str) -> Mapping[str, float]:
        dq = self.getQuantity(quantityCode)
        return dq.getFields()

    def getTime(self) -> datetime:
        return self.__time

@dataclass
class DataPayload():
    __time: datetime
    __channels: Mapping[int, DataChannel]

    def __init__(self, channels: Sequence[DataChannel], time:datetime=None):
        if time and not isinstance(time, datetime):
            raise TypeError('Supplied time: [%s] is not a datetime !' % str(time))

        if not isinstance(channels, Sequence):
            raise TypeError('Supplied channels: [%s] is not a Sequence !' % str(channels))

        # Filter quantities
        filteredChannels = {}
        for c in channels:
            if c:
                if not isinstance(c, DataChannel):
                    raise TypeError('Bad type for supplied channel: [%s] ' % str(c))

                channelId = c.channelId
                if channelId not in filteredChannels:
                    filteredChannels[channelId] = c
                else:
                    raise ValueError('Duplicate channel: [%s] found !' % (str(c.channelId)))
                    # Remove ambiguous data
                    #del(filteredChannels[channelId])

        self.__time = time
        self.__channels = filteredChannels

        if len(self.__channels) == 0:
            raise ValueError('No valid channel supplied !')

    def __eq__(self, other):
        return self.__time == other.__time and self.__channels == other.__channels

    def getChannels(self) -> Sequence[int]:
        return list(self.__channels.keys())

    def getChannel(self, channel:int) -> DataChannel: 
        return self.__channels[channel]

    def getTime(self, channel: int) -> datetime:
        dc = self.getChannel(channel)
        if dc.getTime():
            return dc.getTime()
        return self.__time

    def getFields(self, channel: int, quantityCode: str) -> Mapping[str, float]:
        dc = self.getChannel(channel)
        return dc.getFields(quantityCode)


@dataclass
@addLogger
class Device(db.Model):
    uid: str
    name: str = None
    location: str = None
    # Do not declare __name nor __location as a field hide it in http response
    #__name: str
    #__location: str
    maxSecondsBackInTime: int
    maxSecondsForwardInFutur: int

    __bind_key__ = IOT_STORAGE_BIND

    uid = db.Column(db.String(32), primary_key=True)
    __name = db.Column(db.String(100))
    __location = db.Column(db.String(50))
    maxSecondsBackInTime = db.Column(db.Integer())
    maxSecondsForwardInFutur = db.Column(db.Integer())
    __tags_json = db.Column(db.String(1024))

    def __init__(self, uid:str, name:str=None, location:str=None, maxSecondsBackInTime:int=None, maxSecondsForwardInFutur:int=None):
        if not UID_PATTERN.match(uid):
            # supplied UID is not urlsafe
            raise NotSupportedUid(uid)

        if not maxSecondsBackInTime:
            maxSecondsBackInTime = MAX_SECONDS_BACK_IN_TIME

        if not maxSecondsForwardInFutur:
            maxSecondsForwardInFutur = MAX_SECONDS_FORWARD_IN_FUTUR

        self.__tags_json = "{}"
        self.uid = uid
        self.setName(name)
        self.setLocation(location)
        self.maxSecondsBackInTime = maxSecondsBackInTime
        self.maxSecondsForwardInFutur = maxSecondsForwardInFutur

    def __repr__(self):
        return '<Device #%s %s at %s>' % (self.uid, self.name, self.location)

    def setName(self, value):
        self.__name = value
        if value:
            self.addDeviceTag(iot_tags.DEVICE_NAME_TAG_KEY, value)
        else:
            self.removeDeviceTag(iot_tags.DEVICE_NAME_TAG_KEY)

    @property
    def name(self): 
        return self.__name

    @name.setter
    def name(self, value): 
        self.setName(value)

    def setLocation(self, value):
        self.__location = value
        if value:
            self.addDeviceTag(iot_tags.DEVICE_LOCATION_TAG_KEY, value)
        else:
            self.removeDeviceTag(iot_tags.DEVICE_LOCATION_TAG_KEY)

    @property
    def location(self): 
        return self.__location

    @location.setter
    def location(self, value): 
        self.setLocation(value)

    def __loadTags(self):
        tags = json.loads(self.__tags_json)
        if tags:
            return tags
        return {}

    def __dumpTags(self, tags):
        self.__tags_json = json.dumps(tags)

    def __getTags(self, namespaceHierarchy: list):
        tags = self.__loadTags()
        if not tags: return {}
        for ns in namespaceHierarchy:
            if ns in tags:
                tags = tags.get(ns)
            else:
                tags = {}
                break
        return tags

    def __storeTags(self, namespaceHierarchy: list, newTags):
        root = self.__loadTags()
        if not root: root = {}
        tags = root
        for ns in namespaceHierarchy:
            if ns in tags:
                tags = tags.get(ns)
            else:
                tags[ns] = {}
                tags = tags[ns]
        tags.clear()
        tags.update(newTags)
        self.__dumpTags(root)
        return tags


    def getDeviceName(self):
        nameTag = self.getDeviceTagValue(iot_tags.DEVICE_NAME_TAG_KEY)
        if (nameTag):
            return nameTag
        else:
            return self.uid
            
    def getDeviceLocation(self):
        locationTag = self.getDeviceTagValue(iot_tags.DEVICE_LOCATION_TAG_KEY)
        return locationTag

    def getChannelName(self, channelId:int):
        name = self.getChannelTagValue(channelId, iot_tags.CHANNEL_NAME_TAG_KEY)
        return name

    def getLabel(self, channelId:int, quantityCode:str):
        label = self.getQuantityTagValue(channelId, quantityCode, iot_tags.DATA_LABEL_TAG_KEY)
        return label

    def getUnits(self, channelId:int, quantityCode:str):
        units = self.getQuantityTagValue(channelId, quantityCode, iot_tags.DATA_UNITS_TAG_KEY)
        if not units:
            units = qconf.getUnitsTag(quantityCode)
        return units

    def getAggregationMethod(self, channelId:int, quantityCode:str):
        aggMethod = self.getQuantityTagValue(channelId, quantityCode, iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY)
        if not aggMethod:
            aggMethod = qconf.getDefaultAggregationMethod(quantityCode)
        return aggMethod

    def getAllTags(self):
        allTags = self.__loadTags()
        return allTags

    def clearTags(self):
        self.__dumpTags({})

    def addDeviceTag(self, key: str, value:str):
        iot_tags.validateTagFormat(key, value)
        ns = [DEVICE_NS]
        tags = self.__getTags(ns)
        tags[key] = value
        self.__storeTags(ns, tags)

    def removeDeviceTag(self, key: str):
        ns = [DEVICE_NS]
        tags = self.__getTags(ns)
        tags.pop(key, None)
        self.__storeTags(ns, tags)

    def getDeviceTags(self):
        ns = [DEVICE_NS]
        return self.__getTags(ns)

    def getDeviceTagValue(self, key: str):
        return self.getDeviceTags().get(key, None)


    def addChannelTag(self, channel: int, key: str, value:str):
        validateChannel(channel)
        iot_tags.validateTagFormat(key, value)
        ns = [CHANNELS_NS, str(channel)]
        tags = self.__getTags(ns)
        tags[key] = value
        self.__storeTags(ns, tags)

    def removeChannelTag(self, channel: int, key: str):
        ns = [CHANNELS_NS, str(channel)]
        tags = self.__getTags(ns)
        tags.pop(key, None)
        self.__storeTags(ns, tags)

    def getChannelTags(self, channel: int):
        ns = [CHANNELS_NS, str(channel)]
        channelTags = self.__getTags(ns)
        return channelTags
  
    def getChannelTagValue(self, channel: int, key: str):
        return self.getChannelTags(channel).get(key, None)


    def addDefaultQuantityTag(self, quantity: str, key: str, value:str):
        iot_tags.validateTagFormat(key, value)
        validateQuantity(quantity)
        ns = [DEFAULT_QUANTITIES_NS, quantity]
        tags = self.__getTags(ns)
        tags[key] = value
        self.__storeTags(ns, tags)

    def removeDefaultQuantityTag(self, quantity: str, key: str):
        ns = [DEFAULT_QUANTITIES_NS, quantity]
        tags = self.__getTags(ns)
        tags.pop(key, None)
        self.__storeTags(ns, tags)

    def _getDefaultQuantityTags(self, quantity: str):
        ns = [DEFAULT_QUANTITIES_NS, quantity]
        defaultQuantityTags = self.__getTags(ns)
        return defaultQuantityTags

    def addQuantityTag(self, channel: int, quantity: str, key: str, value:str):
        iot_tags.validateTagFormat(key, value)
        validateChannel(channel)
        validateQuantity(quantity)
        ns = [QUANTITIES_BY_CHANNEL_NS, str(channel), quantity]
        tags = self.__getTags(ns)
        tags[key] = value
        self.__storeTags(ns, tags)

    def removeQuantityTag(self, channel: int, quantity: str, key: str):
        ns = [QUANTITIES_BY_CHANNEL_NS, str(channel), quantity]
        tags = self.__getTags(ns)
        tags.pop(key, None)
        self.__storeTags(ns, tags)

    def getQuantityTags(self, channel: int, quantity: int):
        ns = [QUANTITIES_BY_CHANNEL_NS, str(channel), quantity]
        quantityTags = self.__getTags(ns)
        defaultQuantityTags = self._getDefaultQuantityTags(quantity)
        return {**defaultQuantityTags, **quantityTags}

    def getQuantityTagValue(self, channel: int, quantity: int, key: str):
        return self.getQuantityTags(channel, quantity).get(key, None)


def addEntity(entity: db.Model) -> bool:
    added = False
    try:
        db.session.add(entity)
        db.session.commit()
        added = True
    except Exception as e:
        db.session.rollback()
        __log.warning('Error during DB commit !', exc_info=1)
        # raise e
        
    return added

def mergeEntity(entity: db.Model) -> bool:
    merged = False
    try:
        db.session.merge(entity)
        db.session.commit()
        merged = True
    except Exception as e:
        db.session.rollback()
        __log.warning('Error during DB commit !', exc_info=1)
        # raise e

    return merged

def commit() -> bool:
    commited = False
    try:
        db.session.commit()
        commited = True
    except Exception as e:
        db.session.rollback()
        __log.warning('Error during DB commit !', exc_info=1)
        # raise e
        
    return commited

def deleteEntity(entity: db.Model) -> bool:
    deleted = False
    try:
        db.session.delete(entity)
        db.session.commit()
        deleted = True
    except Exception as e:
        db.session.rollback()
        __log.warning('Error during DB commit !', exc_info=1)
        # raise e

    return deleted
