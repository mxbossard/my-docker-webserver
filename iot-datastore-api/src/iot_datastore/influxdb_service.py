import os
import logging
import datetime
import requests
from cayennelpp import LppFrame
from influxdb_client import InfluxDBClient, Point, WriteOptions
from influxdb_client.client.write_api import SYNCHRONOUS


__log = logging.getLogger(__name__)

influxdbUrl = os.environ.get('INFLUXDB_URL')
influxdbDbName = os.environ.get('INFLUXDB_DB_NAME')
influxdbRpName = os.environ.get('INFLUXDB_RP_NAME', None)
influxdbOrg = os.environ.get('INFLUXDB_ORG', 'testOrg')
influxdbUser = os.environ.get('INFLUXDB_USER')
influxdbPassword = os.environ.get('INFLUXDB_PASSWORD')

if influxdbRpName:
    influxdbBucket = '%s/%s' % (influxdbDbName, influxdbRpName)
else:
    influxdbBucket = '"INFLUXDB_DB_NAME-not-configured"'

def __buildInfluxDbClient():
    # Use BASIC auth token
    basicAuthToken = f'{influxdbUser}:{influxdbPassword}'
    influxClient = InfluxDBClient(url=influxdbUrl, token=basicAuthToken, org=influxdbOrg, timeout=3000, enable_gzip=True)

    return influxClient

__influxClient = __buildInfluxDbClient()
__write_api = __influxClient.write_api(write_options=SYNCHRONOUS)
__query_api = __influxClient.query_api()

# Deprecated
#def createInluxDbBucket(name):
#    # Create DB if it does not exists
#    print(f'Attempt creation of InfluxDB bucket {name} ...')
#    createDbQuery = f'CREATE DATABASE "{name}";'
#    res = requests.post(f'{INFLUX_URL}/query', data={'q': createDbQuery})
#    #print(res.status_code, res.reason)
#    res.raise_for_status()

# measurement: "nom", tags: map, time: "2009-11-10T23:00:00Z", fields: map
def storeMeasurement(name, tags, time, fields):
    formattedTime = time.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
    #__log.debug("formattedTime: %s", formattedTime)

    p = Point(name).time(formattedTime)
    #log.info("Will plublish %s" % (str(p)))

    for key, value in tags.items():
        p.tag(key, value)

    for key, value in fields.items():
        p.field(key, value)

    __write_api.write(bucket=influxdbBucket, record=p)
    __log.info("Published a data point in influxdb for measurment: [%s] with time: [%s].", name, time)

def reTagMeasurement(measurementName, tagKey, tagOldValue, tagNewValue, fromTime):
    raise NotImplementedError
    # For now we need to duplicate data with new tag then drop previous data
    formattedFromTime = fromTime.strftime('%Y-%m-%dT%H:%M:%S.%fZ')

    query = f'''from(bucket:{influxdbBucket})
        |> range(start: {formattedFromTime})
        |> filter(fn: (r) =>
            r._measurement == "{measurementName}" and
            r.{tagKey} == "{tagOldValue}"
        )'''

    records = __query_api.query_stream(query=query)
    for record in records:
        # Alter tag value
        record[tagKey] = tagNewValue

    write_api = __influxClient.write_api(write_options=WriteOptions(batch_size=50_000, flush_interval=10_000))
    write_api.write(bucket=influxdbBucket, record=records)
    

