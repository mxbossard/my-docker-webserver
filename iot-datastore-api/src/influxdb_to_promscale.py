import os
import sys
import logging
import json
import copy
from datetime import datetime
from threading import Barrier
import requests
from requests.auth import HTTPBasicAuth
from requests.models import Response
import urllib.parse
from dateutil import parser
from flask import Flask
from iot_datastore import quantities_config as qConf
from iot_datastore import iot_tags
from iot_datastore import iot_service
import iot_datastore

# pip install python-dateutil

__log = logging.getLogger(__name__)

influxdbUrl = os.environ.get('INFLUXDB_URL')
influxdbDbName = os.environ.get('INFLUXDB_DB_NAME')
influxdbUser = os.environ.get('INFLUXDB_USER')
influxdbPassword = os.environ.get('INFLUXDB_PASSWORD')

_queryUrl = '%s/query?db=%s' % (influxdbUrl, influxdbDbName)
_influxdbAuth = HTTPBasicAuth(influxdbUser, influxdbPassword)

promscaleUrl = os.environ.get('PROMSCALE_URL', 'PROMSCALE_URL env var not defined !')

__write_url = promscaleUrl + '/write'

sys.stderr.write('Influxdb query URL: [%s]\n' % _queryUrl)
sys.stderr.write('Promscale write URL: [%s]\n' % __write_url)

def queryInfluxdb(influxQuery) -> Response:
    sys.stderr.write('Querying influxdb with query: [%s]\n' % influxQuery)
    quotedQuery = urllib.parse.quote(influxQuery)
    r = requests.get(_queryUrl + '&q=%s' % quotedQuery, auth=_influxdbAuth)
    return r

def _writeUniqPayloadAsJson(payload: dict): 
    r = requests.post(__write_url, json=payload)
    r.raise_for_status()

def _writePayloadsAsJson(payloads: list): 
    jsonPayloads = []
    for payload in payloads:
        p = json.dumps(payload)
        jsonPayloads.append(p)

    r = requests.post(__write_url, data=''.join(jsonPayloads), headers={"content-type":"application/json"})
    r.raise_for_status()

def _buildPromscalePayload(name: str, tags: dict, time:datetime, fieldName: str, value: float):
    formattedTime = int(time.timestamp() * 1000)
    sample = [formattedTime, value]
    # Build labels dict from tags dict
    labels = copy.deepcopy(tags)
    labels['__name__'] = name
    labels['_field'] = fieldName
    payload = {
            "labels": labels,
            "samples": [sample]
    }
    return payload

# measurement: "name", tags: map, time: datetime, fields: map
def storeMeasurement(name: str, tags: dict, time: datetime, fields: dict):
    for (fieldName, value) in fields.items():
        payload = _buildPromscalePayload(name, tags, time, fieldName, value)
        _writeUniqPayloadAsJson(payload)

    __log.info("Published data in promscale for measurment: [%s] with time: [%s].", name, time)

# measurement: "name", tags: map, time: datetime, fields: map
def storeMeasurements(measurements: list):
    payloads = []
    for measurement in measurements:
        name = measurement[0]
        tags = measurement[1]
        time = measurement[2]
        fields = measurement[3]
        for (fieldName, value) in fields.items():
            p = _buildPromscalePayload(name, tags, time, fieldName, value)
            payloads.append(p)

    _writePayloadsAsJson(payloads)
    __log.info("Published data in promscale.")


def _getSckQuantity(channel: int):
    if int(channel) == 10: return 'percentage'
    elif int(channel) == 14: return 'illuminance'
    elif int(channel) == 53: return 'sound_level'
    elif int(channel) == 55: return 'temperature'
    elif int(channel) == 56: return 'relative_humidity'
    elif int(channel) == 58: return 'atmospheric_pressure'
    elif int(channel) == 87: return 'concentration'
    elif int(channel) == 88: return 'concentration'
    elif int(channel) == 89: return 'concentration'
    elif int(channel) == 112: return 'ppm'
    elif int(channel) == 113: return 'ppb'
    else:
        raise Exception(f'Channel [{channel}] not known !')


__deviceCache = {}
def _getDeviceFromDb(uid):
    if uid in __deviceCache:
        return __deviceCache[uid]
    else:
        device:iot_service.DbModels.Device = iot_service.findDeviceById(uid)
        __deviceCache[uid] = device
        return device

def migrateDataChunk(datas: list):
    measurements = []
    for data in datas:
        formattedTime = data[0]
        uid = data[1]
        name = data[2]
        channel = data[3]
        channelName = data[4]
        pressureValue = data[5]
        analogValue = data[6]
        illuValue = data[7]
        tempValue = data[8]
        unixTimestampValue = data[9]
        
        if channel == "0": continue # Do not migrate channel 0

        ingestComponent = 'web_api'
        ingestMethod ='binary_post'
        ingestFormat = 'lpp'

        device = _getDeviceFromDb(uid)
        quantity = _getSckQuantity(channel)
        tags = iot_service.buildMeasurementTags(device, int(channel), quantity, ingestComponent, ingestMethod, ingestFormat, None)

        time = parser.parse(formattedTime)

        if channel == "10":
            # rescale battery level data
            analogValue = analogValue * 100

        if channel == "87" or channel == "88" or channel == "89":
            # clean signed values
            if analogValue < 0:
                analogValue = analogValue + 655.35

        if channel == "112" or channel == "113":
            # rescale eCO2 and VOC data
            analogValue = analogValue * 1000

        if analogValue is not None: fields = {"value": analogValue}
        elif illuValue is not None: fields = {"value": illuValue}
        elif tempValue is not None: fields = {"value": tempValue}
        elif pressureValue is not None: fields = {"value": pressureValue}
        elif unixTimestampValue is not None:  continue
        else: raise Exception('No value found in result: [%s] !' % str(data))

        measurement = ['device_%s' % uid, tags, time, fields]
        measurements.append(measurement)

    storeMeasurements(measurements)


def listSckMeasurements():
    query = "SHOW MEASUREMENTS WHERE _qualifier = 'lpp';"
    r = queryInfluxdb(query)
    payload = json.loads(r.content)
    results = payload.get('results')
    series = results[0].get('series')
    values = series[0].get('values')
    return [val[0] for val in values]

def listDataChunkFromMeasurements(rp, measurement, startTime, beforeTime, batchSize):
    query = f'SELECT _deviceUid, _deviceName, _channel, _channelName, pressure, analog, illuminance, temperature, unix_timestamp \
        from "{influxdbDbName}"."{rp}"."{measurement}" \
        where time >= \'{startTime}\' and time < \'{beforeTime}\' \
        limit {batchSize};'
    r = queryInfluxdb(query)
    payload = json.loads(r.content)
    results = payload.get('results')
    series = results[0].get('series', [{}])
    values = series[0].get('values', {})
    return values

def migrateAllMeasurementData(rp, measurement, beforeTime, batchSize):
    startTime = '1970-01-01T00:00:00Z'
    sys.stdout.write(f'Migrating data of measurement: [{measurement}] from [{startTime}] ...\n')
    foundData = None
    counter = 0
    while foundData is None or len(foundData) == batchSize:
        foundData = listDataChunkFromMeasurements(rp, measurement, startTime, beforeTime, batchSize)
        count = len(foundData)
        migrateDataChunk(foundData)
        sys.stdout.write(f'Migrated [{count}] data.\n')
        counter += count
        lastTimeFound = foundData[len(foundData) - 1][0]
        startTime = lastTimeFound
        

    sys.stdout.write(f'Finished migrated [{counter}] data for measurement: [{measurement}] in RP: [{rp}].\n')
    return counter

def countAllMeasurementData(rp, measurement, beforeTime):
    query = f'SELECT count(*) FROM "{influxdbDbName}"."{rp}"."{measurement}" WHERE time < \'{beforeTime}\';'
    r = queryInfluxdb(query)
    payload = json.loads(r.content)
    #sys.stdout.write('payload: %s\n' % str(payload))
    results = payload.get('results')
    series = results[0].get('series', [{}])
    values = series[0].get('values', [[None]])
    counts = values[0]

    counter = 0
    for c in counts[1:]: 
        if c: counter += c
    return counter

def getFirstEventFormattedTime(rp, measurement):
    query = f'SELECT * FROM "{influxdbDbName}"."{rp}"."{measurement}" LIMIT 1;'
    r = queryInfluxdb(query)
    payload = json.loads(r.content)
    results = payload.get('results')
    series = results[0].get('series', [{}])
    values = series[0].get('values', [[None]])
    return values[0][0]

DB_URL = 'sqlite:////var/lib/sqlite/iot_datastore.db'
app = Flask("migration-batch")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_BINDS'] = {}
iot_service.initDb(app, DB_URL)

batchSize = 10000
retentionPolicies=['immediate_term', 'short_term', 'medium_term', 'long_term', 'infinite', 'infinite_term']

#measurements = listSckMeasurements()
measurements = ["foombd", "rokjjm"]
sys.stdout.write('List of all measurement which will be migrated : %s\n' % str(measurements))

with app.app_context():
    for measurement in measurements:
        beforeTime = '2030-01-01T00:00:00Z'
        for rp in retentionPolicies:
            sys.stdout.write('Attept to migrate measurement: [%s] data in RP: [%s] before: [%s].\n' % (measurement, rp, beforeTime))
            expectedMigrateCount = countAllMeasurementData(rp, measurement, beforeTime)
            if expectedMigrateCount:
                sys.stdout.write(f'Found {expectedMigrateCount} data in RP: [{rp}] to migrate\n')
                migratedCount = migrateAllMeasurementData(rp, measurement, beforeTime, batchSize)
                if expectedMigrateCount > migratedCount: raise Exception('Bad migrated values count ! Expected %d values migrated but %d values where migrated.' % (expectedMigrateCount, migratedCount))

                newBeforeTime = getFirstEventFormattedTime(rp, measurement)
                if newBeforeTime: beforeTime = newBeforeTime
            else:
                sys.stdout.write(f'Skip RP: [{rp}] migration : no data found.\n')
