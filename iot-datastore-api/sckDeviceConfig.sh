
if [ -z "$DEVICE_UID" ]; then
	>&2 echo "DEVICE_UID var must be define !"
	exit 1
fi

sckDeviceConfig='{
	"uid": "'$DEVICE_UID'",
	"maxSecondsBackInTime": '$(( 3600 * 48 ))'
}'

sckTags='{
        "device": {"_device_type": "smart_citizen_kit", "_device_version": "2.1"},
        "defaultQuantities": {
        },
	"channels": {
                "10": {
			"_channelname": "Battery sensor"
		},
                "14": {
			"_channelname": "Rohm BH1721FVC sensor"
		},
                "53": {
			"_channelname": "Invensense ICS-434342 sensor"
		},
                "55": {
			"_channelname": "Sensirion SHT-31 sensor"
		},
                "56": {
			"_channelname": "Sensirion SHT-31 sensor"
		},
                "58": {
			"_channelname": "NXP MPL3115A26 sensor"
		},
                "87": {
			"_channelname": "PMS 5003 sensor"
		},
                "88": {
			"_channelname": "PMS 5003 sensor"
		},
                "89": {
			"_channelname": "PMS 5003 sensor"
		},
                "112": {
			"_channelname": "SGX MICS-4514 sensor"
		},
                "113": {
			"_channelname": "SGX MICS-4514 sensor"
		}

	},
        "byChannelQuantities": {
                "10": {
                        "percentage": {
                                "name": "Battery level"
                        }
                },
                "14": {
                        "illuminance": {
                                "name": "Luminosity"
                        }
                },
                "53": {
                        "sound_level": {
                                "name": "Sound level"
                        }
                },
                "55": {
                        "temperature": {
                                "name": "Temperature"
                        }
                },
                "56": {
                        "relative_humidity": {
                                "name": "Relative humidity"
                        }
                },
                "58": {
                        "atmospheric_pressure": {
                                "name": "Atmospheric pressure"
                        }
                },
                "87": {
                        "concentration": {
                                "name": "PM10",
                                "units": "µg/m³"
                        }
                },
                "88": {
                        "concentration": {
                                "name": "PM2.5",
                                "units": "µg/m³"
                        }
                },
                "89": {
                        "concentration": {
                                "name": "PM1",
                                "units": "µg/m³"
                        }
                },
                "112": {
                        "ppm": {
                                "name": "eCO2 gas level",
                                "units": "ppm"
                        }
                },
                "113": {
                        "ppb": {
                                "name": "VOC gas level",
                                "units": "ppb"
                        }
                }
        }
}'

