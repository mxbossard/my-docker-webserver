#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

$SCRIPTS_DIR/listServiceIps.sh iot-datastore-api 'binary-lpp-mqtt-to-iot-datastore-daemon'
$SCRIPTS_DIR/listServiceIps.sh iot-datastore-api 'sck-mqtt-to-iot-datastore-daemon'
