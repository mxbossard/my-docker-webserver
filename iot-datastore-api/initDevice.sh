#! /bin/bash -e
scriptDir="$( readlink -f $( dirname $0 ) )"

usage() {
	>&2 echo "usage: $0 <DEVICE_UID> [API_BASE_URL]"
	exit 1
}

source $scriptDir/../scripts/projectEnv.sh

DEVICE_UID="$1"
API_BASE_URL="${2:-$HTTP_PROTOCOL://$VIRTUAL_HOST}"
DATASTORE_URL="$API_BASE_URL$API_CONTEXT_ROOT/v1/datastore"

test -n "$DEVICE_UID" || usage

deviceConfig='{
	"uid": "'$DEVICE_UID'",
	"maxSecondsBackInTime": 120
}'

curl -X POST -H 'Content-Type: application/json' --data "$deviceConfig" -- "$DATASTORE_URL/devices"

curl -fsS -X GET -- "$DATASTORE_URL/devices/$DEVICE_UID"

