#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

source $SCRIPT_DIR/../../scripts/projectEnv.sh
source $SCRIPTS_DIR/unitTestsFramework.sh

source $SCRIPT_DIR/helper_iot-datastore.env

newDeviceUidBase="integrationtest_lpp_"
newDeviceUid="$newDeviceUidBase$now"

### Check new Device creation
assertHttpStatus 'Check status for new IoT device POSTing' POST "$DATASTORE_API_BASE_URL/devices/$newDeviceUid" "$HTTP_OK" "$CURL_ARGS" 

assertHttpStatus 'Check status for IoT devices listing' GET "$DATASTORE_API_BASE_URL/devices" "$HTTP_OK" "$CURL_ARGS"
assertHttpStatus 'Check status for new IoT device GETing' GET "$DATASTORE_API_BASE_URL/devices/$newDeviceUid" "$HTTP_OK" "$CURL_ARGS"

### Post LPP
urlBase64Lpp="AWcBEAFlAGQBAAEBAgAyAYgAqYgGIxgBJuw=" #temperature: 27.2 ; illuminance: 100 ; gps_location: 4.34 40.22 755 ; digital: 1 ; analaog: 0.5
assertHttpStatus 'Check status for new LPP POSTing' POST "$DATASTORE_API_BASE_URL/lpp/$newDeviceUid/$urlBase64Lpp" "$HTTP_OK" "$CURL_ARGS"

assertHttpResponseMatch 'Check response for new IoT device GETing' GET "$DATASTORE_API_BASE_URL/devices/$newDeviceUid" '.*"uid"\s*:\s*"'$newDeviceUid'".*' "$CURL_ARGS"

### Check LPP data in influxdb
assertMeasurementContent "$newDeviceUid" "1" "analog_input" "web_api" "url_post" "lpp" "avg" "" "2" "0.5"
assertMeasurementContent "$newDeviceUid" "1" "digital_input" "web_api" "url_post" "lpp" "avg" "" "0" "1"
assertMeasurementContent "$newDeviceUid" "1" "gps_location" "web_api" "url_post" "lpp" "last" "" "136" "altitude=755" "latitude=4.34" "longitude=40.22"
assertMeasurementContent "$newDeviceUid" "1" "illuminance" "web_api" "url_post" "lpp" "avg" "lux" "101" "100"
assertMeasurementContent "$newDeviceUid" "1" "temperature" "web_api" "url_post" "lpp" "avg" "°C" "103" "27.2"

### Delete test devices
deleteIntegrationTestDevices

### Drop integration tests measurements
deleteIntegrationTestMeasurements
