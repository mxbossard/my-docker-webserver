#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

source $SCRIPT_DIR/../../scripts/projectEnv.sh
source $SCRIPTS_DIR/unitTestsFramework.sh

source $SCRIPT_DIR/helper_iot-datastore.env

newDeviceUidBase="integrationtest_datapayload_"
newDeviceUid="$newDeviceUidBase$now"

### Check new Device creation
assertHttpStatus 'Check status for new IoT device POSTing' POST "$DATASTORE_API_BASE_URL/devices/$newDeviceUid" "$HTTP_OK" "$CURL_ARGS" 

assertHttpStatus 'Check status for IoT devices listing' GET "$DATASTORE_API_BASE_URL/devices" "$HTTP_OK" "$CURL_ARGS"
assertHttpStatus 'Check status for new IoT device GETing' GET "$DATASTORE_API_BASE_URL/devices/$newDeviceUid" "$HTTP_OK" "$CURL_ARGS"

### Post Generic Data
dataPayload='{"channels": {"3": {"temperature": 42.7}, "4": {"temperature": [12.4]}, "100": {"foo": 0, "gps_location": [4.34, 40.22, 755]}, "200": {"bar": {"a": 0, "z": 17}}}}'
assertHttpStatus 'Check status for new Generic data POSTing' POST "$DATASTORE_API_BASE_URL/data/$newDeviceUid" "$HTTP_OK" -H "Content-Type: application/json" --data "$dataPayload" "$CURL_ARGS"

assertHttpResponseMatch 'Check response for new IoT device GETing' GET "$DATASTORE_API_BASE_URL/devices/$newDeviceUid" '.*"uid"\s*:\s*"'$newDeviceUid'".*' "$CURL_ARGS"

### Check LPP data in influxdb
assertMeasurementContent "$newDeviceUid" "3" "temperature" "web_api" "json_post" "data_payload" "avg" "°C" "" "42.7"
assertMeasurementContent "$newDeviceUid" "4" "temperature" "web_api" "json_post" "data_payload" "avg" "°C" "" "12.4"
assertMeasurementContent "$newDeviceUid" "100" "foo" "web_api" "json_post" "data_payload" "avg" "" "" "0"
assertMeasurementContent "$newDeviceUid" "100" "gps_location" "web_api" "json_post" "data_payload" "last" "" "" "altitude=755" "latitude=4.34" "longitude=40.22"
assertMeasurementContent "$newDeviceUid" "200" "bar" "web_api" "json_post" "data_payload" "avg" "" "" "a=0" "z=17"


### Delete test devices
deleteIntegrationTestDevices

### Drop integration tests measurements
deleteIntegrationTestMeasurements
