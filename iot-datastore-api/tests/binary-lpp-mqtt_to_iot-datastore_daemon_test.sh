#! /bin/bash -e
scriptDir="$(dirname $(readlink -f $0))"

source $scriptDir/../../scripts/projectEnv.sh
source $SCRIPTS_DIR/unitTestsFramework.sh

source $scriptDir/helper_iot-datastore.env

# Post LPP on MQTT broker
newDeviceUidBase="integrationtest_mqttbinarylpp_"
newDeviceUid="$newDeviceUidBase$now"
topic="public/$newDeviceUid"
hexLppMessage="018806765ff2960a0003e8" # GPS data
#hexLppMessage="067104D2FB2E0000" # Accelerometer data
mqttHostname="$PROXY_LOCAL_IP"

hexToBytes() {
	hex="$1"
	echo -ne "$( echo $hex | sed -e 's/../\\x&/g' )"
	#echo -n "$hex" | perl -e 'print pack "H*", <STDIN>' 2> /dev/null
}
export -f hexToBytes

### Check new Device creation
assertHttpStatus 'Check status for new IoT device POSTing' POST "$DATASTORE_API_BASE_URL/devices/$newDeviceUid" "$HTTP_OK"

lppTempFile="$( mkTempFile lpp )"
hexToBytes "$hexLppMessage" > $lppTempFile
assertTrue 'Posting MQTT LPP' sh -c "mosquitto_pub -h '$mqttHostname' -p '$SHIFTING_PORT_MQTT' -t '$topic' --qos 1 -s < '$lppTempFile'"

sleep 1

### Check if LPP is stored in influxdb
assertMeasurementContent "$newDeviceUid" "1" "gps_location" "web_api" "binary_post" "lpp" "last" "" "136" "altitude=10" "latitude=42.3519" "longitude=-87.9094"


### Delete test devices
deleteIntegrationTestDevices

### Drop integration tests measurements
deleteIntegrationTestMeasurements
