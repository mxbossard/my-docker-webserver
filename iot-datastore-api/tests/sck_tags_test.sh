#! /bin/bash -e
scriptDir="$(dirname $(readlink -f $0))"

source $scriptDir/../../scripts/projectEnv.sh
source $SCRIPTS_DIR/unitTestsFramework.sh

source $scriptDir/helper_iot-datastore.env

# Post LPP on MQTT broker
newDeviceUidBase="integrationtest_sck_tags_"
newDeviceUid="$newDeviceUidBase$now"
topic="device/sck/$newDeviceUid/readings/raw"
sckNowTime=$( date '+%FT%TZ' --date "@$now" --utc )
sckMessage="{t:$sckNowTime,10:99,14:97,55:20.77,56:47.03,53:51.17,58:100.86,113:101.00,112:1068.00,89:6,87:600,88:60000}"
mqttHostname="$PROXY_LOCAL_IP"

DEVICE_UID="$newDeviceUid"
. "$scriptDir/../sckDeviceConfig.sh"

### Check new Device creation
assertHttpStatus 'Check status for new SCK device POSTing' POST "$DATASTORE_API_BASE_URL/devices" "$HTTP_OK" -H "Content-Type: application/json" --data "$sckDeviceConfig"

assertHttpStatus 'Check status for IoT device sck tags POSTing' POST "$DATASTORE_API_BASE_URL/devices/$newDeviceUid/tags" "$HTTP_OK" -H "Content-Type: application/json" --data "$sckTags"

expectedDeviceConfig='{"location":null,"maxSecondsBackInTime":172800,"maxSecondsForwardInFutur":120,"name":null,"uid":"'$newDeviceUid'"}\n'
assertHttpResponseIs 'Check new SCK device config' GET "$DATASTORE_API_BASE_URL/devices/$newDeviceUid" "$expectedDeviceConfig"

assertTrue 'Posting MQTT SCK raw data' sh -c "mosquitto_pub -h '$mqttHostname' -p '$SHIFTING_PORT_MQTT' -t '$topic' --qos 1 -m '$sckMessage'"

sleep 1

### Check if LPP is stored in DB

assertMeasurementTime "$newDeviceUid" "10" "percentage" "$now"
assertMeasurementContent "$newDeviceUid" "10" "percentage" "web_api" "json_post" "data_payload" "avg" "%" "" "99"
assertMeasurementContent "$newDeviceUid" "14" "illuminance" "web_api" "json_post" "data_payload" "avg" "lux" "" "97"
assertMeasurementContent "$newDeviceUid" "53" "sound_level" "web_api" "json_post" "data_payload" "avg" "dbA" "" "51.17"
assertMeasurementContent "$newDeviceUid" "55" "temperature" "web_api" "json_post" "data_payload" "avg" "°C" "" "20.77"
assertMeasurementContent "$newDeviceUid" "56" "relative_humidity" "web_api" "json_post" "data_payload" "avg" "%" "" "47.03"
assertMeasurementContent "$newDeviceUid" "58" "atmospheric_pressure" "web_api" "json_post" "data_payload" "avg" "hPa" "" "1008.6"
assertMeasurementContent "$newDeviceUid" "87" "concentration" "web_api" "json_post" "data_payload" "avg" "µg/m³" "" "600"
assertMeasurementContent "$newDeviceUid" "88" "concentration" "web_api" "json_post" "data_payload" "avg" "µg/m³" "" "60000"
assertMeasurementContent "$newDeviceUid" "89" "concentration" "web_api" "json_post" "data_payload" "avg" "µg/m³" "" "6"
assertMeasurementContent "$newDeviceUid" "112" "ppm" "web_api" "json_post" "data_payload" "avg" "ppm" "" "1068"
assertMeasurementContent "$newDeviceUid" "113" "ppb" "web_api" "json_post" "data_payload" "avg" "ppb" "" "101"

### Delete test devices
deleteIntegrationTestDevices

### Drop integration tests measurements
deleteIntegrationTestMeasurements
