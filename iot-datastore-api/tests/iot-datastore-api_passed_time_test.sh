#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

source $SCRIPT_DIR/../../scripts/projectEnv.sh
source $SCRIPTS_DIR/unitTestsFramework.sh

source $SCRIPT_DIR/helper_iot-datastore.env

newDeviceUidBase="integrationtest_passedtime_"
newDeviceUid="$newDeviceUidBase$now"

hexLppMessage="018806765ff2960a0003e8" # GPS data
hexToBytes() {
	hex="$1"
	echo -ne "$( echo $hex | sed -e 's/../\\x&/g' )"
	#echo -n "$hex" | perl -e 'print pack "H*", <STDIN>' 2> /dev/null
}
export -f hexToBytes

passedTime=$( date --date 'now - 2min' '+%s' )
hexPassedTime="$( printf '%x' $passedTime )"
# Passed time on channel 9
hexLppWithPassedTime="${hexLppMessage}0985${hexPassedTime}"

### Check new Device creation
assertHttpStatus 'Check status for new IoT device POSTing' POST "$DATASTORE_API_BASE_URL/devices/$newDeviceUid" "$HTTP_OK" "$CURL_ARGS" 

assertHttpStatus 'Check status for IoT devices listing' GET "$DATASTORE_API_BASE_URL/devices" "$HTTP_OK" "$CURL_ARGS"
assertHttpStatus 'Check status for new IoT device GETing' GET "$DATASTORE_API_BASE_URL/devices/$newDeviceUid" "$HTTP_OK" "$CURL_ARGS"

### Post PassedLPP
lppTempFile="$( mkTempFile lpp )"
hexToBytes "$hexLppWithPassedTime" > $lppTempFile
assertHttpStatus 'Check status for new LPP POSTing' POST "$DATASTORE_API_BASE_URL/lpp/$newDeviceUid" "$HTTP_OK" "-H" "Content-Type: application/octet-stream" "--data-binary" "@$lppTempFile" "$CURL_ARGS"

assertHttpResponseMatch 'Check response for new IoT device GETing' GET "$DATASTORE_API_BASE_URL/devices/$newDeviceUid" '.*"uid"\s*:\s*"'$newDeviceUid'".*' "$CURL_ARGS"

### Check LPP data in influxdb
assertMeasurementTime "$newDeviceUid" "1" "gps_location" "$passedTime"
assertMeasurementContentAfter "$newDeviceUid" "1" "gps_location" "$passedTime" "web_api" "binary_post" "lpp" "last" "" "136" "altitude=10" "latitude=42.3519" "longitude=-87.9094"

### Post too far in passed LPP
time=$( date --date 'now - 10min' '+%s' )
hexTime="$( printf '%x' $time )"
hexLpp="${hexLppMessage}0985${hexTime}"
hexToBytes "$hexLpp" > $lppTempFile
assertHttpStatus 'Check status for too far in passed LPP POSTing' POST "$DATASTORE_API_BASE_URL/lpp/$newDeviceUid" "409" "-H" "Content-Type: application/octet-stream" "--data-binary" "@$lppTempFile" "$CURL_ARGS"

### Post too far in futur LPP
time=$( date --date 'now + 5min' '+%s' )
hexTime="$( printf '%x' $time )"
hexLpp="${hexLppMessage}0985${hexTime}"
hexToBytes "$hexLpp" > $lppTempFile
assertHttpStatus 'Check status for too far in futur LPP POSTing' POST "$DATASTORE_API_BASE_URL/lpp/$newDeviceUid" "409" "-H" "Content-Type: application/octet-stream" "--data-binary" "@$lppTempFile" "$CURL_ARGS"

### Delete test devices
deleteIntegrationTestDevices

### Drop integration tests measurements
deleteIntegrationTestMeasurements
