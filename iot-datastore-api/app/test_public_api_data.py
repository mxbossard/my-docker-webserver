from datetime import datetime
import logging

from dataclasses import fields
import pytest
import flask
from flask import json
from pytest_mock import MockerFixture
import iot_datastore.iot_service as IotService
import iot_datastore.db_models as DbModels
from iot_datastore.exceptions import NotValidDataTime
import public_api as IotStorageApi
from test_public_api_commons import *


__log = logging.getLogger(__name__)


def testPostDataWithoutDevice(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    payload = { "channels": {} }

    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data', json=payload)
    assert 400 == rv.status_code, 'Error should be return for /data if no Device supplied ! Response: %s' % rv.data

    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data/', json=payload)
    assert 400 == rv.status_code, 'Error should be return for /data if no Device supplied ! Response: %s' % rv.data

def testPostDataWithUnknownDevice(app, client, mocker):
    def mockedStoreDataPayload(uid, payload, ingestComponent, ingestMethod):
        raise KeyError('Device not Found') 
    mocker.patch('iot_datastore.iot_service.storeDataPayload', side_effect=mockedStoreDataPayload)

    payload = { "channels": {} }

    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data/' + 'unknownDevice', json=payload)
    assert 404 == rv.status_code, 'NotFound should be return for unknown Device ! Response: %s' % rv.data

def testPostDataWithBadContentType(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeDataPayload')

    payload = { "channels": {} }
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data/' + DEVICE_1.uid, content_type='not_supported', json=payload)
    assert 400 == rv.status_code, 'Error should be return if not suited Content-Type supplied ! Response: %s' % rv.data

def testPostSimpleData(app, client, mocker):
    mock = mocker.patch('iot_datastore.iot_service.storeDataPayload', autospec=True)

    payload = { "channels": {
        1: {
            "temperature": 42.3
        }
    } }
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data/' + DEVICE_1.uid, json=payload)
    assert 200 == rv.status_code, 'Simple data should be posted ! Response: %s' % rv.data

    expectedQuantities = [DbModels.DataQuantity('temperature', [42.3])]
    expectedChannels = [DbModels.DataChannel(1, expectedQuantities)]
    expectedPayload = DbModels.DataPayload(expectedChannels)
    mock.assert_called_once_with(DEVICE_1.uid, expectedPayload, IotStorageApi.API_INGEST_COMPONENT, IotStorageApi.JSON_POST_INGEST_METHOD)

def testPostSimpleDataArray(app, client, mocker):
    mock = mocker.patch('iot_datastore.iot_service.storeDataPayload', autospec=True)

    payload = { "channels": {
        1: {
            "gps_location": [1.2, 3.4, 122.7]
        }
    } }
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data/' + DEVICE_1.uid, json=payload)
    assert 200 == rv.status_code, 'Simple data should be posted ! Response: %s' % rv.data

    expectedQuantities = [DbModels.DataQuantity('gps_location', [1.2, 3.4, 122.7])]
    expectedChannels = [DbModels.DataChannel(1, expectedQuantities)]
    expectedPayload = DbModels.DataPayload(expectedChannels)
    mock.assert_called_once_with(DEVICE_1.uid, expectedPayload, IotStorageApi.API_INGEST_COMPONENT, IotStorageApi.JSON_POST_INGEST_METHOD)


def testPostSimpleDataMap(app, client, mocker):
    def mockedStoreDataPayload(uid, payload, ingestComponent, ingestMethod):
        assert DEVICE_1.uid == uid
        gpsFields = payload.getFields(1, 'gps_location')
        assert 3 == len(gpsFields)
        assert 'lat' in gpsFields
        assert 'lon' in gpsFields
        assert 'alti' in gpsFields

        assert {
                "lat": 1.2,
                "lon": 3.4,
                "alti": 122.7
            } == gpsFields

    mock = mocker.patch('iot_datastore.iot_service.storeDataPayload', side_effect=mockedStoreDataPayload)

    payload = { "channels": {
        1: {
            "gps_location": {
                "lat": 1.2,
                "lon": 3.4,
                "alti": 122.7
            }
        }
    } }
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data/' + DEVICE_1.uid, json=payload)
    assert 200 == rv.status_code, 'Simple data should be posted ! Response: %s' % rv.data

    mock.assert_called_once()


def testPostEmptyData(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeDataPayload')

    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data/' + DEVICE_1.uid)
    assert 400 == rv.status_code, 'Empty data should produce an Error ! Response: %s' % rv.data

def testPostEmptyData2(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeDataPayload')

    emptyPayload = {"uid": DEVICE_1.uid}
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data/', json=emptyPayload)
    assert 400 == rv.status_code, 'Empty data should produce an Error ! Response: %s' % rv.data

def testPostEmptyData3(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeDataPayload')

    emptyPayload = {"uid": DEVICE_1.uid, "channels": {}}
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data/' + DEVICE_1.uid, json=emptyPayload)
    assert 400 == rv.status_code, 'Empty Binary Lpp should produce an Error ! Response: %s' % rv.data

def testPostBadChannelTypeData(app, client, mocker):
    mock = mocker.patch('iot_datastore.iot_service.storeDataPayload')

    payload = { "channels": {
        "a": {
            "gps_location": [1.2, 3.4, 122.7]
        }
    } }
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data/' + DEVICE_1.uid, json=payload)
    assert 400 == rv.status_code, 'Simple data should not be posted ! Response: %s' % rv.data

def testPostNotValidDataTime(app, client, mocker):
    def mockedFunc(deviceUid: str, payload: DbModels.DataPayload, ingestComponent, ingestMethod):
        raise NotValidDataTime("foo", datetime.now())
    mock = mocker.patch('iot_datastore.iot_service.storeDataPayload', side_effect=mockedFunc)

    payload = { "channels": {
        "1": {
            "gps_location": [1.2, 3.4, 122.7]
        }
    } }
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data/' + DEVICE_1.uid, json=payload)
    assert 409 == rv.status_code, 'Too far in passed data should return 409 !'

def testPostEmptyChannelData(app, client, mocker):
    mock = mocker.patch('iot_datastore.iot_service.storeDataPayload')

    payload = { "channels": {
        1: {}
    } }
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data/' + DEVICE_1.uid, json=payload)
    assert 400 == rv.status_code, 'Simple data should not be posted ! Response: %s' % rv.data

def testPostBadQuantityTypeData(app, client, mocker):
    mock = mocker.patch('iot_datastore.iot_service.storeDataPayload')

    payload = { "channels": {
        1: {
            "a-z": 42
        }
    } }
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data/' + DEVICE_1.uid, json=payload)
    assert 400 == rv.status_code, 'Simple data should not be posted ! Response: %s' % rv.data

def testPostEmptyQuantityData(app, client, mocker):
    mock = mocker.patch('iot_datastore.iot_service.storeDataPayload')

    payload = { "channels": {
        1: {
            'temperature': None
        }
    } }
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data/' + DEVICE_1.uid, json=payload)
    assert 400 == rv.status_code, 'Simple data should not be posted ! Response: %s' % rv.data

def testPostBadValueTypeData(app, client, mocker):
    mock = mocker.patch('iot_datastore.iot_service.storeDataPayload')

    payload = { "channels": {
        1: {
            'temperature': 'foo'
        }
    } }
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data/' + DEVICE_1.uid, json=payload)
    assert 400 == rv.status_code, 'Simple data should not be posted ! Response: %s' % rv.data

def testPostDataWithUnknownDevice(app, client, mocker):
    def mockedStoreDataPayload(uid, payload, ingestComponent, ingestMethod):
        raise KeyError('Device not Found') 
    mocker.patch('iot_datastore.iot_service.storeDataPayload', side_effect=mockedStoreDataPayload)

    payload = {"uid": "unknownDevice", "channels": {1: {"temperature": 42}}}
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data/', json=payload)
    assert 404 == rv.status_code, 'NotFound should be return for unknown Device ! Response: %s' % rv.data

def testPostInconsistentDeviceUidJsonLpp(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeDataPayload')

    payload = {"uid": DEVICE_2.uid, "channels": {1: {"temperature": 42}}}
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/data/' + DEVICE_1.uid, json=payload)
    assert 409 == rv.status_code, 'Inconsistent Device UID JSON Lpp should produce an Error ! Response: %s' % rv.data

def testGetData(app, client):
    rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/data')
    assert 405 == rv.status_code, 'GET on /data should return an error ! Response: %s' % rv.data

def testDeleteData(app, client):
    rv: flask.wrappers.Response = client.delete(API_CONTEXT_ROOT + '/data')
    assert 405 == rv.status_code, 'DELETE on /data should return an error ! Response: %s' % rv.data
