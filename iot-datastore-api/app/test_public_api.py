import os
import re
import logging
import tempfile
import copy
import base64
import pytest
import flask
from flask import json
from pytest_mock import MockerFixture
import iot_datastore.iot_service as IotService
import iot_datastore.db_models as DbModels
import public_api as IotStorageApi
from iot_datastore.exceptions import NotProcessablePayload
from test_public_api_commons import *


__log = logging.getLogger(__name__)


def testGetStatus(app, client, mocker):
    rv: flask.wrappers.Response = client.get(IotStorageApi.STATUS_PATH)
    assertJsonResponse('Retrieving service status', rv, {'status': 'ok'})
