import os
import tempfile
import base64
import pytest
import flask
from flask import json
from pytest_mock import MockerFixture
import iot_datastore.iot_service as IotService
import iot_datastore.db_models as DbModels
import public_api as IotStorageApi
from iot_datastore.exceptions import NotProcessablePayload

API_CONTEXT_ROOT = IotStorageApi.DATASTORE_PATH

@pytest.fixture
def app(mocker):
    #__log.info('app fixture called')
    app = flask.Flask(__name__)
    IotStorageApi.loadApi(app)
    yield app

@pytest.fixture
def client(mocker, app):
    #__log.info('client fixture called')
    #mocker.patch('iot_datastore.influxdb_service.createInluxDbBucket')

    db_fd, app.config['DATABASE'] = tempfile.mkstemp()
    app.config['TESTING'] = True
    app.config['SQLALCHEMY_BINDS'] = {}
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_ECHO'] = False
    app.config['BUNDLE_ERRORS'] = True

    with app.test_client() as client:
        with app.app_context():
            IotService.initDb(app, 'sqlite:///' + app.config['DATABASE'], False)
        yield client

    os.close(db_fd)
    os.unlink(app.config['DATABASE'])

DEVICE_1 = DbModels.Device('xxx1', 'device1', 'location1')
DEVICE_2 = DbModels.Device('xxx2', 'device2', 'location2')
DEVICE_3 = DbModels.Device('xxx3', 'device3', 'location3')
DEVICE_4 = DbModels.Device('xxx4', 'device4', 'location4', 42, 107)

DEVICE_WITH_TAGS_1 = DbModels.Device('xxx1', 'device1', 'location1')
DEVICE_WITH_TAGS_1.addDeviceTag('tag1', 'value1')
#DEVICE_WITH_TAGS_1.setLppChannelMapping(1, 'chan1', {0: 'foo1'}, {'tag1': 'val1'})
DEVICE_WITH_TAGS_1.addChannelTag(1, '_channelName', 'chan1')
DEVICE_WITH_TAGS_1.addQuantityTag(1, 'digital_input', '_label', 'foo1')
DEVICE_WITH_TAGS_1.addChannelTag(1, 'tag1', 'val1')

DEVICE_WITH_TAGS_2 = DbModels.Device('xxx2', 'device2', 'location2')
DEVICE_WITH_TAGS_2.addDeviceTag('tag21', 'value21')
DEVICE_WITH_TAGS_2.addDeviceTag('tag22', 'value22')
#DEVICE_WITH_TAGS_2.setLppChannelMapping(1, 'chan1', {0: 'foo1'}, {'tag1': 'val1'})
DEVICE_WITH_TAGS_2.addChannelTag(1, '_channelName', 'chan1')
DEVICE_WITH_TAGS_2.addQuantityTag(1, 'digital_input', '_label', 'foo1')
DEVICE_WITH_TAGS_2.addChannelTag(1, 'tag1', 'val1')

DEVICE_WITH_TAGS_3 = DbModels.Device('xxx3', 'device3', 'location3')
DEVICE_WITH_TAGS_3.addDeviceTag('tag3', 'value3')
#DEVICE_WITH_TAGS_3.setLppChannelMapping(1, 'chan1', {0: 'foo1'}, {'tag1': 'val1'})
DEVICE_WITH_TAGS_3.addChannelTag(1, '_channelName', 'chan1')
DEVICE_WITH_TAGS_3.addQuantityTag(1, 'digital_input', '_label', 'foo1')
DEVICE_WITH_TAGS_3.addChannelTag(1, 'tag1', 'val1')

DEVICE_WITH_MAPPING_1 = DbModels.Device('xxx1', 'device1', 'location1')
DEVICE_WITH_MAPPING_1.addDeviceTag('tag1', 'value1')
#DEVICE_WITH_MAPPING_1.setLppChannelMapping(11, 'chan11', {0: 'foo11', 1: 'bar12'}, {'tag1': 'val1', 'tag2': 'val2'})
DEVICE_WITH_MAPPING_1.addChannelTag(11, '_channelName', 'chan11')
DEVICE_WITH_MAPPING_1.addQuantityTag(11, 'digital_input', '_label', 'foo1')
DEVICE_WITH_MAPPING_1.addQuantityTag(11, 'digital_output', '_label', 'bar12')
DEVICE_WITH_MAPPING_1.addChannelTag(11, 'tag1', 'val1')
DEVICE_WITH_MAPPING_1.addChannelTag(11, 'tag2', 'val2')

DEVICE_WITH_MAPPING_2 = DbModels.Device('xxx2', 'device2', 'location2')
DEVICE_WITH_MAPPING_2.addDeviceTag('tag1', 'value1')
#DEVICE_WITH_MAPPING_2.setLppChannelMapping(21, 'chan21', {0: 'foo211', 1: 'bar212'}, {'tag211': 'val211', 'tag212': 'val212'})
#DEVICE_WITH_MAPPING_2.setLppChannelMapping(22, 'chan22', {0: 'foo221', 1: 'bar222'}, {'tag221': 'val221', 'tag222': 'val222'})
DEVICE_WITH_MAPPING_2.addChannelTag(21, '_channelName', 'chan21')
DEVICE_WITH_MAPPING_2.addQuantityTag(21, 'digital_input', '_label', 'foo211')
DEVICE_WITH_MAPPING_2.addQuantityTag(21, 'digital_output', '_label', 'bar212')
DEVICE_WITH_MAPPING_2.addChannelTag(21, 'tag211', 'val211')
DEVICE_WITH_MAPPING_2.addChannelTag(21, 'tag212', 'val212')
DEVICE_WITH_MAPPING_2.addChannelTag(22, '_channelName', 'chan22')
DEVICE_WITH_MAPPING_2.addQuantityTag(22, 'digital_input', '_label', 'foo221')
DEVICE_WITH_MAPPING_2.addQuantityTag(22, 'digital_output', '_label', 'bar222')
DEVICE_WITH_MAPPING_2.addChannelTag(22, 'tag221', 'val221')
DEVICE_WITH_MAPPING_2.addChannelTag(22, 'tag222', 'val222')

DEVICE_WITH_MAPPING_3 = DbModels.Device('xxx3', 'device3', 'location3')
DEVICE_WITH_MAPPING_3.addDeviceTag('tag1', 'value1')
#DEVICE_WITH_MAPPING_3.setLppChannelMapping(31, 'chan31', {0: 'foo311', 1: 'bar312'}, {'tag311': 'val311', 'tag312': 'val312'})
DEVICE_WITH_MAPPING_2.addChannelTag(31, '_channelName', 'chan31')
DEVICE_WITH_MAPPING_2.addQuantityTag(31, 'digital_input', '_label', 'foo311')
DEVICE_WITH_MAPPING_2.addQuantityTag(31, 'digital_output', '_label', 'bar312')
DEVICE_WITH_MAPPING_2.addChannelTag(31, 'tag311', 'val311')
DEVICE_WITH_MAPPING_2.addChannelTag(31, 'tag312', 'val312')

SIMPLE_BINARY_LPP_1 = bytes([0x03, 0x67, 0x01, 0x10, 0x05, 0x67, 0xff, 0xff])
SIMPLE_BASE64_LPP_1 = base64.standard_b64encode(SIMPLE_BINARY_LPP_1).decode("ascii") 
SIMPLE_URL_BASE64_LPP_1 = base64.urlsafe_b64encode(SIMPLE_BINARY_LPP_1).decode("ascii") 
ERRONEOUS_BASE64_LPP_1 = base64.standard_b64encode(bytes([0x03, 0x67, 0x01])).decode("ascii")
ERRONEOUS_URL_BASE64_LPP_1 = base64.urlsafe_b64encode(bytes([0x03, 0x67, 0x01])).decode("ascii")
NOT_BASE64_LPP_1 = '03670110056700FFz'

def assertJsonResponse(action: str, response: flask.wrappers.Response, expectedJson: dict, expectedStatus=200):
    assert expectedStatus == response.status_code, 'Api should return a status %d on %s ! Body was: [%s]' % (expectedStatus, action, response.data)
    assert expectedJson == json.loads(response.data), 'Response contains some invalid JSON on %s !' % (action)
