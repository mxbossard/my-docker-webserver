import os
import re
import logging
import tempfile
import copy
import base64
import pytest
import flask
from flask import json
from pytest_mock import MockerFixture
import iot_datastore.iot_service as IotService
import iot_datastore.db_models as DbModels
import public_api as IotStorageApi
from iot_datastore.exceptions import NotProcessablePayload
from test_public_api_commons import *

__log = logging.getLogger(__name__)


def testPostLppWithoutDevice(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    textBase64Lpp = SIMPLE_BASE64_LPP_1
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp', data=textBase64Lpp)
    assert 400 == rv.status_code, 'Error should be return for binary LPP if no Device supplied ! Response: %s' % rv.data

    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/', json={'lpp': textBase64Lpp})
    assert 400 == rv.status_code, 'Error should be return for JSON LPP if no Device supplied ! Response: %s' % rv.data

def testPostBinaryLppWithUnknownDevice(app, client, mocker):
    def mockedstoreLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod):
        raise KeyError('Device not Found') 
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid', side_effect=mockedstoreLppByDeviceUid)

    simpleBinaryLpp = SIMPLE_BINARY_LPP_1
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + 'unknownDevice',  content_type='application/octet-stream', data=simpleBinaryLpp)
    assert 404 == rv.status_code, 'NotFound should be return for unknown Device ! Response: %s' % rv.data

def testPostBinaryLppWithBadContentType(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    simpleBinaryLpp = SIMPLE_BINARY_LPP_1
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + DEVICE_1.uid, content_type='not_supported', data=simpleBinaryLpp)
    assert 400 == rv.status_code, 'Error should be return if not suited Content-Type supplied ! Response: %s' % rv.data

def testPostSimpleBinaryLpp(app, client, mocker):
    mock = mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    simpleBinaryLpp = SIMPLE_BINARY_LPP_1
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + DEVICE_1.uid, content_type='application/octet-stream', data=simpleBinaryLpp)
    assert 200 == rv.status_code, 'Simple Binary Lpp should be posted ! Response: %s' % rv.data

    mock.assert_called_once_with(DEVICE_1.uid, simpleBinaryLpp, IotStorageApi.API_INGEST_COMPONENT, IotStorageApi.BINARY_POST_INGEST_METHOD)

def testPostEmptyBinaryLpp1(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + DEVICE_1.uid, content_type='application/octet-stream')
    assert 400 == rv.status_code, 'Empty Binary Lpp should produce an Error ! Response: %s' % rv.data

def testPostEmptyBinaryLpp2(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + DEVICE_1.uid, content_type='application/octet-stream', data=None)
    assert 400 == rv.status_code, 'Empty Binary Lpp should produce an Error ! Response: %s' % rv.data

def testPostEmptyBinaryLpp3(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + DEVICE_1.uid, content_type='application/octet-stream', data=bytes())
    assert 400 == rv.status_code, 'Empty Binary Lpp should produce an Error ! Response: %s' % rv.data

def testPostUrlLppWithUnknownDevice(app, client, mocker):
    def mockedstoreLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod):
        raise KeyError('Device not Found') 
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid', side_effect=mockedstoreLppByDeviceUid)

    simpleLpp = SIMPLE_URL_BASE64_LPP_1
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + 'unknownDevice/' + simpleLpp)
    assert 404 == rv.status_code, 'NotFound should be return for unknown Device ! Response: %s' % rv.data

def testPostSimpleUrlLpp(app, client, mocker):
    mock = mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    assert SIMPLE_URL_BASE64_LPP_1 != SIMPLE_BASE64_LPP_1, 'Standard and URL safe Base64 LPP must differ for tests !'
    simpleUrlBase64Lpp = SIMPLE_URL_BASE64_LPP_1
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + DEVICE_1.uid + '/' + simpleUrlBase64Lpp)
    assert 200 == rv.status_code, 'Simple Base64 Lpp from URL should be posted ! Response: %s' % rv.data

    mock.assert_called_once_with(DEVICE_1.uid, SIMPLE_URL_BASE64_LPP_1, IotStorageApi.API_INGEST_COMPONENT, IotStorageApi.URL_POST_INGEST_METHOD)

def testPostEmptyUrlLpp(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + DEVICE_1.uid + '/')
    assert 400 == rv.status_code, 'Empty Base64 Lpp from URL should produce an Error ! Response: %s' % rv.data

def testPostSimpleTextLpp(app, client, mocker):
    mock = mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    simpleTextLpp = SIMPLE_BASE64_LPP_1
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + DEVICE_1.uid, data=simpleTextLpp)
    assert 200 == rv.status_code, 'Simple Text Lpp should be posted ! Response: %s' % rv.data

    mock.assert_called_once_with(DEVICE_1.uid, simpleTextLpp, IotStorageApi.API_INGEST_COMPONENT, IotStorageApi.TEXT_POST_INGEST_METHOD)

def testPostEmptyTextLpp1(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + DEVICE_1.uid)
    assert 400 == rv.status_code, 'Empty posted Base64 Lpp should produce an Error ! Response: %s' % rv.data

def testPostEmptyTextLpp2(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + DEVICE_1.uid, data='')
    assert 400 == rv.status_code, 'Empty posted Base64 Lpp should produce an Error ! Response: %s' % rv.data

def testPostPartialJsonLpp(app, client, mocker):
    mock = mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    textBase64Lpp = SIMPLE_BASE64_LPP_1
    partialJsonLpp = {"lpp": textBase64Lpp}
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + DEVICE_1.uid, json=partialJsonLpp)
    assert 200 == rv.status_code, 'Simple Partial JSON Lpp should be posted ! Response: %s' % rv.data

    mock.assert_called_once_with(DEVICE_1.uid, textBase64Lpp, IotStorageApi.API_INGEST_COMPONENT, IotStorageApi.JSON_POST_INGEST_METHOD)

def testPostFullJsonLpp(app, client, mocker):
    mock = mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    textBase64Lpp = SIMPLE_BASE64_LPP_1
    fullJsonLpp = {"uid": DEVICE_1.uid, "lpp": textBase64Lpp}
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp', json=fullJsonLpp)
    assert 200 == rv.status_code, 'Simple Full JSON Lpp should be posted ! Response: %s' % rv.data

    mock.assert_called_once_with(DEVICE_1.uid, textBase64Lpp, IotStorageApi.API_INGEST_COMPONENT, IotStorageApi.JSON_POST_INGEST_METHOD)

def testPostInconsistentDeviceUidJsonLpp(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    textBase64Lpp = SIMPLE_BASE64_LPP_1
    fullJsonLpp = {"uid": DEVICE_2.uid, "lpp": textBase64Lpp}
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + DEVICE_1.uid, json=fullJsonLpp)
    assert 409 == rv.status_code, 'Inconsistent Device UID JSON Lpp should produce an Error ! Response: %s' % rv.data

def testPostEmptyJsonLpp1(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp', json=None)
    assert 400 == rv.status_code, 'Empty JSON Lpp 1 should produce an Error ! Response: %s' % rv.data

def testPostEmptyJsonLpp2(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp', json={})
    assert 400 == rv.status_code, 'Empty JSON Lpp 1 should produce an Error ! Response: %s' % rv.data

def testPostEmptyJsonLpp3(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp', content_type='application/json')
    assert 400 == rv.status_code, 'Empty JSON Lpp 1 should produce an Error ! Response: %s' % rv.data

def testPostErroneousUrlLpp(app, client, mocker):
    def mockedstoreLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod):
        raise NotProcessablePayload('Unable to parse LPP !', lpp) 
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid', side_effect=mockedstoreLppByDeviceUid)

    erroneousLpp = ERRONEOUS_URL_BASE64_LPP_1
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + DEVICE_1.uid  + '/' + erroneousLpp)
    assert 501 == rv.status_code, 'Erroneous URL LPP should produce a server error ! Response: %s' % rv.data

def testPostErroneousTextLpp(app, client, mocker):
    def mockedstoreLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod):
        raise NotProcessablePayload('Unable to parse LPP !', lpp) 
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid', side_effect=mockedstoreLppByDeviceUid)

    erroneousLpp = ERRONEOUS_BASE64_LPP_1
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + DEVICE_1.uid, data=erroneousLpp)
    assert 501 == rv.status_code, 'Erroneous Text LPP should produce a server error ! Response: %s' % rv.data

def testPostErroneousJsonLpp(app, client, mocker):
    def mockedstoreLppByDeviceUid(deviceUid, lpp, ingestComponent, ingestMethod):
        raise NotProcessablePayload('Unable to parse LPP !', lpp) 
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid', side_effect=mockedstoreLppByDeviceUid)

    erroneousJsonLpp = {"lpp": ERRONEOUS_BASE64_LPP_1}
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + DEVICE_1.uid, json=erroneousJsonLpp)
    assert 501 == rv.status_code, 'Erroneous JSON LPP should produce a server error ! Response: %s' % rv.data

def testPostNotBase64TextLpp(app, client, mocker):
    mocker.patch('iot_datastore.iot_service.storeLppByDeviceUid')

    badLpp = NOT_BASE64_LPP_1
    rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/lpp/' + DEVICE_1.uid, data=badLpp)
    assert 400 == rv.status_code, 'Not Base64 LPP should produce an error ! Response: %s' % rv.data

def testGetLpp(app, client):
    rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/lpp')
    assert 405 == rv.status_code, 'GET on /lpp should return an error ! Response: %s' % rv.data

def testDeleteLpp(app, client):
    rv: flask.wrappers.Response = client.delete(API_CONTEXT_ROOT + '/lpp')
    assert 405 == rv.status_code, 'DELETE on /lpp should return an error ! Response: %s' % rv.data
