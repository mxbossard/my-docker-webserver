import os
import logging
from datetime import datetime
from typing import Mapping
from flask import Flask, jsonify, json, request, make_response
from flask_restx import reqparse, abort, Api, Resource, fields, marshal_with
from iot_datastore import db_models
import iot_datastore.iot_service as IotService
import iot_datastore.helper.logging as LoggingHelper
import iot_datastore.helper.json as JsonHelper
import iot_datastore.helper.encoding as EncodingHelper
from iot_datastore.exceptions import NotProcessablePayload, NotSupportedUid, NotValidDataTime
from iot_datastore.db_models import DataPayload, DataChannel, DataQuantity


__log = logging.getLogger(__name__)

# Retrieve and format API_CONTEXT_ROOT
API_CONTEXT_ROOT = os.environ.get('API_CONTEXT_ROOT', '')
if API_CONTEXT_ROOT == '/':
    API_CONTEXT_ROOT = ''
if len(API_CONTEXT_ROOT) > 0 and API_CONTEXT_ROOT[0] != '/':
    API_CONTEXT_ROOT = '/' + API_CONTEXT_ROOT 

API_VERSION = 'v1'
DATASTORE_PATH = f'{API_CONTEXT_ROOT}/{API_VERSION}/datastore'
STATUS_PATH = f'{API_CONTEXT_ROOT}/{API_VERSION}/status'
API_ENDPOINTS = [DATASTORE_PATH, STATUS_PATH]

API_INGEST_COMPONENT = 'web_api'
JSON_POST_INGEST_METHOD = 'json_post'
BINARY_POST_INGEST_METHOD = 'binary_post'
TEXT_POST_INGEST_METHOD = 'text_post'
URL_POST_INGEST_METHOD = 'url_post'

@LoggingHelper.addLogger
class StatusController(Resource):
    def get(self):
        return make_response({'status': 'ok'}, 200)
        #return make_response(jsonify({'status': 'ok'}), 200)

dataPayloadJsonParser = reqparse.RequestParser()
dataPayloadJsonParser.add_argument('uid', type=str, required=False)
dataPayloadJsonParser.add_argument('time', type=int, required=False)
dataPayloadJsonParser.add_argument('channels', type=dict, required=True, help='channels parameter is mandatory')
@LoggingHelper.addLogger
class DataController(Resource):
    def post(self, deviceId=None):
        if not request.json:
            abort(400, message='Missing JSON payload !')

        jsonUid = request.json.get('uid', None)
        if deviceId and jsonUid:
            if jsonUid and deviceId and not jsonUid == deviceId:
                abort(409, error='Inconsistent Device UID between JSON and URL !')
        elif not deviceId and jsonUid:
            deviceId = jsonUid
    
        defaultTime = request.json.get('time', None)
        if defaultTime:
            defaultTime = datetime.utcfromtimestamp(defaultTime)

        channels = request.json.get('channels', {})

        dataChannels = []
        buildErrors = []
        for (channelId, channel) in channels.items():
            channelTime = channel.get('time', None)
            if channelTime:
                channelTime = datetime.utcfromtimestamp(channelTime)
            dataQuantities = []
            for (key, values) in channel.items():
                try:
                    dq = DataQuantity(key, values)
                    dataQuantities.append(dq)
                except (ValueError, TypeError) as e:
                    #FIXME
                    buildErrors.append(e)
                    pass
            try:
                dc = DataChannel(int(channelId), dataQuantities, channelTime)
                dataChannels.append(dc)
            except (ValueError, TypeError) as e:
                #FIXME
                buildErrors.append(e)
                pass
        try:
            gp = DataPayload(dataChannels, defaultTime)
        except Exception as e:
            messages = [getattr(e, 'message', repr(e))]
            for e in buildErrors:
                messages.append(getattr(e, 'message', repr(e)))
            abort(400, message='Unable to process inconsistent data ! Messages were: %s' % str(messages))

        try:
            IotService.storeDataPayload(deviceId, gp, API_INGEST_COMPONENT, JSON_POST_INGEST_METHOD)
        except KeyError as e:
            message = getattr(e, 'message', repr(e))
            return abort(404, error=message)

lppJsonParser = reqparse.RequestParser()
lppJsonParser.add_argument('uid', required=False)
lppJsonParser.add_argument('lpp', required=True, help='lpp parameter is mandatory')
@LoggingHelper.addLogger
class LppController(Resource):
    def post(self, deviceId=None, base64Lpp=None):
        #if (deviceId):
        #    self.__log.info("Posted LPP for device #%s", deviceId)

        if (deviceId and base64Lpp):
            if (request.content_type and (
                request.content_type.startswith('application/json')
                or request.content_type.startswith('application/octet-stream'))):
                abort(400, message='Inconsistent content-type Header with URL posting data !')
            
            return self._processPostedUrl(deviceId, base64Lpp)
        
        elif(deviceId and not request.content_type):
            return self._processPostedText(deviceId)

        elif (request.content_type and request.content_type.startswith('application/json')):
            return self._processPostedJson(deviceId)

        elif (request.content_type and request.content_type.startswith('application/octet-stream')):
            return self._processPostedBinary(deviceId)

        abort(400, error='Unable to process the request ! You may have supplied a bad combinaison of data and headers.')

    def _processLpp(self, deviceId, lpp, ingestComponent, ingestMethod):
        self.__log.info("Received an LPP for device: %s", deviceId)

        if (deviceId and lpp):
            try:
                IotService.storeLppByDeviceUid(deviceId, lpp, ingestComponent, ingestMethod)
                return {"message": "LPP stored."}
            except KeyError as e:
                message = getattr(e, 'message', repr(e))
                return abort(404, error=message)

        return abort(400, error='Device UID or LPP data are missing !')
    
    def _processPostedUrl(self, deviceId, lpp):
        if not EncodingHelper.isUrlSafeBase64(lpp):
            abort(400, error='Supplied URL LPP: [%s] is not URL safe base64 encoded !' % lpp)
        return self._processLpp(deviceId, lpp, API_INGEST_COMPONENT, URL_POST_INGEST_METHOD)

    def _processPostedBinary(self, deviceId):
        lppBinaryPayload = request.data
        # lppPayload = ''.join(["%02x" % char for char in lppData])
        # log.info("Hex LPP in string format: %s" % lppPayload)
        return self._processLpp(deviceId, lppBinaryPayload, API_INGEST_COMPONENT, BINARY_POST_INGEST_METHOD)
    
    def _processPostedText(self, deviceId):
        lppTextPayload = request.data.decode("ascii")
        if not EncodingHelper.isBase64(lppTextPayload):
            abort(400, error='Supplied Text LPP: [%s] is not base64 encoded !' % lppTextPayload)
        return self._processLpp(deviceId, lppTextPayload, API_INGEST_COMPONENT, TEXT_POST_INGEST_METHOD)

    def _processPostedJson(self, deviceId):
        #args = lppJsonParser.parse_args(strict=True)
        #self.__log.info("JSON payload: %s", json.dumps(args))
        jsonUid = request.json.get('uid', deviceId)
        if jsonUid and deviceId and not jsonUid == deviceId:
            abort(409, error='Inconsistent Device UID between JSON and URL !')

        if not jsonUid:
            abort(400, error='No Device UID supplied !')

        jsonLpp = request.json.get('lpp', None)
        if not EncodingHelper.isBase64(jsonLpp):
            abort(400, error='Supplied JSON LPP: [%s] is not base64 encoded !' % jsonLpp)

        return self._processLpp(jsonUid, jsonLpp, API_INGEST_COMPONENT, JSON_POST_INGEST_METHOD)

deviceParser = reqparse.RequestParser(bundle_errors=True)
deviceParser.add_argument('uid', type=str, required=True, help='uid is missing')
deviceParser.add_argument('name', type=str, required=False)
deviceParser.add_argument('location', type=str, required=False)
deviceParser.add_argument('mapping', type=dict, required=False)
deviceParser.add_argument('tags', type=dict, required=False)
deviceFiedls = {
    "uid": fields.String,
    "name": fields.String,
    "location": fields.String,
    "maxSecondsBackInTime": fields.Integer,
    "maxSecondsForwardInFutur": fields.Integer
}
@LoggingHelper.addLogger
class DevicesController(Resource):

    #@marshal_with(deviceFiedls)
    def get(self, deviceId=None):
        #self.__log.info('GET on devices')
        if (deviceId):
            device = IotService.findDeviceById(deviceId)
            if (device):
                return make_response(jsonify(device), 200)
            else:
                abort(404, error="Device not found !")
        else:
            devices = IotService.findAllDevice()
            return make_response(jsonify(IotService.findAllDevice()), 200)

    @JsonHelper.required_params({"uid": str}, {"name": str, "location": str, "maxSecondsBackInTime": int, "maxSecondsForwardInFutur": int})
    def post(self, deviceId=None):
        #self.__log.info('POST on devices with Content-Type: %s', request.headers.get('Content-Type'))
        newDevice = None

        if (deviceId):
            #self.__log.info('deviceId supplied in URL')
            existingDevice = IotService.findDeviceById(deviceId)
            if existingDevice:
                abort(409, error="Device already exists !")
           
            try:
                newDevice = IotService.createDevice(deviceId)
            except NotSupportedUid:
                abort(400, error="Supplied UID is not supported !")
        else:
            self.__log.info('deviceId not supplied in URL')
            jsonObj = request.validated_json
            if jsonObj:
                deviceId = jsonObj['uid']

                existingDevice: db_models.Device = IotService.findDeviceById(deviceId)
                if existingDevice:
                    # update device
                    if 'name' in jsonObj:
                        existingDevice.name = jsonObj['name']
                    if 'location' in jsonObj:
                        existingDevice.location = jsonObj['location']
                    if 'maxSecondsBackInTime' in jsonObj:
                        existingDevice.maxSecondsBackInTime = jsonObj['maxSecondsBackInTime']
                    if 'maxSecondsForwardInFutur' in jsonObj:
                        existingDevice.maxSecondsForwardInFutur = jsonObj['maxSecondsForwardInFutur']
                    
                    newDevice = IotService.updateDevice(existingDevice)
                else:
                    # create device
                    try:
                        newDevice = IotService.createDevice(deviceId, jsonObj.get('name', None), jsonObj.get('location', None), jsonObj.get('maxSecondsBackInTime', None), jsonObj.get('maxSecondsForwardInFutur', None))
                    except NotSupportedUid:
                        abort(400, error="Supplied UID is not supported !")
                        
        if newDevice:
            return make_response(jsonify(newDevice), 200)
        else:
            return make_response('Unable to add nor update device !', 400)

    def delete(self, deviceId=None):
        if (not deviceId):
            abort(400, error="You must supply a device Id !")

        device = IotService.findDeviceById(deviceId)
        if (not device):
            abort(404, error="Device does not exists !")

        IotService.deleteDevice(deviceId)

        return make_response(jsonify(device), 200)


class DeviceTagsController(Resource):
    def get(self, deviceId, tagKey=None, tagValue=None):
        if tagValue:
            return abort(400, error='Tag value should not be supplied on get !')

        device = IotService.findDeviceById(deviceId)
        if (not device):
            return abort(404, error="Device does not exists !")

        if (tagKey):
            tagValue = device.getDeviceTagValue(tagKey)
            if tagValue:
                return make_response({tagKey: tagValue}, 200)
            else:
                return abort(404, error="Device tag does not exists !")
        else:
            return make_response(device.getDeviceTags(), 200)
    
    def post(self, deviceId, tagKey=None, tagValue=None):
        device = IotService.findDeviceById(deviceId)
        if (not device):
            return abort(404, error="Device does not exists !")

        if (tagKey and tagValue):
            device.addDeviceTag(tagKey, tagValue)
            IotService.commitDbSession()
            return make_response({tagKey: tagValue}, 200)
        
        return abort(400, error="Error posting device tag !")

    def delete(self, deviceId=None, tagKey=None, tagValue=None):
        if tagValue:
            return abort(400, error='Tag value should not be supplied on delete !')

        device = IotService.findDeviceById(deviceId)
        if (not device):
            return abort(404, error="Device does not exists !")

        if (tagKey):
            tagValue = device.getDeviceTagValue(tagKey)
            if tagValue:
                device.removeDeviceTag(tagKey)
                IotService.commitDbSession()
                return make_response({tagKey: tagValue}, 200)
            else:
                return abort(404, error="Device tag does not exists !")
            

        return abort(400, error="Error deleting tag !")


tagsJsonParser = reqparse.RequestParser()
tagsJsonParser.add_argument(db_models.DEVICE_NS, type=dict, required=False)
tagsJsonParser.add_argument(db_models.CHANNELS_NS, type=dict, required=False)
tagsJsonParser.add_argument(db_models.DEFAULT_QUANTITIES_NS, type=dict, required=False)
tagsJsonParser.add_argument(db_models.QUANTITIES_BY_CHANNEL_NS, type=dict, required=False)
@LoggingHelper.addLogger
class TagsController(Resource):
    def get(self, deviceId):
        device = IotService.findDeviceById(deviceId)
        if (not device):
            return abort(404, error="Device does not exists !")

        allTags = device.getAllTags()

        return make_response(allTags, 200)
    
    def post(self, deviceId):
        device:db_models.Device = IotService.findDeviceById(deviceId)
        if (not device):
            return abort(404, error="Device does not exists !")

        args = tagsJsonParser.parse_args()
        deviceTags:dict = args[db_models.DEVICE_NS]
        channelsTags:dict = args[db_models.CHANNELS_NS]
        defaultQuantitiesTags:dict = args[db_models.DEFAULT_QUANTITIES_NS]
        byChannelQuantitiesTags:dict = args[db_models.QUANTITIES_BY_CHANNEL_NS]
        if not deviceTags and not channelsTags and not defaultQuantitiesTags and not byChannelQuantitiesTags:
            return abort(400, error="No tag data to parse !")

        errorsList = []
        try:
            device.clearTags()
            if deviceTags:
                for (key, value) in deviceTags.items():
                    device.addDeviceTag(key, value)
            if channelsTags:
                for (channel, tags) in channelsTags.items():
                    for (key, value) in tags.items():
                        device.addChannelTag(int(channel), key, value)
            if defaultQuantitiesTags:
                for (quantity, tags) in defaultQuantitiesTags.items():
                    for (key, value) in tags.items():
                        device.addDefaultQuantityTag(quantity, key, value)
            if byChannelQuantitiesTags:
                for (channel, quantitiesTags) in byChannelQuantitiesTags.items():
                    for (quantity, tags) in quantitiesTags.items():
                        for (key, value) in tags.items():
                            device.addQuantityTag(int(channel), quantity, key, value)

            allTags = device.getAllTags()
            IotService.commitDbSession()
            return make_response(allTags, 200)
        except (ValueError, TypeError) as e:
            #FIXME
            #raise e
            self.__log.warning('Format error in tags ! %s', getattr(e, 'message', repr(e)))
            errorsList.append(e)
        
        if len(errorsList) > 0:
            messages = []
            for e in errorsList:
                messages.append(getattr(e, 'message', repr(e)))
            return abort(400, message='Unable to process inconsistent data ! Messages were: %s' % str(messages))
        
        return abort(400, error="No tag data posted !")

    def delete(self, deviceId=None):
        device = IotService.findDeviceById(deviceId)
        if (not device):
            return abort(404, error="Device does not exists !")

        try:
            deletedTags = device.getAllTags()
            device.clearTags()
            IotService.commitDbSession()
            return make_response(deletedTags, 200)
        except Exception as e:
            self.__log.warning('Error deleting Tags !')
            message = getattr(e, 'message', repr(e))
            return abort(500, error=message)


def loadApi(app):
    __log.info('API endpoints: %s', API_ENDPOINTS)

    api = Api(app, errors=None)
    api.add_resource(StatusController, STATUS_PATH)
    api.add_resource(DataController, f'{DATASTORE_PATH}/data', f'{DATASTORE_PATH}/data/', f'{DATASTORE_PATH}/data/<deviceId>', 
        f'{DATASTORE_PATH}/data/<deviceId>/')
    api.add_resource(LppController, f'{DATASTORE_PATH}/lpp', f'{DATASTORE_PATH}/lpp/', f'{DATASTORE_PATH}/lpp/<deviceId>', 
        f'{DATASTORE_PATH}/lpp/<deviceId>/', f'{DATASTORE_PATH}/lpp/<deviceId>/<base64Lpp>', f'{DATASTORE_PATH}/lpp/<deviceId>/<base64Lpp>/')
    api.add_resource(DevicesController, f'{DATASTORE_PATH}/devices', f'{DATASTORE_PATH}/devices/', f'{DATASTORE_PATH}/devices/<deviceId>')
    api.add_resource(TagsController, f'{DATASTORE_PATH}/devices/<deviceId>/tags', f'{DATASTORE_PATH}/devices/<deviceId>/tags/')
    api.add_resource(DeviceTagsController, f'{DATASTORE_PATH}/devices/<deviceId>/deviceTags', f'{DATASTORE_PATH}/devices/<deviceId>/deviceTags/', 
        f'{DATASTORE_PATH}/devices/<deviceId>/deviceTags/<tagKey>', f'{DATASTORE_PATH}/devices/<deviceId>/deviceTags/<tagKey>/', 
        f'{DATASTORE_PATH}/devices/<deviceId>/deviceTags/<tagKey>/<tagValue>')

    app.json_encoder = JsonHelper.CustomJSONEncoder


    @api.errorhandler(NotValidDataTime)
    def handle_NotValidDataTime_exception(error):
        return {'message': error.err}, 409
        
    @api.errorhandler(NotProcessablePayload)
    def handle_NotProcessablePayload_exception(error):
        return {'message': error.err}, 501


    @app.before_first_request
    def before_first_request():
        pass
        # admin = User(username='admin', email='admin@example.com')
        # guest = User(username='guest', email='guest@example.com')

        # try:
        #     db.session.add(admin)
        #     db.session.add(guest)
        #     db.session.commit()
        # except Exception as e:
        #     db.session.rollback()
        #     __log.error('Error during DB commit !', exc_info=1)

        # __log.info("query.all(): %s", User.query.all())
        # __log.info("query.filter_by(): %s", User.query.filter_by(username='admin').first())



