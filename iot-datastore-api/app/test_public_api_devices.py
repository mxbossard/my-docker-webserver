from operator import pos
import os
import re
import logging
import tempfile
import copy
import base64
import pytest
import flask
from flask import json
from pytest_mock import MockerFixture
import iot_datastore.iot_service as IotService
import iot_datastore.db_models as DbModels
import public_api as IotStorageApi
from iot_datastore.exceptions import NotProcessablePayload
from iot_datastore import iot_tags
from test_public_api_commons import *


__log = logging.getLogger(__name__)

def buildExpectedDeviceRepresentation(device):
    return {
            "uid": device.uid, 
            "name": device.name,
            "location": device.location,
            "maxSecondsBackInTime": device.maxSecondsBackInTime, 
            "maxSecondsForwardInFutur": device.maxSecondsForwardInFutur
    }

#@pytest.mark.skip(reason="test")
def testGetEmptyDevicesList(app, client):
    rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices')
    assertJsonResponse('Retrieving empty device liste', rv, [])

    rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/')
    assertJsonResponse('Retrieving empty device liste', rv, [])

def testGetOneDeviceList(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.commit()

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices')

        assert 200 == rv.status_code, 'Should found one device !'
        content = '"uid": "%s"' % DEVICE_1.uid
        assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the requested device UID !'

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/')

        assert 200 == rv.status_code, 'Should found one device !'
        expectedResponse = [buildExpectedDeviceRepresentation(DEVICE_1)]
        assert expectedResponse == json.loads(rv.data), 'Bad HTTP body response! '

def testGetMultipleDeviceList(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_2))
        DbModels.db.session.add(copy.deepcopy(DEVICE_3))
        DbModels.db.session.commit()

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices')

        assert 200 == rv.status_code, 'Should found multiple devices !'

        expectedResponse = [
            buildExpectedDeviceRepresentation(DEVICE_2), 
            buildExpectedDeviceRepresentation(DEVICE_3)
        ]
        assert expectedResponse == json.loads(rv.data), 'Bad HTTP body response! '

def testGetEmptyDevice(app, client):
    rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid)
    assert 404 == rv.status_code, 'Should not found queried devices !'

#@pytest.mark.skip(reason="test")
def testGetNotExistingDevice(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.commit()

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + DEVICE_2.uid)
        assert 404 == rv.status_code, 'Should not found not existing device !'

def testGetUniquelyExistingDevice(app, client):
    with app.app_context():
        device1 = copy.deepcopy(DEVICE_1)
        DbModels.db.session.add(device1)
        DbModels.db.session.commit()

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid)
        assert 200 == rv.status_code, 'Should found unique queried device !'

        expectedResponse = buildExpectedDeviceRepresentation(DEVICE_1)
        assert expectedResponse == json.loads(rv.data), 'Bad HTTP body response! '

#@pytest.mark.skip(reason="test")
def testGetMultipleExistingDevice(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.add(copy.deepcopy(DEVICE_2))
        DbModels.db.session.add(copy.deepcopy(DEVICE_3))
        DbModels.db.session.commit()

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + DEVICE_2.uid)
        assert 200 == rv.status_code, 'Should found queried device !'

        expectedResponse = buildExpectedDeviceRepresentation(DEVICE_2)
        assert expectedResponse == json.loads(rv.data), 'Bad HTTP body response! '

def testPostNewDevicesByUid(app, client):
    with app.app_context():
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid)
        assert 200 == rv.status_code, 'Device 1 should be posted !'
        expectedResponse = buildExpectedDeviceRepresentation(DbModels.Device(DEVICE_1.uid))
        assert expectedResponse == json.loads(rv.data), 'Bad HTTP body response! '

        postedDevice = IotService.findDeviceById(DEVICE_1.uid)
        assert postedDevice, 'Posted device should be stored in DB !'
        assert DEVICE_1.uid == postedDevice.uid, 'Posted device should have the supplied UID'
        assert None == postedDevice.name, 'Posted device should not have a name !'
        assert None == postedDevice.location, 'Posted device should not have a location !'

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_2.uid)
        assert 200 == rv.status_code, 'Device 2 should be posted !'
        expectedResponse = buildExpectedDeviceRepresentation(DbModels.Device(DEVICE_2.uid))
        assert expectedResponse == json.loads(rv.data), 'Bad HTTP body response! '

        postedDevice = IotService.findDeviceById(DEVICE_2.uid)
        assert postedDevice, 'Posted device should be stored in DB !'
        assert DEVICE_2.uid == postedDevice.uid, 'Posted device should have the supplied UID'
        assert None == postedDevice.name, 'Posted device should not have a name !'
        assert None == postedDevice.location, 'Posted device should not have a location !'

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid)
        assert 409 == rv.status_code, 'Response should be an error on duplicate insertion attempt !'

def testPostNewDevicesWithBadlyFormattedUid(app, client):
    with app.app_context():

        def checkBadUid(label, badUid):
            rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + badUid)
            assert 400 == rv.status_code, f'Device with badUid {label} should not be posted !'

        def checkGoodUid(label, goodUid, expected):
            rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + goodUid)
            assert 200 == rv.status_code, f'Device with goodUid {label} should be posted !'
            # content = '"uid": "%s"' % expected
            # assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the expected device UID !'
            expectedResponse = buildExpectedDeviceRepresentation(DbModels.Device(goodUid))
            assert expectedResponse == json.loads(rv.data), 'Bad HTTP body response! '

        checkBadUid('badUid0', '')
        checkBadUid('badUid1', '42+')
        checkBadUid('badUid2', '42%20')
        checkBadUid('badUid3', '42!')
        checkBadUid('badUid4', '?')
        checkBadUid('badUid5', '42,')
        checkBadUid('badUid6', '42;')
        checkBadUid('badUid7', '42 a')
        checkBadUid('badUid8', '42Z_')
        checkBadUid('badUid9', '42-9~')
        checkBadUid('badUid10', '42-9.')
        checkBadUid('badUid11', '42-9-')
        checkBadUid('badUid12', '42-')
        checkBadUid('badUid13', '_')
        checkBadUid('badUid14', '_a')
        checkBadUid('badUid15', 'a_')
        checkBadUid('badUid16', '__')
        checkBadUid('badUid17', '__a')
        checkBadUid('badUid18', 'a__')
        checkBadUid('badUid19', 'abc_4_e_5_Z')
        checkBadUid('badUid20', 'abcD')
        checkBadUid('badUid21', 'Foo')

        checkGoodUid('goodUid1', '42', '42')
        checkGoodUid('goodUid2', '42az', '42az')
        checkGoodUid('goodUid4', 'abc_4', 'abc_4')
        checkGoodUid('goodUid5', 'abc_4_d', 'abc_4_d')
        checkGoodUid('goodUid6', 'abc_4_e_5_z', 'abc_4_e_5_z')

def testPostNewDevicesWithJsonPayload(app, client):
    with app.app_context():
        jsonPayload = DEVICE_1
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices', json=jsonPayload)
        assert 200 == rv.status_code, 'Device 1 should be posted !'
        # content = '"uid": "%s"' % DEVICE_1.uid
        # assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the posted device UID !'
        expectedResponse = buildExpectedDeviceRepresentation(DEVICE_1)
        assert expectedResponse == json.loads(rv.data), 'Bad HTTP body response! '
        

        postedDevice = IotService.findDeviceById(DEVICE_1.uid)
        assert postedDevice, 'Posted device should be stored in DB !'
        assert DEVICE_1.uid == postedDevice.uid, 'Posted device should have the supplied UID'
        assert DEVICE_1.name == postedDevice.name, 'Posted device should have the supplied name !'
        assert DEVICE_1.location == postedDevice.location, 'Posted device should have the supplied location !'

        jsonPayload = DEVICE_2
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/', json=jsonPayload)
        assert 200 == rv.status_code, 'Device 2 should be posted !'
        # content = '"uid": "%s"' % DEVICE_2.uid
        # assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the posted device UID !'
        expectedResponse = buildExpectedDeviceRepresentation(DEVICE_2)
        assert expectedResponse == json.loads(rv.data), 'Bad HTTP body response! '

        postedDevice = IotService.findDeviceById(DEVICE_2.uid)
        assert postedDevice, 'Posted device should be stored in DB !'
        assert DEVICE_2.uid == postedDevice.uid, 'Posted device should have the supplied UID'
        assert DEVICE_2.name == postedDevice.name, 'Posted device should have a name !'
        assert DEVICE_2.location == postedDevice.location, 'Posted device should have a location !'

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid)
        assert 409 == rv.status_code, 'Response should be an error on duplicate insertion attempt !'

def testPostNewDevicesWithBadlyFormattedUidInJsonPayload(app, client):
    with app.app_context():

        def checkBadUid(label, badUid):
            payload = {
                "uid": badUid
            }
            rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/', json=payload)
            assert 400 == rv.status_code, f'Device with badUid {label} should not be posted !'

        def checkGoodUid(label, goodUid, expected):
            payload = {
                "uid": goodUid
            }
            rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/', json=payload)
            assert 200 == rv.status_code, f'Device with goodUid {label} should be posted !'
            # content = '"uid": "%s"' % expected
            # assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the expected device UID !'
            expectedResponse = buildExpectedDeviceRepresentation(DbModels.Device(goodUid))
            assert expectedResponse == json.loads(rv.data), 'Bad HTTP body response! '

        checkBadUid('badUid0', '')
        checkBadUid('badUid1', '42+')
        checkBadUid('badUid2', '42%20')
        checkBadUid('badUid3', '42!')
        checkBadUid('badUid4', '?')
        checkBadUid('badUid5', '42,')
        checkBadUid('badUid6', '42;')
        checkBadUid('badUid7', '42 a')
        checkBadUid('badUid8', '42Z_')
        checkBadUid('badUid9', '42-9~')
        checkBadUid('badUid10', '42-9.')
        checkBadUid('badUid11', '42-9-')
        checkBadUid('badUid12', '42-')
        checkBadUid('badUid13', '_')
        checkBadUid('badUid14', '_a')
        checkBadUid('badUid15', 'a_')
        checkBadUid('badUid16', '__')
        checkBadUid('badUid17', '__a')
        checkBadUid('badUid18', 'a__')
        checkBadUid('badUid19', 'abc_4_e_5_Z')
        checkBadUid('badUid20', 'abcD')
        checkBadUid('badUid21', 'Foo')

        checkGoodUid('goodUid1', '42', '42')
        checkGoodUid('goodUid2', '42az', '42az')
        checkGoodUid('goodUid4', 'abc_4', 'abc_4')
        checkGoodUid('goodUid5', 'abc_4_d', 'abc_4_d')
        checkGoodUid('goodUid6', 'abc_4_e_5_z', 'abc_4_e_5_z')


def testPostDeviceUpdateWithJsonPayload(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.add(copy.deepcopy(DEVICE_2))
        DbModels.db.session.add(copy.deepcopy(DEVICE_3))
        DbModels.db.session.commit()

        jsonPayload = copy.deepcopy(DEVICE_1)
        newName = "newName"
        newLocation = None
        newTiming = 42
        jsonPayload.name = newName
        jsonPayload.location = newLocation
        jsonPayload.maxSecondsBackInTime = newTiming
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices', json=jsonPayload)
        assert 200 == rv.status_code, 'Device 1 should be posted !'
        # content = '"uid": "%s"' % DEVICE_1.uid
        # assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the posted device UID !'
        expectedResponse = buildExpectedDeviceRepresentation(jsonPayload)
        assert expectedResponse == json.loads(rv.data), 'Bad HTTP body response! '

        postedDevice = IotService.findDeviceById(DEVICE_1.uid)
        assert postedDevice, 'Posted device should be stored in DB !'
        assert DEVICE_1.uid == postedDevice.uid, 'Posted device should have the supplied UID'
        assert newName == postedDevice.name, 'Posted device should have the updated name !'
        assert newLocation == postedDevice.location, 'Posted device should have the supplied location !'
        assert newTiming == postedDevice.maxSecondsBackInTime, 'Posted device should have the updated name !'
        assert DEVICE_1.maxSecondsForwardInFutur == postedDevice.maxSecondsForwardInFutur, 'Posted device should have an untouched time !'
        assert { iot_tags.DEVICE_NAME_TAG_KEY: newName } == postedDevice.getDeviceTags()


def testPostNewDeviceWithTimings(app, client):
    with app.app_context():
        jsonPayload = DEVICE_4
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices', json=jsonPayload)
        assert 200 == rv.status_code, 'Device 4 should be posted !'
        # content = '"uid": "%s"' % DEVICE_4.uid
        # assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the posted device UID !'
        expectedResponse = buildExpectedDeviceRepresentation(DEVICE_4)
        assert expectedResponse == json.loads(rv.data), 'Bad HTTP body response! '

        postedDevice = IotService.findDeviceById(DEVICE_4.uid)
        assert postedDevice, 'Posted device should be stored in DB !'
        assert DEVICE_4.uid == postedDevice.uid, 'Posted device should have the supplied UID'
        assert DEVICE_4.name == postedDevice.name, 'Posted device should have supplied name !'
        assert DEVICE_4.location == postedDevice.location, 'Posted device should have supplied location !'
        assert DEVICE_4.maxSecondsBackInTime == postedDevice.maxSecondsBackInTime, 'Posted device should have supplied maxSecondsBackInTime'
        assert DEVICE_4.maxSecondsForwardInFutur == postedDevice.maxSecondsForwardInFutur, 'Posted device should have supplied maxSecondsForwardInFutur'

def testPostNewDevicesWithPartialJsonPayload(app, client):
    with app.app_context():
        deviceUid = 'foo1'
        jsonPayload = {'uid': deviceUid}
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices', json=jsonPayload)
        assert 200 == rv.status_code, 'Device 1 should be posted !'
        # content = '"uid": "%s"' % deviceUid
        # assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the posted device 1 UID !'
        expectedResponse = buildExpectedDeviceRepresentation(DbModels.Device(deviceUid))
        assert expectedResponse == json.loads(rv.data), 'Bad HTTP body response! '

        postedDevice = IotService.findDeviceById(deviceUid)
        assert postedDevice, 'Posted device 1 should be stored in DB !'
        assert deviceUid == postedDevice.uid, 'Posted device 1 should have the supplied UID'
        assert None == postedDevice.name, 'Posted device 1 should not have a name !'
        assert None == postedDevice.location, 'Posted device 1 should not have a location !'

        deviceUid = 'foo2'
        deviceName = 'name2'
        jsonPayload = {'uid': deviceUid, 'name': deviceName}
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices', json=jsonPayload)
        assert 200 == rv.status_code, 'Device 2 should be posted !'
        # content = '"uid": "%s"' % deviceUid
        # assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the posted device 2 UID !'
        expectedResponse = buildExpectedDeviceRepresentation(DbModels.Device(deviceUid, deviceName))
        assert expectedResponse == json.loads(rv.data), 'Bad HTTP body response! '

        postedDevice = IotService.findDeviceById(deviceUid)
        assert postedDevice, 'Posted device 2 should be stored in DB !'
        assert deviceUid == postedDevice.uid, 'Posted device 2 should have the supplied UID'
        assert deviceName == postedDevice.name, 'Posted device 2 should have a name !'
        assert None == postedDevice.location, 'Posted device 2 should not have a location !'

        deviceUid = 'foo3'
        deviceLocation = 'location3'
        jsonPayload = {'uid': deviceUid, 'location': deviceLocation}
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices', json=jsonPayload)
        assert 200 == rv.status_code, 'Device 3 should be posted !'
        # content = '"uid": "%s"' % deviceUid
        # assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the posted device 3 UID !'
        expectedResponse = buildExpectedDeviceRepresentation(DbModels.Device(deviceUid, None, deviceLocation))
        assert expectedResponse == json.loads(rv.data), 'Bad HTTP body response! '

        postedDevice = IotService.findDeviceById(deviceUid)
        assert postedDevice, 'Posted device 3 should be stored in DB !'
        assert deviceUid == postedDevice.uid, 'Posted device 3 should have the supplied UID'
        assert None == postedDevice.name, 'Posted device 3 should not have a name !'
        assert deviceLocation == postedDevice.location, 'Posted device 3 should have a location !'

        deviceUid = 'foo4'
        deviceLocation = 'location4'
        jsonPayload = {'uid': deviceUid, 'location': deviceLocation, 'otherParam': 'otherValue'}
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices', json=jsonPayload)
        assert 200 == rv.status_code, 'Device 4 should be posted !'
        # content = '"uid": "%s"' % deviceUid
        # assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the posted device 4 UID !'
        expectedResponse = buildExpectedDeviceRepresentation(DbModels.Device(deviceUid, None, deviceLocation))
        assert expectedResponse == json.loads(rv.data), 'Bad HTTP body response! '

        postedDevice = IotService.findDeviceById(deviceUid)
        assert postedDevice, 'Posted device 4 should be stored in DB !'
        assert deviceUid == postedDevice.uid, 'Posted device 4 should have the supplied UID'
        assert None == postedDevice.name, 'Posted device 4 should not have a name !'
        assert deviceLocation == postedDevice.location, 'Posted device 4 should have a location !'

        deviceLocation = 'location5'
        jsonPayload = {'location': deviceLocation, 'otherParam': 'otherValue'}
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices', json=jsonPayload)
        assert 400 == rv.status_code, 'Device 5 should not be posted !'

def testPostNewDevicesWithEmptyJsonPayload(app, client):
    with app.app_context():
        jsonPayload = {}
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices', json=jsonPayload)
        assert 400 == rv.status_code, 'Device 1 should not be posted !'

        jsonPayload = None
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices', json=jsonPayload)
        assert 400 == rv.status_code, 'Device 2 should not be posted !'

        jsonPayload = {'data': 'data'}
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices', json=jsonPayload)
        assert 400 == rv.status_code, 'Device 3 should not be posted !'

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices')
        assert 400 == rv.status_code, 'Device 4 should not be posted !'

def testPostNewDevicesWithBadlyTypedJsonPayload(app, client):
    with app.app_context():
        jsonPayload = {'uid': 41}
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices', json=jsonPayload)
        assert 400 == rv.status_code, 'Device 1 should not be posted !'

        jsonPayload = {'uid': '42', 'location': 42}
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices', json=jsonPayload)
        assert 400 == rv.status_code, 'Device 2 should not be posted !'

        jsonPayload = {'uid': '43', 'name': 43}
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices', json=jsonPayload)
        assert 400 == rv.status_code, 'Device 3 should not be posted !'

        jsonPayload = {'uid': '44', 'foo': 44}
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices', json=jsonPayload)
        assert 200 == rv.status_code, 'Device 4 should be posted !'

def testDeleteDeviceWithoutUid(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_2))
        DbModels.db.session.add(copy.deepcopy(DEVICE_3))
        DbModels.db.session.commit()

        rv: flask.wrappers.Response = client.delete(API_CONTEXT_ROOT + '/devices')
        assert 400 == rv.status_code, 'Api should return an error without UID !'

        devices = DbModels.Device.query.all()
        assert 2 == len(devices), 'No device should be deleted !'

        rv: flask.wrappers.Response = client.delete(API_CONTEXT_ROOT + '/devices/')
        assert 400 == rv.status_code, 'Api should return an error without UID !'

        devices = DbModels.Device.query.all()
        assert 2 == len(devices), 'No device should be deleted !'

def testDeleteNotExistingDevice(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_2))
        DbModels.db.session.add(copy.deepcopy(DEVICE_3))
        DbModels.db.session.commit()

        rv: flask.wrappers.Response = client.delete(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid)
        assert 404 == rv.status_code, 'Api should return an error if Device does not exists !'

        devices = DbModels.Device.query.all()
        assert 2 == len(devices), 'No device should be deleted !'

def testDeleteExistingDevice(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_2))
        DbModels.db.session.add(copy.deepcopy(DEVICE_3))
        DbModels.db.session.commit()

        rv: flask.wrappers.Response = client.delete(API_CONTEXT_ROOT + '/devices/' + DEVICE_2.uid)
        assert 200 == rv.status_code, 'Api should return OK on device deletion !'
        expectedResponse = buildExpectedDeviceRepresentation(DEVICE_2)
        assert expectedResponse == json.loads(rv.data), 'Bad HTTP body response! '

        devices = DbModels.Device.query.all()
        assert 1 == len(devices), 'One device should be deleted !'
        assert DEVICE_3.uid == devices[0].uid, 'Wrong device deleted !'
