import os
import re
import logging
import tempfile
import copy
import base64
import pytest
import flask
from flask import json
from pytest_mock import MockerFixture
import iot_datastore.iot_service as IotService
import iot_datastore.db_models as DbModels
import public_api as IotStorageApi
from iot_datastore.exceptions import NotProcessablePayload
from iot_datastore import iot_tags
from test_public_api_commons import *

__log = logging.getLogger(__name__)


def testGetDeviceTags(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.add(copy.deepcopy(DEVICE_WITH_TAGS_2))
        DbModels.db.session.add(copy.deepcopy(DEVICE_WITH_TAGS_3))
        DbModels.db.session.commit()

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + 'NotExistingUid' + '/deviceTags')
        assert 404 == rv.status_code, 'Api should return 404 for not existing Device /deviceTags listing !'

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_2.uid + '/deviceTags')
        assert 200 == rv.status_code, 'Api should return OK for DEVICE_WITH_TAGS_2 /deviceTags listing !'
        for key, val in DEVICE_WITH_TAGS_2.getDeviceTags().items():
            content = '"%s": "%s"' % (key, val)
            assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the tag %s !' % key

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_3.uid + '/deviceTags')
        assert 200 == rv.status_code, 'Api should return OK for DEVICE_WITH_TAGS_3 /deviceTags listing !'
        for key, val in DEVICE_WITH_TAGS_3.getDeviceTags().items():
            content = '"%s": "%s"' % (key, val)
            assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the tag %s !' % key

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_2.uid + '/deviceTags/')
        assert 200 == rv.status_code, 'Api should return OK for DEVICE_WITH_TAGS_2 /deviceTags/ listing !'
        for key, val in DEVICE_WITH_TAGS_2.getDeviceTags().items():
            content = '"%s": "%s"' % (key, val)
            assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the tag %s !' % key

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/deviceTags')
        assert 200 == rv.status_code, 'Api should return OK for /tags listing on Device without tags !'
        expectedTags = {iot_tags.DEVICE_NAME_TAG_KEY: DEVICE_1.name, iot_tags.DEVICE_LOCATION_TAG_KEY: DEVICE_1.location}
        assertJsonResponse('listing device tags without posting tags', rv, expectedTags)

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/deviceTags/notExistingTag')
        assert 404 == rv.status_code, 'Api should return NotFound for /deviceTags/tag evaluating on Device without tags !'

        device2ExistingTagKey = list(DEVICE_WITH_TAGS_2.getDeviceTags().items())[0][0]
        device2ExistingTagValue = list(DEVICE_WITH_TAGS_2.getDeviceTags().items())[0][1]
        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_2.uid + '/deviceTags/' + device2ExistingTagKey)
        assert 200 == rv.status_code, 'Api should return OK for DEVICE_WITH_TAGS_2 /deviceTags/tag evaluating on tagged Device !'
        content = '%s' % device2ExistingTagValue
        assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the tag value %s !' % device2ExistingTagValue

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_2.uid + '/deviceTags/notExistingTag')
        assert 404 == rv.status_code, 'Api should return NotFound for DEVICE_WITH_TAGS_2 /deviceTags/tag evaluating on not existing tag !'

        devices = DbModels.Device.query.all()
        assert 3 == len(devices), "Tag listing should not changed the device count in DB !"

def testPostDeviceTags(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.add(copy.deepcopy(DEVICE_2))
        DbModels.db.session.add(copy.deepcopy(DEVICE_WITH_TAGS_3))
        DbModels.db.session.commit()

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + 'unknownDevice' + '/deviceTags')
        assert 404 == rv.status_code, 'Api should return NotFound for posting device tags on not existing Device !'

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/deviceTags')
        assert 400 == rv.status_code, 'Api should return Error for post on /deviceTags without tag data !'

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/deviceTags/')
        assert 400 == rv.status_code, 'Api should return Error for post on /deviceTags/ without tag data !'

        newTagKey1 = 'newTagKey1'
        newTagVal1 = 'newTagVal1'
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/deviceTags/' + newTagKey1 + '/' + newTagVal1)
        assertJsonResponse('posting new tag 1', rv, {newTagKey1: newTagVal1})

        devices = DbModels.Device.query.all()
        device1: DbModels.Device = devices[0]
        assert device1.getDeviceTagValue(newTagKey1) == newTagVal1, 'New Tag is not persisted in DB !'

        newTagKey2 = '2'
        newTagVal2 = '2'
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/deviceTags/' + newTagKey2 + '/' + newTagVal2)
        assertJsonResponse('posting new tag 2', rv, {newTagKey1: newTagVal1, newTagKey2: newTagVal2})

        devices = DbModels.Device.query.all()
        device1: DbModels.Device = devices[0]
        assert device1.getDeviceTagValue(str(newTagKey2)) == str(newTagVal2), 'Numbered tag should be transforme to string !'

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/deviceTags/' + newTagKey1 + '/' + newTagVal2)
        assertJsonResponse('replacing tag value 1', rv, {newTagKey1: newTagVal2, newTagKey2: newTagVal2})

        devices = DbModels.Device.query.all()
        device1: DbModels.Device = devices[0]
        assert device1.getDeviceTagValue(newTagKey1) == str(newTagVal2), "Tag should have get it's value updated !"

        devices = DbModels.Device.query.all()
        device3: DbModels.Device = devices[2]
        assert len(device3.getTagsDict().items()) == len(DEVICE_WITH_TAGS_3.getDeviceTags().items()), 'Original tag not stored in DB !'

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_3.uid + '/deviceTags/' + newTagKey1 + '/' + newTagVal1)
        assertJsonResponse('posting new tag 3', rv, {**DEVICE_WITH_TAGS_3.getDeviceTags(), newTagKey1: newTagVal1})

        devices = DbModels.Device.query.all()
        device3: DbModels.Device = devices[2]
        assert device3.getDeviceTagValue(newTagKey1) == newTagVal1, 'New Tag is not persisted in DB !'
        assert len(device3.getDeviceTags().items()) == len(DEVICE_WITH_TAGS_3.getDeviceTags().items()) + 1, 'Original tag not stored anymore in DB !'

        devices = DbModels.Device.query.all()
        assert 3 == len(devices), "Tag posting should not changed the device count in DB !"

def testDeleteDeviceTags(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_WITH_TAGS_1))
        DbModels.db.session.add(copy.deepcopy(DEVICE_2))
        DbModels.db.session.add(copy.deepcopy(DEVICE_WITH_TAGS_3))
        DbModels.db.session.commit()

        rv: flask.wrappers.Response = client.delete(API_CONTEXT_ROOT + '/devices/' + 'unknownDevice' + '/deviceTags')
        assert 404 == rv.status_code, 'Api should return NotFound for unknown device tags deletion !'

        rv: flask.wrappers.Response = client.delete(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_1.uid + '/deviceTags')
        assert 400 == rv.status_code, 'Api should return Error for delete on /tags without tag data !'

        rv: flask.wrappers.Response = client.delete(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_1.uid + '/deviceTags/')
        assert 400 == rv.status_code, 'Api should return Error for delete on /tags/ without tag data !'

        rv: flask.wrappers.Response = client.delete(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_1.uid + '/deviceTags/notExistingTag')
        assert 404 == rv.status_code, 'Api should return NotFound for unknown tag deletion !'

        device1TagKey = list(DEVICE_WITH_TAGS_1.getDeviceTags().items())[0][0]
        device1TagVal = list(DEVICE_WITH_TAGS_1.getDeviceTags().items())[0][1]
        rv: flask.wrappers.Response = client.delete(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_1.uid + '/deviceTags/' + device1TagKey + '/')
        assertJsonResponse('deleting tag 1', rv, {device1TagKey: device1TagVal})

        rv: flask.wrappers.Response = client.delete(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_1.uid + '/deviceTags/' + device1TagKey + '/foo')
        assert 400 == rv.status_code, 'Api should return an Error if an extra param is added !'
        
        rv: flask.wrappers.Response = client.delete(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_1.uid + '/deviceTags/' + device1TagKey)
        assert 404 == rv.status_code, 'Api should return an Error if no tag to delete !'

        device3TagKey = list(DEVICE_WITH_TAGS_3.getDeviceTags().items())[0][0]
        device3TagVal = list(DEVICE_WITH_TAGS_3.getDeviceTags().items())[0][1]
        rv: flask.wrappers.Response = client.delete(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_3.uid + '/deviceTags/' + device3TagKey)
        assertJsonResponse('deleting tag 2', rv, {device3TagKey: device3TagVal})

        devices = DbModels.Device.query.all()
        assert 3 == len(devices), "Tag deletion should not changed the device count in DB !"


def testGetTags(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.add(copy.deepcopy(DEVICE_WITH_TAGS_2))
        DbModels.db.session.add(copy.deepcopy(DEVICE_WITH_TAGS_3))
        DbModels.db.session.commit()

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + 'NotExistingUid' + '/tags')
        assert 404 == rv.status_code, 'Api should return 404 for not existing Device /tags listing !'

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_2.uid + '/tags')
        assert 200 == rv.status_code, 'Api should return OK for DEVICE_WITH_TAGS_2 /tags listing !'
        for key, val in DEVICE_WITH_TAGS_2.getDeviceTags().items():
            content = '"%s": "%s"' % (key, val)
            assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the tag %s !' % key

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_3.uid + '/tags')
        assert 200 == rv.status_code, 'Api should return OK for DEVICE_WITH_TAGS_3 /tags listing !'
        for key, val in DEVICE_WITH_TAGS_3.getDeviceTags().items():
            content = '"%s": "%s"' % (key, val)
            assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the tag %s !' % key

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_2.uid + '/tags/')
        assert 200 == rv.status_code, 'Api should return OK for DEVICE_WITH_TAGS_2 /tags/ listing !'
        for key, val in DEVICE_WITH_TAGS_2.getDeviceTags().items():
            content = '"%s": "%s"' % (key, val)
            assert bytes(content, 'UTF-8') in rv.data, 'Response should contain the tag %s !' % key

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/tags')
        assert 200 == rv.status_code, 'Api should return OK for /tags listing on Device without tags !'
        expectedTags = {DbModels.DEVICE_NS: {iot_tags.DEVICE_NAME_TAG_KEY: DEVICE_1.name, iot_tags.DEVICE_LOCATION_TAG_KEY: DEVICE_1.location}}
        assertJsonResponse('listing tags without posting tags', rv, expectedTags)

        rv: flask.wrappers.Response = client.get(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/tags/notExistingTag')
        assert 404 == rv.status_code, 'Api should return NotFound for /tags/tag evaluating on Device without tags !'

        devices = DbModels.Device.query.all()
        assert 3 == len(devices), "Tag listing should not changed the device count in DB !"

def testPostDeviceTags(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.add(copy.deepcopy(DEVICE_2))
        DbModels.db.session.add(copy.deepcopy(DEVICE_WITH_TAGS_3))
        DbModels.db.session.commit()

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + 'unknownDevice' + '/tags')
        assert 404 == rv.status_code, 'Api should return NotFound for posting tags on not existing Device !'

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/tags')
        assert 400 == rv.status_code, 'Api should return Error for post on /tags without tag data !'

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/tags/')
        assert 400 == rv.status_code, 'Api should return Error for post on /tags/ without tag data !'

        newTagKey1 = 'newTagKey1'
        newTagVal1 = 'newTagVal1'
        newTags1 = {"device": {newTagKey1: newTagVal1}}
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/tags/', json=newTags1)
        assertJsonResponse('posting newTags1', rv, newTags1)

        devices = DbModels.Device.query.all()
        device1: DbModels.Device = devices[0]
        assert device1.getDeviceTagValue(newTagKey1) == newTagVal1, 'New Tag is not persisted in DB !'

        newTagKey2 = '2'
        newTagVal2 = '2'
        newTags2 = {"device": {newTagKey2: newTagVal2}}
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/tags/', json=newTags2)
        assertJsonResponse('replacing by newTags2', rv, newTags2)

        devices = DbModels.Device.query.all()
        device1: DbModels.Device = devices[0]
        assert device1.getDeviceTagValue(str(newTagKey2)) == str(newTagVal2), 'Numbered tag should be transforme to string !'

        newTags3 = {"device": {newTagKey1: newTagVal2, newTagKey2: newTagVal2}}
        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/tags', json=newTags3)
        assertJsonResponse('replacing by newTags3', rv, newTags3)

        devices = DbModels.Device.query.all()
        device1: DbModels.Device = devices[0]
        assert device1.getDeviceTagValue(newTagKey1) == str(newTagVal2), "Tag should have get it's value updated !"

        devices = DbModels.Device.query.all()
        device3: DbModels.Device = devices[2]
        assert len(device3.getAllTags().items()) == len(DEVICE_WITH_TAGS_3.getAllTags().items()), 'Original tag not stored in DB !'

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_3.uid + '/tags', json=newTags1)
        assertJsonResponse('erasing device3 tags with newTags1', rv, newTags1)

        devices = DbModels.Device.query.all()
        device3: DbModels.Device = devices[2]
        assert device3.getDeviceTagValue(newTagKey1) == newTagVal1, 'New Tag is not persisted in DB !'

        devices = DbModels.Device.query.all()
        assert 3 == len(devices), "Tag posting should not changed the device count in DB !"

def testPostComplexTags(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.commit()

        complexTagging = {
            DbModels.DEVICE_NS: {
                "version": "1.0"
            },
            DbModels.CHANNELS_NS: {
                "0": {
                    iot_tags.CHANNEL_NAME_TAG_KEY: "eCO2"
                },
                "1": {
                    iot_tags.CHANNEL_NAME_TAG_KEY: "VOC"
                }
            },
            DbModels.DEFAULT_QUANTITIES_NS: {
                "concentration": {
                    iot_tags.DATA_UNITS_TAG_KEY: "ppm"
                },
                "digital_input": {
                    iot_tags.DATA_AGGREGATION_METHOD_TAG_KEY: "sum"
                }
            },
            DbModels.QUANTITIES_BY_CHANNEL_NS: {
                "0": {
                    "concentration": {
                        iot_tags.DATA_LABEL_TAG_KEY: "CO2",
                    }
                },
                "1": {
                    "concentration": {
                        iot_tags.DATA_LABEL_TAG_KEY: "VOC",
                        iot_tags.DATA_UNITS_TAG_KEY: "ppb"
                    }
                }
            }
        }

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/tags/', json=complexTagging)
        assertJsonResponse('posting newTags1', rv, complexTagging)

        devices = DbModels.Device.query.all()
        assert 1 == len(devices)
        device = devices[0]
        assert {"version": "1.0"} == device.getDeviceTags()
        assert "eCO2" == device.getChannelName(0)
        assert "VOC" == device.getChannelName(1)
        assert "ppm" == device.getUnits(0, "concentration")
        assert "ppb" == device.getUnits(1, "concentration")
        assert "sum" == device.getAggregationMethod(0, "digital_input")
        assert "sum" == device.getAggregationMethod(1, "digital_input")
        assert "avg" == device.getAggregationMethod(0, "concentration")
        assert "avg" == device.getAggregationMethod(1, "concentration")
        assert "CO2" == device.getLabel(0, "concentration")
        assert "VOC" == device.getLabel(1, "concentration")



def testBadChannelFormatTags(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.commit()

        badFormatTags1 = {
            DbModels.QUANTITIES_BY_CHANNEL_NS: {
                "a": {
                    "concentration": {
                        iot_tags.DATA_LABEL_TAG_KEY: "CO2"
                    }
                }
            }
        }

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/tags', json=badFormatTags1)
        assert 400 == rv.status_code, 'Api should return 400 for bad format tags !'

        devices = DbModels.Device.query.all()
        assert 1 == len(devices), "Tag post should not changed the device count in DB !"
        assert {} == devices[0].getQuantityTags("a", "concentration")


def testBadQuantityFormatTags1(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.commit()

        badFormatTags1 = {
            DbModels.QUANTITIES_BY_CHANNEL_NS: {
                "0": {
                    "_foo": {
                        iot_tags.DATA_LABEL_TAG_KEY: "CO2"
                    }
                }
            }
        }

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/tags', json=badFormatTags1)
        assert 400 == rv.status_code, 'Api should return 400 for bad format tags !'

        devices = DbModels.Device.query.all()
        assert 1 == len(devices), "Tag post should not changed the device count in DB !"
        assert {} == devices[0].getQuantityTags("0", "_foo")


def testBadQuantityFormatTags2(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.commit()

        badFormatTags1 = {
            DbModels.QUANTITIES_BY_CHANNEL_NS: {
                "0": {
                    "foo-bar": {
                        iot_tags.DATA_LABEL_TAG_KEY: "CO2"
                    }
                }
            }
        }

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/tags', json=badFormatTags1)
        assert 400 == rv.status_code, 'Api should return 400 for bad format tags !'

        devices = DbModels.Device.query.all()
        assert 1 == len(devices), "Tag post should not changed the device count in DB !"
        assert {} == devices[0].getQuantityTags("0", "foo-bar")


def testBadTagFormat1(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.commit()

        badFormatTags1 = {
            DbModels.QUANTITIES_BY_CHANNEL_NS: {
                "0": {
                    "foo": {
                        "__label": "CO2"
                    }
                }
            }
        }

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/tags', json=badFormatTags1)
        assert 400 == rv.status_code, 'Api should return 400 for bad format tags !'

        devices = DbModels.Device.query.all()
        assert 1 == len(devices), "Tag post should not changed the device count in DB !"
        assert {} == devices[0].getQuantityTags("0", "__label")

def testBadTagFormat2(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_1))
        DbModels.db.session.commit()

        badFormatTags1 = {
            DbModels.QUANTITIES_BY_CHANNEL_NS: {
                "0": {
                    "foo": {
                        "_label~foo": "CO2"
                    }
                }
            }
        }

        rv: flask.wrappers.Response = client.post(API_CONTEXT_ROOT + '/devices/' + DEVICE_1.uid + '/tags', json=badFormatTags1)
        assert 400 == rv.status_code, 'Api should return 400 for bad format tags !'

        devices = DbModels.Device.query.all()
        assert 1 == len(devices), "Tag post should not changed the device count in DB !"
        assert {} == devices[0].getQuantityTags("0", "_label-foo")


def testDeleteDeviceTags(app, client):
    with app.app_context():
        DbModels.db.session.add(copy.deepcopy(DEVICE_WITH_TAGS_1))
        DbModels.db.session.add(copy.deepcopy(DEVICE_2))
        DbModels.db.session.add(copy.deepcopy(DEVICE_WITH_TAGS_3))
        DbModels.db.session.commit()

        rv: flask.wrappers.Response = client.delete(API_CONTEXT_ROOT + '/devices/' + 'unknownDevice' + '/tags')
        assert 404 == rv.status_code, 'Api should return NotFound for unknown device tags deletion !'

        rv: flask.wrappers.Response = client.delete(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_1.uid + '/tags/notExistingTag')
        assert 404 == rv.status_code, 'Api should return NotFound for unknown tag deletion !'

        device1Tags = DEVICE_WITH_TAGS_1.getAllTags()
        rv: flask.wrappers.Response = client.delete(API_CONTEXT_ROOT + '/devices/' + DEVICE_WITH_TAGS_1.uid + '/tags/')
        assertJsonResponse('deleting tag 1', rv, device1Tags)

        devices = DbModels.Device.query.all()
        assert 3 == len(devices), "Tag deletion should not changed the device count in DB !"
