import logging
import os
from flask import Flask, json, request
from flask_sqlalchemy import SQLAlchemy
import public_api as IotStorageApi
import iot_datastore.iot_service as IotService
#import iot_datastore.db_models as DbModels

logLevel = os.environ.get('LOG_LEVEL', 'INFO').upper()
logging.basicConfig(level=logLevel)
__log = logging.getLogger(__name__)

DB_DATA_DIR = os.environ.get('DB_DATA_DIR', None)
iotStorageDbFile = None

__log.warning('Logging is configured to level: %s', logLevel)

if (DB_DATA_DIR):
    iotStorageDbFile = DB_DATA_DIR + '/iot_datastore.db'
    __log.info('Using file %s for iot_datastore db.', iotStorageDbFile)
else:
    __log.error('DB_DATA_DIR environment variable is not defined !')
    exit(1)

DB_URL = 'sqlite:////' + iotStorageDbFile


app = Flask("iot-storage-api")

#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_BINDS'] = {}
app.config['BUNDLE_ERRORS'] = True

def main():
    IotStorageApi.loadApi(app)
    IotService.initDb(app, DB_URL)

    app.run(host = "0.0.0.0", debug = False)


if __name__ == '__main__':
    main()
