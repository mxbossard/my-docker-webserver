#! /bin/bash -e
scriptDir="$( readlink -f $( dirname $0 ) )"

usage() {
	>&2 echo "usage: $0"
	exit 1
}

source $scriptDir/../scripts/projectEnv.sh

influxdbIp="$( $SCRIPTS_DIR/listServiceIps.sh influxdb influxdb | awk '{ print $1 }' )"
promscaleIp="$( $SCRIPTS_DIR/listServiceIps.sh promscale promscale | awk '{ print $1 }' )"

export INFLUXDB_URL="http://$influxdbIp:8086"
export INFLUXDB_DB_NAME="$IOT_DATASTORE_INFLUXDB_DB"
export INFLUXDB_USER="$IOT_DATASTORE_INFLUXDB_READER_USER"
export INFLUXDB_PASSWORD="$IOT_DATASTORE_INFLUXDB_READER_PASSWORD"

export PROMSCALE_URL="http://$promscaleIp:9201"

$scriptDir/initSckDevice.sh foombd $VIRTUAL_HOST
$scriptDir/initSckDevice.sh rokjjm $VIRTUAL_HOST

time $scriptDir/run.sh -m influxdb_to_promscale.py
