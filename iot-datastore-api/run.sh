#! /bin/bash -e
SCRIPT_DIR=$( dirname $( readlink -f $0 ) )
cd $SCRIPT_DIR

IMAGE_PREFIX="mby/"
projectName="${PWD##*/}"
image="$IMAGE_PREFIX$projectName"
ctName="$projectName"

usage() {
	>&2 echo "usage: $0 [-r] [tag] [args]"
	>&2 echo ""
	>&2 echo "Run current project image."
	>&2 echo "By default tag is latest. If first command arg is an existing tag, will consume it as a tag."
	>&2 echo ""
	>&2 echo "	-r : renew the build kill and start the fresh image."
	>&2 echo ""
	>&2 echo "examples:"
        >&2 echo "	$0 latest foo bar baz"
	>&2 echo "	$0 foo bar baz"
	exit 1
}

renew=false
test=false
coverage=false
migration=false
if [ "$1" = "-r" ]
then
	renew=true
	shift 1
	>&2 echo "Killing container: [$ctName] ..."
	docker kill $ctName || true
fi

if [ "$1" = "-t" ]
then
	test=true
	renew=true
	shift 1
fi

if [ "$1" = "-c" ]
then
	test=true
	renew=true
	coverage=true
	shift 1
fi

if [ "$1" = "-m" ]
then
	test=false
	renew=true
	coverage=false
	migration=true
	shift 1
fi

>&2 echo "List of existing images:"
if $renew || ! docker image ls $image | tail -n-1 | grep "$image"
then
	$renew || >&2 echo "Docker image $projectName not found. Launching build ..."
	./build.sh latest
fi

tag="$1"
args="$@"
if docker image ls $image:$tag | tail -n-1 | grep "$image:$tag" > /dev/null
then
	# First arg is an existing tag consume it
	image="$image:$tag"
	shift 1
	args="$@"
fi

# Filtering args
args="$( echo $args | sed -re 's|^(.*/)?src/||g' -e 's|^(.*/)?app/||g' )"

>&2 echo "Launching container: [$ctName] with image: [$image] and args: [$args] ..."

if $coverage; then
	mkdir -p "$SCRIPT_DIR/htmlcov"
	coverageRunArgs="-v $SCRIPT_DIR/htmlcov:/home/flaskapp/htmlcov"
	coveragePytestAgs="--cov=iot_datastore --cov-report=html"
fi

if $test
then
	ctName=test_$ctName
	cmd="docker run --rm -t --name=$ctName $coverageRunArgs $image -m pytest $coveragePytestAgs --log-cli-level INFO $args"
elif $migration; then
	envArgs="-e INFLUXDB_URL -e INFLUXDB_DB_NAME -e INFLUXDB_USER -e INFLUXDB_PASSWORD -e PROMSCALE_URL"
	source $SCRIPT_DIR/../scripts/globalEnv.sh
	volumeRegex="^${NAMESPACE}"'_.*_iot-datastore-api-db-storage$'
	iotDatastoreApiDbStorageVolume="$( docker volume ls -q | egrep "$volumeRegex" || true )"
	test -n "$iotDatastoreApiDbStorageVolume" || die "Unable to found iot-datastore-api DB storage volume with regex: [$volumeRegex] !"
	cmd="docker run --rm -t --network=host $envArgs $migrationArgs --name=$ctName -p5000:5000 -v "$iotDatastoreApiDbStorageVolume:/var/lib/sqlite:ro" $image $args"
else
	cmd="docker run --rm -t --network=host --name=$ctName -p5000:5000 -v/tmp/iot_sqlite:/var/lib/sqlite $image $args"
fi

>&2 echo "$cmd"
eval "$cmd"

#if ! $test
#then
#	docker logs -f $ctName
#fi
