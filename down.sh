#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
source $SCRIPT_DIR/scripts/scriptFramework.sh
source $SCRIPT_DIR/scripts/globalEnv.sh

force=false
if [ "$1" == "-f" ]; then
	# down only listed projects (not ancestors)
	force=true
	shift
fi

INPUT_PROJECTS="$@"
test -n "$INPUT_PROJECTS" || die "You must supply at least on project to up !"

if $force; then
	projectsToDown=$( $SCRIPTS_DIR/listProjects.sh $INPUT_PROJECTS )
else
	projectsToDown=$( $SCRIPTS_DIR/projectAncestors.sh --self $INPUT_PROJECTS )
fi

checkExistingContainers() {
	local nameFilter="$1"
	local namespaceLabel="$2"

	if [ $(docker ps -aq -f "name=^$nameFilter" | wc -l ) -gt 0 ]; then
		>&2 alertMsg "Some container still exist(s) in $namespaceLabel !"
	       	docker ps -a -f "name=^$nameFilter" | while read line; do
			>&2 echo -e "$YELLOW$line$NC"
		done
		ctIds="$( echo $( docker ps -aq -f name=$nameFilter ) )"
		errOut "To kill them all, use: "
		>&2 purpleMsg "docker rm -f $ctIds"
	else
		>&2 successMsg "All containers successfully removed from $namespaceLabel."
	fi

}

for project in $projectsToDown
do
	cyanMsg "## Downing project $project ..."
	export QUIET=true
	(
		# execute in subshell
		cd "$PARENT_DIR/$project"
		! source $SCRIPTS_DIR/projectEnv.sh
		! source $SCRIPTS_DIR/loadDynamicDockerComposeConfig.sh
		$DEBUG && $DOCKER_COMPOSE_CMD config || true
		$DOCKER_COMPOSE_CMD down --remove-orphans || true

		checkExistingContainers "^${PROJECT_NAMESPACE}_" "project: [$project]"
	) || alertMsg "Fail to down project: [$PURPLE$project$NC] !"
done

if [ "$INPUT_PROJECTS" == "all" ]; then
	checkExistingContainers "^${ENV}_" "environment: [$ENV]"
fi
