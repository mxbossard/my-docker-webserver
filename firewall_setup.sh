#! /bin/sh

sudo apt install ufw

sudo ufw default deny incoming
sudo ufw default allow outgoing

sudo ufw allow ssh

yes | sudo ufw enable

echo "## Firewall status"
sudo ufw status
