#! /bin/bash -e
scriptDir="$(dirname $(readlink -f $0))"

source $scriptDir/../../scripts/projectEnv.sh
source $SCRIPTS_DIR/unitTestsFramework.sh

webUrl="http://$VIRTUAL_HOST"
testRoom="integrationTest_$( date '+%s' )"

assertHttpResponseMatch "Check jitsi-meet web page" "GET" "$webUrl" '.*<div id="react"></div>.*'

assertHttpStatus "Create a test room" "GET" "$webUrl/$testRoom" "$HTTP_OK"

assertHttpResponseMatch "Check test room content" "GET" "$webUrl/$testRoom" '.*<div id="react"></div>.*'

#assertHttpStatus "Check Web Sockets passing" "GET" "http://$VIRTUAL_HOST/xmpp-websocket?room=$testRoom" "101" "-i -N -H'Connection: Upgrade' -H'Upgrade: websocket' -H'Host: $VIRTUAL_HOST' -H'Origin: $webUrl' -H'Sec-WebSocket-Protocol: xmpp' -H'Sec-WebSocket-Version: 13' -H'Sec-WebSocket-Extensions: permessage-deflate' --http1.1"

#ctIps=$( $SCRIPTS_DIR/listContainersIps.sh '^jitsi-meet_jvb' | awk '{print $1}' )
#jvbUrl="http://$ctIp:8080"

assertNoError
