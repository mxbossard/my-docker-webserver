#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

QUIET=true
source $SCRIPT_DIR/../scripts/projectEnv.sh

jvbIps=$( $SCRIPTS_DIR/listServiceIps.sh jitsi-meet jvb | awk '{print $1}' ) 
test -z "$jvbIps" && >&2 echo "Unable to find an IP, container may not be running !" && exit 1 || true

$SCRIPTS_DIR/loopExec.sh "curl -f -sS http://{}:8080/about/health" $jvbIps
