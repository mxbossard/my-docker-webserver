#! /bin/sh -e
scriptDir="$( readlink -f $( dirname $0 ) )"

>&2 echo "Perform jitsi-meet uninstallation (script $0) ..."
cd "$scriptDir"
rm -f -- .env || true
rm -f ~/.jitsi-meet-cfg/{web/letsencrypt,transcripts,prosody/config,prosody/prosody-plugins-custom,jicofo,jvb,jigasi,jibri} || true

