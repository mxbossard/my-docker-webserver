#! /bin/sh -e
scriptDir="$( readlink -f $( dirname $0 ) )"

if [ ! -f "$scriptDir/.env" ]
then
	# .env file does not exists
	# Will perform install described here : https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-docker
	>&2 echo "Perform jitsi-meet installation (script $0) ..."
	cd "$scriptDir"
	cp env.example .env
	./gen-passwords.sh
fi

# This is idempotent it can be done every time
mkdir -p ~/.jitsi-meet-cfg/{web/letsencrypt,transcripts,prosody/config,prosody/prosody-plugins-custom,jicofo,jvb,jigasi,jibri}
