#!/usr/bin/env bats

parentDir="/code"

resolvePath() {
        realpath -s $@ || >&2 echo "Failed to execute cmd: [realpath $@] !"
}

@test "loadEnv.sh from project dir" {
	projectDir="$parentDir/grafana"
	cd "$projectDir"
	DEBUG=true
	SOURCING_SCRIPT_PATH="$projectDir/up.sh"

	. ../scripts/loadEnv.sh

	[ -n "$SCRIPTS_DIR" ]
	[ "$( resolvePath $parentDir/scripts )" == "$( resolvePath $SCRIPTS_DIR )" ]

	[ -n "$PARENT_DIR" ]
	[ "$( resolvePath $parentDir )" == "$( resolvePath $PARENT_DIR )" ]

	[ -n "$PROJECT_DIR" ]
	[ "$( resolvePath $projectDir )" == "$( resolvePath $PROJECT_DIR )" ]

	[ -f "$GLOBAL_ENV_PATH" ]
	[ "$( resolvePath $parentDir/env.sh )" == "$( resolvePath $GLOBAL_ENV_PATH )" ]

	[ -n "$PROJECT_ENV_PATH" ]
	[ "$( resolvePath $projectDir/env.sh )" == "$( resolvePath $PROJECT_ENV_PATH )" ]
}

@test "loadEnv.sh from non project dir 1" {
	cd "$parentDir"
        SOURCING_SCRIPT_PATH="$parentDir/up.sh"
        run . ./scripts/loadEnv.sh
	[ "$status" -ne 0 ]
	echo $output
	[ "$output" == "ERROR: Script sourced from outside project scope !" ]
}

@test "loadEnv.sh from non project dir 2" {
	cd "/"
        SOURCING_SCRIPT_PATH="/up.sh"
	touch $SOURCING_SCRIPT_PATH
        run . $parentDir/scripts/loadEnv.sh
	[ "$status" -ne 0 ]	
	[ "$output" == "ERROR: Script sourced from outside project scope !" ]
}

@test "loadEnv.sh with empty global env.sh" {
	projectDir="$parentDir/web"
	cd "$projectDir"
	DEBUG=true
	SOURCING_SCRIPT_PATH="$projectDir/up.sh"
	globalEnvPath="$parentDir/tests/ressources/emptyGlobalEnv.sh"
	projectEnvPath="$parentDir/tests/ressources/minimalProjectEnv.sh"
	GLOBAL_ENV_PATH="$globalEnvPath"
	PROJECT_ENV_PATH="$projectEnvPath"

	. ../scripts/loadEnv.sh

	[ -n "$SCRIPTS_DIR" ]
	[ "$( resolvePath $parentDir/scripts )" == "$( resolvePath $SCRIPTS_DIR )" ]

	[ -n "$PARENT_DIR" ]
	[ "$( resolvePath $parentDir )" == "$( resolvePath $PARENT_DIR )" ]

	[ -n "$PROJECT_DIR" ]
	[ "$( resolvePath $projectDir )" == "$( resolvePath $PROJECT_DIR )" ]

	[ -f "$GLOBAL_ENV_PATH" ]
	[ "$( resolvePath $globalEnvPath )" == "$( resolvePath $GLOBAL_ENV_PATH )" ]

	[ -n "$PROJECT_ENV_PATH" ]
	[ "$( resolvePath $projectEnvPath )" == "$( resolvePath $PROJECT_ENV_PATH )" ]

	[ -z "$GLOBAL" ]
	[ "$PROJECT" == "bar" ]
}

@test "loadEnv.sh with empty project env.sh" {
	projectDir="$parentDir/web"
	cd "$projectDir"
	DEBUG=true
	SOURCING_SCRIPT_PATH="$projectDir/up.sh"
	globalEnvPath="$parentDir/tests/ressources/minimalGlobalEnv.sh"
	projectEnvPath="$parentDir/tests/ressources/emptyProjectEnv.sh"
	GLOBAL_ENV_PATH="$globalEnvPath"
	PROJECT_ENV_PATH="$projectEnvPath"

	. ../scripts/loadEnv.sh

	[ -n "$SCRIPTS_DIR" ]
	[ "$( resolvePath $parentDir/scripts )" == "$( resolvePath $SCRIPTS_DIR )" ]

	[ -n "$PARENT_DIR" ]
	[ "$( resolvePath $parentDir )" == "$( resolvePath $PARENT_DIR )" ]

	[ -n "$PROJECT_DIR" ]
	[ "$( resolvePath $projectDir )" == "$( resolvePath $PROJECT_DIR )" ]

	[ -f "$GLOBAL_ENV_PATH" ]
	[ "$( resolvePath $globalEnvPath )" == "$( resolvePath $GLOBAL_ENV_PATH )" ]

	[ -n "$PROJECT_ENV_PATH" ]
	[ "$( resolvePath $projectEnvPath )" == "$( resolvePath $PROJECT_ENV_PATH )" ]

	[ "$GLOBAL" == "foo" ]
	[ -z "$PROJECT" ]
}

@test "loadEnv.sh with minimal global and project env.sh" {
	projectDir="$parentDir/web"
	cd "$projectDir"
	DEBUG=true
	SOURCING_SCRIPT_PATH="$projectDir/up.sh"
	globalEnvPath="$parentDir/tests/ressources/minimalGlobalEnv.sh"
	projectEnvPath="$parentDir/tests/ressources/minimalProjectEnv.sh"
	GLOBAL_ENV_PATH="$globalEnvPath"
	PROJECT_ENV_PATH="$projectEnvPath"

	. ../scripts/loadEnv.sh

	[ -n "$SCRIPTS_DIR" ]
	[ "$( resolvePath $parentDir/scripts )" == "$( resolvePath $SCRIPTS_DIR )" ]

	[ -n "$PARENT_DIR" ]
	[ "$( resolvePath $parentDir )" == "$( resolvePath $PARENT_DIR )" ]

	[ -n "$PROJECT_DIR" ]
	[ "$( resolvePath $projectDir )" == "$( resolvePath $PROJECT_DIR )" ]

	[ -f "$GLOBAL_ENV_PATH" ]
	[ "$( resolvePath $globalEnvPath )" == "$( resolvePath $GLOBAL_ENV_PATH )" ]

	[ -n "$PROJECT_ENV_PATH" ]
	[ "$( resolvePath $projectEnvPath )" == "$( resolvePath $PROJECT_ENV_PATH )" ]

	[ "$GLOBAL" == "foo" ]
	[ "$PROJECT" == "bar" ]
}

@test "loadEnv.sh with overriding project env.sh" {
	projectDir="$parentDir/web"
	cd "$projectDir"
	DEBUG=true
	SOURCING_SCRIPT_PATH="$projectDir/up.sh"
	globalEnvPath="$parentDir/tests/ressources/minimalGlobalEnv.sh"
	projectEnvPath="$parentDir/tests/ressources/overridingProjectEnv.sh"
	GLOBAL_ENV_PATH="$globalEnvPath"
	PROJECT_ENV_PATH="$projectEnvPath"

	. ../scripts/loadEnv.sh

	[ -n "$SCRIPTS_DIR" ]
	[ "$( resolvePath $parentDir/scripts )" == "$( resolvePath $SCRIPTS_DIR )" ]

	[ -n "$PARENT_DIR" ]
	[ "$( resolvePath $parentDir )" == "$( resolvePath $PARENT_DIR )" ]

	[ -n "$PROJECT_DIR" ]
	[ "$( resolvePath $projectDir )" == "$( resolvePath $PROJECT_DIR )" ]

	[ -f "$GLOBAL_ENV_PATH" ]
	[ "$( resolvePath $globalEnvPath )" == "$( resolvePath $GLOBAL_ENV_PATH )" ]

	[ -n "$PROJECT_ENV_PATH" ]
	[ "$( resolvePath $projectEnvPath )" == "$( resolvePath $PROJECT_ENV_PATH )" ]

	[ "$GLOBAL" == "baz" ]
	[ "$PROJECT" == "bar" ]
}
