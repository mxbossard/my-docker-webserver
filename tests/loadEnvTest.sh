#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
. $SCRIPT_DIR/../scripts/scriptFramework.sh
. $SCRIPT_DIR/../scripts/unitTestsFramework.sh

. $SCRIPT_DIR/../scripts/projectEnv.sh

resolvePath() {
	realpath -s $@
}

assertEquals "$( resolvePath ../scripts )" "$( resolvePath $SCRIPTS_DIR )" 
