#! /bin/bash -e
scriptDir="$(dirname $(readlink -f $0))"

source $scriptDir/../scripts/projectEnv.sh
source $SCRIPTS_DIR/unitTestsFramework.sh

### Some queries
# curl --data-urlencode "db=amipo_capteurs" --data-urlencode "q=DROP MEASUREMENT amipo_temperature_coin" http://127.0.0.1:8086/query

now="$(date +%s)"

db="iot_datastore"
testMeasurementBaseName="testCq"
initialRp="immediate_term"
aggregatedRp="short_term"
influxdbIp=$( $SCRIPTS_DIR/listServiceIps.sh influxdb influxdb | awk '{print $1}' | head -1 )
influxdbUrl="http://$influxdbIp:8086"
queryApiUrl="$influxdbUrl/query"
writeApiUrl="$influxdbUrl/write"
basicAuth="-u $INFLUXDB_ADMIN_USER:$INFLUXDB_ADMIN_PASSWORD"
resultTempFile=$( mkTempFile )
rmOnExit "$resultTempFile"

pushValue() {
	measurement="$1"
	tags="$2"
	value="$3"
	curl -X POST "$writeApiUrl?db=$db&rp=$initialRp" $basicAuth --data-binary "$measurement,$tags value=$value"	
}

pushTestValues() {
	measurement="$1"
	tags="$2"

	values="1 3 2 20 3 -1 0"
	for value in $values; do
		pushValue "$measurement" "$tags" "$value"
		sleep 0.1
	done	
}

queryForAggregatedValue() {
	measurment="$1"

	curl -sS -X POST "$queryApiUrl?db=$db&rp=$aggregatedRp&epoch=s&pretty=true" $basicAuth --data-urlencode "q=SELECT value FROM $measurment WHERE time >= $((now - 60))s ORDER BY time DESC LIMIT 1" | jq '.results[].series[0].values[0][1]'
}

aggMethods="mean last first min max median mode sum count none stuff"
for aggMethod in $aggMethods; do
	measurement="${testMeasurementBaseName}_$aggMethod"
	tags="test=cq,_aggregation_method=$aggMethod"
	pushTestValues "$measurement" "$tags"
done

# Empty _aggregation_method tag
measurement="${testMeasurementBaseName}_emptyAggTag"
tags="test=cq,_aggregation_method=''"
pushTestValues "$measurement" "$tags"

# No _aggregation_method tag
measurement="${testMeasurementBaseName}_noAggTag"
tags="test=cq"
pushTestValues "$measurement" "$tags"

## Query for latest data
curl -X POST "$queryApiUrl?db=$db&rp=$initialRp&epoch=s&pretty=true" $basicAuth --data-urlencode "q=SELECT _aggregation_method, value FROM /${testMeasurementBaseName}.*/ WHERE time >= ${now}s ORDER BY time DESC LIMIT 10" -o $resultTempFile
cat $resultTempFile #| jq '.results[].series[]'


timeToWait=70
>&2 echo "Waiting $timeToWait sec for aggregation to be performed ..."
SECONDS=0
while test $SECONDS -lt $timeToWait; do
	sleep 1
	echo -n "."
done

curl -X POST "$queryApiUrl?db=$db&rp=$aggregatedRp&epoch=s&pretty=true" $basicAuth --data-urlencode "q=SELECT _aggregation_method, value FROM /${testMeasurementBaseName}.*/ WHERE time >= $((now - 60))s ORDER BY time DESC LIMIT 10" #| jq '.results[].series[]'

assertEquals 'Check for last tag measurment aggregated value' "0" "$( queryForAggregatedValue "${testMeasurementBaseName}_last" )"
assertEquals 'Check for first tag measurment aggregated value' "1" "$( queryForAggregatedValue "${testMeasurementBaseName}_first" )"
assertEquals 'Check for min tag measurment aggregated value' "-1" "$( queryForAggregatedValue "${testMeasurementBaseName}_min" )"
assertEquals 'Check for max tag measurment aggregated value' "20" "$( queryForAggregatedValue "${testMeasurementBaseName}_max" )"
assertEquals 'Check for median tag measurment aggregated value' "2" "$( queryForAggregatedValue "${testMeasurementBaseName}_median" )"
assertEquals 'Check for mode tag measurment aggregated value' "3" "$( queryForAggregatedValue "${testMeasurementBaseName}_mode" )"
assertEquals 'Check for sum tag measurment aggregated value' "28" "$( queryForAggregatedValue "${testMeasurementBaseName}_sum" )"
assertEquals 'Check for count tag measurment aggregated value' "7" "$( queryForAggregatedValue "${testMeasurementBaseName}_count" )"
assertEquals 'Check for mean tag measurment aggregated value' "4" "$( queryForAggregatedValue "${testMeasurementBaseName}_mean" )"
assertEquals 'Check for none tag measurment aggregated value' "null" "$( queryForAggregatedValue "${testMeasurementBaseName}_none" )"
assertEquals 'Check for stuff tag measurment aggregated value' "4" "$( queryForAggregatedValue "${testMeasurementBaseName}_stuff" )"
assertEquals 'Check for empty tag measurment aggregated value' "4" "$( queryForAggregatedValue "${testMeasurementBaseName}_emptyAggTag" )"
assertEquals 'Check for no tag measurment aggregated value' "4" "$( queryForAggregatedValue "${testMeasurementBaseName}_noAggTag" )"

