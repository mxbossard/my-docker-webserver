#! /bin/bash -e
scriptDir="$(dirname $(readlink -f $0))"

source $scriptDir/../../scripts/projectEnv.sh
source $SCRIPTS_DIR/unitTestsFramework.sh

### Some queries
now="$(date +%s)"

testDb="$IOT_DATASTORE_INFLUXDB_DB"
measurementName="integration_test_$now"
testData="$measurementName,foo=bar CPU=17.2 ${now}000000000"

influxdbIp=$( $SCRIPTS_DIR/listServiceIps.sh influxdb influxdb | awk '{print $1}' | head -1 )
influxdbUrl="http://$influxdbIp:8086"
queryApiUrl="$influxdbUrl/query"
writeApiUrl="$influxdbUrl/write"
adminBasicAuth="-u $INFLUXDB_ADMIN_USER:$INFLUXDB_ADMIN_PASSWORD"
readerBasicAuth="-u $IOT_DATASTORE_INFLUXDB_READER_USER:$IOT_DATASTORE_INFLUXDB_READER_PASSWORD"
writerBasicAuth="-u $IOT_DATASTORE_INFLUXDB_WRITER_USER:$IOT_DATASTORE_INFLUXDB_WRITER_PASSWORD"
resultTempFile=$( mkTempFile )
rmOnExit "$resultTempFile"


assertHttpStatus 'Check writer user cannot create DB' POST "$queryApiUrl" "403" $writerBasicAuth --data-urlencode "q=CREATE DATABASE \"integration_test_writer_$now\""
assertHttpStatus 'Check reader user cannot create DB' POST "$queryApiUrl" "403" $readerBasicAuth --data-urlencode "q=CREATE DATABASE \"integration_test_writer_$now\""

## Push test data
assertHttpStatus 'Check anonymous user cannot push data' POST "$writeApiUrl?db=$testDb" "401" --data-binary "$testData"
assertHttpStatus 'Check reader user cannot push data' POST "$writeApiUrl?db=$testDb" "403" $readerBasicAuth --data-binary "$testData"
assertHttpStatus 'Check writer user can push test data' POST "$writeApiUrl?db=$testDb" "204" $writerBasicAuth --data-binary "$testData"

## Query for latest data
assertHttpStatus "Check anonymous user cannot query for data" POST "$queryApiUrl?db=$testDb&epoch=s" "401" --data-urlencode "q=SELECT * FROM \"$measurementName\" WHERE time >= ${now}s ORDER BY time DESC LIMIT 1"
assertHttpStatus "Check reader user can query for data" POST "$queryApiUrl?db=$testDb&epoch=s" "200" $readerBasicAuth --data-urlencode "q=SELECT * FROM \"$measurementName\" WHERE time >= ${now}s ORDER BY time DESC LIMIT 1"
assertHttpStatus "Check writer user cannot query for data" POST "$queryApiUrl?db=$testDb&epoch=s" "403" $writerBasicAuth --data-urlencode "q=SELECT * FROM \"$measurementName\" WHERE time >= ${now}s ORDER BY time DESC LIMIT 1"

# Delete DB and measurements which could be created
#curl -X POST $adminBasicAuth --data-urlencode "q=SHOW DATABASES" -- "$queryApiUrl"
curl -sS -X POST $adminBasicAuth --data-urlencode "q=DROP DATABASE \"integration_test_.*\"" -- "$queryApiUrl" > /dev/null
#curl -X POST $adminBasicAuth --data-urlencode "q=SHOW MEASUREMENTS" -- "$queryApiUrl?db=$testDb"
curl -sS -X POST $adminBasicAuth --data-urlencode "q=DROP SERIES FROM /integration_test_.*/" -- "$queryApiUrl?db=$testDb" > /dev/null


assertNoError
