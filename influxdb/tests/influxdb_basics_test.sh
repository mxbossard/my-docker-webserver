#! /bin/bash -e
scriptDir="$(dirname $(readlink -f $0))"

source $scriptDir/../../scripts/projectEnv.sh
source $SCRIPTS_DIR/unitTestsFramework.sh

### Some queries
# curl --data-urlencode "db=amipo_capteurs" --data-urlencode "q=DROP MEASUREMENT amipo_temperature_coin" http://127.0.0.1:8086/query

now="$(date +%s)"

testDb="integration_test_DB"
measurementName="HARDWARE_$now"
testData="$measurementName,foo=bar CPU=17.2 ${now}000000000"

influxdbIp=$( $SCRIPTS_DIR/listServiceIps.sh influxdb influxdb | awk '{print $1}' | head -1 )
influxdbUrl="http://$influxdbIp:8086"
queryApiUrl="$influxdbUrl/query"
writeApiUrl="$influxdbUrl/write"
basicAuth="-u $INFLUXDB_ADMIN_USER:$INFLUXDB_ADMIN_PASSWORD"
resultTempFile=$( mkTempFile )
rmOnExit "$resultTempFile"


assertHttpStatus 'Check anonymous user cannot create a bucket' POST "$queryApiUrl" "401" --data-urlencode "q=CREATE DATABASE \"$testDb\""

## Create DB if it don't exists
assertHttpStatus 'Create influxDB bucket' POST "$queryApiUrl" "$HTTP_OK" $basicAuth --data-urlencode "q=CREATE DATABASE \"$testDb\" WITH DURATION 1h"

## Push test data
assertHttpStatus 'Push test data in test bucket' POST "$writeApiUrl?db=$testDb" "204" $basicAuth --data-binary "$testData"

## Query for latest data
assertHttpStatus "Query for latest data" POST "$queryApiUrl?db=$testDb&epoch=s" "$HTTP_OK" $basicAuth --data-urlencode "q=SELECT * FROM \"$measurementName\" WHERE time >= ${now}s ORDER BY time DESC LIMIT 1"

httpQuery POST "$queryApiUrl?db=$testDb&epoch=s" $basicAuth --data-urlencode "q=SELECT * FROM \"$measurementName\" WHERE time >= ${now}s ORDER BY time DESC LIMIT 1" -o "$resultTempFile"
>&2 cat $resultTempFile #| jq '.results[].series[]'

assertHttpStatus "Check anonymous user cannot querry data" POST "$queryApiUrl?db=$testDb&epoch=s" "401" --data-urlencode "q=SELECT * FROM \"$measurementName\" WHERE time >= ${now}s ORDER BY time DESC LIMIT 1"


assertInfluxdbJsonResponseField() {
        columnIndex="$1"
        columnExpectedName="$2"
        columnExpectedValue="$3"

        assertEquals "Check for influxdb $columnExpectedName field name" "\"$columnExpectedName\"" "$( jq ".results[].series[]|.columns[$columnIndex]" $resultTempFile )"
        assertEquals "Check for influxdb $columnExpectedName field value" "$columnExpectedValue" "$( jq ".results[].series[]|.values[0][$columnIndex]" $resultTempFile )"
}

#echo "## Assert results"
assertEquals 'Check for serie name' "\"$measurementName\"" "$( jq '.results[].series[]|.name' $resultTempFile )"
assertInfluxdbJsonResponseField 1 "CPU" "17.2"
assertInfluxdbJsonResponseField 2 "foo" '"bar"'

## Drop database
assertHttpStatus 'Delete influxDB bucket' POST "$queryApiUrl" "$HTTP_OK" $basicAuth --data-urlencode "q=DROP DATABASE \"$testDb\""

assertNoError
