#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

influxdbIps=$( $SCRIPTS_DIR/listServiceIps.sh influxdb influxdb | awk '{print $1}' ) 
test -z "$influxdbIps" && >&2 echo "Unable to find an IP, container may not be running !" && exit 1 || true

$SCRIPTS_DIR/loopExec.sh "curl -f -sS http://{}:8086/health" $influxdbIps

