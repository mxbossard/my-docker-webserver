
influxCmd="influx -execute"

# Restore backup or init DB
backupPath="/var/backups/backupToRestore"

if [ -n "$INFLUXDB_BACKUP_TO_RESTORE_FILEPATH" ]; then
	echo "Backup to restore is configured by INFLUXDB_BACKUP_TO_RESTORE_FILEPATH env property."
else
	echo "NO backup to restore configured by INFLUXDB_BACKUP_TO_RESTORE_FILEPATH env property."
	exit 0
fi


if [ -f "$backupPath" ]; then
	extractedBackupPath="/tmp/extracteInfluxdbdBackup_$( date '+%s' )"
	rm -rf -- "$extractedBackupPath"
	mkdir -p "$extractedBackupPath"
	echo "Extracting backup archive: [$backupPath] ..."
	tar -xzf "$backupPath" -C "$extractedBackupPath"

	backupPath="$( dirname $( find $extractedBackupPath -name "*.manifest" | head -1 ) )"
fi

if [ -d "$backupPath" ]; then
	echo "Restoring backup directory: [$backupPath] ..."
	influxd restore -portable "$backupPath"
else
	echo "Unable to restore backup (backupPath: [$backupPath]) !"
fi

