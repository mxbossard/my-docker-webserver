#!/bin/sh
set -e

INFLUX_CMD2="influx -host 127.0.0.1 -port $INFLUXDB_INIT_PORT -username ${INFLUXDB_ADMIN_USER} -password ${INFLUXDB_ADMIN_PASSWORD}"
#INFLUX_CMD2="influx"

for f in $( ls /docker-entrypoint-initdb.d/* | grep -v "$( basename $0 )" ); do
	if echo "$f" | grep ".iql$" > /dev/null; then
		>&2 echo
		>&2 echo "########## $0: running $f"; cat "$f" | $INFLUX_CMD2 || ( >&2 echo "Error loading $f file !"; exit 1 )
		>&2 echo
	elif echo "$f" | grep ".sh$" > /dev/null; then
		>&2 echo
		>&2 echo "########## $0: executing $f"; /bin/sh "$f" || ( >&2 echo "Error executing $f file !"; exit 1 )
		>&2 echo
	fi
done

# Return exit 1 to exit from influxdb init-script
exit 1
