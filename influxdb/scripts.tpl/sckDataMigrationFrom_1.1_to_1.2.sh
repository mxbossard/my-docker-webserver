#! /bin/sh

usage() {
	>&2 echo "usage: $0 <BEFORE_EPOCH_TIMESTAMP>"
	exit 1
}

INFLUX_CMD2="influx -host 127.0.0.1 -port $INFLUXDB_INIT_PORT -username ${INFLUXDB_ADMIN_USER} -password ${INFLUXDB_ADMIN_PASSWORD} -database iot_datastore"

test -n "$1" || usage

if test "$1" -ge "0" 2> /dev/null; then
	BEFORE_EPOCH_TIMESTAMP="$1"
else
	BEFORE_EPOCH_TIMESTAMP="$( date '+%s' --date "$1" )"
fi

>&2 read -p  "Will migrate data stored before $( date '+%F %T' --date "@$BEFORE_EPOCH_TIMESTAMP" ). Do you confirm ? [y/N]" confirm
if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ]; then
	exit 1
fi

RETENTION_POLICIES="medium_term long_term infinite_term short_term immediate_term"
# channelId quantity label units aggregationMethod fieldName
CHANNELS_FILE=/tmp/channels.csv
echo "
0,timestampToDel,timestampToDel,timestampToDel,timestampToDel,unix_timestamp
10,percentage,Battery level,%,mean,analog
14,illuminance,Luminosity,Lux,mean,illuminance
53,sound_level,Sound level,dbA,mean,analog
55,temperature,Temperature,°C,mean,temperature
56,humidity,Humidity,%,mean,analog
58,pressure,Atmospheric pressure,hPa,mean,pressure
87,concentration,PM10,µg/m³,sum,analog
88,concentration,PM2.5,µg/m³,sum,analog
89,concentration,PM1,µg/m³,sum,analog
112,concentration,eCO2 gas level,ppm,mean,analog
113,concentration,VOC gas level,ppp,mean,analog
" > $CHANNELS_FILE

migrateDeviceMeasurements() {
	local deviceUid="$1"
	local source="lpp_binary_post"
	local inputFormat="lpp"

	local deviceMigratedPointsCount=0
	for rp in $RETENTION_POLICIES; do
		local rpMigratedPointsCount=0
		rpPointsCountToMigrateQuery="select count(*) from iot_datastore.$rp.$deviceUid;"
		rpPointsCountToMigrate=$( $INFLUX_CMD2 -execute "$rpPointsCountToMigrateQuery" | tail -1 | awk 'sum=0; {for(i=2;i<=NF;i++) sum = sum + $i" "; print sum }' )

		echo "Found [$rpPointsCountToMigrate] data points to migrate for device: [$deviceUid], in RP: [$rp]."

		while read line; do
			test -n "$line" || continue 
			local channelId="$( echo "$line" | cut -f1 -d',' )"
			local quantity="$( echo "$line" | cut -f2 -d',' )"
			local label="$( echo "$line" | cut -f3 -d',' )"
			local units="$( echo "$line" | cut -f4 -d',' )"
			local aggregationMethod="$( echo "$line" | cut -f5 -d',' )"
			local fieldName="$( echo "$line" | cut -f6 -d',' )"

			echo "Parsing channel info row: [$line]."
			test -z "$channelId" && >&2 echo "Missing channelId in channel mapping !" && exit 1
			test -z "$quantity" && >&2 echo "Missing quantity in channel mapping !" && exit 1
			test -z "$label" && >&2 echo "Missing label in channel mapping !" && exit 1
			test -z "$units" && >&2 echo "Missing units in channel mapping !" && exit 1
			test -z "$aggregationMethod" && >&2 echo "Missing aggregationMethod in channel mapping !" && exit 1
			test -z "$fieldName" && >&2 echo "Missing fieldName in channel mapping !" && exit 1

			fromMeasurement="iot_datastore.$rp.$deviceUid"
			intoMeasurement="iot_datastore.$rp.${deviceUid}__${channelId}__${quantity}"

			validationQuery="select $fieldName from $fromMeasurement where time < ${BEFORE_EPOCH_TIMESTAMP}s and _channel='$channelId' limit 1;"

			clearPreviousAttemptQuery="delete from ${deviceUid}__${channelId}__${quantity} where time < ${BEFORE_EPOCH_TIMESTAMP}s;"

			countFromQuery="select count($fieldName) from $fromMeasurement where _channel='$channelId' and time < ${BEFORE_EPOCH_TIMESTAMP}s;"
			countIntoQuery="select count(value) from $intoMeasurement where _channel='$channelId' and time < ${BEFORE_EPOCH_TIMESTAMP}s;"
			#migrationQuery="select \"$deviceUid\" as _deviceUid, _deviceName as _deviceName, \"$channelId\" as _channel, _channelName as _channelName, \"\" as _qualifier, \"$quantity\" as _quantity, \"$label\" as _label, \"$units\" as _units, \"$source\" as _source, \"$inputFormat\" as _inputFormat, \"$aggregationMethod\" as _aggregationMethod, $fieldName as value into $intoMeasurement from $fromMeasurement where time < ${BEFORE_EPOCH_TIMESTAMP}s and _channel='$channelId' group by "'*;'
			migrationQuery="select \"$deviceUid\" as _deviceUid, \"$channelId\" as _channel, \"\" as _qualifier, \"$quantity\" as _quantity, \"$label\" as _label, \"$units\" as _units, \"$source\" as _source, \"$inputFormat\" as _inputFormat, \"$aggregationMethod\" as _aggregationMethod, $fieldName as value into $intoMeasurement from $fromMeasurement where time < ${BEFORE_EPOCH_TIMESTAMP}s and _channel='$channelId';"
			#migrationQuery="select \"$deviceUid\" as _deviceUid, _deviceName, \"$channelId\" as _channel, _channelName, \"$quantity\" as _quantity, \"$label\" as _label, \"$units\" as _units, \"$source\" as _source, \"$inputFormat\" as _inputFormat, \"$aggregationMethod\" as _aggregationMethod, $fieldName as value into $intoMeasurement from ( select _deviceName, _channelName, $fieldName from $fromMeasurement where time < ${BEFORE_EPOCH_TIMESTAMP}s and _channel='$channelId' );"
			echo $countFromQuery
			fromCount=$( $INFLUX_CMD2 -execute "$countFromQuery" | tail -1 | awk '{ print $2 }' )
			if [ "${fromCount:-0}" -gt 0 ] 2> /dev/null; then
				echo "Found a measurement to migrate: [iot_datastore.$rp.$deviceUid] with channel: [$channelId]"
				echo "Found [$fromCount] data points to migrate."
				intoCount=$( $INFLUX_CMD2 -execute "$countIntoQuery" | tail -1 | awk '{ print $2 }' )
				if [ "${intoCount:-0}" -gt 0 ]; then
					# Detected data before migration point
					echo "Found [$intoCount] data points before migration point. Will delete those points ..."
					echo $clearPreviousAttemptQuery
					$INFLUX_CMD2 -execute "$clearPreviousAttemptQuery" || true

					intoCount=$( $INFLUX_CMD2 -execute "$countIntoQuery" | tail -1 | awk '{ print $2 }' )
					if [ 0 -eq "${intoCount:-0}" ]; then
						echo "Data points were deleted."
					else
						echo "Unable to delete previous data points !"
						exit 1
					fi
				fi
				echo $migrationQuery
				$INFLUX_CMD2 -execute "$migrationQuery"
				echo $countIntoQuery
				intoCount=$( $INFLUX_CMD2 -execute "$countIntoQuery" | tail -1 | awk '{ print $2 }' )
				echo "Found [$intoCount] data points migrated."

				if [ "$fromCount" -eq "$intoCount" ] 2> /dev/null; then
					echo "=> Successfully migrated [$intoCount] data points."
				else
					echo "Migration problem: [$fromCount] data points to migrate, but [$intoCount] data points migrated !"
					exit 1
				fi

				rpMigratedPointsCount=$(( rpMigratedPointsCount + intoCount ))
				deviceMigratedPointsCount=$(( migratedPointsCount + intoCount ))

				$INFLUX_CMD2 -execute "select * from $intoMeasurement where time < ${BEFORE_EPOCH_TIMESTAMP}s limit 1;"
			fi

		done < $CHANNELS_FILE

		if [ -z "$rpPointsCountToMigrate" ]; then
			echo "No datapoints to migrate for RP: [$rp]."
		elif [ "$rpPointsCountToMigrate" -eq "$rpMigratedPointsCount" ]; then
			echo "=> Successfully migrated [$rpMigratedPointsCount] data points for RP: [$rp]."
		else
			echo "Migration problem: [$rpPointsCountToMigrate] data points to migrate in RP: [$rp], but [$rpMigratedPointsCount] data points migrated !"
			exit 1
		fi

	done

	echo "=> Successfully migrated [$deviceMigratedPointsCount] data points for device: [$deviceUid]."

	# For now need manual check for migration before deleting old data
	# We may automate migration check with a data count
	dropMigratedMeasurementQuery="drop series from $deviceUid;"

	#echo $dropMigratedMeasurementQuery
	#$INFLUX_CMD2 -execute "$dropMigratedMeasurementQuery"

	dropTimestampMeasurementQuery="drop series from ${deviceUid}__0__timestampToDel;"
	echo $dropTimestampMeasurementQuery
	$INFLUX_CMD2 -execute "$dropTimestampMeasurementQuery"


}


$INFLUX_CMD2 -execute "drop series from testEnProd;"

measurementToMigrateQuery="show measurements where _qualifier='lpp';"

migrateDeviceMeasurements "foombd"
migrateDeviceMeasurements "rokjjm"

