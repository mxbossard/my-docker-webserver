# List all shared networks or shared networks for a dedicated project.
# A dedicated project get all shared networks minus its own networks
# List networkName:networkAlias

#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
source $SCRIPT_DIR/scriptFramework.sh

source $SCRIPT_DIR/globalEnv.sh

cmd="docker network ls -f 'label=fr.mby.shared=env' --format '{{.Label \"com.docker.compose.project\"}} {{.Name}} {{.Label \"fr.mby.alias\"}}' | grep \"^${ENV}_\""
if [ -n "$1" ]
then
	projectName="${ENV}_$( $SCRIPT_DIR/listProjects.sh $1 )"
	eval "$cmd" | grep -v "^$projectName" | awk '{ alias=$2; gsub(/^.*_/, "", alias);  $3=match($3, /[^ ]/)? $3 : alias ; print $2 ":" $3; }' || true
else
	eval "$cmd" | awk '{ alias=$2; gsub(/^.*_/, "", alias);  $3=match($3, /[^ ]/)? $3 : alias ; print $2 ":" $3; }' || true
fi

