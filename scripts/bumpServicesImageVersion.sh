#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
export QUIET=true
source $SCRIPT_DIR/scriptFramework.sh
source $SCRIPT_DIR/globalEnv.sh
source "$SCRIPTS_DIR/dockerImageEnv.sh"


usage() {
	>&2 echo "$1"
	>&2 echo "usage: $0 [-p] [-f] <qualifier> <project> [services]"
	>&2 echo "qualifier '-' mean release."
	>&2 echo "qualifier '#' mean keep qualifier and increment it if numeric qualifier."
	>&2 echo 
	exit 1
}

forceConfig=false
pushImage=false
while echo ":-f:-p:" | grep ":$1:" > /dev/null; do
	if [ "$1" == "-f" ]; then
		forceConfig=true
		shift
	elif [ "$1" == "-p" ]; then
		pushImage=true
		shift
	fi
done

if [ "$1" == "-" ]; then
	# release qualifier
	inputQualifier=""
	shift
elif [ -n "$1" ]; then
	inputQualifier="$1"
	shift
else
	usage "No qualifier supplied !"
fi

if [ "$1" == "all" ]; then
	inputProjects="$( $SCRIPTS_DIR/listProjects.sh )"
	# Bump all project, trash services selection
	inputServices=""
	shift
elif [ -n "$1" ]; then
	inputProjects="$( $SCRIPTS_DIR/listProjects.sh "$1" )" || usage "Supplied project: [$1] does not exists !"
	shift
	inputServices="$@"
else
	usage "No project supplied !"
fi

outTmpFile="$( mkTempFile "bumpImagesVersion" )"
for project in $inputProjects; do
	cd "$PARENT_DIR/$project"
	source $SCRIPT_DIR/projectEnv.sh
	source $SCRIPTS_DIR/loadDynamicDockerComposeConfig.sh

	if [ -z "$inputServices" ]; then
		# Use all services
		services=$( composeCmd config --services )
	else
		services="$inputServices"
	fi
	
	for service in $services; do
		confirmedTag=""
		serviceBuiltImage="$( getServiceBuiltImage "$project" "$service" )"
		test -n "$serviceBuiltImage" || continue
		# Manage only built images

		# Search service for a built image
		imageName="$( echo "$serviceBuiltImage" | cut -d: -f1 )"
		imageTag="$( echo "$serviceBuiltImage" | cut -d: -f2 )"
		test -n "$imageTag" || die "No tag found in image: [$BLUE$serviceBuiltImage$NC] of service: [$PURPLE$project$NC/$GREEN$service$NC] !"

		>&2 infoMsg "Found built image: [$CYAN$serviceBuiltImage$NC] for service: [$PURPLE$project$NC/$GREEN$service$NC]."

		# FIXME: quelle image digest prend-on ? celle dans le registry local by repo:tag ou bien celle available en cours d'utilisation par compose ?
		#imageDigest="$(docker images -q --no-trunc "$serviceBuiltImage" )" # Image in local repo (by image name)
		imageDigest="sha256:$(composeCmd images -q "$service" )" # Image in use by compose service
		test -n "$imageDigest" || die "Unable to find the image digest of service: [$PURPLE$project$NC/$GREEN$service$NC]"

		# Search if this digest exists on remote registry
		# FIXME don't works because digest used by registry are based on compressed image.
		#remoteTags="$( getImageTagsOnRegistryByDigest "$imageName" "$imageDigest" )"
		#if [ -n "$remoteTags" ]; then
		#	# This image (same digest same layer) already exists on remote registry
		#	# Propose to take highest tag by default.
		#	highestTag=$( echo "$remoteTags" | rev | cut -d' ' -f1 | rev )
		#	>&2 infoMsg "Found this image: ($CYAN$( shortImageId "$imageDigest" )$NC)  on remote registry with name: [$CYAN$imageName:$highestTag$NC]."
		#	proposedTag="$highestTag"
		#	if [ "$imageTag" == "$proposedTag" ]; then
		#		# Current tag is published tag and image are the same so propose to stay this way.
		#		>&2 cyanMsg "Suggesting to keep current tag: [$BLUE$imageTag$NC] which is already published."
		#	else
		#		# Current tag and published tag differ => propose to use published tag.
		#		>&2 cyanMsg "Suggesting to use published tag: [$YELLOW$proposedTag$NC] instead of current tag: [$BLUE$imageTag$NC]."
		#	fi

		#	# If forced, do not ask for confirmation
		#	if $forceConfig; then
		#	       	confirmedTag="$proposedTag"
		#	else
		#		# Ask for a confirmation
		#		>&2 read -p "Do you confirm [y/N] ? " confirm
		#		if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ]; then
		#			# default do not confirm
		#			true
		#		else
		#			confirmedTag="$proposedTag"
		#		fi
		#	fi
		#fi

		if [ -z "$confirmedTag" ]; then
			# Find a root version not already released
			rootVersion="$( echo "$imageTag" | cut -d- -f1 )"
			while doesDockerTagExistsOnRegistry "$imageName" "$rootVersion"; do
				# Tag already exists on registry: increment it
				rootVersion="$( $SCRIPTS_DIR/utils/incrementVersion.sh "$rootVersion" 2> /dev/null )"
			done

			# Initial proposal is based on a non published root version
			#proposedTag="$rootVersion$( echo "$imageTag" | sed -e 's/^[0-9.]\+//' )"
			if [ -n "$inputQualifier" ]; then
				proposedTag="$( $SCRIPTS_DIR/utils/incrementVersion.sh "$rootVersion-dummyQualifier" "$inputQualifier" 2> /dev/null )"

				# Search for a tag proposition which don't exists on registry
				while doesDockerTagExistsOnRegistry "$imageName" "$proposedTag"; do
					# Tag already exists on registry: increment it
					proposedTag="$( $SCRIPTS_DIR/utils/incrementVersion.sh "$proposedTag" "$inputQualifier" 2> /dev/null )"
				done
			else
				proposedTag="$rootVersion"
			fi

			# If forced, do not ask for confirmation
			$forceConfig && confirmedTag="$proposedTag" || confirmedTag=""
		fi

		while [ -z "$confirmedTag" ]; do
			# Propose
			if [ "$imageTag" == "$proposedTag" ]; then
				>&2 cyanMsg "Suggesting to keep the tag: [$BLUE$proposedTag$NC]."
			else
				>&2 cyanMsg "Suggesting to bump the tag to: [$YELLOW$proposedTag$NC] from: [$BLUE$imageTag$NC]."
			fi

			# Ask for a confirmation
			>&2 read -p "Do you confirm [y/N] ? " confirm
			if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ]; then
				# default do not confirm

				# Ask for an alternative
				userProposal=""
				while [ -z "$userProposal" ]; do
					>&2 read -p "Choose your bumped tag ? " userProposal
				done

				# Check alternative existance on remote registry
				if doesDockerTagExistsOnRegistry "$imageName" "$userProposal"; then
					>&2 read -p "The tag: [$userProposal] already exists on remote registry. Do you want to use it [y/N] ? " confirm
					if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ]; then
						# default do not confirm
						continue
					else
						confirmedTag="$userProposal"
					fi
				else
					proposedTag="$userProposal"
				fi
			else
				confirmedTag="$proposedTag"
			fi
		done

		#if [ "$imageTag" != "$confirmedTag" ]; then
		if true; then
			bumpedImage="$imageName:$confirmedTag"
			# Tag image with new confirmedTag
			>&2 cyanMsg "Tagging bumped image: [$GREEN$bumpedImage$NC] from: [$BLUE$serviceBuiltImage$NC] ..."
			>&2 docker tag "$serviceBuiltImage" "$bumpedImage"
			if $pushImage; then
				>&2 cyanMsg "Pushing bumped image: [$GREEN$bumpedImage$NC] ..."
				>&2 docker push "$bumpedImage" || die "Failure pushing image: [$bumpedImage] !"
			fi

			# Perform file modifications
			>&2 cyanMsg "Assigning bumped image: [$GREEN$bumpedImage$NC] in compose config ..."
			#$forceConfig && forceOpt="-f" || forceOpt=""
			forceOpt="-f"
			$SCRIPTS_DIR/assignServiceImage.sh $forceOpt "$project" "$service" "$bumpedImage"
		else
			>&2 cyanMsg "No changes applied."
		fi
	done

done > $outTmpFile

cat "$outTmpFile" | sort -u

