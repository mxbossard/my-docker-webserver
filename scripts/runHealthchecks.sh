#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
. $SCRIPT_DIR/scriptFramework.sh

INPUT_PROJECTS="${@:-}"
export QUIET=true

tempOutput=$( mkTempFile )
rmOnExit "$tempOutput"

error=false
projectCount=0
for project in $( $SCRIPT_DIR/listProjects.sh $INPUT_PROJECTS )
do
	projectCount=$(( projectCount + 1 ))
	#infoMsg "Project: [$PURPLE$project$NC]" >> $tempOutput
	
	cd $SCRIPT_DIR/../$project
	set -o pipefail
	$SCRIPT_DIR/healthcheckWrapper.sh | tee -a $tempOutput || error=true
	set +o pipefail
done

if [ $projectCount -gt 1 ]; then
	errOut ""
        >&2 cyanMsg "----- Script $0 report: -----"
        cat $tempOutput
fi

! $error || exit 1
