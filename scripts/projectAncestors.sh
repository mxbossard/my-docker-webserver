#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
. $SCRIPT_DIR/scriptFramework.sh

DEPENDENCY_TREE_FILE="$( readlink -f $SCRIPT_DIR/../conf/projectDependencyTree.txt )"

displaySelf=false
if [ "$1" == "--self" ]
then
	displaySelf=true
	shift
fi

INPUT_PROJECTS="$@"
ALL_PROJECTS=false

if test -z "$INPUT_PROJECTS"
then
	exit 0
elif [ "all" == "$1" ]
then
	ALL_PROJECTS=true
	INPUT_PROJECTS="all"
fi

i=0
tempFile=$( mkTempFile )
rmOnExit "$tempFile"

projectFound=false
ancestorFound=false
# strip /
INPUT_PROJECTS="$( echo $INPUT_PROJECTS | sed -e 's|/||g' )"
for project in $INPUT_PROJECTS
do
	projectDir="$SCRIPT_DIR/../$project"
	$ALL_PROJECTS || test -d "$projectDir" || die "Project [$project] referenced in file $DEPENDENCY_TREE_FILE does not exists !"
	ancestors=""
	while read line
	do
		projectName=$( echo $line | cut -d: -f1 )
		projectDependencies=$( echo $line | cut -d: -f2- )

		if [ "$project" == "$projectName" ]
		then
			projectFound=true
		fi

		# Do not treat empty lines and commented lines
		if test -n "$projectName" && echo "$projectName" | grep -ve '^\s*#' > /dev/null
		then
			#>&2 echo "projectName: $projectName ; projectDependencies: $projectDependencies"		
			if $ALL_PROJECTS || echo "$projectDependencies" | grep -e "$project" > /dev/null
			then
				ancestorFound=true
				#>&2 echo "matching line: $projectName :: $projectDependencies"
				ancestors="$ancestors $projectName"
			fi

		fi

	done < $DEPENDENCY_TREE_FILE 

	if ! $projectFound && ! $ALL_PROJECTS
	then
		die "Project [$project] is not referenced in file $DEPENDENCY_TREE_FILE !"
	fi

	# if no ancestor project found for $project, it should be print.
	if $ancestorFound
	then
		#>&2 echo "ancestors: $ancestors"
		# Recusrive call
		$0 --self $ancestors
	fi

	if ! $ALL_PROJECTS
	then
		$displaySelf && echo $project || true
	fi
done >> $tempFile

# Remove duplicate lines
cat $tempFile | awk '!x[$0]++' 
