#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
export QUIET=true
source $SCRIPT_DIR/scriptFramework.sh
source $SCRIPT_DIR/globalEnv.sh

inputProjects=""
while test -n "$1" && projects="$( $SCRIPTS_DIR/listProjects.sh $1 2> /dev/null )"; do
	inputProjects="$inputProjects $projects"
	shift
done

test -n "$inputProjects" || inputProjects="$( basename $PWD )"
debug "inputProjects: $inputProjects"

for project in $inputProjects; do
	cd "$PARENT_DIR/$project"
	source $SCRIPT_DIR/projectEnv.sh
	source $SCRIPTS_DIR/loadDynamicDockerComposeConfig.sh

	dcCmd="$DOCKER_COMPOSE_CMD $@"
	debug "dcCmd: $dcCmd"
	$dcCmd
done
