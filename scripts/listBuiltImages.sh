#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
export QUIET=true
source $SCRIPT_DIR/scriptFramework.sh
source $SCRIPT_DIR/globalEnv.sh

inputProjects=""
while test -n "$1" && projects="$( $SCRIPTS_DIR/listProjects.sh $1 2> /dev/null )"; do
	inputProjects="$inputProjects $projects"
	shift
done

test "$1" == "all" && inputProjects="$( $SCRIPTS_DIR/listProjects.sh )" || true
test -n "$inputProjects" || die "No projects selected (use all to select all projects) !"

debug "inputProjects: $inputProjects"

>&2 infoMsg "Listing built docker images (with a build section in compose config) ..."

for project in $inputProjects; do
	cd "$PARENT_DIR/$project"
	source $SCRIPT_DIR/projectEnv.sh
	source $SCRIPTS_DIR/loadDynamicDockerComposeConfig.sh

	$DOCKER_COMPOSE_CMD config | $SCRIPTS_DIR/yq.sh eval -j - | jq -r '.services | to_entries | map( select(.value | has("build")) | (.key + " " + .value.image)) | .[]' | while read serviceAndImage; do
		echo "$project $serviceAndImage"
	done
done
