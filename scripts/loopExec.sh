#! /bin/bash
SCRIPT_DIR="$( dirname $( readlink -f $0 ) )"

. $SCRIPT_DIR/scriptFramework.sh

cmd="$1"
shift
iterable=("$@")

test -n "$iterable" || die "No iterable supplied to loop execution for !"
debug "Loop executing: [$cmd] for items: [${iterable[@]}]."
for item in ${iterable[@]}
do
	toExec=$( echo $cmd | sed -e "s/{}/$item/g" )
	debug "Loop executing: [$toExec] ..."
	eval "$toExec"
done
