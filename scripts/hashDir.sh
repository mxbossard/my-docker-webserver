#! /bin/sh -e

DEFAULT_EXCLUSIONS="'*.bkp' '*.bak' '*.swp' '*.rendered' '.git/*'"
DEFAULT_EXCLUSIONS="${HASH_DEFAULT_EXCLUSIONS:-$DEFAULT_EXCLUSIONS}"
VERBOSE=false
$DEBUG > /dev/null 2>&1 && DEBUG="true" || DEBUG="false"

usage() {
	>&2 echo "usage: $0 [-v] <dirToHash> [pathExclusion1] ... [pathExclusionN]" 
	>&2 echo "by default, exclusions are: $DEFAULT_EXCLUSIONS (overriden by HASH_DEFAULT_EXCLUSIONS env var)."
        >&2 echo
	exit 1
}

if [ "$1" = "-v" ]; then
	VERBOSE=true
	shift
fi

dir="$1"
test -d "$dir" || usage
shift

# Default exclusions if no exclusion specified
test $# -gt 0 || set -- $DEFAULT_EXCLUSIONS

excludeArgs=""
if test -n "$1"; then
	# first exclusion
	opt=$( echo "$1" | sed -e "s/['\"]//g" )
	excludeArgs="-path '$opt'"
	shift
fi
while test -n "$1"; do
	# next exclusion
	opt=$( echo "$1" | sed -e "s/['\"]//g" )
	excludeArgs="$excludeArgs -o -path '$opt'"
	shift
done
test -z "$excludeArgs" || excludeArgs="\( $excludeArgs \) -prune -o"
$DEBUG && >&2 echo "excludeArgs: $excludeArgs"

listFilesCmd="find $dir $excludeArgs -type f -print0"
listFilesAndDirPermsCmd="find $dir $excludeArgs \( -type f -o -type d \) -print0"

export LC_ALL=POSIX
hashDirContent() {
	eval "$listFilesCmd" | sort -z | xargs -0 sha1sum
	eval "$listFilesAndDirPermsCmd" | sort -z | xargs -0 stat -c '%n %a'
}

$VERBOSE && >&2 hashDirContent || true
hashDirContent | sha1sum | awk '{ print $1 }'
