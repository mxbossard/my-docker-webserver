#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
export QUIET=true
source $SCRIPT_DIR/scriptFramework.sh
source $SCRIPT_DIR/globalEnv.sh

usage() {
	errOut "$1"
	errOut "usage: $0 [-f] <project> <service> <image>"
	errOut 
	exit 1
}

forceConfig=false
if [ "$1" == "-f" ]; then
	forceConfig=true
	shift
fi

project="$1"
service="$2"
image="$3"

test -n "$project" || usage "project is missing !"
test -n "$service" || usage "service is missing !"
test -n "$image" || usage "image is missing !"

project="$( $SCRIPTS_DIR/listProjects.sh "$project" )" || usage "project: [$project] not found !"

cd "$PARENT_DIR/$project"
source $SCRIPT_DIR/projectEnv.sh
source $SCRIPTS_DIR/loadDynamicDockerComposeConfig.sh

composeCmd config --services | grep "^$service$" > /dev/null || usage "service: [$service] not found in project: [$project] !"

configuredImageVal="$( composeCmd config | $SCRIPTS_DIR/yq.sh eval ".services.$service.image" - )"

>&2 infoMsg "Assigning image: [$GREEN$image$NC] for service: [$PURPLE$project$NC/$GREEN$service$NC] (previous image was: [$BLUE$configuredImageVal$NC] ) ..."

### Configuring compose files
configured=false
outTmpFile=$( mkTempFile "out" )
# Find project config files to update
for composeFile in $DOCKER_COMPOSE_CONFIG_FILES; do
	test -f "$composeFile" || continue
	debug "Scanning composeFile: [$composeFile] for service: [$service] image definition ..."

	serviceImageVal="$( cat "$composeFile" | $SCRIPTS_DIR/yq.sh eval ".services.$service.image | select(. != null)" - )"
	trace "Found service image: [$serviceImageVal]."
	# Check if image field correspond to configured image 
	# (if check raw then envsubst: -o "$image" == "$( echo $serviceImageVal | envsubst )" )
	if [ "$image" == "$serviceImageVal" ]; then
		# This file is already configured with image: do nothing
		>&2 infoMsg "Image is already configured in file: [$GREY$composeFile$NC]."
		configured=true
	#elif [ "$configuredImageVal" != "null" ]; then
	elif [ -n "$serviceImageVal" ]; then
		# This file is configured with actual image: propose to update
		>&2 warnMsg "Will replace image: [$BLUE$serviceImageVal$NC] by: [$CYAN$image$NC] in file: [$GREY$composeFile$NC]."
		if ! $forceConfig; then 
			read -p "Do you want to update the file (if no exit the process) ? [y/N] " confirm
			if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ]; then
				exit 1
			fi
		fi

		rm -f -- "$composeFile.version.bak"
		cp -a "$composeFile" "$composeFile.version.bak"
		if ! cat "$composeFile.version.bak" | $SCRIPTS_DIR/yq.sh eval ".services.$service.image = \"$image\"" - > $composeFile; then
			# Rollback if edition failed 
			mv "$composeFile.version.bak" "$composeFile"
			die "Unable to bump image version in file: [$composeFile]"
		fi
		>&2 infoMsg "Configured file: [$GREY$composeFile$NC]."
		>&2 diff "$composeFile.version.bak" "$composeFile" || true
		configured=true

		# Print modified file
		echo "$composeFile"
	fi
done > $outTmpFile

### Configuration check
$configured || die "Unable to find image definition in a compose yml file !"

>&2 infoMsg "Checking config effectiveness ..."
source $SCRIPTS_DIR/loadDynamicDockerComposeConfig.sh --force
actualImageValue=$( $DOCKER_COMPOSE_CMD config | $SCRIPTS_DIR/yq.sh eval ".services.$service.image" - )

if [ "$image" == "$actualImageValue" ]; then
	cat "$outTmpFile" | sort -u
	>&2 successMsg "Image configuration is successful."
else
	# rollback files
	for file in $( cat "$outTmpFile" ); do
		mv "$file.version.bak" "$file"
	done
	die "Service: [$PURPLE$project$NC/$GREEN$service$NC] image was not configured as expected (expected: [$CYAN$image$NC], but actual is: [$RED$actualImageValue$NC]) ! Compose config files were rolledback."
fi
