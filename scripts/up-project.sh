#! /bin/bash -e 
SCRIPT_DIR="$( dirname $( readlink -f $0 ) )"

#source $SCRIPT_DIR/globalEnv.sh

ERROR_MESSAGE="Unknown failure during init phase !"
errorMessage() {
	ERROR_MESSAGE="$1"
	exit 1
}

onErrHook() {
	alertMsg "Project: [$PURPLE$PROJECT_NAME$NC] was not up ! $ERROR_MESSAGE"
}

source $SCRIPT_DIR/projectEnv.sh || errorMessage "Unable to load project env."
cd "$PROJECT_DIR"

quiet=false
forceBuild=false
forceNoBuild=false
forceUp=false
forcePull=false
disableTrust=true
strictTesting=false
while [ -n "$1" ]
do
	if [ "$1" == "-f" -o "$1" == "--force" ]
	then
		forceUp=true
	elif [ "$1" == "-b" -o "$1" == "--build" ]
	then
		forceBuild=true
	elif [ "$1" == "-n" -o "$1" == "--no-build" ]
	then
		forceNoBuild=true
	elif [ "$1" == "-p" -o "$1" == "--pull" ]
	then
		forcePull=true
	elif [ "$1" == "-q" -o "$1" == "--quiet" ]
	then
		quiet=true
	
	elif [ "$1" == "-s" -o "$1" == "--strict" ]
	then
		strictTesting=true

	elif [ "$1" == "-t" -o "$1" == "--trust" ]
	then
		disableTrust=false
	fi
	shift
done

! $quiet || QUIET=true

PROJECT_HASH_FILE="$PROJECT_DIR/.project.hash"
ENVIRONMENT_AUDIT_FILE="$PROJECT_DIR/.environment.audit"
DOCKER_CONFIG_AUDIT_FILE="$PROJECT_DIR/.docker_config.audit"
DEPLOY_AUDIT_FILE="$PROJECT_DIR/.deploy_metadata.audit"

dockerCompose() {
	source $SCRIPTS_DIR/loadDynamicDockerComposeConfig.sh
	$DOCKER_COMPOSE_CMD "$@"
}

dockerConfigTempFile=
cachedDockerConfig() {
	if [ ! -f "$dockerConfigTempFile" ]; then
		dockerConfigTempFile="$( mkTempFile dockerConfig )"
		# FIXME need to force load of dyn compose config to have image bumped and rolledback working
		source $SCRIPTS_DIR/loadDynamicDockerComposeConfig.sh
		debug "Caching docker compose config."
		dockerCompose config > "$dockerConfigTempFile"
	else
		debug "Loading docker compose config from cache."
	fi
	cat "$dockerConfigTempFile"
}

hashDirExclusions="'*.bkp' '*.bak' '*.swp' '*.rendered' '*/.git' '$PROJECT_DIR/env.sh' '$PROJECT_DIR/*.yml' '$PROJECT_DIR/up.sh' '$PROJECT_DIR/*.audit' '$PROJECT_DIR/tests' '$PROJECT_HASH_FILE'"
hasSourcesChanged() {
	# Check if sources have changed
	$SCRIPTS_DIR/isSourceDirChanged.sh --hash-file "$PROJECT_HASH_FILE" "$PROJECT_DIR" $hashDirExclusions 2> /dev/null && return 0 || return 1
}

isFirstDeployment() {
	# Check if project was previously deployed
	test -f "$DOCKER_CONFIG_AUDIT_FILE" && return 1 || return 0
}

hasDockerConfigChanged() {
	# Check if docker-compose config has changed
	$DEBUG && diffOut=/dev/stderr || diffOut=/dev/null
	cachedDockerConfig | diff "$DOCKER_CONFIG_AUDIT_FILE" - > $diffOut && return 1 || return 0
}

isDockerImageBuildMissing() {
	# Check if image is missing locally
	# if pull forced: ignore config image change
	if ! $forcePull; then
		targetServices=$( cachedDockerConfig | $SCRIPTS_DIR/yq.sh eval '.services.* | select(. | has("build")) | .image' - )
		info "Checking for a missing build for target services: [$targetServices] ..."
		for image in $targetServices; do
			debug "Checking if image: [$image] build is missing ..."
			docker images -q "$image" | grep "." > /dev/null || return 0
		done
	fi
	return 1
}

isDockerImageOutdated() {
	# Check if currently deployed image is outdated

	# Deployed images:
	targetServices=$( cachedDockerConfig | $SCRIPTS_DIR/yq.sh eval '.services.* | path | .[-1]' - ) || return 42
	info "Checking deployed images are not outdated for target services: [$targetServices] ..."
	test "$DOCKER_COMPOSE_PROJECT" == "$PROJECT_NAME" || source $SCRIPTS_DIR/loadDynamicDockerComposeConfig.sh
	for service in $targetServices; do
		deployedImageId=$( dockerCompose images -q $service ) || return 42
		if [ -n "$deployedImageId" ]; then
			targetImageName=$( dockerCompose config | $SCRIPTS_DIR/yq.sh eval ".services.$service.image" - ) || return 42
			targetImageId=$( docker images -q --no-trunc $targetImageName | sed -e 's/^[^:]\+://' ) || return 42
			debug "Checking if deployed image id: [$deployedImageId] is identical to target image id: [$targetImageId]"
			test "$targetImageId" == "$deployedImageId" || return 0
		else
			# Target service don't exists ?
			return 0
		fi
	done
	return 1
}

### Render template
templatesRendered=false
renderTemplatesOnce() {
	# Render project templates
	$templatesRendered || $SCRIPTS_DIR/renderProjectTemplates.sh ".tpl" "$PROJECT_DIR" && templatesRendered=true || errorMessage "Fail rendering project templates !"
}

if [ "$ENV" == "dev" -a -n "$SUB_DOMAIN_NAME" ]
then
	>&2 $PARENT_DIR/dev/addDevSslDomain.sh "$SUB_DOMAIN_NAME"
fi

info "Uping project: [$PROJECT_NAME] ..."

ERROR_MESSAGE="Unknown failure during pull phase !"
#trap 'errorMessage "Unknown failure during pull phase !" $? $LINENO $BASH_LINENO "$BASH_COMMAND" $(printf "::%s" ${FUNCNAME[@]})' ERR

export HEALTHCHECK_INITIAL_DELAY=false

$DEBUG && debugOut=/dev/stderr || debugOut=/dev/null

### Pull
pulledNewImage=false
if $forcePull; then

	projectImages=$( dockerCompose config | $SCRIPTS_DIR/yq.sh eval '.services.*.image' - )
	debug "projectImages: [$projectImages]"
	tempFile=$( mkTempFile "dockerPullOut" )
	for dockerImage in $projectImages; do
		>&2 infoMsg "Pulling docker image: [$dockerImage] ..."
		docker pull --disable-content-trust=$disableTrust $dockerImage | >&2 tee $tempFile || errorMessage "Unable to pull docker image: [$dockerImage] !"
		$disableTrust || >&2 docker trust inspect $dockerImage || errorMeesage "Unable to verify trust for docker image: [$dockerImage] !"
		if ! grep "^Status: Image is up to date" $tempFile > /dev/null; then
			infoMsg "Project: [$PURPLE$PROJECT_NAME$NC] pulled a new Docker image: [$dockerImage]."
			pulledNewImage=true
		fi
	done
fi

ERROR_MESSAGE="Unknown failure during build phase !"
#trap 'errorMessage "Unknown failure during build phase !" $? $LINENO $BASH_LINENO "$BASH_COMMAND" $(printf "::%s" ${FUNCNAME[@]})' ERR

### Need to build ?
doBuild=false
if $forcePull; then
	info "Forcing pull so we won't build images for project: [$PROJECT_NAME]."
elif $forceNoBuild; then
	info "Forcing no build so we won't build images for project: [$PROJECT_NAME]."
	# FIXME: Pull image if don't exists locally ?
elif $forceBuild; then
	info "Forcing build for project: [$PROJECT_NAME]."
	doBuild=true
	doBuildReason="--build supplied"
elif hasSourcesChanged; then
	info "Trigger build because sources have changed for project: [$PROJECT_NAME]."
	doBuild=true
	doBuildReason="sources have changed"
elif isDockerImageBuildMissing; then 
	info "Trigger build because a built image is missing for project: [$PROJECT_NAME]."
	doBuild=true
	doBuildReason="missing local image"
else
	info "Skip docker-compose build for project: [$PROJECT_NAME]."
fi


### Build

# Hook scripts
beforeBuildScript="$( readlink -f ./before-build.sh )"
afterBuildScript="$( readlink -f ./after-build.sh )"

if $doBuild; then
	mappingTempFile="$( mkTempFile "mapping" )"
	# Check versionning 
	tempTagQualifier="temp_$( date '+%s' )"
	$SCRIPTS_DIR/listBuiltImages.sh "$PROJECT_NAME" 2> $debugOut | while read line; do
		service="$( echo "$line" | awk '{ print $2 }' )"
		image="$( echo "$line" | awk '{ print $3 }' )"
		imageName="$( echo "$image" | cut -d: -f1 )"
		imageTag="$( echo "$image" | cut -d: -f2 )"
		>&2 infoMsg "Checking if service: [$service] image: [$image] tag: [$imageTag] need a bump ..."
		source "$SCRIPTS_DIR/dockerImageEnv.sh"

		if [ -z "$( docker images -q $image )" ]; then
			# if image does not exists, try to pull it
			debug "Pulling attempt of image: [$image] ..."
			>&2 docker pull $image || true
			#continue
		fi

		if echo "$imageTag" | egrep "^[0-9.]+$" > /dev/null; then
			# if released => bump version with dev qualifier
			# released version
			newImageTag="$( $SCRIPTS_DIR/utils/incrementVersion.sh "$imageTag" "dev" 2> $debugOut )"
		elif doesDockerTagExistsOnRegistry "$imageName" "$imageTag" 2> $debugOut; then
			# if tag exists on remote registry => increment tag if possible
			newImageTag="$( $SCRIPTS_DIR/utils/incrementVersion.sh "$imageTag" "#" 2> $debugOut )"
		else
			newImageTag="$imageTag"
		fi
		newImage="$imageName:$newImageTag"
		tempImage="$newImage-$tempTagQualifier"
		echo "$service;$image;$tempImage;$newImage" >> "$mappingTempFile"
		if [ -n "$newImageTag" ]; then
			debug "Bumping image to: [$tempImage] ..."
			# Reload compose config
			source $SCRIPTS_DIR/loadDynamicDockerComposeConfig.sh --force
			$SCRIPTS_DIR/assignServiceImage.sh -f "$PROJECT_NAME" "$service" "$tempImage" > $debugOut 2>&1
		fi

	done

	# Render templates
	renderTemplatesOnce

	# docker-compose build
	buildCmd="dockerCompose build"
	debug "docker-compose build cmd: [$buildCmd]"
	buildTempFile="$( mkTempFile build )"
	buildDone=false

	# Hook before build
        if [ -x "$beforeBuildScript" ]
        then
                info "Executing 'before build hook' script: [$beforeBuildScript]"
                >&2 $beforeBuildScript || errorMessage "Fail executing 'before build hook' script: [$beforeBuildScript] !"
        fi

	if $buildCmd > "$buildTempFile"; then
		>&2 cat "$buildTempFile"
		grep -v "uses an image, skipping$" "$buildTempFile" > /dev/null && buildDone=true
	else
		>&2 cat "$buildTempFile" 
		warn "You may need to rollback docker compose files !"
		errorMessage "Fail executing docker-compose build !"
	fi

	# Hook after build
	if [ -x "$afterBuildScript" ]
	then
		info "Executing 'after build hook' script: [$afterBuildScript]"
		$afterBuildScript || errorMessage "Fail executing 'after build hook' script: [$afterBuildScript] !"
	fi

	if $buildDone; then

		# Check if new image digest is different than previous image digest
		info "Checking if built image are different from previous build ..."
		newBuiltImages=" "
		while read mapping; do
			service="$( echo "$mapping" | cut -d';' -f1 )"
			originalImage="$( echo "$mapping" | cut -d';' -f2 )"
			tempImage="$( echo "$mapping" | cut -d';' -f3 )"
			newImage="$( echo "$mapping" | cut -d';' -f4 )"
			debug "Checking for image digest changed from: [$originalImage] to: [$tempImage] ..."
			originalImageDigest="$( docker image ls -q --no-trunc "$originalImage" )" || true
			debug "original image digest: [$originalImageDigest]"
			tempImageDigest="$( docker image ls -q --no-trunc "$tempImage" )"
			debug "temp image digest: [$tempImageDigest]"
			# Reload compose config
			source $SCRIPTS_DIR/loadDynamicDockerComposeConfig.sh --force
			if [ "$originalImageDigest" == "$tempImageDigest" ]; then
				# No image change => Remove temp image and rollback config
				info "Rollbacking image from: [$tempImage] to: [$originalImage] ..."
				>&2 docker tag "$tempImage" "$originalImage" # retag originalImage just in case
				>&2 docker rmi -f "$tempImage"
				$SCRIPTS_DIR/assignServiceImage.sh -f "$PROJECT_NAME" "$service" "$originalImage" > $debugOut 2>&1
			else
				# Image changed => keep new Image renaming it without temp qualifier
				info "Commiting image to: [$newImage] ..."
				>&2 docker tag "$tempImage" "$newImage"
				$SCRIPTS_DIR/assignServiceImage.sh -f "$PROJECT_NAME" "$service" "$newImage" > $debugOut 2>&1
				>&2 docker rmi -f "$tempImage"
				newBuiltImages="${newBuiltImages}${newImage} "
				debug "newBuiltImages: [$newBuiltImages]"
			fi
		done < "$mappingTempFile"

		if [ "$newBuiltImages" == " " ]; then
			cyanMsg "Project: [$PURPLE$PROJECT_NAME$NC] was (re)built ($doBuildReason). Built images were not newer and were discarded."
		else
			cyanMsg "Project: [$PURPLE$PROJECT_NAME$NC] was (re)built ($doBuildReason). New built images are: [$GREEN$newBuiltImages$NC]"
		fi

		# Reload compose config	
		dockerConfigTempFile=
		source $SCRIPTS_DIR/loadDynamicDockerComposeConfig.sh --force
	else
		# If build not done (no image to build in project) nothing to do
		info "No image to build found by compose."
	fi
fi || errorMessage "Failed building images !"

ERROR_MESSAGE="Unknown failure during up phase !"
#trap 'errorMessage "Unknown failure during up phase !" $? $LINENO $BASH_LINENO "$BASH_COMMAND" $(printf "::%s" ${FUNCNAME[@]})' ERR

### Need to up ?
doUp=false
doHealthcheck=true
if $forceUp; then
	info "Force docker-compose up for project: [$PROJECT_NAME] ..."
	doUp=true
	doUpReason="--force suplied"

elif isFirstDeployment; then
	info "Launching docker-compose up for project: [$PROJECT_NAME] because never deployed yet ..."
	doUp=true
	doUpReason="never deployed yet"
	
elif hasDockerConfigChanged; then
	info "Launching docker-compose up for project: [$PROJECT_NAME] because docker config has changed ..."
	doUp=true
	doUpReason="docker config has changed"

elif hasSourcesChanged; then
        info "Launching docker-compose up for project: [$PROJECT_NAME] because sources have changed ..."
        doUp=true
        doUpReason="sources have changed"

elif isDockerImageOutdated; then
	info "Launching docker-compose up for project: [$PROJECT_NAME] because at least one image is outdated ..."
	doUp=true
	doUpReason="some images were outdated"

elif info "Checking if project: [$PROJECT_NAME] is already up ..." && ! $SCRIPT_DIR/isProjectUp.sh "$PROJECT_NAME" >&2; then
	info "Launching docker-compose up for project: [$PROJECT_NAME] because not healthy ..."
	doUp=true
	doUpReason="project is not healthy"
else
	info "Skip docker-compose up for project: [$PROJECT_NAME] (already up and healthy)."
	doHealthcheck=false
fi

### Up

# Hook scripts
beforeUpScript="$( readlink -f ./before-up.sh )"
afterUpScript="$( readlink -f ./after-up.sh )"

if $doUp
then
	# Render templates
	renderTemplatesOnce

	test "$DOCKER_COMPOSE_PROJECT" == "$PROJECT_NAME" || source $SCRIPTS_DIR/loadDynamicDockerComposeConfig.sh

	# Hook before up
	if [ -x "$beforeUpScript" ]
	then
		info "Executing 'before up hook' script: [$beforeUpScript]"
		>&2 $beforeUpScript || errorMessage "Fail executing 'before up hook' script: [$beforeUpScript] !"
	fi

	#$DEBUG && >&2 cachedDockerConfig || true
	$DEBUG && >&2 dockerCompose config || true

	# docker-compose up
	upCmd="dockerCompose up -d --renew-anon-volumes --remove-orphans --no-build $args"
	debug "docker-compose up cmd: [$upCmd]"
	>&2 $upCmd || errorMessage "Fail executing docker-compose up !"

	# Hook after up
	if [ -x "$afterUpScript" ]
	then
		info "Executing 'after up hook' script: [$afterUpScript]"
		$afterUpScript || errorMessage "Fail executing 'after up hook' script: [$afterUpScript] !"
	fi

	# Write audit files
	$SCRIPTS_DIR/printProjectMetaData.sh > $DEPLOY_AUDIT_FILE
	cachedDockerConfig > "$DOCKER_CONFIG_AUDIT_FILE"

	export HEALTHCHECK_INITIAL_DELAY=true

	cyanMsg "Project: [$PURPLE$PROJECT_NAME$NC] was (re)deployed ($doUpReason)."
else
	infoMsg "Project: [$PURPLE$PROJECT_NAME$NC] was not touched."
fi

# If no error we can overwrite hash dir
if hasSourcesChanged; then
	info "Generating new dir content hash ..."
	$DEBUG && hashArgs="-v" || hashArgs=""
	$SCRIPTS_DIR/hashDir.sh $hashArgs "$PROJECT_DIR" $hashDirExclusions > $PROJECT_HASH_FILE
fi

# Healthchecks
ERROR_MESSAGE="Failure during healthcheck phase !"

if ! $doHealthcheck || $SCRIPTS_DIR/healthcheckWrapper.sh; then
	echo > /dev/null # Do nothing
else
	#alertMsg "Project: [$PURPLE$PROJECT_NAME$NC] failed to up (Healthchecks KO) !"
	exit 1
fi

# Integration tests
ERROR_MESSAGE="Failure during integration tests phase !"

$SCRIPTS_DIR/runIntegrationTests.sh "$PROJECT_NAME" || ! $strictTesting

