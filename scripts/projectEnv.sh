# This script cannot be moved from scripts/ directory.

test -n "$DEBUG" && $DEBUG > /dev/null 2>&1 && export DEBUG=true || export DEBUG=false
test -n "$QUIET" && $QUIET > /dev/null 2>&1 && export QUIET=true || export QUIET=false

! $TRACE || >&2 echo "Sourcing project env ..."

### Determine PARENT_DIR env var
if [ ! -d "$PARENT_DIR" -o ! -f "$PARENT_DIR/scripts/globalEnv.sh" ]
then
        ! $TRACE || >&2 echo "Evaluating PARENT_DIR ..."

	absolutePath() {
                cd "$( dirname "$1" )"
                echo "$PWD/$( basename "$1" )"
        }

        ### Determine SOURCED_SCRIPT (cache it because we don't need to export it
        sourcedScriptScanOrder="SOURCED_SCRIPT_PATH BASH_SOURCE[0]"
        for varName in $sourcedScriptScanOrder
        do
                varValue="$(eval echo \${$varName:- Not Defined !})"
                ! $TRACE || >&2 echo "Scanning for sourced script path in: [$varName (=$varValue)] ..."
                test -f "$varValue" && sourcedScriptPath="$( absolutePath "$varValue" )" && break || true
        done
        ! $TRACE || >&2 echo "sourced script path found: [$sourcedScriptPath]" || true
        test -f "$sourcedScriptPath" || ( >&2 echo "Unable to detect sourced script path !" ; exit 1 )

        SCRIPTS_DIR=$( dirname $( readlink -f "$sourcedScriptPath" ) )
	UTILS_DIR="$SCRIPTS_DIR/utils"
        PARENT_DIR="$( readlink -f $SCRIPTS_DIR/.. )"

        # Export static env vars
        export PARENT_DIR
	export UTILS_DIR
        export SCRIPTS_DIR

fi

test ! -d "$PARENT_DIR" && >&2 echo "Unable to determine PARENT_DIR env var !" && exit 1 || true

source $SCRIPTS_DIR/scriptFramework.sh

### Determine PROJECT_DIR env var
sourcingScriptScanOrder="SOURCING_SCRIPT_PATH 0"
for varName in $sourcingScriptScanOrder
do
	varValue="$(eval echo \${$varName:- Not Defined !})"
	trace "Scanning for sourcing script path in: [$varName (=$varValue)] ..."
	test -f "$varValue" && sourcingScriptPath="$( $UTILS_DIR/absolutePath.sh "$varValue" )" && break || true
done
if [ -f "$sourcingScriptPath" ]
then
	trace "Sourcing script path found: $sourcingScriptPath"
else
	trace "Unable to detect sourcing script path !"
fi

projectDirScanOrder="$sourcingScriptPath $PWD/foo"
projectDir=""
projectName=""
for file in $projectDirScanOrder
do
	trace "projectDirScanOrder: file: $file"
	projectDir=$( readlink -f $( $UTILS_DIR/absolutePath.sh "$file" | egrep -o "$PARENT_DIR/[^/]+/" || echo "") 2> /dev/null ) || true
	test -d "$projectDir" || continue #die "Script sourced from outside project scope !"
	projectName="$( $SCRIPTS_DIR/listProjects.sh $( basename $projectDir ) 2> /dev/null || true )"
	test -n "$projectName" && break || true
done

test -d "$projectDir" || die "Unable to determine PROJECT_DIR env var. Maybe the project is not declared in $PARENT_DIR/conf/projectDependencyTree.txt"

export PROJECT_DIR="$projectDir"
export PROJECT_NAME="$projectName"
trace "PROJECT_DIR: [$PROJECT_DIR]"
trace "PROJECT_NAME: [$PROJECT_NAME]"


if [ "$_PROJECT_ENV_LOADED" != "$projectName" ]
then
	# Do not load project env twice
	trace "Evaluating project: [$projectName] env ..."

	# Source global env
	source "$PARENT_DIR/scripts/globalEnv.sh"
	export PROJECT_DIR="$projectDir"
	export PROJECT_NAME=$projectName
	export PROJECT_NAMESPACE="${NAMESPACE}_$PROJECT_NAME"

	debug "Loading project: [$PROJECT_NAME] env ..."

	# Namespaced docker compose cmd
	export DOCKER_COMPOSE_CMD="docker-compose -p $PROJECT_NAMESPACE --project-directory $PROJECT_DIR"

	export PROJECT_ENV_PATH="$PROJECT_DIR/env.sh"
	overridingProjectEnvFile="$PROJECT_DIR/env.override.sh"
	test -f "$PROJECT_ENV_PATH" || die "You must create a dedicated env.sh file for the project setup."
	trace "Sourcing PROJECT_ENV_PATH: [$PROJECT_ENV_PATH] ..."
	source $PROJECT_ENV_PATH
	if [ -f "$overridingProjectEnvFile" ]; then
		trace "Sourcing overriding project env file: [$overridingProjectEnvFile] ..."
	       source "$overridingProjectEnvFile"
        fi

	if [ -n "$SUB_DOMAIN_NAME" ] && [ -n $ROOT_DOMAIN_NAME ]
	then
		export VIRTUAL_HOST="${SUB_DOMAIN_NAME}.${ROOT_DOMAIN_NAME}"
		export LETSENCRYPT_HOST="${SUB_DOMAIN_NAME}.${ROOT_DOMAIN_NAME}"
	fi

	### Source project env.sh file
	test -f "$PROJECT_ENV_PATH" || die "You must create a dedicated env.sh file for the project setup."
	set -a
	source "$PROJECT_ENV_PATH"
	test -f "$overridingProjectEnvFile" && source "$overridingProjectEnvFile" || true
	if [ "$INSECURED_ENV" == "true" ]; then
		#If insecured env source example credentials file first
		source "$CREDENTIALS_FILEPATH.example"
	fi
	test -f "$CREDENTIALS_FILEPATH" && source "$CREDENTIALS_FILEPATH" || true
	set +a

	### Auto shift SHIFTING_PORTS_...
	shiftPort() {
		local port="$1"
		local portShift="$2"

		shiftedPort=$(( $port + $portShift ))
		test $shiftedPort -lt 60000 || shiftedPort=$(( $shiftedPort % 60000 ))
		test $shiftedPort -gt 1024 || shiftedPort=$(( $shiftedPort + 10000 ))

		alreadyUsedPorts="$( ss -tunlp | awk '{ print $5; }' | grep '[0-9]:' | cut -d: -f2 )"
		alreadyUsedPorts=":$( echo $alreadyUsedPorts | tr ' ' ':' ):"
		trace "alreadyUsedPorts: [$alreadyUsedPorts]"

		# FIXME: once service is started port is used and shift again so integration test fails !
		#while echo "$alreadyUsedPorts" | grep ":$shiftedPort:" > /dev/null; do
		#	# If calculated shifted port is already in use increment it.
		#	trace "Port: [$shiftedPort] is already in use."
		#	shiftedPort=$(( shiftedPort + 1 ))
		#done

		echo $shiftedPort
	}

	if [ -n $PORT_SHIFT -a $PORT_SHIFT -gt 0 ]; then
		for varName in ${!SHIFTING_PORT_*}
		do
			portValue="${!varName}"
			test -n "$portValue" || continue
			test $portValue -gt 0 -a $portValue -le 65535 || die "Declared $varName got a bad port value: [$portValue] ! It should be between (1 - 65535)."
			shiftedPortValue=$( shiftPort $portValue $PORT_SHIFT )
			trace "Shift $varName from [$portValue] to [$shiftedPortValue]."
			export $varName=$shiftedPortValue
		done
	fi

	varsToDisplay="PROJECT_DIR VIRTUAL_HOST GLOBAL_ENV_PATH OVERRIDING_ENV_FILES PROJECT_ENV_PATH PROJECT_NAMESPACE DEBUG"

	! $DEBUG || displayVars $varsToDisplay

	export _PROJECT_ENV_LOADED="$projectName"
else
	debug "Project: [$PROJECT_NAME] env already loaded."
fi

### Check for mandatory env vars
test -n "$MANDATORY_VARS" || die "MANDATORY_VARS env var is not defined !"
for var in $MANDATORY_VARS; do
	test -n "${!var}" || die "$var en var is not defined !"
done
