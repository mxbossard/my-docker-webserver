#! /bin/bash -e
SCRIPT_DIR="$( dirname $( readlink -f $0 ) )"

. $SCRIPT_DIR/scriptFramework.sh

error=false
checked=false
tempFile=$( mkTempFile )
rmOnExit "$tempFile"

export MANAGED_CURL_HOST_IPS="$PROXY_LOCAL_IP"

if [ -n "${!HEALTHCHECK_URLS*}" ]
then
	for varName in ${!HEALTHCHECK_URLS*}
	do
		url="${!varName}"
		debug "Curling healthcheck URL: [$url] ..."
		rc=0
		managedCurl -L -k -sS -f "$url" > "$tempFile" 2>&1 || rc=$?
		trace "rc: $rc"
		if [ $rc -gt 0 ]; then
			errOut "Fail probing healthcheck URL: $url"
			>&2 cat "$tempFile"
			error=true
		fi
		checked=true
	done
fi

if [ -n "${!HEALTHCHECK_CMDS*}" ]
then
	for varName in ${!HEALTHCHECK_CMDS*}
	do
		script="${!varName}"
		debug "Executing script: [$script] ..."
		! eval "$script" > $tempFile 2>&1 && errOut "Fail executing script : [$script]" && >&2 cat $tempFile && error=true || true
		checked=true
	done
fi

if [ -n "${!HEALTHCHECK_CONTAINER_CMDS*}" ]
then
	for varName in ${!HEALTHCHECK_CONTAINER_CMDS*}
	do
		scriptExpr=${!varName}
		cd $PROJECT_DIR
		debug "Container script expression: [$scriptExpr]"
		sep="@"
		if echo $scriptExpr | grep "$sep" > /dev/null
		then
			# filter containers
			containersExpr=$( echo $scriptExpr | cut -d$sep -f1 )
			containers=$( docker-compose ps --services | egrep -x "$containersExpr" )
			script=$( echo $scriptExpr | cut -d$sep -f2 )
		else
			# all containers
			containers=$( docker-compose ps --services )
			script=$scriptExpr
		fi

		test -n "$script" || die "No script found in script expression: [$scriptExpr] !"
		test -n "$containers" || die "No running containers to execute healthcheck script: [$script] !"

		for container in $containers
		do
			debug "Executing script: [$script] on container: [$container] ..."
			cmd="docker exec $container $script"
			debug "cmd: $cmd"
			! eval "$cmd" > $tempFile 2>&1 && errOut "Fail executing script: [$script] on container: [$container]" && >&2 cat $tempFile && error=true || true
			checked=true
		done
	done
fi

! $checked && err "No healthcheck executed ! Configure one with either HEALTHCHECK_URLS, HEALTHCHECK_CMDS or HEALTHCHECK_CONTAINER_CMDS !" && exit 2
$error && exit 1 || true
