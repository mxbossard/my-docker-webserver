#! /bin/sh
REGISTRY=https://index.docker.io/v2
#REGISTRY="https://registry.hub.docker.com/v2"
#REGISTRY="https://registry.docker.io/v2"
#REGISTRY="https://registry-1.docker.io/v2"
#REGISTRY="https://hub.docker.com/v2"

REPO=mxbossard
IMAGE=mdws-api
# Could also be a repo digest
TAG=latest
digest="sha256:a2caa2d5ce6e00e1a404dd8feed0ba4d7bf8218aa1a5be15abc5e20cfd3ed445"
badDigest="sha256:b2caa2d5ce6e00e1a404dd8feed0ba4d7bf8218aa1a5be15abc5e20cfd3ed445"

# Query tags
#curl "$REGISTRY/repositories/$REPO/$IMAGE/tags/"

# Query manifest
#curl -iL "$REGISTRY/$REPO/$IMAGE/manifests/$TAG"
# HTTP/1.1 401 Unauthorized
# Www-Authenticate: Bearer realm="https://auth.docker.io/token",service="registry.docker.io",scope="repository:library/debian:pull"

TOKEN=$(curl -sSL "https://auth.docker.io/token?service=registry.docker.io&scope=repository:$REPO/$IMAGE:pull" | jq --raw-output .token)
TOKEN2=$(curl -sSL "https://auth.docker.io/token?service=registry.docker.io&scope=repository:$REPO:pull" | jq --raw-output .token)
#>&2 echo "token: $TOKEN"

>&2 echo "tags"
curl -LH "Authorization: Bearer ${TOKEN}" "$REGISTRY/$REPO/$IMAGE/tags/list"
>&2 echo "blob"
curl -I -LH "Authorization: Bearer ${TOKEN}" "$REGISTRY/$REPO/$IMAGE/blobs/$digest"
>&2 echo "blob not existing"
curl -I -LH "Authorization: Bearer ${TOKEN}" "$REGISTRY/$REPO/$IMAGE/blobs/$badDigest"
>&2 echo "manifest"
curl -fsSL -I -H "Authorization: Bearer $TOKEN" "$REGISTRY/$REPO/$IMAGE/manifests/$digest"
>&2 echo "manifest not existing"
curl -fsSL -I -H "Authorization: Bearer $TOKEN" "$REGISTRY/$REPO/$IMAGE/manifests/$badDigest"
exit 0

curl -LH "Authorization: Bearer ${TOKEN}" "$REGISTRY/$REPO/$IMAGE/manifests/$TAG"

# Some repos seem to return V1 Schemas by default

REPO=nginxinc
IMAGE=nginx-unprivileged 
TAG=1.17.2

curl -LH "Authorization: Bearer $(curl -sSL "https://auth.docker.io/token?service=registry.docker.io&scope=repository:$REPO/$IMAGE:pull" | jq --raw-output .token)" \
 "$REGISTRY/$REPO/$IMAGE/manifests/$TAG"

# Solution: Set the Accept Header for V2

curl -LH "Authorization: Bearer $(curl -sSL "https://auth.docker.io/token?service=registry.docker.io&scope=repository:$REPO/$IMAGE:pull" | jq --raw-output .token)" \
  -H "Accept:application/vnd.docker.distribution.manifest.v2+json" \
 "$REGISTRY/$REPO/$IMAGE/manifests/$TAG"
