#! /bin/sh -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
DEFAULT_HASH_FILE="${HASH_DEFAULT_HASH_FILE:-.hashDir.txt}"
DEFAULT_EXCLUSIONS="'*.bkp' '*.bak' '*.swp' '*.rendered' '.git/*'"
DEFAULT_EXCLUSIONS="${HASH_DEFAULT_EXCLUSIONS:-$DEFAULT_EXCLUSIONS}"

usage() {
	>&2 echo "usage: $0 [--hash-file hashFile] <directory> [pathExclusion1] ... [pathExclusionN]"
	>&2 echo "by default, hashFile is $DEFAULT_HASH_FILE (overriden by HASH_DEFAULT_HASH_FILE env var."
	>&2 echo "by default, exclusions are: $DEFAULT_EXCLUSIONS (overriden by HASH_DEFAULT_EXCLUSIONS env var)."
	>&2 echo
	exit 1
}

if [ "$1" = "--hash-file" ]; then
	test -n "$2" || usage
	hashFile="$2"
	shift 2
else
	hashFile="$DEFAULT_HASH_FILE"
fi

dir="$1"
shift
exclusions="${@:-$DEFAULT_EXCLUSIONS}"

test -d "$dir" || usage
cd $dir

dirHash=$( $SCRIPT_DIR/hashDir.sh "$dir" "$hashFile" $DEFAULT_EXCLUSIONS $exclusions 2> /dev/null )
if [ -f "$hashFile" ] && [ "$( cat $hashFile )" = "$dirHash" ]; then
	>&2 echo "Directory: [$dir] has not changed (hash: [$dirHash] ; hashFile: [$hashFile])"
	exit 1
else
	>&2 echo "Directory: [$dir] has changed (new hash: [$dirHash] ; hashFile: [$hashFile])"
	exit 0
fi

