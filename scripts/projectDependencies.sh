#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
. $SCRIPT_DIR/scriptFramework.sh

DEPENDENCY_TREE_FILE="$( readlink -f $SCRIPT_DIR/../conf/projectDependencyTree.txt )"

displaySelf=false
if [ "$1" == "--self" ]
then
	displaySelf=true
	shift
fi

INPUT_PROJECTS="$@"
ALL_PROJECTS=false

if test -z "$INPUT_PROJECTS"
then
	exit 0
elif [ "all" == "$1" ]
then
	ALL_PROJECTS=true
fi

i=0
tempFile=$( mkTempFile )
rmOnExit "$tempFile"

# strip /
INPUT_PROJECTS="$( echo $INPUT_PROJECTS | sed -e 's|/||g' )"
for project in $INPUT_PROJECTS
do
	depsFound=false
	while ! $depsFound && read line
	do
		projectName=$( echo $line | cut -d: -f1 )
		projectDepencies=$( echo $line | cut -d: -f2- )

		# Do not treat empty lines and commented lines
		if test -n "$projectName" && echo "$projectName" | grep -ve '^\s*#' > /dev/null
		then
			projectDir="$SCRIPT_DIR/../$projectName"
			test -d "$projectDir" || die "Project [$projectName] referenced in file $DEPENDENCY_TREE_FILE does not exists !"
		
			if [ "$project" == "$projectName" ]
			then
				depsFound=true
				# Recusrive call
				$0 --self $projectDepencies
				$displaySelf && echo $projectName || true
				break
			elif $ALL_PROJECTS
			then
				# Recusrive call
				$0 $projectDepencies
				echo $projectName
			fi
		fi
	done < $DEPENDENCY_TREE_FILE

	$depsFound || $ALL_PROJECTS || die "Project [$project] is not referenced in file $DEPENDENCY_TREE_FILE !"
done > "$tempFile"

cat "$tempFile" | awk '!x[$0]++' # Remove duplicate lines
