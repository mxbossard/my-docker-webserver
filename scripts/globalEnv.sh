# This script cannot be moved from scripts/ directory.
set -o pipefail

test -n "$DEBUG" && $DEBUG > /dev/null 2>&1 && export DEBUG=true || export DEBUG=false
test -n "$QUIET" && $QUIET > /dev/null 2>&1 && export QUIET=true || export QUIET=false

! $TRACE || >&2 echo "Sourcing global env ..."

### Determine PARENT_DIR env var
if [ ! -d "$PARENT_DIR" -o ! -f "$PARENT_DIR/scripts/globalEnv.sh" ]
then
	! $TRACE || >&2 echo "Evaluating PARENT_DIR ..."

	absolutePath() {
		cd "$( dirname "$1" )"
		echo "$PWD/$( basename "$1" )"
	}

	### Determine SOURCED_SCRIPT (cache it because we don't need to export it
	sourcedScriptScanOrder="SOURCED_SCRIPT_PATH BASH_SOURCE[0]"
	for varName in $sourcedScriptScanOrder
	do
		varValue="$(eval echo \${$varName:- Not Defined !})"
		! $TRACE || >&2 echo "Scanning for sourced script path in: [$varName (=$varValue)] ..."
		test -f "$varValue" && sourcedScriptPath="$( absolutePath $varValue )" && break || true
	done
	! $TRACE || >&2 echo "sourced script path found: [$sourcedScriptPath]" || true
	test -f "$sourcedScriptPath" || ( >&2 echo "Unable to detect sourced script path !" ; exit 1 )

	SCRIPTS_DIR="$( dirname $( readlink -f "$sourcedScriptPath" ) )"
	UTILS_DIR="$SCRIPTS_DIR/utils"
	PARENT_DIR="$( readlink -f $SCRIPTS_DIR/.. )"

	# Export static env vars
	export PARENT_DIR
	export UTILS_DIR
	export SCRIPTS_DIR

fi

test ! -d "$PARENT_DIR" && >&2 echo "Unable to determine PARENT_DIR env var !" && exit 1 || true

source $SCRIPTS_DIR/scriptFramework.sh

trace "PARENT_DIR: $PARENT_DIR"

### Source Static env
if [ "$_STATIC_ENV_LOADED" != "true" ]
then
	trace "Evaluating static env ..."

	source $SCRIPTS_DIR/commonFunctions.sh

	export CONF_DIR="$PARENT_DIR/conf"

	export _STATIC_ENV_LOADED="true"
fi

PROJECT_NAME="__global__"
if [ "$_PROJECT_ENV_LOADED" != "$PROJECT_NAME" ]
then
	debug "Loading __global__ env ..."

	### Source all env.sh files
	# Default env files
	defaultOverridingEnvFiles='$CONF_DIR/common_env.sh $CONF_DIR/${ENV}.env.sh'

	### Read envName
	envNameFile="$PARENT_DIR/name.txt"
	test -f "$envNameFile" || die "You must name your environment in file: [$envNameFile] !"
	ENV="$( cat $envNameFile | head -1 )"
	### Test variable presence and add dynamic variables
	test -n "$ENV" || die "Env name is not defined in file: [$envNameFile] !"
	echo "$ENV" | egrep "^[-a-zA-Z0-9]+" > /dev/null || die "Env name (presently: [$ENV]) specified in file: [$envNameFile] should only contains sybols [-a-zA-Z0-9]+ (digits, letters and minus sympol) !." 

	export ENV
	export NAMESPACE="$ENV"

	### Then source in order OVERRIDING_ENV_FILES
	credentialsFile="$CONF_DIR/.credentials.sh"
	overridingEnvFiles="$defaultOverridingEnvFiles $credentialsFile $OVERRIDING_ENV_FILES"

	if [ "$INSECURED_ENV" == "true" ]; then
		# if insecured env and credentialsFile don't exists use the example one.
		overridingEnvFiles="$defaultOverridingEnvFiles $credentialsFile.example $credentialsFile $OVERRIDING_ENV_FILES"
	else
		# If secured env, check for non weak passwords
		test -f "$credentialsFile" || die "The credentials file: [$credentialsFile] is missing. You can copy [$credentialsFile.example] or set var INSECURED_ENV=true !"

		egrep "PASSWORD|KEY|TOKEN" "$credentialsFile" | cut -d'=' -f2- | egrep -i "change|root|admin|password" >&2 && die "You must have non trivial passwords in credentials file: [$credentialsFile] !"
	fi

	evaluatedOverridingEnvFiles=$( eval "echo $overridingEnvFiles" )
	debug "Sourcing overriding env files: [$evaluatedOverridingEnvFiles] ..."

	set -a # All variables are exported by default
	sourceOverridingEnvFiles $evaluatedOverridingEnvFiles #2> /dev/null
	#test -f "$PARENT_DIR/.credentials.sh" && source "$PARENT_DIR/.credentials.sh"
	set +a

	# Source GLOBAL_ENV_PATH or root env.sh file
	test -f "$GLOBAL_ENV_PATH" || GLOBAL_ENV_PATH="$PARENT_DIR/env.sh"
	if [ -f "$GLOBAL_ENV_PATH" ] ; then
		warn "Detected a Global env file: [$GLOBAL_ENV_PATH]. This env file will override all other env files."	
	debug "Sourcing GLOBAL_ENV_PATH: [$GLOBAL_ENV_PATH] ..."
	set -a # All variables are exported by default
	source "$GLOBAL_ENV_PATH"
	set +a
	fi

	export MANDATORY_VARS="ENV"
	export CREDENTIALS_FILEPATH="$credentialsFile"

	# Remember enw was loaded
	export _PROJECT_ENV_LOADED="$PROJECT_NAME"
else
	debug "__global__ env already loaded."
fi

### Check for mandatory env vars
test -n "$MANDATORY_VARS" || die "No MANDATORY_VARS env var is defined !"
for var in $MANDATORY_VARS; do
	test -n "${!var}" || die "No $var en var is defined !"
done
