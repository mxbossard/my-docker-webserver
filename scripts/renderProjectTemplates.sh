#! /bin/bash -e

# If only one arg => substitution on stdout
# If two args => firstArg is filter substitute *filter to *-filter

SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
QUIET=true

source $SCRIPT_DIR/globalEnv.sh

usage() {
	errOut "usage: $0 <templateFile>"
	errOut "usage: $0 <templatesSuffix> <templateFile1|templateDir1> ... [templateFileN|templateDirN]"
	errOut "	templatesSuffix: exemple .tpl"
	exit 1
}

renderOneFile=false

if [ $# -eq 1 ]; then
	renderOneFile=true
	! test -f "$1" && errOut "$1 is not a file !" && usage || true
elif [ $# -gt 1 ]; then
	templatesSuffix="$1"
	test -z "$templatesSuffix" && errOut "templatesSuffix cannot be empty !" && usage || true
	test -f "$templatesSuffix" && errOut "templatesSuffix is missing !" && usage || true
	shift
else
	usage
fi

source $SCRIPT_DIR/projectEnv.sh

renderFiles() {
	for file; do
		trace "Rendering project template file: [$file] ..."
		file=$( readlink -f "$file" )
		if [ -d "$file" ]; then
			if echo "$file" | grep ".bak$" > /dev/null; then
				# Do not render .bak dir content
				continue
			elif echo "$file" | grep ".rendered$" > /dev/null; then
				# Do not render .rendered dir content
				continue
			elif echo "$file" | egrep "$templatesSuffix$" > /dev/null; then
				# The directory is marked as a template => copy it.
				renderingDirName=$( dirname "$file" )/$( basename "$file" "$templatesSuffix" ).rendered
				if [ -d "$renderingDirName" ]; then
					if test -e "$renderingDirName.bak"; then
						debug "Removing backup dir: [$renderingDirName.bak] ..."
						rm -rf -- "$renderingDirName.bak" || true #2> /dev/null || true
					fi
					debug "Backuping rendered dir: [$renderingDirName] into: [$renderingDirName.bak] ..."
					#mv "$renderingDirName" "$renderingDirName.bak"
					rsync -a "$renderingDirName/" "$renderingDirName.bak/"
					rm -rf -- "$renderingDirName"
				fi
				debug "Copying template dir: [$file] into: [$renderingDirName] ..."
				#cp -a "$file" "$renderingDirName"
				rsync -rlpgod "$file/" "$renderingDirName/" # rsync not preserving files timings
				file="$renderingDirName"
			fi
			renderFiles $file/*
		elif $renderOneFile; then
			#Render  one file on stdout
			cat $file | envsubst
		elif echo "$file" | egrep "$templatesSuffix$" > /dev/null; then
			substitutedFileName=$( dirname "$file" )/$( basename "$file" "$templatesSuffix" )
			debug "Rendering template: [$file] into: [$substitutedFileName] ..."
			cat $file | envsubst > $substitutedFileName
			# FIX: Remove template file from rendered dir
			echo "$substitutedFileName" | grep ".rendered" > /dev/null && rm -- "$file"
		fi
	done
}

renderFiles "$@"
