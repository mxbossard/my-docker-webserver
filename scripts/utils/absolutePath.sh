#! /bin/sh
# Like readlink -f but do not follow symlinks

path="$1"

cd "$( dirname "$path" )"
echo "$PWD/$( basename "$path" )"

