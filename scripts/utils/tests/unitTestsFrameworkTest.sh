#! /bin/bash
scriptDir="$( readlink -f $( dirname $0 ) )"
source $scriptDir/minimalTestFramework.sh

export TRACE=false
export DEBUG=false
source $scriptDir/../unitTestsFramework.sh "$scriptDir/.."
set +e

# assertTrue tests
initOuts
assertTrue "msg" true > "$stdOut" 2> "$stdErr"
assertOutsContents "assertTrue simple success" "" "Running test: \"msg\" ... OK"
assertTrue "msg" false > "$stdOut" 2> "$stdErr"
assertOutsContents "assertTrue simple failure" "" "Running test: \"msg\" ... KO${NEW_LINE}Expected evaluating to 'true' but evaluation failed with Errors:${NEW_LINE}Output was:${NEW_LINE}executing cmd: ([false])."

assertTrue "msg" echo foo bar baz > "$stdOut" 2> "$stdErr"
assertOutsContents "assertTrue multiple args success" "" "Running test: \"msg\" ... OK"
assertTrue "msg" sh -c ">&2 echo 'foo bar baz' ; false || echo 'plop' ; false" > "$stdOut" 2> "$stdErr"
assertOutsContents "assertTrue subshell failure" "" "Running test: \"msg\" ... KO${NEW_LINE}Expected evaluating to 'true' but evaluation failed with Errors:${NEW_LINE}foo bar baz${NEW_LINE}Output was:${NEW_LINE}plop${NEW_LINE}executing cmd: ([sh] [-c] [>&2 echo 'foo bar baz' ; false || echo 'plop' ; false])."
assertTrue "msg" find . -name "*" -type f -exec ls {} \; > "$stdOut" 2> "$stdErr"
assertOutsContents "assertTrue complex command success" "" "Running test: \"msg\" ... OK"
assertTrue "msg" find . -name "*" -type f -exec ls {} \; --notexistingopt > "$stdOut" 2> "$stdErr"
assertOutsContents "assertTrue complex command failure" "" "Running test: \"msg\" ... KO${NEW_LINE}Expected evaluating to 'true' but evaluation failed with Errors:${NEW_LINE}find: unknown predicate \`--notexistingopt'${NEW_LINE}Output was:${NEW_LINE}executing cmd: ([find] [.] [-name] [*] [-type] [f] [-exec] [ls] [{}] [;] [--notexistingopt])."

initOuts
assertTrue "msg" curl -k -f -sS -XPOST --data-urlencode "q=foo" -- "https://httpbin.org/post" > "$stdOut" 2> "$stdErr"
assertOutsContents "assertTrue complex curl command success" "" "Running test: \"msg\" ... OK"
url="https://httpbin.org/post"
queryParam="SELECT * FROM \"foo\" WHERE time >= \"33s\" ORDER BY time DESC LIMIT 10;"
assertTrue "msg" curl -k -f -sS -XPOST --data-urlencode "q=$queryParam" -o "/tmp/poulp" -- "$url" > "$stdOut" 2> "$stdErr"
assertOutsContents "assertTrue complex curl command with variables success" "" "Running test: \"msg\" ... OK"
assertTrue "msg" test "foo" == "foo" > "$stdOut" 2> "$stdErr"
assertOutsContents "assertTrue simple string == test" "" "Running test: \"msg\" ... OK"
assertTrue "msg" test "foo" != "bar" > "$stdOut" 2> "$stdErr"
assertOutsContents "assertTrue simple string != test" "" "Running test: \"msg\" ... OK"
assertTrue "msg" test "foo" == "bar" > "$stdOut" 2> "$stdErr"
assertOutsContents "assertTrue simple string == false test 1" "" "Running test: \"msg\" ... KO${NEW_LINE}Expected evaluating to 'true' but evaluation failed with Errors:${NEW_LINE}Output was:${NEW_LINE}executing cmd: ([test] [foo] [==] [bar])."
assertTrue "msg" test "foo" == '' > "$stdOut" 2> "$stdErr"
assertOutsContents "assertTrue simple string == '' test" "" "Running test: \"msg\" ... KO${NEW_LINE}Expected evaluating to 'true' but evaluation failed with Errors:${NEW_LINE}Output was:${NEW_LINE}executing cmd: ([test] [foo] [==])."
assertTrue "msg" test "" == 'bar' > "$stdOut" 2> "$stdErr"
assertOutsContents "assertTrue simple \"\" == string test" "" "Running test: \"msg\" ... KO${NEW_LINE}Expected evaluating to 'true' but evaluation failed with Errors:${NEW_LINE}Output was:${NEW_LINE}executing cmd: ([test] [] [==] [bar])."

# Not a good unit test
#assertTrue "msg" curl -k -f -sS -XPOST -u iotReader:changeit --data-urlencode 'q=SELECT * FROM "integrationTest_1615304549" WHERE time >= "1615304549s" ORDER BY time DESC LIMIT 10' -o /tmp/iot-datastore-api_basics_test.sh.3GIsgCO7kP -- "http://172.28.0.2:8086/query?db=iot_datastore&rp=immediate_term&epoch=s" > "$stdOut" 2> "$stdErr"
#assertOutsContents "assertTrue influxdb curl command" "" "Running test: \"msg\" ... OK"

#assertOutsMatches "trace enabled" "" "^.* \[TRACE\] \($0\): foo$"


