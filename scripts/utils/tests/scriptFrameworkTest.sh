#! /bin/bash
scriptDir="$( readlink -f $( dirname $0 ) )"
source $scriptDir/minimalTestFramework.sh

export TRACE=false
export DEBUG=false
source $scriptDir/../scriptFramework.sh

### removeMsgColors tests
initOuts
removeMsgColors "${_cyan}foo bar $_nc " > "$stdOut" 2> "$stdErr"
assertOutsContents "removeMsgColors 1 arg" "foo bar  " ""
echo "$_cyan foo bar $nc g" | removeMsgColors > "$stdOut" 2> "$stdErr"
assertOutsContents "removeMsgColors one line piped" " foo bar  g" ""
echo "$_cyan foo$nc
$_red bar $nc g" | removeMsgColors > "$stdOut" 2> "$stdErr"
assertOutsContents "removeMsgColors 2 lines piped" " foo$NEW_LINE bar  g" ""
removeMsgColors "${_cyan}foo bar$_nc " > "$stdOut" 2> "$stdErr"
assertOutsContents "removeMsgColors 1 arg with space" "foo bar " ""
removeMsgColors "${_cyan}" "foo bar" "baz" "$_nc" > "$stdOut" 2> "$stdErr"
assertOutsContents "removeMsgColors mutli args" "${NEW_LINE}foo bar${NEW_LINE}baz" ""


### trace tests
initOuts
trace "foo" > "$stdOut" 2> "$stdErr"
assertOutsContents "trace disabled 1" "" ""
assertOutsContents "shouldFail 1" "" " "
assertOutsContents "shouldFail 2" "" "a"

DEBUG=true trace "foo" > "$stdOut" 2> "$stdErr"
assertOutsContents "trace debug enabled" "" ""

TRACE=true trace "foo" > "$stdOut" 2> "$stdErr"
assertOutsMatches "trace enabled" "" "^.* \[TRACE\] \($0\): foo$"
assertOutsMatches "should fail 3" "^$" "^.*foo "
assertOutsMatches "should fail 4" "^$" "^ $"
assertOutsMatches "should not fail" "^$" " "

### debug tests
initOuts
debug "bar" > "$stdOut" 2> "$stdErr"
assertOutsContents "debug disabled 1" "" ""
assertOutsMatches "debug disabled 2" "" ""
TRACE=true debug "bar" > "$stdOut" 2> "$stdErr"
assertOutsMatches "debug trace enabled" "" "^.* \[DEBUG\] \($0\): bar$"
DEBUG=true debug "bar" > "$stdOut" 2> "$stdErr"
assertOutsMatches "debug enabled" "" "^.* \[DEBUG\] \($0\): bar$"

### info tests
initOuts
info "bar " > "$stdOut" 2> "$stdErr"
assertOutsMatches "info debug disabled" "" "^.* \[INFO\]: bar $"
DEBUG=true info "bar " > "$stdOut" 2> "$stdErr"
assertOutsMatches "info debug enabled" "" "^.* \[INFO\]: bar $"
info "bar " baz "foo" - > "$stdOut" 2> "$stdErr"
assertOutsMatches "info multi message" "" "^.* \[INFO\]: bar baz foo -$"

### xxxxxMsg (successMsg, ...) tests
for cmd in successMsg alertMsg warnMsg infoMsg cyanMsg purpleMsg; do
	initOuts
	$cmd "bar " > "$stdOut" 2> "$stdErr"
	assertOutsMatches "$cmd debug disabled" "^bar $" ""
	DEBUG=true $cmd "bar " > "$stdOut" 2> "$stdErr"
	assertOutsMatches "$cmd debug enabled" "^bar $" ""
	$cmd "bar " baz "foo" - > "$stdOut" 2> "$stdErr"
	assertOutsMatches "$cmd multi message" "^bar baz foo -$" ""
done

### truncateMessage tests
initOuts
truncateMessage "foo" > "$stdOut" 2> "$stdErr"
assertOutsContents "truncateMessage xs message" "foo" ""
truncateMessage " foo bar baz too much " > "$stdOut" 2> "$stdErr"
assertOutsContents "truncateMessage small message" " foo bar baz too much " ""
truncateMessage "a123456789b123456789c123456789d123456789e123456789f123456789g123456789h123456789i123456789j123456789k123456789l123456789m123456789n123456789o123456789p123456789q123456789r123456789s123456789t123456789u123456789v123456789w123456789x123456789y123456789z123456789" > "$stdOut" 2> "$stdErr"
assertOutsContents "truncateMessage long message" "a123456789b123456789c123456789d123456789e123456789f123456789g123456789h123456789i123456789j123456789[... truncated ...]q123456789r123456789s123456789t123456789u123456789v123456789w123456789x123456789y123456789z123456789" ""
truncateMessage "foo" "bar" "baz" "too" "much" > "$stdOut" 2> "$stdErr"
assertOutsContents "truncateMessage multiple args" "foo${NEW_LINE}bar${NEW_LINE}baz${NEW_LINE}too${NEW_LINE}much" ""
truncateMessage a b c d e f g h i j k l m n o p q r s t y v w x y z > "$stdOut" 2> "$stdErr"
assertOutsContents "truncateMessage too much args" "a${NEW_LINE}b${NEW_LINE}c${NEW_LINE}d${NEW_LINE}e${NEW_LINE}[... truncated ...]${NEW_LINE}v${NEW_LINE}w${NEW_LINE}x${NEW_LINE}y${NEW_LINE}z" ""

initOuts
echo "foo" | truncateMessage > "$stdOut" 2> "$stdErr"
assertOutsContents "truncateMessage piped xs message" "foo" ""
echo " foo bar baz too much " | truncateMessage > "$stdOut" 2> "$stdErr"
assertOutsContents "truncateMessage piped small message" " foo bar baz too much " ""
echo "a123456789b123456789c123456789d123456789e123456789f123456789g123456789h123456789i123456789j123456789k123456789l123456789m123456789n123456789o123456789p123456789q123456789r123456789s123456789t123456789u123456789v123456789w123456789x123456789y123456789z123456789" | truncateMessage  > "$stdOut" 2> "$stdErr"
assertOutsContents "truncateMessage piped long message" "a123456789b123456789c123456789d123456789e123456789f123456789g123456789h123456789i123456789j123456789[... truncated ...]q123456789r123456789s123456789t123456789u123456789v123456789w123456789x123456789y123456789z123456789" ""
echo "foo
bar
baz
too
much" | truncateMessage > "$stdOut" 2> "$stdErr"
assertOutsContents "truncateMessage multiple args" "foo${NEW_LINE}bar${NEW_LINE}baz${NEW_LINE}too${NEW_LINE}much" ""
echo -e "a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\ny\nv\nw\nx\ny\nz" | truncateMessage > "$stdOut" 2> "$stdErr"
assertOutsContents "truncateMessage too much args" "a${NEW_LINE}b${NEW_LINE}c${NEW_LINE}d${NEW_LINE}e${NEW_LINE}[... truncated ...]${NEW_LINE}v${NEW_LINE}w${NEW_LINE}x${NEW_LINE}y${NEW_LINE}z" ""
