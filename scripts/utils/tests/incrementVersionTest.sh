#! /bin/bash
scriptDir="$( readlink -f $( dirname $0 ) )"
source $scriptDir/../unitTestsFramework.sh "$scriptDir/.."

subject="$scriptDir/../incrementVersion.sh"

assertOutput "Increment release 1" "1.0.1" $subject "1.0.0"
assertOutput "Increment release 2" "2.3" $subject "2.2"
assertOutput "Increment release 3" "3" $subject "2"
assertOutput "Increment release 4" "1.5.3.8.42" $subject "1.5.3.8.41"
assertOutput "Increment release with qualifier" "1.0.1-dev" $subject "1.0.0" "dev"
assertOutput "Change not release qualifier" "1.0.1-rc" $subject "1.0.1-dev" rc
assertOutput "Release qualified" "1.2.3" $subject "1.2.3-rc12"
assertOutput "Increment qualifier" "1.0.1-rc15" $subject "1.0.1-rc14" "rc#"
assertOutput "Start with v0 qualifier" "1.0.1-foo0" $subject "1.0.1-bar15" "foo#"


assertOutput "Do not increment release with hash qualifier" "1.0.0" $subject "1.0.0" "#"
assertOutput "Do not increment not numeric qualifier with hash qualifier" "1.0.0-dev" $subject "1.0.0-dev" "#"
assertOutput "Increment numeric qualifier with hash qualifier" "1.0.0-rc13" $subject "1.0.0-rc12" "#"

assertOutput "Increment qualified version release 1" "1.0.1-foo" $subject -r "foo" "1.0.0-foo"
assertOutput "Increment qualified version release 2" "1.0.1-foo" $subject -r "foo" "1.0.0"
assertOutput "Increment qualified version release with qualifier" "2.0.1-foo-bar" $subject -r foo "2.0.0-foo" "bar"
assertOutput "Change qualified version not relase" "1.0.1-alpine-rc" $subject -r alpine "1.0.1-alpine-dev" "rc"
assertOutput "Release qualified version 1" "1.0.1-alpine" $subject -r alpine "1.0.1-alpine-dev" ""
assertOutput "Release qualified version 2" "1.0.1-alpine" $subject -r alpine "1.0.1-dev" ""
assertOutput "Increment qualified version qualifier" "1.0.1-alpine-rc27" $subject -r alpine 1.0.1-alpine-rc26 "rc#"
assertOutput "Start with v0 qualified version qualifier" "1.0.1-alpine-rc0" $subject -r alpine "1.0.0-alpine" "rc#"

assertReturnCode "No params" 1 $subject
assertReturnCode "Empty version param" "1" $subject ""
assertFalse "Blank version param" $subject " "
assertFalse "Bad format version param 1" $subject "-1"
assertFalse "Bad format version param 2" $subject "a"
assertFalse "Blank qualifier param" $subject "1.0.0" " "
assertFalse "Bad format qualifier param 1" $subject "1.0.0" "-dev"
assertFalse "Bad format qualifier param 2" $subject "1.0.0" "-"
assertFalse "Bad format qualified version 1" $subject -r "-" "1.0.0"
assertFalse "Bad format qualified version 2" $subject -r "-foo" "1.0.0"

assertNoError

