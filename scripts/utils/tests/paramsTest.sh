#! /bin/bash
scriptDir="$( readlink -f $( dirname $0 ) )"
#source $scriptDir/minimalTestFramework.sh

printParams() {
	echo
	echo '##### printParams: $@'
	echo $@

	echo
	echo '##### printParams: "$@"'
	echo "$@"

	args=("$@")
	echo
	echo '##### printParams: ${args[@]}'
	echo ${args[@]}

	echo
	echo '##### printParams: "${args[@]}"'
	echo "${args[@]}"

	echo
	echo '##### printParams: for param in $@'
	for param in $@; do
		echo $param
	done

	echo
	echo '##### printParams: for "param" in $@'
	for param in $@; do
		echo "$param"
	done

	echo
	echo '##### printParams: for param in "$@"'
	for param in "$@"; do
		echo $param
	done

	echo
	echo '##### printParams: for "param" in "$@"'
	for param in "$@"; do
		echo "$param"
	done

	echo
	echo '##### printParams: while $1'
	while [ -n "$1" ]; do
		echo -n "\"$1\" "
		shift
	done
	echo
}

printArgs() {
	while [ -n "$1" ]; do
		echo -n "\"$1\" "
		shift
	done
	echo
}

delegatingPrintArgs() {
	echo
	echo 'printArgs: $@'
	printArgs $@

	echo
	echo 'printArgs: "$@"'
	printArgs "$@"

	echo
	echo 'printArgs: echo "$( printArgs $@ )"'
	echo "$( printArgs $@ )"

	echo
	echo 'printArgs: echo $( printArgs "$@" )'
	echo $( printArgs "$@" )

	echo
	echo 'printArgs: echo "$( printArgs "$@" )"'
	echo "$( printArgs "$@" )"

	echo
	echo 'printArgs: echo "$( printArgs ""$@"" )"'
	echo "$( printArgs ""$@"" )"

	echo
	echo 'printArgs: echo $( printArgs \"$@\" )'
	echo "$( printArgs \"$@\" )"
}

delegatingPrintArgs "msg" curl -k -f -sS -XPOST -u iotReader:changeit --data-urlencode 'q=SELECT 1 FROM "integrationTest_1615304549" WHERE time >= "1615304549s" ORDER BY time DESC LIMIT 10' -o /tmp/iot-datastore-api_basics_test.sh.3GIsgCO7kP -- "http://172.28.0.2:8086/query?db=iot_datastore&rp=immediate_term&epoch=s"

foo=bar

echo $( echo "$foo" )
echo "$( echo "$foo" )"
echo $( echo "\"$foo\"" )
echo "$( echo "\"$foo\"" )"
echo $( echo "'$foo'" )
echo "$( echo "'$foo'" )"

