export COLORS_DISABLED=true

stdOut="/tmp/stdout"
stdErr="/tmp/stderr"

_red='\033[1;31m'
_green='\033[1;32m'
_yellow='\033[1;33m'
_blue='\033[1;34m'
_cyan='\033[1;36m'
_nc='\033[0m'

initOuts() {
	> "$stdOut"
	> "$stdErr"
}

success() {
	echo -e "$_green" "SUCCESS" "$_nc "
	#>&2 echo -e "$@"
}

failure() {
	echo -e "$_red" "FAILURE" "$_nc "
	#>&2 echo -e "$@"
}

assertOutsContents() {
	msg="$1"
	expectedOut="$2"
	expectedErr="$3"

	echo -n "Testing: $msg ..."

	actualOut="$( cat $stdOut )"
	actualErr="$( cat $stdErr )"
	rcOut=0
	rcErr=0
	test "$expectedOut" == "$actualOut" || rcOut=$?
	test "$expectedErr" == "$actualErr" || rcErr=$?

	test $rcOut -eq 0 -a $rcErr -eq 0 && success || failure
       	test $rcOut -eq 0 || >&2 echo -e "bad stdout, expected: [$_cyan$expectedOut$_nc] but was: [$_yellow$actualOut$_nc] !"
	test $rcErr -eq 0 || >&2 echo -e "bad stderr, expected: [$_cyan$expectedErr$_nc] but was: [$_yellow$actualErr$_nc] !"
}

# perl regexp: https://stackoverflow.com/questions/3717772/regex-grep-for-multi-line-search-needed
assertOutsMatches() {
	msg="$1"
	expectedOutRegex="$2"
	expectedErrRegex="$3"

	echo -n "Testing: $msg ..."

	actualOut="$( cat $stdOut )"
	actualErr="$( cat $stdErr )"
	rcOut=0
	rcErr=0
	if [ -z "$expectedOutRegex" -o "^$" == "$expectedOutRegex" ]; then
		outMessage="bad stdout, expected: [] but was: [$_yellow$actualOut$_nc] !"
		test "" == "$actualOut" || rcOut=$?
	else
		outMessage="bad stdout, expected matching: [$_cyan$expectedOutRegex$_nc] but was: [$_yellow$actualOut$_nc] !"
		grep -E -e "$expectedOutRegex" "$stdOut" > /dev/null || rcOut=$?
	fi
	if [ -z "$expectedErrRegex" -o "^$" == "$expectedErrRegex" ]; then
		errMessage="bad stderr, expected: [] but was: [$_yellow$actualErr$_nc] !"
		test "" == "$actualErr" || rcErr=$?
	else
		errMessage="bad stderr, expected matching: [$_cyan$expectedErrRegex$_nc] but was: [$_yellow$actualErr$_nc] !"
		grep -E -e "$expectedErrRegex" "$stdErr" > /dev/null || rcErr=$?
		#sed -ne "/$expectedErrRegex/!{q100};" "$stdErr" > /dev/null
	fi

	test $rcOut -eq 0 -a $rcErr -eq 0 && success || failure
	test $rcOut -eq 0 || >&2 echo -e "$outMessage"
       	test $rcErr -eq 0 || >&2 echo -e "$errMessage"
}

export NEW_LINE='
'
