#! /bin/bash -e

usage() {
	>&2 echo "message: $1"
	>&2 echo "usage: $0 [-r releaseQualifier] <versionToIncrement> [newQualifier]"
	>&2 echo "If newQualifier is empty the increment will be a stable release."
	>&2 echo "If newQualifier is a hash ('#') the qualifier will be kept and incremented if possible."
	exit 1
}

if [ "$1" == "-r" ]; then
	releaseQualifier="$2"
	shift 2 || usage "Missing realeaseQualifier !"
fi

versionIn="$1"
qualifierIn="$2"
test -n "$versionIn" || usage "versionToIncrement parameter is mandatory !"
echo "$versionIn" | egrep "^[0-9][0-9.]*(-[a-zA-Z0-9][-_a-zA-Z0-9.]+)?$" > /dev/null || usage "Bad versionToIncrement format !"
#echo "$versionIn" | egrep "^[0-9]$" > /dev/null || usage "Bad versionToIncrement format !"
test -z "$qualifierIn" || test "$qualifierIn" == "#" || echo "$qualifierIn" | egrep "^[a-zA-Z0-9][-_a-zA-Z0-9.]+#?$" > /dev/null || usage "Bad newQualifier format !"
test -z "$releaseQualifier" || echo "$releaseQualifier" | egrep "^[a-zA-Z0-9][-_a-zA-Z0-9.]+$" > /dev/null || usage "Bad realeaseQualifier format !"
>&2 echo "versionIn: [$versionIn] ; qualifierIn: [$qualifierIn] ; releaseQualifier: [$releaseQualifier]"

# Accepts a version string and prints it incremented by one.
# Usage: increment_version <version> [<position>] [<leftmost>]
increment_version() {
   local usage=" USAGE: $FUNCNAME [-l] [-t] <version> [<position>] [<leftmost>]
           -l : remove leading zeros
           -t : drop trailing zeros
    <version> : The version string.
   <position> : Optional. The position (starting with one) of the number
                within <version> to increment.  If the position does not
                exist, it will be created.  Defaults to last position.
   <leftmost> : The leftmost position that can be incremented.  If does not
                exist, position will be created.  This right-padding will
                occur even to right of <position>, unless passed the -t flag."

   # Get flags.
   local flag_remove_leading_zeros=0
   local flag_drop_trailing_zeros=0
   while [ "${1:0:1}" == "-" ]; do
      if [ "$1" == "--" ]; then shift; break
      elif [ "$1" == "-l" ]; then flag_remove_leading_zeros=1
      elif [ "$1" == "-t" ]; then flag_drop_trailing_zeros=1
      else echo -e "Invalid flag: ${1}\n$usage"; return 1; fi
      shift; done

   # Get arguments.
   if [ ${#@} -lt 1 ]; then echo "$usage"; return 1; fi
   local v="${1}"             # version string
   local targetPos=${2-last}  # target position
   local minPos=${3-${2-0}}   # minimum position

   # Split version string into array using its periods.
   local IFSbak; IFSbak=IFS; IFS='.' # IFS restored at end of func to
   read -ra v <<< "$v"              #  avoid breaking other scripts.

   # Determine target position.
   if [ "${targetPos}" == "last" ]; then
      if [ "${minPos}" == "last" ]; then minPos=0; fi
      targetPos=$((${#v[@]}>${minPos}?${#v[@]}:$minPos)); fi
   if [[ ! ${targetPos} -gt 0 ]]; then
      echo -e "Invalid position: '$targetPos'\n$usage"; return 1; fi
   (( targetPos--  )) || true # offset to match array index

   # Make sure minPosition exists.
   while [ ${#v[@]} -lt ${minPos} ]; do v+=("0"); done;

   # Increment target position.
   v[$targetPos]=`printf %0${#v[$targetPos]}d $((10#${v[$targetPos]}+1))`;

   # Remove leading zeros, if -l flag passed.
   if [ $flag_remove_leading_zeros == 1 ]; then
      for (( pos=0; $pos<${#v[@]}; pos++ )); do
         v[$pos]=$((${v[$pos]}*1)); done; fi

   # If targetPosition was not at end of array, reset following positions to
   #   zero (or remove them if -t flag was passed).
   if [[ ${flag_drop_trailing_zeros} -eq "1" ]]; then
        for (( p=$((${#v[@]}-1)); $p>$targetPos; p-- )); do unset v[$p]; done
   else for (( p=$((${#v[@]}-1)); $p>$targetPos; p-- )); do v[$p]=0; done; fi

   echo "${v[*]}"
   IFS=IFSbak
   return 0
}


versionInMinusQualifier="$( echo $versionIn | cut -d- -f1 )"
versionInQualifier=$( echo $versionIn | sed -e "s/^[^-]*\(-$releaseQualifier\)\?-\?//" )

test -z "$releaseQualifier" || releaseQualifier="-$releaseQualifier"
test -z "$qualifierIn" || qualifierIn="-$qualifierIn"
test -z "$versionInQualifier" || versionInQualifier="-$versionInQualifier"
>&2 echo "version input: [$versionInMinusQualifier] [$releaseQualifier] [$versionInQualifier]"
>&2 echo "qualifier input: [$qualifierIn]"

if [ "$qualifierIn" == "-#" ]; then
	# Try to increment qualifier only. If qualifier is not numeric do not change it.
	actualQualifierVersion=$( echo "$versionInQualifier" | egrep -o "[0-9]+$" ) || true
	>&2 echo "actualQualifierVersion: $actualQualifierVersion"
	if [ -n "$actualQualifierVersion" ]; then
		# Numeric qualifier => we can increment it and continue
		incrementedQualifierVersion=$(( actualQualifierVersion + 1 ))
		versionInQualifierTrunk="$( echo "$versionInQualifier" | egrep -o '^[^0-9]+' )"
		qualifierIn="${versionInQualifierTrunk}$incrementedQualifierVersion"
	else
		# Not numeric qualifier => keep current version
		echo "$versionIn"
		exit 0
	fi

elif echo "$qualifierIn" | grep '#' > /dev/null; then
	# Using Incremental qualifier
	qualifierInTrunk="$( echo $qualifierIn | cut -d# -f1 )" # example rc in rc#
	if echo $versionInQualifier | egrep "^$qualifierInTrunk[0-9]+$" > /dev/null; then
		# qualifier in input version is based on qualifierInTrunk => Incrementing qualifier version
		actualQualifierVersion=$( echo "$versionInQualifier" | egrep -o "[0-9]+$" )
		incrementedQualifierVersion=$(( actualQualifierVersion + 1 ))
		qualifierIn="${qualifierInTrunk}$incrementedQualifierVersion"
	else
		# Replacing qualifier
		qualifierIn="${qualifierInTrunk}0"
	fi
fi

if [ "$versionIn" == "$versionInMinusQualifier$releaseQualifier" ]; then
	# Version in is a release (does not have a qualifier)
	# Always increment a release
	incrementedVersion="$( increment_version $versionInMinusQualifier )$releaseQualifier$qualifierIn"
elif [ "$versionInQualifier" != "$qualifierIn" ]; then
	# Qualifiers differs replace the qualifier only
	incrementedVersion="$versionInMinusQualifier$releaseQualifier$qualifierIn"
elif [ "$versionInQualifier" == "$qualifierIn" ]; then
	# Qualifiers are identical update version, keep the qualifier
	incrementedVersion="$( increment_version $versionInMinusQualifier )$releaseQualifier$qualifierIn"
else
	# Not supported for now
	>&2 echo "Not supported !"
	exit 1
fi

echo "$incrementedVersion"

