
# Load scriptFramework.sh
if [ -d "$SCRIPTS_DIR" ]
then
	source "$SCRIPTS_DIR/scriptFramework.sh"
elif [ -f "./scriptFramework.sh" ]
then
	source "./scriptFramework.sh"
elif [ -d "$1" ] && [ -f "$1/scriptFramework.sh" ]
then
	source "$1/scriptFramework.sh"
else
	>&2 echo "Unable to source scriptFramework.sh"
	exit 1
fi

maxErrorOutLines=10

HTTP_OK="200"
HTTP_REDIRECT_301="301"
HTTP_REDIRECT_302="302"
HTTP_FORBIDEN="403"
HTTP_NOT_FOUND="404"

OK_MSG="${GREEN}OK$NC"
KO_MSG="${RED}KO$NC"

tempFile="$( mkTempFile "unittests.temp" )"
resultFile="$( mkTempFile "unittests.result" )"
errorFile="$( mkTempFile "unitests.error" )"
rmOnExit "$tempFile" "$resultFile" "$errorFile"

errorCount=0
checkCount=0
differentialCount=0

incrementErrorCount() {
	errorCount=$(( errorCount + 1 ))
}

incrementCheckCount() {
	checkCount=$(( checkCount + 1 ))
	differentialCount=$(( differentialCount + 1 ))
}

displayMessage() {
	>&2 echo -n -e "Running test: \"$PURPLE$@$NC\" ... "
}

displaySuccess() {
	>&2 echo -e "$OK_MSG"
}

displayFailure() {
	>&2 echo -e "$KO_MSG"
	message=""

	#trace "displayFailure first arg: [$1]"
	for line; do
		>&2 purpleMsg "$line"
	done
}

disableOnError=false
assertNoError() {
	test $checkCount -eq 0 && alertMsg "No assertion executed !" && exit 1 || true

	# If tests already validated by a assertNoError call but no new tests run, return.
	test $checkCount -gt 0 && test $differentialCount -eq 0 && return 0

	differentialCount=0
	if [ $errorCount -eq 0 ]
	then
		>&2 successMsg "Test suite completed without error."
	else
		>&2 alertMsg "Test suite failed with $errorCount error(s) !"
		disableOnError=true
		exit 1
	fi
}

onTestExit() {
	assertNoError
}

onTestErr() {
	returnCode=${1:-0}
	! $disableOnError || exit $returnCode

	if [ "$returnCode" -ne 0 ]; then
		>&2 alertMsg "Test script: [$0] failed unexpectedly with status code: [$returnCode] !"
	fi
	export DEBUG=true
}
#declare -F onExit &> /dev/null || trap 'onTestExit' TERM QUIT ABRT EXIT
#declare -F onError &> /dev/null || trap 'onTestErr $? $LINENO $BASH_LINENO "$BASH_COMMAND" $(printf "::%s" ${FUNCNAME[@]})' ERR


assertHttpStatus() {
	set -f # Disable shell expansion
	local message="$1"
	local method="$2"
        local url="$3"
        local expectedStatus="$4"
	shift 4
	local curlArgs=("$@")

	> "$tempFile"
	> "$resultFile"
	> "$errorFile"

	incrementCheckCount
	displayMessage "$message"

	local curlCmd="managedCurl ${curlArgs[@]} -L -X $method -sS -w '%{http_code}' --output $tempFile -- $url"
	debug "assertHttpStatus curlCmd: $curlCmd"
	rc=0
	$SCRIPTS_DIR/managedCurl.sh -o "$tempFile" "$@" -L -X "$method" -sS -w "%{http_code}\n" -o "$tempFile" -- "$url" 2> "$errorFile" > "$resultFile"
	foundStatus=$( cat "$resultFile" | tail -1 | grep "^[0-9]\+$" || echo 0 )

        if [ -n "$foundStatus" -a "$foundStatus" -eq "$expectedStatus" ]
	then
		displaySuccess
	else
		displayFailure "Expected HTTP status was: [$( forceColor $YELLOW $expectedStatus )] but actual is: [$( forceColor $RED $foundStatus )] !"
	       	errOut "Errors were:"
		#cat $errorFile
		>&2 truncateMessage $errorFile | forceColor $RED
		errOut "Output was:"
		#cat $tempFile
		>&2 truncateMessage $tempFile | forceColor $CYAN
		errOut "calling URL: [$GREY$url$NC] with curl args: [$curlArgs]."
		incrementErrorCount
	fi
	set +f
}

httpQuery() {
	local method="$1"
        local url="$2"
	shift 2
	local curlArgs=("$@")

	#trace "@: [$@] ; curlArgs: [$curlArgs] ; (curlArgs): ${curlArgs[@]}"
	local curlCmd="managedCurl ${curlArgs[@]} -L -X $method -sS -- $url"
	trace "httpQuery curlCmd: $curlCmd"
	$SCRIPTS_DIR/managedCurl.sh "$@" -sS -L -X "$method" -- "$url"
}

assertHttpResponseIs() {
	set -f # Disable shell expansion
	local message="$1"
	local method="$2"
        local url="$3"
        local expectedResponse="$4"
	shift 4
	local curlArgs=("$@")

	incrementCheckCount
	displayMessage "$message"

	expectedResponseFile="$( mkTempFile "unittests.expectedResponse.temp" )"
	echo -en "$expectedResponse" > $expectedResponseFile

	cmd="httpQuery $method $url ${curlArgs[@]}"
	debug "assertHttpResponseIs cmd: $cmd"
	$cmd > $tempFile 2> $errorFile || true
        #if tail -1 "$tempFile" | grep -x "$expectedResponse" > /dev/null
	if diff "$expectedResponseFile" "$tempFile" >> $errorFile
	then
		displaySuccess
	else
		displayFailure "Expected HTTP response was: [$YELLOW$expectedResponse$NC] but actual is: "
		>&2 truncateMessage < $tempFile | forceColor $YELLOW
	       	errOut "Errors were:"
		>&2 truncateMessage $errorFile | forceColor $RED
		errOut "calling URL: [$GREY$url$NC] with curl args: [$curlArgs]."
		incrementErrorCount
	fi
	set +f
}

assertHttpResponseMatch() {
	set -f # Disable shell expansion
	local message="$1"
	local method="$2"
        local url="$3"
        local expectedRegex="$4"
	shift 4
	local curlArgs=("$@")

	incrementCheckCount
	displayMessage "$message"

	cmd="httpQuery $method $url ${curlArgs[@]}"
	debug "assertHttpResponseMatch cmd: $cmd"
	$cmd > $tempFile 2> $errorFile || true
        if egrep -x "$expectedRegex" $tempFile > /dev/null
	then
		displaySuccess
	else
		displayFailure "Expected HTTP response was matching: [$( forceColor "$CYAN" "$expectedRegex" )] but actual is: "
		>&2 truncateMessage < $tempFile | forceColor $YELLOW
	       	errOut "Errors were:"
		>&2 truncateMessage < $errorFile | forceColor $RED
		errOut "calling URL: [$GREY$url$NC] with curl args: [$curlArgs]."
		incrementErrorCount
	fi
	set +f
}

assertEquals() {
	local message="$1"
	local expected="$2"
	local actual="$3"

	incrementCheckCount
	displayMessage "$message"

	if [ "$actual" == "$expected" ]
	then
		displaySuccess
	else
		displayFailure "Expected value was: [$YELLOW$expected$NC] but actual is: [$RED$actual$NC] !"
		incrementErrorCount
	fi
}

assertTrue() {
	set -f # Disable shell expansion
	local message="$1"
	shift

	incrementCheckCount
	displayMessage "$message"

	debug "executing cmd: [$( printArgs "$@" )] ..."
	touch "$tempFile" "$errorFile"
	if "$@" > $tempFile 2> $errorFile 
	then
		displaySuccess
	else
		displayFailure "Expected evaluating to 'true' but evaluation failed with Errors:"
                >&2 truncateMessage < $errorFile | forceColor $RED
                errOut "Output was:"
                >&2 truncateMessage < $tempFile
		errOut "executing cmd: ($GREY$( printArgs "$@" )$NC)."

		incrementErrorCount
	fi
	set +f
}

assertFalse() {
	set -f # Disable shell expansion
	local message="$1"
	shift

	incrementCheckCount
	displayMessage "$message"

	debug "executing cmd [$( printArgs "$@" )] ..."
	touch "$tempFile" "$errorFile"
	if ! "$@" > $tempFile 2> $errorFile
	then
		displaySuccess
	else
		displayFailure "Expected evaluating to 'false' but evaluation successed with Errors:"
                >&2 truncateMessage $errorFile | forceColor $RED
                errOut "Output was:"
                >&2 truncateMessage < $tempFile
		errOut "executing cmd: ($GREY$( printArgs "$@" )$NC)."

		incrementErrorCount
	fi
	set +f
}

assertReturnCode() {
	set -f # Disable shell expansion
	local message="$1"
	local expected="$2"
	shift 2

	incrementCheckCount
	displayMessage "$message"

	debug "executing cmd [$( printArgs "$@" )] ..."
	touch "$tempFile" "$errorFile"
	rc=0
	"$@" > $tempFile 2> $errorFile || rc=$?

	if [ $rc -eq $expected ]
	then
		displaySuccess
	else
		displayFailure "Expected return code to be: [$expected], but was: [$rc]. Errors were:"
                >&2 truncateMessage $errorFile | forceColor $RED
                errOut "Output was:"
                >&2 truncateMessage < $tempFile
		errOut "executing cmd: ($GREY$( printArgs "$@" )$NC)."

		incrementErrorCount
	fi
	set +f
}

assertOutput() {
	set -f # Disable shell expansion
	local message="$1"
	local expected="$2"
	shift 2

	incrementCheckCount
	displayMessage "$message"

	debug "executing cmd [$( printArgs "$@" )] ..."
	touch "$tempFile" "$errorFile"
	"$@" > $tempFile 2> $errorFile || true

	outResult="$( cat "$tempFile" )"
	if [ "$outResult" == "$expected" ]
	then
		displaySuccess
	else
		displayFailure "Expected output to be: [$expected], but was: [$outResult]. Errors were:"
		>&2 truncateMessage $errorFile | forceColor $RED
		displayFailure "Output was:"
		>&2 truncateMessage < $tempFile
		errOut "executing cmd: ($GREY$( printArgs "$@" )$NC)."

		incrementErrorCount
	fi
	set +f
}

