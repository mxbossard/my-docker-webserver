set -o pipefail

export filesToDelete=""
# Load once by process
if [ "$_PROCESS_EXECUTING" != "$BASHPID" ]; then
	export _PROCESS_EXECUTING="$BASHPID"
	export filesToDelete=""
fi

if [ -z "$_SCRIPT_FMK_LOADED" -o ! "$_SCRIPT_FMK_LOADED" == "true" ]
then

	test -n "$COLORS_DISABLED" && $COLORS_DISABLED 2> /dev/null && COLORS_DISABLED=true || COLORS_DISABLED=false

	if $COLORS_DISABLED; then
		GREY=''
		RED=''
		GREEN=''
		YELLOW=''
		BLUE=''
		PURPLE=''
		CYAN=''
		WHITE=''
		NC=''
	else
		GREY='\033[1;30m'
		RED='\033[1;31m'
		GREEN='\033[1;32m'
		YELLOW='\033[1;33m'
		BLUE='\033[1;34m'
		PURPLE='\033[1;35m'
		CYAN='\033[1;36m'
		WHITE='\033[1;37m'

		NC='\033[0m'
		#NC='\x1B[0m'
	fi

	removeMsgColors() {
		if [ -z "$1" ] || [ "$1" == "-" ]; then
			while read -r message; do
				echo -e "$message" | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]//g"
			done
		else
			for message; do
				echo -e "$message" | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]//g"
			done
		fi
	}

	colorMsg() {
		local color="$1"
		shift
		protectedColor="$( echo "$color" | sed -re 's|\\|\\\\|g' )"
		colored="$color$( echo -e "$@" | sed -r 's|\x1B\[0m|\\x1B[0m'"$protectedColor"'|g' )$NC"
		echo -e "$colored"
	}

	timestamp() {
		echo "$( date '+%F %T' )"
	}

	stdOut() {
		echo -e "$@"
	}

	errOut() {
		>&2 echo -e "$@"
	}

	forceColor() {
		local color="$1"
		shift
		if [ -z "$1" ] || [ "$1" == "-" ]; then
			while read message; do
				message="$( removeMsgColors "$message" )"
				echo -e "$color$message$NC"
			done
		else
			for message; do
				message="$( removeMsgColors "$message" )"
				echo -e "$color$message$NC"
			done
		fi
	}	

	test -n "$TRACE" && $TRACE > /dev/null 2>&1 && export TRACE=true || export TRACE=false
	trace() {
		local messages="$( removeMsgColors "$@" )"
		! $TRACE || errOut "$GREY$( timestamp ) [TRACE] ($0): "$messages"$NC"
	}

	test -n "$DEBUG" && $DEBUG > /dev/null 2>&1 && export DEBUG=true || export DEBUG=false
	debug() {
		local messages="$( removeMsgColors "$@" )"
		! $DEBUG && ! $TRACE || errOut "$GREY$( timestamp ) [DEBUG] ($0): "$messages"$NC"
	}

	info() {
		local messages="$( removeMsgColors "$@" )"
		errOut "$BLUE$( timestamp ) [INFO]: "$messages"$NC"
	}

	warn() {
		local messages="$( removeMsgColors "$@" )"
		errOut "$YELLOW$( timestamp ) [WARN]: "$messages"$NC"
	}

	error() {
		err "$@"
	}

	err() {
		local messages="$( removeMsgColors "$@" )"
		errOut "$RED$( timestamp ) [ERROR]: "$messages"$NC"
	}

	fatal() {
		local messages="$( removeMsgColors "$@" )"
		errOut "$PURPLE$( timestamp ) [FATAL]: "$messages"$NC"
	}

	die() {
		fatal "Exiting: $@"
		exit 1
	}

	successMsg() {
		local messages="$( colorMsg $GREEN "$@" )"
		stdOut "$messages"
	}

	alertMsg() {
		local messages="$( colorMsg $RED "$@" )"
		stdOut "$messages"
	}

	warnMsg() {
		local messages="$( colorMsg $YELLOW "$@" )"
		stdOut "$messages"
	}

	infoMsg() {
		local messages="$( colorMsg $BLUE "$@" )"
		stdOut "$messages"
	}

	cyanMsg() {
		local messages="$( colorMsg $CYAN "$@" )"
		stdOut "$messages"
	}

	purpleMsg() {
		local messages="$( colorMsg $PURPLE "$@" )"
		stdOut "$messages"
	}

	rmOnExit() {
		filesToDelete="$filesToDelete $@"
	}
	
	removeFilesOnExit() {
		if [ -n "$filesToDelete" ]
		then
			trace "Removing filesToDelete: [$filesToDelete] ..."
			rm -f -- $filesToDelete
		fi
	}

	onError() {
		local err=$1 # error status
		local line=$2 # LINENO
		local linecallfunc=$3 
		local command="$4"
		local funcstack="$5"

		if $DEBUG
		then
			errOut "<---"
			errOut "ERROR: line $line - command '$command' exited with status: $err" 
			if [ "$funcstack" != "::" ]; then
				errOut -n "   ... Error at ${funcstack} "
				if [ "$linecallfunc" != "" ]; then
					errOut -n "called at line $linecallfunc"
				fi
			else
				errOut -n "   ... internal debug info from function ${FUNCNAME} (line $linecallfunc)"
			fi
			errOut
			errOut "--->"
		fi
		exit $err
		
		#removeFilesOnExit
	}

	onExit() {

		if [ "$1" -eq 0 ]; then
			trace "exiting ..."
			declare -F onExitHook &>/dev/null && onExitHook || true
			declare -F onTestExit &>/dev/null && onTestExit || true
			removeFilesOnExit
		else
			declare -F onErrHook &>/dev/null && onErrHook "$@" || true
			declare -F onTestErr &>/dev/null && onTestErr "$@" || true
			onError "$@"
		fi
	}

	onCtrlC() {
		onExit
		errOut "Interruption from Keyboard detected !"
		exit 130
	}

	set -e
	set -o errtrace
	#trap 'onError $? $LINENO $BASH_LINENO "$BASH_COMMAND" $(printf "::%s" ${FUNCNAME[@]})' ERR
	#trap onExit TERM QUIT ABRT EXIT
	trap 'onExit $? $LINENO $BASH_LINENO "$BASH_COMMAND" $(printf "::%s" ${FUNCNAME[@]})' TERM QUIT ABRT EXIT
	trap onCtrlC INT

	mkTempFile() {
		test -n "$1" && qualifier=".$1" || qualifier=""
		qualifier="$( echo $qualifier | sed -e 's/X//g' )" # remove X from template.
		local pattern="$( basename $0 )$qualifier.XXXXXXXXXX"
		tempFile=$( mktemp "${TMPDIR:-/tmp}/$pattern" ) || true
		trace "mkTempFile: [$tempFile]"
		#touch "$tempFile"
		if test -n "$tempFile"; then
			> "$tempFile"
			echo "$tempFile"
		else
			echo "UNABLE_TO_MAKE_TEMP_FILE_WITH_PATTERN-$1"
			die "Unable to make temp file with pattern: [$1] !"
		fi
	}

	# Usage: retry maxAttemptCount timeWetweenRetries command

	retry() {
		local maxAttempt=$1
		shift
		local timeout=$1
		shift
		local cmd=("$@")
		local attempt=1

		test $maxAttempt -gt 0 || ( err "retry maxAttempt must be > 0 !" ; exit 1 )
		test $timeout -gt 0 || ( err "retry timeout must be > 0 !" ; exit 1 )

		while [ $attempt -lt $(( maxAttempt + 1 )) ]
		do
			err "Attempt #$attempt/$maxAttempt for command: [ ${cmd[@]} ]"
			eval "${cmd[@]}" && break || true
			attempt=$(( attempt + 1 ))
			sleep $timeout
		done || true

		# Check for number of attempt <= maxAttempt
		test $attempt -le $maxAttempt
	}

	managedCurl() {
		args=("$@")

		scriptsDir="${SCRIPTS_DIR:-.}"
		managedCurlScript="$scriptsDir/managedCurl.sh"
		test -x "$managedCurlScript" || ("Unable to find manageCurl.sh script !"; exit 1)
		managedCurlCmd="$managedCurlScript ${args[@]}"
		trace "managedCurl cmd: $managedCurlCmd"
		$managedCurlCmd
	}

	truncateMessage() {
		source=""
		doNotTruncateIfDebug=false
		maxErrorOutLines=10
		maxLineSize=200

		tmpFile=$( mkTempFile "truncateMessage" )
		trace "tmpFile: $tmpFile"

		if [ -z "$1" ] || [ "$1" == "-" ]; then
			source="stdin"
			trace "truncateMessage from stdin ..."
			while IFS= read -r line || [ -n "$line" ]; do
				trace "Reading line: $line"
				echo -e "$line" >> $tmpFile
			done
		elif [ -f "$1" ]; then
			source="file"
			trace "truncateMessage from file ..."
			while IFS= read -r line || [ -n "$line" ]; do
				trace "Reading line: $line"
				echo -e "$line" >> $tmpFile
			done < "$1"
		elif [ -n "$1" ]; then
			source="string"
			trace "truncateMessage from args ..."
			# Read line from args
			for line in "$@"; do
				trace "Reading arg: $line"
				echo -e "$line" >> $tmpFile
			done
		fi

		# Display all content if DEBUG and doNotTruncateIfDebug
		if $doNotTruncateIfDebug && $DEBUG || $TRACE ; then
		       cat "$tmpFile"
		       return 0
		fi

		if [ "$( cat "$tmpFile" | wc -l )" -gt "$maxErrorOutLines" ]; then
			# If content too long
			trace "Too much lines"
			halfCount=$(( maxErrorOutLines / 2 ))
			grep -v "^ *$" $tmpFile | head -$halfCount | while IFS= read -r line || [ -n "$line" ]; do
				trace "Reading trimmed line: $line"
				echo -e "$line"
			done
			echo "[... truncated ...]"
			grep -v "^ *$" $tmpFile | tail -$halfCount | while IFS= read -r line || [ -n "$line" ]; do
				trace "Reading trimmed line: $line"
				echo -e "$line"
			done
		else
			# Content not too long
			trace "Few lines"
			while IFS= read -r line || [ -n "$line" ]; do
				trace "Reading kept line: $line"
				echo -e "$line"
			done < "$tmpFile"
		fi | while IFS= read -r line || [ -n "$line" ]; do
			trace "Reading to truncate line: $line"
			lineSize=$( echo "$line" | wc -m )
			if [ $lineSize -gt $maxLineSize ]; then
				halfSize=$(( maxLineSize / 2 ))
				echo -ne "$( echo -e "$line" | cut -c1-$halfSize )"
				echo -n "[... truncated ...]"
				echo -e "$line" | cut -c$(( lineSize - halfSize ))-$lineSize		
			else
				echo -e "$line"
			fi
		done
	}

	printArgs() {
		if [ -n "$1" ]; then
			echo -n "[$1]"
			shift
		fi

		while [ -n "$1" ]; do
			echo -n " [$1]"
			shift
		done
		echo
	}
fi

_SCRIPT_FMK_LOADED=true
