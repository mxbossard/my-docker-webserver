#! /bin/sh -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

cd $SCRIPT_DIR/../nginx-proxy

docker-compose logs -f | less -R
