#! /bin/sh
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

usage() {
	>&2 echo "usage: $0 <sideEnv>"
	exit 1
}

envName="$1"
test -n "$envName" || usage

# rm all env containers
ctIds=$( docker ps -q -f "name=^${envName}_.*" )
>&2 echo "Removing containers ${envName}_.* ..."
test -z "$ctIds" || docker rm -f $ctIds

# rm all env volumes
if [ "$KEEP_ENV_VOLUMES" != "true" ]; then
	>&2 echo "Removing volumes ${envName}_.* ..."
	volIds=$( docker volume ls -q -f "name=^${envName}_.*" )
	test -z "$volIds" || docker volume rm $volIds
fi

# rm all env networks
>&2 echo "Removing networks ${envName}_.* ..."
netIds=$( docker network ls -q -f "name=^${envName}_.*" )
test -z "$netIds" || docker network rm $netIds

rm -rf -- "$SCRIPT_DIR/../sideEnvs/$envName"
