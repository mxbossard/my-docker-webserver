#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
source $SCRIPT_DIR/scriptFramework.sh

projectName="$1"
serviceName="$2"
networkRegexp="${3:-.*}"

usage() {
	die "Usage: $0 projectName serviceName [networkRegexp]"
}

test -n "$projectName" || usage
test -n "$serviceName" || usage

source $SCRIPT_DIR/globalEnv.sh

tempFile=$( mkTempFile )
rmOnExit "$tempFile"

containerNames=$( docker ps --format '{{.Names}}' | grep "^${ENV}_${projectName}_${serviceName}_" )
for name in $containerNames
do
	docker inspect -f '{{$name := .Name}}{{range $key, $network := .NetworkSettings.Networks}}{{print $network.IPAddress}} {{$name}} {{println $key}}{{end}}' $name | egrep '.+' | sed -e 's|/||g' 
done > $tempFile

debug "docker inspect result: [$( cat $tempFile )]"

while read line
do
	network=$( echo $line | awk '{ print $3 }' )
	if [ -n "$network" ]; then
		if echo "$network" | egrep "$networkRegexp" > /dev/null
		then
			echo $line
		fi
	else 
		warn "Malformated service line from $0 script: [$line]. Service may not be started !"
		continue
	fi
done < $tempFile

rm -f -- "${TMPDIR:-/tmp}/listServiceIps.*"
