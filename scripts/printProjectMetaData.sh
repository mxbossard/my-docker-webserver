#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
QUIET=true
source $SCRIPT_DIR/scriptFramework.sh
source $SCRIPT_DIR/globalEnv.sh

inputProjects=""
while test -n "$1" && projects="$( $SCRIPTS_DIR/listProjects.sh $1 2> /dev/null )"; do
	inputProjects="$inputProjects $projects"
	shift
done

test -n "$inputProjects" || inputProjects="$( basename $PWD )"
debug "inputProjects: $inputProjects"

for project in $inputProjects; do
	cd "$PARENT_DIR/$project"
	source $SCRIPT_DIR/projectEnv.sh
	source $SCRIPTS_DIR/loadDynamicDockerComposeConfig.sh

	echo "##### Project $project meta data #####"
	echo
	echo "time: $( date '+%F %T' )"
	echo "-----"
	echo 
done
