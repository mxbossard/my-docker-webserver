#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
source $SCRIPT_DIR/scriptFramework.sh

source $SCRIPT_DIR/globalEnv.sh

usage() {
	errOut "Usage: $0 <newEnvName> <portShift> [gitBranch|gitTag] [gitRepoPath]"
	errOut "If gitBranch is ommited, duplicate local project not using git."
	errOut "If gitRepoPath is ommited, use local repo remote URL."
	exit 1
}

gitRemoteUrl="$( git remote get-url $( git remote ) )"
if echo "$gitRemoteUrl" | grep "^git@" > /dev/null
then
	gitRemoteUrl="$( echo $gitRemoteUrl | sed -re 's|^git@([^:]+):(.*)|https://\1/\2|' )"
fi

newEnvName="$1"
portShift="$2"
gitBranch="$3"
gitRepoPath="$4"
test -n "$newEnvName" || usage
test -n "$portShift" && test "$portShift" -gt 0 || usage

gitRepo="${gitRepoPath:-$gitRemoteUrl}"

multiEnvDir="$SCRIPT_DIR/../sideEnvs"
newEnvDir="$multiEnvDir/$newEnvName"
newConfDir="$newEnvDir/conf"

rm -rf -- "$newEnvDir/*"
mkdir -p "$newEnvDir"

if [ -n "$gitBranch" ]; then
	infoMsg "Cloning repo: $gitRepo ..."
	rc=0

	yes | git clone -b $gitBranch --depth 1 --recurse-submodules --shallow-submodules "$gitRepo" "$newEnvDir" || rc=$?
	if [ 0 -ne "$rc" -a 141 -ne "$rc" ]; then
		>&2 echo "Error cloning git branch: [$gitBranch] with return code: $rc !"
		exit 1
	fi
	#rm -rf -- "$newEnvDir/scripts"
	#cp -rf "$SCRIPTS_DIR" "$newEnvDir/scripts"
else
	infoMsg "Rsyncing project ..."
	rsync -az --stats --exclude "$( basename $multiEnvDir )" --exclude "*.bak" --exclude "*.rendered" --exclude "*.audit" --exclude "*.project.hash" --exclude ".git" "$PARENT_DIR/" "$newEnvDir/"
fi

infoMsg "Finished cloning side env sources."

cd "$newEnvDir"

newNameFile="$newEnvDir/name.txt"
newGlobalEnvFile="$newEnvDir/env.sh"
echo "$newEnvName" > $newNameFile
rm -f -- "$newGlobalEnvFile"

parentConfEnvFile="$CONF_DIR/$ENV.env.sh"
newConfEnvFile="$newConfDir/$newEnvName.env.sh"
parentCredsFile="$CONF_DIR/.$newEnvName.credentials.sh"

# Do not erase new conf env file if it exists.
if test -f "$newConfEnvFile"; then
	infoMsg "Config file: [$newConfEnvFile] already exists. Will not erase it."
else
	cp $parentConfEnvFile $newConfEnvFile
fi

parentRootDomainName="$ROOT_DOMAIN_NAME"
(
	source $newConfEnvFile

	if [ -n "$PORT_SHIFT" ] && [ "$PORT_SHIFT" -ne 0 ]; then
		test "$PORT_SHIFT" -eq "$portShift" || warnMsg "Specified portShift: [$portShift] missmatch the one configured in file: [$newConfEnvFile] (PORT_SHIFT=$PORT_SHIFT)"
	else
		warnMsg "Specify PORT_SHIFT in file: [$newConfEnvFile]."
		# Set PORT_SHIFT and ROOT_DOMAIN_NAME mandatory config.
		echo "PORT_SHIFT=$portShift" >> $newConfEnvFile
	fi

	if [ "$ROOT_DOMAIN_NAME" == "$parentRootDomainName" ]; then
		warnMsg "Specify ROOT_DOMAIN_NAME in file: [$newConfEnvFile]."
		echo "ROOT_DOMAIN_NAME=\"$newEnvName.\$ROOT_DOMAIN_NAME\"" >> $newConfEnvFile
	fi
)

# Copy creds file.
test -f "$parentCredsFile" && ln -s "$parentCredsFile" "$newConfDir/.credentials.sh"

# Conf nginx-proxy docker-compose to load new docker-compose.behind-frontal.yml config file.
echo "COMPOSE_FILE=docker-compose.yml:docker-compose.behind-frontal.yml" > $newEnvDir/nginx-proxy/env.override.sh
cat <<EOF >> $newEnvDir/nginx-proxy/docker-compose.behind-frontal.yml
services: 
  nginx-proxy:
    environment:
      VIRTUAL_HOST: "$newEnvName.$ROOT_DOMAIN_NAME,*.$newEnvName.$ROOT_DOMAIN_NAME"
      VIRTUAL_PORT: 80
      VIRTUAL_GROUP: "$ENV"
    networks:
      front-http-proxy:

networks:
  front-http-proxy:
    name: "${ENV}_http-proxy"
    external: true
EOF

