#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
. $SCRIPT_DIR/scriptFramework.sh

INPUT_PROJECTS="${@}"
test -z "$INPUT_PROJECTS" && die "Usage: $0 PROJECT_LIST" || true
export QUIET=true

healthcheckRetries=3
healthcheckRetriesIntervalSec=1

for project in $( $SCRIPT_DIR/listProjects.sh $INPUT_PROJECTS )
do
	cd $SCRIPT_DIR/../$project
	(
		source $SCRIPT_DIR/projectEnv.sh
		upCtCount=$( docker ps --format '{{.Names}}' | grep "^${PROJECT_NAMESPACE}_" | wc -l )
		allCtCount=$( docker ps -a --format '{{.Names}}' | grep "^${PROJECT_NAMESPACE}_" | wc -l )
		test $upCtCount -gt 0 || die "Project: [$project] is DOWN (got no running container) !"
		test $upCtCount -eq $allCtCount || die "Project: [$project] is DOWN (got bad count of running containers) !"
		# Quicker healthchecks
		export HEALTHCHECK_INITIAL_DELAY_SEC=0
		export HEALTHCHECK_RETRIES=$healthcheckRetries
		export HEALTHCHECK_RETRIES_INTERVAL_SEC=$healthcheckRetriesIntervalSec
		#$SCRIPT_DIR/runHealthchecks.sh $project > /dev/null 2>&1 || die "Project: [$project] is DOWN (healthcheks failed) !"
		$SCRIPT_DIR/runHealthchecks.sh $project 2> /dev/null || die "Project: [$project] is DOWN (healthcheks failed) !"
	)
done
