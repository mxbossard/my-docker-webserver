#! /bin/sh -e

#docker run -i -v "${PWD}":/workdir:ro mikefarah/yq "$@"
#docker run --name yq_daemon --rm -d --entrypoint sh mikefarah/yq:4.6.1 -c "sleep 600" > /dev/null 2>&1 || true

# Wait for container to be up
maxAttempt=30
k=0
while [ "$maxAttempt" -gt "$k" -a -z "$( docker ps -q -f 'name=^yq_daemon$' )" ]; do
	test 0 -eq "$k" && >&2 echo -n "Waiting for yq_daemon to start ..." || true
	docker rm -f yq_daemon
	docker run --name yq_daemon --rm -d --entrypoint sh mikefarah/yq:4.6.1 -c "sleep 600" > /dev/null || true
	sleep 1
	>&2 echo -n "."
	k=$(( k + 1 ))
done
test 0 -lt "$k" && >&2 echo || true

test "$maxAttempt" -gt "$k" || >&2 echo "yq_daemon container failed to start !"

docker exec -i yq_daemon yq "$@"
