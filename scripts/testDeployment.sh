#! /bin/sh -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

# Test deployment in test env : up project building images from dev source project dir.

envName="test"

>&2 echo "Deploying local files on test side env and up it --strict ..."
>&2 echo

# up strict  all by default
opts="${@:-all}"
$SCRIPT_DIR/deploySideEnv.sh "$envName" "10000" "-" "-" --strict $opts

