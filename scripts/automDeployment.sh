#! /bin/sh -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

# Test deployment in autom env : up project pulling images only from git reference.

envName="autom"

usage() {
	>&2 echo "usage: $0 <gitBranch>"
	exit 1
}

gitBranch="$1"
test -n "$gitBranch" || usage
shift 1

>&2 echo "Deploying git remote $gitBranch branch on $envName side env and up it --strict --pull ..."

# up strict and force pull all by default
opts="${@:-all}"
$SCRIPT_DIR/deploySideEnv.sh "$envName" 30000 "$gitBranch" "-" --strict --pull $opts
