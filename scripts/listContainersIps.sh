#! /bin/sh

containersExpr="$1"
networkRegexp="${2:-.*}"
#echo $containersExpr $networkRegexp

tempFile=$( mkTempFile )
rmOnExit "$tempFile"

containerNames=$( docker ps --format '{{.Names}}' | egrep "$containersExpr" )
for name in $containerNames
do
	docker inspect -f '{{$name := .Name}}{{range $key, $network := .NetworkSettings.Networks}}{{print $network.IPAddress}} {{$name}} {{println $key}}{{end}}' $name | egrep '.+' | sed -e 's|/||g' 
done > $tempFile

while read line
do
	network=$( echo $line | awk '{ print $3 }' )
	if echo "$network" | egrep "$networkRegexp" > /dev/null
	then
		echo $line
	fi
done < $tempFile

