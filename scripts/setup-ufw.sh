#! /bin/sh -e

yes | sudo ufw reset 

sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw default deny routed

sudo ufw allow ssh

yes | sudo ufw enable
sudo ufw status verbose

