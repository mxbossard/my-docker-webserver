#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
export QUIET=true
source $SCRIPT_DIR/scriptFramework.sh
source $SCRIPT_DIR/globalEnv.sh

usage() {
	>&2 echo "usage: $0 <qualifier> [project1] ... [projectN]"
	>&2 echo "Specify '-' qualifier to bump without qualifier."
	>&2 echo "Specify all as project to select all projects."
	>&2 echo 

	exit 1
}

if [ "$1" == "-" ]; then
	qualifier=""
	shift
elif [ -n "$1" ]; then
	qualifier="-$1"
	shift
else
	usage
fi

inputProjects=""
if [ "$1" == "all" ]; then
	inputProjects="$( $SCRIPTS_DIR/listProjects.sh )"
else
	while test -n "$1" && projects="$( $SCRIPTS_DIR/listProjects.sh $1 2> /dev/null )"; do
		inputProjects="$inputProjects $projects"
		shift
	done
fi

test -n "$inputProjects" || usage
debug "inputProjects: $inputProjects"

doesDockerTagExists() {
	>&2 infoMsg "Checking if image: [$CYAN$1:$2$NC] exists on remote repo ..."
	curl --silent -f -lSL https://index.docker.io/v1/repositories/$1/tags/$2 > /dev/null 2>&1
}

getRepoImageId() {
	local imageWithTag="$1"
	>&2 infoMsg "Checking if image: [$CYAN$imageWithTag$NC] exists on remote repo, retriving imageId ..."
	docker manifest inspect "$imageWithTag" 2> /dev/null | jq -r '.config.digest' | sed -e 's/[^:]\+://'
}

# Accepts a version string and prints it incremented by one.
# Usage: increment_version <version> [<position>] [<leftmost>]
increment_version() {
   local usage=" USAGE: $FUNCNAME [-l] [-t] <version> [<position>] [<leftmost>]
           -l : remove leading zeros
           -t : drop trailing zeros
    <version> : The version string.
   <position> : Optional. The position (starting with one) of the number 
                within <version> to increment.  If the position does not 
                exist, it will be created.  Defaults to last position.
   <leftmost> : The leftmost position that can be incremented.  If does not
                exist, position will be created.  This right-padding will
                occur even to right of <position>, unless passed the -t flag."

   # Get flags.
   local flag_remove_leading_zeros=0
   local flag_drop_trailing_zeros=0
   while [ "${1:0:1}" == "-" ]; do
      if [ "$1" == "--" ]; then shift; break
      elif [ "$1" == "-l" ]; then flag_remove_leading_zeros=1
      elif [ "$1" == "-t" ]; then flag_drop_trailing_zeros=1
      else echo -e "Invalid flag: ${1}\n$usage"; return 1; fi
      shift; done

   # Get arguments.
   if [ ${#@} -lt 1 ]; then echo "$usage"; return 1; fi
   local v="${1}"             # version string
   local targetPos=${2-last}  # target position
   local minPos=${3-${2-0}}   # minimum position

   # Split version string into array using its periods. 
   local IFSbak; IFSbak=IFS; IFS='.' # IFS restored at end of func to                     
   read -ra v <<< "$v"               #  avoid breaking other scripts.

   # Determine target position.
   if [ "${targetPos}" == "last" ]; then 
      if [ "${minPos}" == "last" ]; then minPos=0; fi
      targetPos=$((${#v[@]}>${minPos}?${#v[@]}:$minPos)); fi
   if [[ ! ${targetPos} -gt 0 ]]; then
      echo -e "Invalid position: '$targetPos'\n$usage"; return 1; fi
   (( targetPos--  )) || true # offset to match array index

   # Make sure minPosition exists.
   while [ ${#v[@]} -lt ${minPos} ]; do v+=("0"); done;

   # Increment target position.
   v[$targetPos]=`printf %0${#v[$targetPos]}d $((10#${v[$targetPos]}+1))`;

   # Remove leading zeros, if -l flag passed.
   if [ $flag_remove_leading_zeros == 1 ]; then
      for (( pos=0; $pos<${#v[@]}; pos++ )); do
         v[$pos]=$((${v[$pos]}*1)); done; fi

   # If targetPosition was not at end of array, reset following positions to
   #   zero (or remove them if -t flag was passed).
   if [[ ${flag_drop_trailing_zeros} -eq "1" ]]; then
        for (( p=$((${#v[@]}-1)); $p>$targetPos; p-- )); do unset v[$p]; done
   else for (( p=$((${#v[@]}-1)); $p>$targetPos; p-- )); do v[$p]=0; done; fi

   echo "${v[*]}"
   IFS=IFSbak
   return 0
}

askForNewTag() {
	localImageId="$1"
	imageName="$2"
	initialTag="$3"
	proposedTag="$4"

	if [ "$initialTag" == "$proposedTag" ]; then
		# Propose to not changing tag
		message="Enter a different tag if you don't want to keep the current tag ? "
	else
		# Propose to change the tag
		message="Enter a different tag if the proposed bumped tag do not fit your expectations ? "
	fi

	>&2 read -p "$message" newTag
	newTag="${newTag:-$proposedTag}"

	# Replace qualifier
	newTag="$( echo $newTag | cut -d- -f1 )$qualifier"

	if test "$newTag" != "$proposedTag" && repoImageId="$( getRepoImageId $imageName:$newTag )" && test "$localImageId" != "$repoImageId"; then
		>&2 read -p "The tag: [$newTag] already exists on remote repo (you may override it) ! Do you want to continue [y/N] ? " confirm
		if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ]; then
			askForNewTag "$localImageId" "$imageName" "$initialTag" "$proposedTag"
			return 0
		fi
	fi

	>&2 infoMsg "You choose the tag: [$CYAN$newTag$NC]."
	>&2 read -p "Do you confirm [y/N] ? " confirm
	if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ]; then
		# If no confirmation
		>&2 read -p "Do you want to continue [Y/n] ? " confirm
		if [ "$confirm" != "n" ] && [ "$confirm" != "N" ]; then
			askForNewTag "$localImageId" "$imageName" "$initialTag" "$proposedTag"
			return 0
		else
			exit 1
		fi
	fi
	echo "$newTag"
}

shortImageId() {
	echo "$1" | cut -c1-12
}

>&2 infoMsg "Bumping built docker images version tags ..."

outTmpFile=$( mkTempFile "out" )
for project in $inputProjects; do
	cd "$PARENT_DIR/$project"

	tagAlreadyExists=false
	for line in $( $SCRIPTS_DIR/listReadyToPushImages.sh "$project" | tr ' ' ';' ); do
		serviceName="$( echo $line | cut -d';' -f2 )"
		imageNameWithTag="$( echo $line | cut -d';' -f3 )"
		localImageId="$( echo $line | cut -d';' -f4 )"
		imageName="$( echo $imageNameWithTag | cut -d':' -f1 )"
		imageTag="$( echo $imageNameWithTag | cut -d':' -f2 )"

		>&2 infoMsg "Processing service: [$PURPLE$project$NC/$GREEN$serviceName$NC] image: [$CYAN$imageNameWithTag$NC] ..."

		qualifiedTag="$( echo $imageTag | cut -d- -f1 )$qualifier"
		imageWithNewQualifier="$imageName:$qualifiedTag"
		# Check if tag already exists on repo
		imageToCheck="$imageWithNewQualifier"
		if repoImageId="$( getRepoImageId $imageToCheck )"; then
			tagAlreadyExists=true
			if test "$localImageId" == "$repoImageId"; then
				warnMsg "Image: [$CYAN$imageToCheck$NC] already exists on remote repo (imageId: $BLUE$( shortImageId $repoImageId )$NC)."
				#info "Skipping, image: [$imageToCheck] already exists already on remote repo. "
				#continue
			else
				warnMsg "Image: [$CYAN$imageToCheck$NC] already exists on remote repo with a different layer (localId: $RED$( shortImageId $localImageId )$NC vs remoteId: $BLUE$( shortImageId $repoImageId )$NC)."
			fi
		fi

		if $tagAlreadyExists; then
			# Do not propose to overwrite tag by default: propose to bump the tag.
			imageTagWithoutQualifier="$( echo $qualifiedTag | cut -d- -f1 )"
			proposedTag="$( increment_version $imageTagWithoutQualifier 3 )$qualifier"
			>&2 infoMsg "The qualified tag: [$CYAN$qualifiedTag$NC] already exists on remote repo. Bumping by default to: [$PURPLE$proposedTag$NC]."
		else
			# Current tag does not exists propose this tag by default
			proposedTag="$qualifiedTag"
			# Add qualifier to proposition
			proposedTag="$( echo $proposedTag | cut -d- -f1 )$qualifier"
			>&2 infoMsg "The qualified tag: [$CYAN$qualifiedTag$NC] don't exists on remote repo. Keeping this tag by default."
		fi

		choosenTag="$( askForNewTag $localImageId $imageName $imageTag $proposedTag )"
		bumpedImage="$imageName:$choosenTag"
		debug "Selected new tag: [$choosenTag]."
	
		if [ "$imageTag" != "$choosenTag" ]; then
			source $SCRIPT_DIR/projectEnv.sh
			source $SCRIPTS_DIR/loadDynamicDockerComposeConfig.sh

			>&2 infoMsg "Tagging image in local registry: [$BLUE$imageNameWithTag$NC] => [$CYAN$bumpedImage$NC] ..."
			>&2 docker tag "$imageNameWithTag" "$bumpedImage"

			bumped=false
			# Find project config files to update
			for composeFile in $DOCKER_COMPOSE_CONFIG_FILES; do
				test -f "$composeFile" || continue
				debug "Scanning composeFile: [$composeFile] for service: [$serviceName] image definition ..."

				serviceImageVal=$( cat "$composeFile" | $SCRIPTS_DIR/yq.sh eval ".services.$serviceName.image" - )
				trace "Found service image: [$serviceImageVal]."
				# Check if image field correspond to initial image (check raw then envsubst)
				if [ "$imageNameWithTag" == "$serviceImageVal" -o "$imageNameWithTag" == "$( echo $serviceImageVal | envsubst )" ]; then
					debug "Found service image: [$serviceImageVal] match [$imageNameWithTag]."
					rm -f -- "$composeFile.version.bak"
					cp -a "$composeFile" "$composeFile.version.bak"
					if ! cat "$composeFile.version.bak" | $SCRIPTS_DIR/yq.sh eval ".services.$serviceName.image = \"$bumpedImage\"" - > $composeFile; then
						mv "$composeFile.version.bak" "$composeFile"
						die "Unable to bump image version in file: [$composeFile]"
					fi
					>&2 infoMsg "Bumped file: [$composeFile]."
					>&2 diff "$composeFile.version.bak" "$composeFile" || true
					bumped=true

					# Print modified file
					echo "$composeFile"
				fi
			done

			# Bump check
			$bumped || die "Unable to find image definition in a compose yml file !"
			
			>&2 infoMsg "Checking bump effectiveness ..."
			source $SCRIPTS_DIR/loadDynamicDockerComposeConfig.sh --force
			actualImageValue=$( $DOCKER_COMPOSE_CMD config | $SCRIPTS_DIR/yq.sh eval ".services.$serviceName.image" - )
			test "$bumpedImage" == "$actualImageValue" && >&2 successMsg "Bumping of image: [$BLUE$imageName$NC] [$BLUE$imageTag$NC] => [$CYAN$choosenTag$NC] is successful." || die "Service: [$PURPLE$project$NC/$GREEN$serviceName$NC] image was not bumped as expected (expected: [$CYAN$bumpedImage$NC], but actual is: [$RED$actualImageValue$NC]) !"

		else
			>&2 infoMsg "You choose to keep the current tag. Nothing do do."

		fi

	done

done > $outTmpFile

cat "$outTmpFile" | sort -u

