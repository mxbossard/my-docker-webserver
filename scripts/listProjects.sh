#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
. $SCRIPT_DIR/scriptFramework.sh

DEPENDENCY_TREE_FILE="$( readlink -f $SCRIPT_DIR/../conf/projectDependencyTree.txt )"

ALL_PROJECTS=false

if [ -z "$1" ]
then
	ALL_PROJECTS=true
fi

errorFound=false
tempFile=$( mktemp "${TMPDIR:-/tmp}/listProjects.XXXXXXXXXX" )
rmOnExit "$tempFile"

while $ALL_PROJECTS || test -n "$1"
do
	# strip /
	project=$( basename "$1" )
	$ALL_PROJECTS || shift

	projectFound=false
	while read line
	do
		projectName=$( echo $line | cut -d: -f1 )
		projectDepencies=$( echo $line | cut -d: -f2- )

		# Do not treat empty lines and commented lines
		if test -n "$projectName" && echo "$projectName" | grep -ve '^\s*#' > /dev/null
		then
			projectDir="$SCRIPT_DIR/../$projectName"
			
			if $ALL_PROJECTS || test "$project" == "$projectName"
			then
				projectFound=true
				echo $projectName
				$ALL_PROJECTS || break
			fi
		fi
	done < $DEPENDENCY_TREE_FILE

	ALL_PROJECTS=false
	! $projectFound && errorFound=true && err "Project [$project] is not referenced in file $DEPENDENCY_TREE_FILE !" || true
	test ! -d "$projectDir" && errorFound=true && err "Project directory [$project/] does not exists !" || true
done > $tempFile

#echo errorFound: $errorFound
$errorFound && exit 1 || cat $tempFile
