#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
export QUIET=true
source $SCRIPT_DIR/scriptFramework.sh
source $SCRIPT_DIR/globalEnv.sh

usage() {
	errOut "$1"
	errOut "usage: $0 [-f] [projects] [images]"
	errOut 
	exit 1
}

forceConfig=false
if [ "$1" == "-f" ]; then
        forceConfig=true
        shift
fi

projects=""
images=""
for arg; do
	if [ "$arg" == "all" ]; then
		projects="$( $SCRIPTS_DIR/listProjects.sh )"
		images=""
		break
	elif $SCRIPTS_DIR/listProjects.sh "$arg" > /dev/null 2>&1; then
		projects="$projects $arg"
	elif imageTag=$( docker images --format "{{ .Tag }}" "$arg" | grep "" ); then
	       	if echo "$imageTag" | grep "^[0-9.]\+-.\+$" > /dev/null; then
			images="$images $arg"
		else
			>&2 warnMsg "Ignoring released image: [$arg]."
		fi
	else
		>&2 warnMsg "Ignoring arg: [$arg] which don't correspond to an image nor a project."
	fi
done

if [ -n "$projects" -o -z "$1" ]; then
	>&2 infoMsg "Searching for non released build image candidate to release ..."
	# Scan built release candidate images
	# if it was something other than rc# => rc0 we do nothing
	images="$images$( $SCRIPTS_DIR/listBuiltImages.sh $projects | awk '{ print $3 }' | grep ":[0-9.]\+-rc[0-9]\+$" | tr ' ' '\n' | sort -u )"
fi

if test -z "$images"; then
	cyanMsg "No candidate image for Release promotion found."
	exit 0
fi
>&2 cyanMsg "Candidate images to Release promotion: \n$images"

builtImagesTempFile="$( mkTempFile "builtImages" )"
toCommitTempFile="$( mkTempFile "toCommit" )"
bumpedImagesTempFile="$( mkTempFile "bumpImages" )"
$SCRIPTS_DIR/listBuiltImages.sh "all" > "$builtImagesTempFile"

>&2 infoMsg "Check for non commited modifications of index file ..."
if git status -s | grep ""; then
	# Some modifications in the index file from HEAD commit
	# Ask confirm to continue with stashing
	>&2 read -p "You have some non commited changes. Do you want to stash it to continue ? [Y/n] " confirm
	if [ "$confirm" == n -o "$confirm" == "N" ]; then
		>&2 warnMsg "Cannot continue promotion if some changes stay not commited !"
		exit 1
	fi
fi

stashMessage="releasing_$( date +%s )"
git stash push -m "$stashMessage"

popStash() {
	if stashItem="$( git stash list | grep "$stashMessage$" )"; then
		stash="$( echo "$stashItem" | cut -d: -f1 )"
		info "Poping stash: [$stash] ($stashItem) ..."
		git stash pop "$stash"
	fi
}

onErrorLocal() {
        alertMsg "An error occured. Trying to reset modifications with a git checkout -- $PARENT_DIR."
	git checkout -- "$PARENT_DIR" || true
	popStash || true
        onError "$@"
        exit 1
}
trap 'onErrorLocal $? $LINENO $BASH_LINENO "$BASH_COMMAND" $(printf "::%s" ${FUNCNAME[@]})' ERR

for image in $images; do
	# Releasing rc# image only

	>&2 read -p "Do you want to release image: [$image] ? [y/N] " confirm
	if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ]; then
		>&2 warnMsg "Ignoring image: [$image]."
		continue	
	fi

	# Identify which project/service use this image
	serviceFound=false
	bumpedFiles="$( mkTempFile "bumpedFiles" )"
	for projectService in $( grep " $image$" "$builtImagesTempFile" | awk '{ print $1 "/" $2 }' ); do
		serviceFound=true
		project="$( echo "$projectService" | cut -d/ -f1 )"
		service="$( echo "$projectService" | cut -d/ -f2 )"
		# Propose to bump service image
		>&2 cyanMsg "Image in use by the service: [$PURPLE$project$NC/$GREEN$service$NC]. Proceeding to bump ..."
		$forceConfig && forceOpt="-f" || forceOpt=""
		$SCRIPTS_DIR/bumpServicesImageVersion.sh -p $forceOpt "-" "$project" "$service" >> "$toCommitTempFile"
	done

	if ! $serviceFound; then
		>&2 warnMsg "No service found using the image: [$image]."
		imageName="$( echo "$image" | cut -d: -f1 )"
		if grep " $imageName:" "$builtImagesTempFile" > /dev/null; then
			>&2 infoMsg "Some services may use another tag of the image :"
			grep " $imageName:" "$builtImagesTempFile"
		fi
	fi
done

if [ -n "$( cat "$toCommitTempFile" )" ]; then
	>&2 cyanMsg "Commiting image release ..."
	git add $( cat "$toCommitTempFile" )
	git commit -m "Releasing docker images."
else
	>&2 cyanMsg "No changes to commit."
	
fi

popStash

exit 0

