
getImageTagsOnRegistryByDigest() {
	local imageWithRepo="$1"
	local imageDigest="$2"
	local registryUrl="${3:-https://registry.hub.docker.com/v2/repositories}"

	errOut "Retrieving tags list of digest: [$CYAN$( shortDigest "$imageDigest" )$NC] for repository: [$CYAN$imageWithRepo$NC] on remote registry: [$GREY$registryUrl$NC] ..."
	$DEBUG && >&2 curl -sS -L -- "$registryUrl/$imageWithRepo/tags/" || true
	curl -sS -L -- "$registryUrl/$imageWithRepo/tags/" 2> /dev/null | $SCRIPTS_DIR/yq.sh eval "select(.results.[].images.[].digest == '$imageDigest') | .name" - 2> /dev/null | sed '/-/!{s/$/_/}' | sort -V | sed 's/_$//' || true # Sort respecting semantic versioning (no qualifer > with qualifier)
}

getDockerTagsOnRegistry() {
	local imageWithRepo="$1"
	local registryUrl="${2:-https://registry.hub.docker.com/v2/repositories}"

	errOut "Retrieving tags list of repository: [$CYAN$imageWithRepo$NC] on remote registry: [$GREY$registryUrl$NC] ... "
	#TOKEN=$(curl -sSL "https://auth.docker.io/token?service=registry.docker.io&scope=repository:$imageWithRepo:pull" | jq --raw-output .token)
	curl -sS -L -- "$registryUrl/$imageWithRepo/tags/list" | $SCRIPTS_DIR/yq.sh eval '.results.[].name' -
}

doesDockerTagExistsOnRegistry() {
	local image="$1"
	local tag="$2"
	local registryUrl="${3:-registry.hub.docker.com/v2/repositories}"

	errOut -n "Checking if image: [$CYAN$image:$tag$NC] exists on remote registry: [$GREY$registryUrl$NC] ... "
	if curl --silent -f -lSL -- "$registryUrl/$image/tags/$tag" > /dev/null 2>&1; then
		>&2 successMsg "found"
		return 0
	else
		>&2 warnMsg "not found"
		return 1
	fi
}

getServiceBuiltImage() {
	local project="$1"
	local service="$2"
	errOut "Listing built image of service: [$project/$service] ..."
	$SCRIPTS_DIR/listBuiltImages.sh "$project" | grep "^$project $service " | awk '{ print $3 }' 2> /dev/null || true
}

