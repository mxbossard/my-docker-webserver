#! /bin/bash -e
SCRIPT_DIR="$( dirname $( readlink -f $0 ) )"

. $SCRIPT_DIR/scriptFramework.sh

set -f

#trace "managedCurl @: $@"

insecureOpt=""
test ! "$DISABLE_LETS_ENCRYPT" == "true" || insecureOpt="-k"

ipAddresses="$MANAGED_CURL_HOST_IPS"
test -n "$ipAddresses" || warn "No MANAGED_CURL_HOST_IPS defined to use."

curlMaxTime=${MANAGED_CURL_MAX_TIME:-10}
curlConnectTimeout=${MANAGED_CURL_CONNECT_TIMEOUT:-1}

tempFile=$( mkTempFile )
errorFile=$( mkTempFile )
rmOnExit "$tempFile"
rmOnExit "$errorFile"

managedArgs="--connect-timeout $curlConnectTimeout --max-time $curlMaxTime $insecureOpt"

curlCmd="curl $managedArgs $@"
rc=0
debug "managedCurl will execute: [$curlCmd]"

curl $managedArgs "$@" > "$tempFile"  2> "$errorFile" || rc=$?
if [ "$rc" -eq 0 ]; then
	# cURL OK, nothing to do
	debug "cURL executed successfully."
	#$DEBUG && >&2 cat "$errorFile"
elif [ -n "$ipAddresses" ] && [ "$rc" -eq 6 -o "$rc" -eq 28 ]; then
	# cURL was Unable to resolve the Host maybe we can curl directly to the IP ...
	#$DEBUG && >&2 cat "$errorFile"
	debug "cURL did not found host trying with ipAddresses: [$ipAddresses]."	

	url="$( echo "${@: -1}" | sed -e "s/^[\"']//" -e "s/[\"']$//" )"
	splitted="$( echo $url | sed -re 's|^([^:]+)://([^/]+)(/?.*)$|\1 \2 \3|g' )" || true
	protocol="$( echo $splitted | awk '{ print $1; }' )" || true
	hostName="$( echo $splitted | awk '{ print $2; }' | sed -e 's|^[^@]*@||g' -e 's|:[0-9]*$||' )" || true
	port="$( echo $splitted | awk '{ print $2; }' | egrep ':[0-9]+$' | sed -r -e 's|.*:([0-9]+)|\1|' )" || true
	requestPath="$( echo $splitted | awk '{ print $3; }' )" || true

	for ipAddress in $ipAddresses; do
		trace "protocol: [$protocol] ; hostName: [$hostName] ; port: [$port] ; requestPath: [$requestPath] ; ipAddress: [$ipAddress] ; url: [$url] ; splittedUrl: [$splitted]"
		if [ -z "$port" ]; then
			newUrlAttempt="$protocol://$ipAddress$requestPath"
		else
			newUrlAttempt="$protocol://$ipAddress:$port$requestPath"
		fi
		
		argsExceptUrl=("${@:1:$#-1}")
		curlCmd="curl $managedArgs -H \"Host: $hostName\" \"${argsExceptUrl[@]}\" -- \"$newUrlAttempt\""
		warn "managed cURL failed to resolve host: [$hostName]. Will attempt to execute: [$curlCmd] ..."
		rc=0
		curl $managedArgs -H "Host: $hostName" "${argsExceptUrl[@]}" -- "$newUrlAttempt" > "$tempFile" 2> "$errorFile" || rc=$?
		test "$rc" -ne 0 || break
	done
else
	warn "No alternative curl request to perform."
fi

>&2 cat "$errorFile"
cat "$tempFile"

exit $rc
