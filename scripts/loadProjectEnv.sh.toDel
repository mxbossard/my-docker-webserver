# This script cannot be moved from scripts/ directory.

if [ -z "$_ENV_LOADED" -o ! "$_ENV_LOADED" == "true" ]
then
	# Do not load env twice.

	! $DEBUG || >&2 echo "Loading env ..." || true
	test -n "$DEBUG" && $DEBUG > /dev/null 2>&1 && export DEBUG=true || export DEBUG=false
	test -n "$QUIET" && $QUIET > /dev/null 2>&1 && export QUIET=true || export QUIET=false

	export _ENV_LOADED=true
fi

SOURCED_SCRIPT_SCAN_ORDER="SOURCED_SCRIPT_PATH BASH_SOURCE[0]"
for varName in $SOURCED_SCRIPT_SCAN_ORDER
do
	varValue="$(eval echo \${$varName:- Not Defined !})"
	! $DEBUG || >&2 echo "Scanning for SOURCED_SCRIPT in: [$varName (=$varValue)] ..."
	test -f "$varValue" && export SOURCED_SCRIPT="$( $UTILS_DIR/absolutePath.sh $varValue )" && break || true
done
! $DEBUG || >&2 echo "SOURCED_SCRIPT found: $SOURCED_SCRIPT" || true
test -f "$SOURCED_SCRIPT" || ( >&2 echo "Unable to detect sourced script path !" ; exit 1 )

SCRIPTS_DIR=$( dirname $( readlink -f "$SOURCED_SCRIPT" ) )
. $SCRIPTS_DIR/scriptFramework.sh
debug "SCRIPTS_DIR: $SCRIPTS_DIR"

SOURCING_SCRIPT_SCAN_ORDER="SOURCING_SCRIPT_PATH 0"
for varName in $SOURCING_SCRIPT_SCAN_ORDER
do
	varValue="$(eval echo \${$varName:- Not Defined !})"
	debug "Scanning for SOURCING_SCRIPT in: [$varName (=$varValue)] ..."
	test -f "$varValue" && export SOURCING_SCRIPT="$( $UTILS_DIR/absolutePath.sh $varValue )" && break || true
done
debug "SOURCING_SCRIPT found: $SOURCING_SCRIPT" || true
if [ ! -f "$SOURCING_SCRIPT" ]
then
	debug "Unable to detect sourcing script path !"
	export SOURCING_SCRIPT=""
fi

PARENT_DIR="$( readlink -f $SCRIPTS_DIR/.. )"
CONF_DIR="$PARENT_DIR/conf"
debug "PARENT_DIR: $PARENT_DIR"

PROJECT_DIR_SCAN_ORDER="$SOURCING_SCRIPT $PWD/foo"
for file in $PROJECT_DIR_SCAN_ORDER
do
	projectDir=$( $UTILS_DIR/absolutePath.sh $file | egrep -o "$PARENT_DIR/[^/]+/" || echo "")
	test -d "$projectDir" || die "Script sourced from outside project scope !"
	projectName=$( basename "$projectDir" )
	if $SCRIPTS_DIR/listProjects.sh $projectName > /dev/null 2>&1
	then
		PROJECT_DIR=$( readlink -f "$projectDir" )
		debug "PROJECT_DIR: $PROJECT_DIR"
		PROJECT_NAME=$projectName
		debug "PROJECT_NAME: $PROJECT_NAME"
		break
	fi
done

test -d "$PROJECT_DIR" || die "Unable to determine PROJECT_DIR env var. Maybe the project is not declared in $PARENT_DIR/conf/projectDependencyTree.txt"

sourceOverridingEnvFiles() {
	files="$@"

	for file in $files
	do
		if test -f "$file"
		then
			debug "Sourcing overriding env file: [$file] ..."
			source "$file"
		else
			warn "Overriding env file: [$file] does not exists !"
		fi
	done
}

if [ "$_PROJECT_ENV_LOADED" != "$PROJECT_NAME" ]
then
	# Do not load project env twice
	debug "Loading project: [$PROJECT_NAME] env ..." || true

	# Default OVERRIDING_ENV_FILES
	defaultOverridingEnvFiles='$CONF_DIR/default_env.sh $CONF_DIR/$ENV.env.sh'
	OVERRIDING_ENV_FILES="$defaultOverridingEnvFiles"

	### Source GLOBAL_ENV_PATH or root env.sh file
	test -f "$GLOBAL_ENV_PATH" || GLOBAL_ENV_PATH="$PARENT_DIR/env.sh"
	test -f "$GLOBAL_ENV_PATH" || ( echo "You must create a global env.sh file for the serveur setup. You may want to use cp env.sh.example env.sh" && exit 1 )
	debug "Sourging GLOBAL_ENV_FILE: [$GLOBAL_ENV_PATH] ..."
	source "$GLOBAL_ENV_PATH"
	warn "1: $OVERRIDING_ENV_FILES"
	source "$GLOBAL_ENV_PATH"
	warn "2: $OVERRIDING_ENV_FILES"
	### Then source in order OVERRIDING_ENV_FILES
	OVERRIDING_ENV_FILES="${OVERRIDING_ENV_FILES:-$defaultOverridingEnvFiles}"
	sourceOverridingEnvFiles $OVERRIDING_ENV_FILES

	### Then source project env.sh file
	if test -n "$PROJECT_ENV_PATH"
	then
		test -f "$PROJECT_ENV_PATH" || die "Supplied PROJECT_ENV_PATH file does not exists !"
	else
		PROJECT_ENV_PATH="$PROJECT_DIR/env.sh"
	fi

	test -f "$PROJECT_ENV_PATH" || ( echo "You must create a dedicated env.sh file for the project setup." && exit 1 )

	source "$PROJECT_ENV_PATH"

	### Test variable presence and add dynamic variables
	test -n "$ENV" || die "ENV environment variable is not defined !"
	echo "$ENV" | egrep "^[-a-zA-Z0-9]+" > /dev/null || die "ENV environment variable (predently: [$ENV]) should only contains sybols [-a-zA-Z0-9]+ (digits, letters ans minus sympol) !." 
	NAMESPACE="$ENV"
	PROJECT_NAMESPACE="${NAMESPACE}_$PROJECT_NAME"

	### Manage splitted docker-compose config
	currentDir="$( pwd )"
	cd "$PROJECT_DIR"
	# Use COMPOSE_FILE docker-compose env var by default
	composeFiles="$( echo $COMPOSE_FILE | tr ':' ' ' )"
	composeReferenceFiles="${composeFiles:-$PROJECT_DIR/docker-compose.yml $PROJECT_DIR/docker-compose.override.yml}"
	composeFilesPriority="$PARENT_DIR/conf/docker-compose.common.yml $composeReferenceFiles"

	debug "Expanding compose config files: [$composeFilesPriority] ..."
	composeFilesArgs=""
	expandedFiles=""
	referencedNetworks=":"
	for file in $composeFilesPriority
	do
		if [ -f "$file" ]
		then
			expandedFile=$( $SCRIPTS_DIR/expandComposeConfig.sh $file $composeReferenceFiles )
			expandedFiles="$expandedFiles$expandedFile "
			composeFilesArgs="$composeFilesArgs-f $expandedFile "
		fi

	done
	
	# Search for referenced networks in docker-compose project config
	relativesExpandedFiles=$( echo "$expandedFiles" | sed -e "s|/tmp/||g" )
	debug "files passed to yq: [$relativesExpandedFiles]"
	referencedNetworks="$( docker run --rm -i -v "/tmp":/workdir mikefarah/yq eval '.services.*.networks' $relativesExpandedFiles | grep '^[^ ]' | sed -e 's/^- //' -e 's/:$//' -e 's/^null//' -e 's/^---//' -e 's/^#.*//' )"
	referencedNetworks=":$( echo $referencedNetworks | tr ' ' ':' ):"
	debug "Project referenced networks: $referencedNetworks"	

	# Build Shared networks dynamic docker-compose config
	if [ -n "$DOCKER_SHARED_NETWORKS" ]
	then
		composeDynSharedNetworksFile=$( mkTempFile "docker-compose.shared-networks.yml.XXXXXXXXXX" )
		echo "networks:" > $composeDynSharedNetworksFile
		
		debug "Building dynamic shared networks compose config file ..."

		noNetworks=true
		sharedNetworks=":"
		for sharedNet in $DOCKER_SHARED_NETWORKS
		do
			netAlias="$( echo $sharedNet | cut -d@ -f1 )"
			netProjectOwner="$( echo $sharedNet | cut -d@ -f2 )"
			test -n "$netAlias" -a -n "$netProjectOwner" || die "Bad shared network declaration in SHARED_NETWORKS env variable: [$sharedNet] ! Should be of form: \"netAlias@projectName\"."

			debug "Check if alias: [$netAlias] is part of referenced alias: [$referencedNetworks] ..."
			echo $referencedNetworks | grep ":$netAlias:" > /dev/null || continue

			netName="${NAMESPACE}_$netAlias"
			if [ "$PROJECT_NAME" == "$netProjectOwner" ]
			then
				# Project is the net owner
				debug "Adding network owning ref for alias: [$netAlias] on network: [$netName] ..."
				cat <<EOF >> $composeDynSharedNetworksFile
  ${netAlias}:
    name: $netName
    labels:
      fr.mby.shared: env
      fr.mby.alias: ${netAlias}
EOF

			else
				# Project is a net user
				debug "Adding external network ref for alias: [$netAlias] on network: [$netName] ..."
				cat <<EOF >> $composeDynSharedNetworksFile
  ${netAlias}:
    name: $netName
    external: true
EOF

			fi
			networkAliasAdded="$networkAliasAdded$alias:"
			noNetworks=false
		done


		if $noNetworks 
		then
			rm -f -- $composeDynSharedNetworksFile
		else
			expandedFile=$( $SCRIPTS_DIR/expandComposeConfig.sh $composeDynSharedNetworksFile $composeReferenceFiles )
			# Add dynamic shared networks config with least priority
			composeFilesArgs="-f $expandedFile $composeFilesArgs"
			debug "Dynamically add following network alias: [$networkAliasAdded]."
		fi
	fi

	DOCKER_COMPOSE_CMD="docker-compose -p $PROJECT_NAMESPACE --project-directory $PROJECT_DIR $composeFilesArgs"
	DOCKER_COMPOSE_CONFIG_FILES="$composeFilesPriority"

	cd "$currentDir"

	if [ -n "$SUB_DOMAIN_NAME" ] && [ -n $ROOT_DOMAIN_NAME ]
	then
		VIRTUAL_HOST="${SUB_DOMAIN_NAME}.${ROOT_DOMAIN_NAME}"
		LETSENCRYPT_HOST="${SUB_DOMAIN_NAME}.${ROOT_DOMAIN_NAME}"
	fi

	VARS_TO_EXPORT="PARENT_DIR SCRIPTS_DIR PROJECT_DIR PROJECT_NAME VIRTUAL_HOST LETSENCRYPT_HOST PROJECT_ENV_PATH DEBUG NAMESPACE PROJECT_NAMESPACE DOCKER_COMPOSE_CMD DOCKER_COMPOSE_CONFIG_FILES"
	VARS_TO_DISPLAY="PROJECT_DIR VIRTUAL_HOST GLOBAL_ENV_PATH OVERRIDING_ENV_FILES PROJECT_ENV_PATH PROJECT_NAMESPACE DEBUG"

	for varName in $VARS_TO_EXPORT
	do
		varValue="$(eval echo \${$varName:- Not Defined !})"
		export $varName="$varValue"
	done

	if ! $QUIET
	then
		>&2 echo "++++++++++++++++++++ Env for PROJECT: $PROJECT_DIR ++++++++++++++++++++"
		for varName in $VARS_TO_DISPLAY
		do
			varValue="$(eval echo \${$varName:- Not Defined !})"
			>&2 echo "var $varName: $varValue"
		done
		>&2 echo "-------------------- Env for PROJECT: $PROJECT_DIR --------------------"
	fi

	set -a # All variables are exported by default
	source "$GLOBAL_ENV_PATH"
	sourceOverridingEnvFiles $OVERRIDING_ENV_FILES 2> /dev/null
	source "$PROJECT_ENV_PATH"
	test -f "$PARENT_DIR/.credentials.sh"i && source "$PARENT_DIR/.credentials.sh"
	set +a

	export _PROJECT_ENV_LOADED="$PROJECT_NAME"
else
	debug "Project: [$PROJECT_NAME] env already loaded."
fi

