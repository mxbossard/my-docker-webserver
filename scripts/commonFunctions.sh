# A function to source overriding env files
sourceOverridingEnvFiles() {
	files="$@"
	for file in $files
	do
		if test -f "$file"
		then
			debug "Sourcing overriding env file: [$file] ..."
			source "$file"
		else
			warn "Overriding env file: [$file] does not exists !"
		fi
	done
}
export -f sourceOverridingEnvFiles

displayVars() {
	varsToDisplay="$@"
	if ! $QUIET
	then
		>&2 echo "++++++++++++++++++++ Env for PROJECT: $PROJECT_NAME ++++++++++++++++++++"
		for varName in $varsToDisplay
		do
			varValue="$(eval echo \${$varName:- Not Defined !})"
			>&2 echo "var $varName: $varValue"
		done
		>&2 echo "-------------------- Env for PROJECT: $PROJECT_NAME --------------------"
	fi
}
export -f displayVars

shortDigest() {
        echo "$1" | cut -d: -f2 | cut -c1-12
}

shortImageId() {
        shortDigest "$1"
}

