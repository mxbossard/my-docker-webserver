#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
. $SCRIPT_DIR/scriptFramework.sh

INPUT_PROJECTS="${@:-}"
export QUIET=true

for project in $( $SCRIPT_DIR/listProjects.sh $INPUT_PROJECTS )
do
	cd $SCRIPT_DIR/../$project
	docker-compose ps -a | tail -n+3 | awk '{ print $1 }'
done
