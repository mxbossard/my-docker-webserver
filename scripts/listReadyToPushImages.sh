#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
export QUIET=true
source $SCRIPT_DIR/scriptFramework.sh
source $SCRIPT_DIR/globalEnv.sh

upProjectsFile="$( mkTempFile upProjects )"
$SCRIPTS_DIR/listUpServices.sh -p > $upProjectsFile

inputProjects=""
while test -n "$1" && projects="$( $SCRIPTS_DIR/listProjects.sh $1 2> /dev/null )"; do
	inputProjects="$inputProjects $projects"
	shift
done

test -n "$inputProjects" || inputProjects="$( $SCRIPTS_DIR/listProjects.sh )"
debug "inputProjects: $inputProjects"

>&2 infoMsg "Listing built docker images of up services only (if your image is not here, you must start and test the service) ..."

for project in $inputProjects; do
	# Filter not up projects
	! grep "^$project$" "$upProjectsFile" > /dev/null && debug "Project: [$project] is not up)" && continue || true

	cd "$PARENT_DIR/$project"
	source $SCRIPT_DIR/projectEnv.sh
	source $SCRIPTS_DIR/loadDynamicDockerComposeConfig.sh

	$DOCKER_COMPOSE_CMD config | $SCRIPTS_DIR/yq.sh eval -j - | jq -r '.services | to_entries | map( select(.value | has("build")) | (.key + " " + .value.image)) | .[]' | while read serviceAndImage; do
		debug "serviceAndImage: $serviceAndImage"
		serviceName="$( echo $serviceAndImage | awk '{ print $1 }' )"
		image="$( echo $serviceAndImage | awk '{ print $2 }' )"
		# Checking if this images is currently deployed
		deployedImage=$( $DOCKER_COMPOSE_CMD images "$serviceName" | tail -n+3 | awk '{ print $2 ":" $3 }' ) || die "Built image: [$image] is currently not deployed !"
		test "$image" == "$deployedImage" || >&2 warnMsg "Deployed image: [$GREEN$deployedImage$NC] and image from compose config: [$BLUE$image$NC] missmatch !"
		imageId="$(docker images -q --no-trunc $image | sed -e 's/[^:]\+://' )"
		deployedImageId="$( $DOCKER_COMPOSE_CMD images -q $serviceName )"
		test -n "$imageId" || die "Image: [$BLUE$image$NC] does not exists in local registry !"
		test "$deployedImageId" == "$imageId" || die "Deployed imageId: [$GREEN$( echo $deployedImageId | cut -c1-12 )$NC] and imageId from local registry: [$BLUE$( echo $imageId | cut -c1-12 )$NC] missmatch !"
		echo "$project $serviceAndImage $deployedImageId"
	done
done
