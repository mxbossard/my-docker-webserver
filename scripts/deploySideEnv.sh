#! /bin/sh -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

usage() {
	>&2 echo "usage: $0 <sideEnvName> <portShift> <gitBranch|gitTag|-> <gitRepoPath|->"
	>&2 echo "	<gitBranch|gitTag> Use - to rsync instead of git clone"
	>&2 echo "	<gitRepoPath> Use - to use local repo"
	exit 1
}

envName="$1"
portShift="$2"
gitBranch="$3"
gitRepoPath="$4"
test -n "$envName" || usage
test -n "$portShift" || usage
test -n "$gitBranch" || usage
test -n "$gitRepoPath" || usage
shift 4

test "$gitBranch" != "-" || gitBranch=""
test "$gitRepoPath" != "-" || gitRepoPath=""

clearEnv() {
        if [ "$KEEP_ENV" != "true" ]; then
                $SCRIPT_DIR/rmEnv.sh "$envName"
        fi
}

onExit() {
        clearEnv
        exit 0
}
trap onExit EXIT TERM QUIT ABRT INT

waitForManualExit() {
        >&2 echo -n "Waiting CTRL-C to clean $envName env ..."

        while true; do
                sleep 1
                echo -n "."
        done
}


envDir="$SCRIPT_DIR/../sideEnvs/$envName"

# rm all env containers and volumes
clearEnv

# create env
$SCRIPT_DIR/createSideEnv.sh "$envName" "$portShift" "$gitBranch" "$gitRepoPath"

# up front nginx-proxy
$SCRIPT_DIR/../up.sh --strict nginx-proxy

$envDir/up.sh "$@"

waitForManualExit
