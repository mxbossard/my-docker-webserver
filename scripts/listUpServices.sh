#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
export QUIET=true
source $SCRIPT_DIR/scriptFramework.sh
source $SCRIPT_DIR/globalEnv.sh

listOnlyProjects=false
listOnlyServices=false
if [ "$1" == "-p" ]; then 
	listOnlyProjects=true
	shift
elif [ "$1" == "-s" ]; then 
	listOnlyServices=true
	shift
fi

env="${1:-$ENV}"

docker ps -f "name=^${env}_" --format "{{ .Names }}" | cut -d_ -f1-3 | while read line; do
	candidateProject="$( echo $line | cut -d'_' -f2 )"
	if $SCRIPTS_DIR/listProjects.sh "$candidateProject" > /dev/null 2>&1; then
		if $listOnlyProjects; then
			echo "$candidateProject"
		fi
		if [ "$env" != "$ENV" ]; then
			envName="$( echo $line | cut -d'_' -f1 )/"
		fi
		serviceName="$( echo $line | cut -d'_' -f3 )"
		if $listOnlyServices; then
			echo "$serviceName"
		elif ! $listOnlyProjects; then
			#echo "$serviceName $candidateProject"
			echo "$envName$candidateProject/$serviceName"
		fi
	fi
done | sort -u
