#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
source $SCRIPT_DIR/scriptFramework.sh

INPUT_PROJECTS="${@:-}"
export QUIET=true

tempOutput=$( mkTempFile )
rmOnExit "$tempOutput"

error=false
projectCount=0
for project in $( $SCRIPT_DIR/listProjects.sh $INPUT_PROJECTS )
do
	projectCount=$(( projectCount + 1 ))
	#infoMsg "Project: [$PURPLE$project$NC]" >> $tempOutput

	debug "Scanning project: [$project] for integration tests ..."
	scriptCount=0
	successCount=0
	cd $SCRIPT_DIR/../$project

	testsDir=$( readlink -f "./tests" )
	if [ -d "$testsDir" ]
	then
		SECONDS=0
		>&2 infoMsg "Executing integration tests for project: [$PURPLE$project$NC]"
		for script in $( find "$testsDir" -type f -executable -name "*.sh" | sort )
		do
			timeout=false
			scriptCount=$(( scriptCount + 1 ))
			info "Executing script: [$script]"
			rc=0
			(
				source $SCRIPT_DIR/projectEnv.sh
				export MANAGED_CURL_HOST_IPS="$PROXY_LOCAL_IP"
				test "0$INTEGRATION_TEST_TIMEOUT_SEC" -gt "0" || die "INTEGRATION_TEST_TIMEOUT_SEC must be greater than 0 !"
				startTestTime=$SECONDS
				timeout "$INTEGRATION_TEST_TIMEOUT_SEC" "$script"
			) || rc=$?

			if [ "$rc" -eq "0" ]; then
				successCount=$(( successCount + 1 ))
			elif [ "$rc" -eq "124" ]; then
				timeout=true
				error "Test script: [$script] timed out after $INTEGRATION_TEST_TIMEOUT_SEC sec !" | tee -a $tempOutput
				error=true
			else
				error "Test script: [$script] failed after $(( SECONDS - startTestTime )) seconds !" | tee -a $tempOutput
				error=true
			fi
		done
	else
		warnMsg "Project: [$PURPLE$project$NC] got no integration test suite found !" | tee -a $tempOutput
	fi

	if [ "$successCount" -eq "$scriptCount" ]; then
		if [ "$scriptCount" -gt "0" ]; then
			stdOut "Project: [$PURPLE$project$NC] ${GREEN}successfully$NC passed Integration Tests => [$GREEN$successCount$NC/$BLUE$scriptCount$NC] scripts executed successfully." | tee -a $tempOutput
		fi
	else
		warnMsg "Project: [$PURPLE$project$NC] ${RED}failed$NC to pass Integration Tests => [$RED$successCount$NC/$BLUE$scriptCount$NC] scripts executed successfully in $SECONDS seconds." | tee -a $tempOutput
	fi
done

if [ $projectCount -gt 1 ]; then
	errOut ""
	>&2 cyanMsg "----- Script $0 report: -----"
	cat $tempOutput
fi

! $error || exit 1
