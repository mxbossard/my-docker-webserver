#! /bin/bash -e
SCRIPT_DIR="$( dirname $( readlink -f $0 ) )"

source $SCRIPT_DIR/scriptFramework.sh
source $SCRIPT_DIR/projectEnv.sh

#cd "$PROJECT_DIR"

test -n "$HEALTHCHECK_SCRIPTS" || die "You must define HEALTHCHECK_SCRIPTS env variable for project: [$PROJECT_NAME] !"
scriptFiles=""
for script in $HEALTHCHECK_SCRIPTS
do
	healthcheckScriptPriority="$( readlink -f $PROJECT_DIR/$script ) $( readlink -f $script ) $( readlink -f $SCRIPTS_DIR/$script )"
	healthcheckScriptPriority=$( echo "$healthcheckScriptPriority" | tr ' ' '\n' | awk '!x[$0]++' | tr '\n' ' ' )
	for file in $healthcheckScriptPriority
	do
		#err "checking file: $file"
		if [ -f "$file" ]
		then
			test -x "$file" || die "Healthcheck script: [$file] is not executable !"
			scriptFiles="$scriptFiles $file"
			debug "Found a healthcheck script: [$file]"
			break
		fi
	done
done

test -n "$scriptFiles" || die "Cannot find a Healthcheck script. You should write one of this files : [ $healthcheckScriptPriority]"
HEALTHCHECK_SCRIPTS="$scriptFiles"

test "0$HEALTHCHECK_INITIAL_DELAY_SEC" -ge "0" || die "HEALTHCHECK_INITIAL_DELAY_SEC must be positive !" 
test "0$HEALTHCHECK_INTERVAL_SEC" -gt "0" || die "HEALTHCHECK_INTERVAL_SEC must be greater than 0 !" 
test "0$HEALTHCHECK_TIMEOUT_SEC" -gt "0" || die "HEALTHCHECK_TIMEOUT_SEC must be greater than 0 !" 
test "0$HEALTHCHECK_RETRIES" -gt "0" || die "HEALTHCHECK_RETRIES must be greater than 0 !" 
test "0$HEALTHCHECK_RETRIES_INTERVAL_SEC" -gt "0" || die "HEALTHCHECK_RETRIES_INTERVAL_SEC must be greater than 0 !" 

tempFile=$( mkTempFile )
rmOnExit "$tempFile"

>&2 infoMsg "[HEALTHCHECK] Running healthcheck for project: [$PROJECT_NAME]"
startTime=$( date '+%s' )
if [ "$HEALTHCHECK_INITIAL_DELAY" == "true" ]
then
	errOut "Waiting HEALTHCHECK_INITIAL_DELAY_SEC ($HEALTHCHECK_INITIAL_DELAY_SEC sec) ..."
	#sleep $HEALTHCHECK_INITIAL_DELAY_SEC
fi

scriptExecutedCount=0
for script in $HEALTHCHECK_SCRIPTS
do
	debug "Calling healthcheck script: [$script] for project: [$PROJECT_NAME] ..."
	attempt=0
	failures=0
	while [ $attempt -lt $HEALTHCHECK_RETRIES ]
	do
		initialDelayRunning=false
		tempOut=$tempFile
		now=$( date '+%s' )
		if [ $(( startTime + HEALTHCHECK_INITIAL_DELAY_SEC )) -gt $now ]; then
		       # HEALTHCHECK_INITIAL_DELAY_SEC not expired
		       initialDelayRunning=true
		       tempOut=/dev/null
		fi
		debug "initialDelayRunning: $initialDelayRunning ($now > $(( startTime + HEALTHCHECK_INITIAL_DELAY_SEC )) )"

		attempt=$(( attempt +1 ))
		debug "[HEALTHCHECK] attempt #$attempt / $HEALTHCHECK_RETRIES ..."
		if timeout $HEALTHCHECK_TIMEOUT_SEC "$script" > $tempOut 2>&1
		then
			break
		elif ! $initialDelayRunning; then
			failures=$(( failures + 1 ))
			warn "[HEALTHCHECK] attempt #$attempt / $HEALTHCHECK_RETRIES for project [$PURPLE$PROJECT_NAME$NC] failed !"
			errOut "Error messages:"
			while read line
			do
				errOut "$PURPLE$line$NC"
			done < $tempFile
			test $attempt -lt $HEALTHCHECK_RETRIES && sleep $HEALTHCHECK_RETRIES_INTERVAL_SEC
		fi

		if $initialDelayRunning; then
		       # HEALTHCHECK_INITIAL_DELAY_SEC not expired
	 		attempt=0
	       		sleep 1
		fi		
	done

	if [ $failures -ge $HEALTHCHECK_RETRIES ]
	then
		alertMsg "Project: [$PURPLE$PROJECT_NAME$NC] is not healthy ($script failed for $attempt attempts) after $interval second(s) !"
		exit 1
	else
		scriptExecutedCount=$(( scriptExecutedCount + 1 ))
		debug "[HEALTHCHECK] script: [script] executed successfully."
	fi 
done

endTime=$( date '+%s' )
interval=$(( endTime - startTime ))
successMsg "Project: [$PURPLE$PROJECT_NAME$NC] is healthy ($scriptExecutedCount healthcheck script(s) executed) after $interval second(s) !"
