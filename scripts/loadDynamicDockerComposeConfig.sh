test ! -d "$PROJECT_DIR" && >&2 echo "You must source projectEnv.sh before sourcing docker-compose env !" && exit 1 || true

if [ "$_COMPOSE_ENV_LOADED" != "$PROJECT_NAME" -o "$1" == "--force" ]; then
	### Manage splitted docker-compose config
	currentDir="$( pwd )"
	cd "$PROJECT_DIR"

	# Use COMPOSE_FILE docker-compose env var by default
	composeFiles="$( echo $COMPOSE_FILE | tr ':' ' ' )"
	composeReferenceFiles="${composeFiles:-$PROJECT_DIR/docker-compose.yml $PROJECT_DIR/docker-compose.override.yml}"
	composeFilesPriority="$PARENT_DIR/conf/docker-compose.common.yml $composeReferenceFiles"

	debug "Expanding compose config files: [$composeFilesPriority] ..."
	composeFilesArgs=""
	expandedFiles=""
	referencedNetworks=":"
	for file in $composeFilesPriority
	do
		if [ -f "$file" ]
		then
			expandedFile=$( $SCRIPTS_DIR/expandComposeConfig.sh "$file" "$composeReferenceFiles" )
			rmOnExit "$expandedFile"
			expandedFiles="$expandedFiles$expandedFile "
			composeFilesArgs="$composeFilesArgs-f $expandedFile "
		fi
	done

	# Search for referenced networks in docker-compose project config
	relativesExpandedFiles=$( echo "$expandedFiles" | sed -e "s|/tmp/||g" )
	#debug "files passed to yq: [$relativesExpandedFiles]"
	#referencedNetworks="$( docker run --rm -i -v "/tmp":/workdir mikefarah/yq eval '.services.*.networks' $relativesExpandedFiles | grep '^[^ ]' | sed -e 's/^- //' -e 's/:$//' -e 's/^null//' -e 's/^---//' -e 's/^#.*//' )"

	#referencedNetworks="$( cat $expandedFiles | $SCRIPTS_DIR/yq.sh eval '.services.*.networks' - | grep '^[^ ]' | sed -e 's/^- //' -e 's/:$//' -e 's/^null//' -e 's/^---//' -e 's/^#.*//' )"

	refNetsTempFile="$( mkTempFile "refNets" )"
	tempFile="$( mkTempFile "temp" )"
	for expandedFile in $expandedFiles; do
		# works for map (jitsi-meet)
		cat "$expandedFile" | $SCRIPTS_DIR/yq.sh eval '.services.*.networks | select(. != null) | keys | .[]' - > "$tempFile"
		if grep "^[0-9]\+$" "$tempFile" > /dev/null; then
			trace "Networks was an array !"
			# if result is only numeric it was an array
			# If return numeric only it was an array not a map
			# works for array (nginx-proxy)
			cat "$expandedFile" | $SCRIPTS_DIR/yq.sh eval '.services.*.networks | select(. != null) | .[]' -
		else
			cat "$tempFile"
		fi
	done | sort -u > "$refNetsTempFile"
	referencedNetworks=":"
	while read ref; do
		test -z "$ref" || referencedNetworks="$referencedNetworks$ref:"
	done < "$refNetsTempFile"

	debug "Project referenced networks: [$referencedNetworks]"	

	# Build Shared networks dynamic docker-compose config
	if [ -n "$DOCKER_SHARED_NETWORKS" ]
	then
		composeDynSharedNetworksFile=$( mkTempFile "docker-compose.shared-networks.yml" )
		rmOnExit "$composeDynSharedNetworksFile"

		echo "networks:" > $composeDynSharedNetworksFile
		
		debug "Building dynamic shared networks compose config file ..."

		noNetworks=true
		sharedNetworks=":"
		for sharedNet in $DOCKER_SHARED_NETWORKS
		do
			netAlias="$( echo $sharedNet | cut -d@ -f1 )"
			netProjectOwner="$( echo $sharedNet | cut -d@ -f2 )"
			test -n "$netAlias" -a -n "$netProjectOwner" || die "Bad shared network declaration in SHARED_NETWORKS env variable: [$sharedNet] ! Should be of form: \"netAlias@projectName\"."

			debug "Check if alias: [$netAlias] is part of referenced alias: [$referencedNetworks] ..."
			echo $referencedNetworks | grep ":$netAlias:" > /dev/null || continue

			netName="${NAMESPACE}_$netAlias"
			if [ "$PROJECT_NAME" == "$netProjectOwner" ]
			then
				# Project is the net owner
				debug "Adding network owning ref for alias: [$netAlias] on network: [$netName] ..."
				cat <<EOF >> $composeDynSharedNetworksFile
  ${netAlias}:
    name: $netName
    labels:
      fr.mby.shared: env
      fr.mby.alias: ${netAlias}
EOF

			else
				# Project is a net user
				debug "Adding external network ref for alias: [$netAlias] on network: [$netName] ..."
				cat <<EOF >> $composeDynSharedNetworksFile
  ${netAlias}:
    name: $netName
    external: true
EOF

			fi
			networkAliasAdded="$networkAliasAdded$alias:"
			noNetworks=false
		done

		if ! $noNetworks
		then
			expandedFile=$( $SCRIPTS_DIR/expandComposeConfig.sh "$composeDynSharedNetworksFile" "$composeReferenceFiles" )
			rmOnExit "$expandedFile"
			# Add dynamic shared networks config with least priority
			composeFilesArgs="-f $expandedFile $composeFilesArgs"
			debug "Dynamically add following network alias: [$networkAliasAdded]."
		fi
	fi

	# Add docker-compose config files to DOCKER_COMPOSE_CMD
	DOCKER_COMPOSE_CMD="$DOCKER_COMPOSE_CMD $composeFilesArgs"
	DOCKER_COMPOSE_CONFIG_FILES="$composeFilesPriority"

	export DOCKER_COMPOSE_CMD
	export DOCKER_COMPOSE_CONFIG_FILES
	export DOCKER_COMPOSE_PROJECT="$PROJECT_NAME"

	composeCmd() {
		$DOCKER_COMPOSE_CMD "$@"
	}
	export -f composeCmd

	cd "$currentDir"

	export _COMPOSE_ENV_LOADED="$PROJECT_NAME"
else
	debug "Project: [$PROJECT_NAME] compose dyn env already loaded."
fi
