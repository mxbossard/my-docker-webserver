#!/bin/bash

# usage:
# $0 mxbossard/nginx-proxy 0.1.0-rc2 0.1.0-rc1

IMAGE="$1"
LOCAL_TAG="${2:-latest}"
REMOTE_TAG="${3:-$LOCAL_TAG}"


echo -n "Fetching local digest...  "
local_digest=$( docker images -q --no-trunc $IMAGE:$LOCAL_TAG )
>&2 echo "$local_digest"

if [ -z "$local_digest" ]; then
	>&2 echo "Local digest not found for image: [$IMAGE:$LOCAL_TAG] !"
	exit 1
fi


>&2 echo "Fetching Docker Hub token..."
token=$( curl --silent "https://auth.docker.io/token?scope=repository:$IMAGE:pull&service=registry.docker.io" | jq -r '.token' )

>&2 echo -n "Fetching remote digest... "
digest=$( curl --silent -H "Accept: application/vnd.docker.distribution.manifest.v2+json" \
	-H "Authorization: Bearer $token" \
	"https://registry.hub.docker.com/v2/$IMAGE/manifests/$REMOTE_TAG" | jq -r '.config.digest' )
>&2 echo "$digest"

if [ -z "$digest" ] || [ "null" == "$digest" ]; then
	>&2 echo "Remote digest not found for image: [$IMAGE:$REMOTE_TAG] !"
	exit 1
fi

if [ "$digest" != "$local_digest" ] ; then
	>&2 echo "Images differ."
	exit 1
else
	>&2 echo "Images are identical."
	exit 0
fi
