#! /bin/sh -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

# Test deployment in integ env : up project building images from git reference.

envName="integ"

usage() {
        >&2 echo "usage: $0 [gitBranch]"
        exit 1
}

gitBranch="$1"
# Current branch by default
if test -n "$gitBranch"; then
	shift 1
else	
	gitBranch="$( git rev-parse --abbrev-ref HEAD )"
fi

>&2 echo "Deploying git local $gitBranch branch on $envName side env and up it --strict ..."

# up strict and force pull all by default
opts="${@:-all}"
$SCRIPT_DIR/deploySideEnv.sh "$envName" 20000 "$gitBranch" "file://$SCRIPT_DIR/../" --strict $opts

