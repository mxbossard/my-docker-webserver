#! /bin/bash -e

# Inspiration from ufw-docker project.
# Some alterations: drop all forwarded by default with ufw-reject-forward chain.

after_rules="/etc/ufw/after.rules"
declare -a files_to_be_deleted

function rm-on-exit() {
  [[ $# -gt 0 ]] && files_to_be_deleted+=("$@")
}

function on-exit() {
  for file in "${files_to_be_deleted[@]}"; do
    [[ -f "$file" ]] && rm -r "$file"
  done
  files_to_be_deleted=()
}

trap on-exit EXIT INT TERM QUIT ABRT ERR

function ufw-docker--check-install() {
	after_rules_tmp="${after_rules_tmp:-$(mktemp)}"
	rm-on-exit "$after_rules_tmp"

	sed "/^# BEGIN UFW AND DOCKER/,/^# END UFW AND DOCKER/d" "$after_rules" > "$after_rules_tmp"
	>> "${after_rules_tmp}" cat <<-\EOF
	# BEGIN UFW AND DOCKER
	*filter
	:ufw-user-forward - [0:0]
	:ufw-reject-forward - [0:0]
	-A ufw-reject-forward -j DROP
	:DOCKER-USER - [0:0]
	-A DOCKER-USER -j RETURN -s 10.0.0.0/8
	-A DOCKER-USER -j RETURN -s 172.16.0.0/12
	-A DOCKER-USER -j RETURN -s 192.168.0.0/16
	#-A DOCKER-USER -p udp -m udp --sport 53 --dport 1024:65535 -j RETURN
	-A DOCKER-USER -j ufw-user-forward
	#-A DOCKER-USER -j DROP -d 192.168.0.0/16 -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN
	#-A DOCKER-USER -j DROP -d 10.0.0.0/8 -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN
	#-A DOCKER-USER -j DROP -d 172.16.0.0/12 -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN
	#-A DOCKER-USER -j DROP -d 192.168.0.0/16 -p udp -m udp --dport 0:32767
	#-A DOCKER-USER -j DROP -d 10.0.0.0/8 -p udp -m udp --dport 0:32767
	#-A DOCKER-USER -j DROP -d 172.16.0.0/12 -p udp -m udp --dport 0:32767
	#-A DOCKER-USER -j RETURN
	-A DOCKER-USER -j ufw-reject-forward
	COMMIT
	# END UFW AND DOCKER
	EOF

	diff -u --color=auto "$after_rules" "$after_rules_tmp"
}

function ufw-docker--install() {
  	if ! ufw-docker--check-install; then
	  	local after_rules_bak
    		after_rules_bak="${after_rules}-ufw-docker~$(date '+%Y-%m-%d-%H%M%S')~"
	  	err "\\nBacking up $after_rules to $after_rules_bak"
	  	cp "$after_rules" "$after_rules_bak"
	  	cat "$after_rules_tmp" > "$after_rules"
    		err "Please restart UFW service manually by using the following command:"
    		if type systemctl &>/dev/null; then
      			err "    sudo systemctl restart ufw"
    		else
      			err "    sudo service ufw restart"
    		fi
  	fi
}


function err() {
    echo -e "$@" >&2
}

function die() {
    err "ERROR:" "$@"
    exit 1
}

ufw-docker--install

rm -f -- "$after_rules_tmp"

systemctl restart ufw.service

ufw reload
