# Take a docker compose project config file and use it to expand another piece of config.
# Expand it in another file and return the filename

#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"
source $SCRIPT_DIR/scriptFramework.sh

toExpandConfigPath="$1"
shift
refConfigPaths="$@"

usage() {
	errOut "$1"
	die "Usage: $0 <toExpandComposeConfigFile> <refComposeConfigFile1> [refComposeConfigFile2] ...>"
}

test -f "$toExpandConfigPath" || usage "File: [$toExpandConfigPath] does not exists !"
refConfigExistingPaths=""
for file in $refConfigPaths
do
	test -f "$file" && refConfigExistingPaths="$refConfigExistingPaths $file"
done
test -n "$refConfigExistingPaths" || usage "You must supply at least one refComposeConfigFile !"
trace "refConfigExistingPaths: [$refConfigExistingPaths]"

expandedTempFile=$( mkTempFile "$( basename $toExpandConfigPath )" )
trace "expandedTempFile: [$expandedTempFile]"

cat $refConfigExistingPaths | grep "^version:" | tail -1 > "$expandedTempFile"
grep -v "^version:" "$toExpandConfigPath" >> "$expandedTempFile"

echo "$expandedTempFile"
