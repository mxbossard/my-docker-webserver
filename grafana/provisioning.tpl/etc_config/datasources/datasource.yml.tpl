apiVersion: 1
datasources:
  - name: iot_datastore_prom
    uid: iot_datastore_prom
    type: prometheus
    access: proxy
    user: $IOT_DATASTORE_PROMETHEUS_READER_USER
    password: '$IOT_DATASTORE_PROMETHEUS_READER_PASSWORD'
    url: http://prometheus:9090
    isDefault: true
    editable: false

#  - name: iot_datastore_promscale
#    uid: iot_datastore_promscale
#    type: prometheus
#    access: proxy
#    user: $IOT_DATASTORE_PROMETHEUS_READER_USER
#    password: '$IOT_DATASTORE_PROMETHEUS_READER_PASSWORD'
#    url: http://promscale:9201
#    isDefault: false
#    editable: false

#  - name: iot_datastore
#    uid: iot_datastore
#    type: influxdb
#    access: proxy
#    database: iot_datastore
#    user: $IOT_DATASTORE_INFLUXDB_READER_USER
#    password: '$IOT_DATASTORE_INFLUXDB_READER_PASSWORD'
#    url: http://influxdb:8086
#    isDefault: false
#    editable: false

deleteDatasources:
  - name: iot_datastore_promscale
    orgId: 1

  - name: iot_datastore
    orgId: 1
