SUB_DOMAIN_NAME="grafana"

MANDATORY_VARS="GRAFANA_USERNAME GRAFANA_PASSWORD"

HEALTHCHECK_URLS="http://$VIRTUAL_HOST/login"
HEALTHCHECK_INITIAL_DELAY_SEC=60

GRAFANA_IMAGE_TAG=7.4.2
GF_INSTALL_PLUGINS="https://github.com/alexandrainst/alexandra-trackmap-panel/archive/2.1.0.zip;alexandra-trackmap-panel"
