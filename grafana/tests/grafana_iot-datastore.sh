#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

#source $SCRIPT_DIR/../../scripts/projectEnv.sh
source $SCRIPTS_DIR/unitTestsFramework.sh

GRAFANA_BASE_URL="http://$VIRTUAL_HOST"
API_BASE_URL="http://$PROXY_LOCAL_IP/api/v1/datastore"
DS_NAME="iot_datastore_prom"
DB_NAME="iot_datastore"

now="$(date +%s)"

assertHttpResponseMatch 'Check Home dashboard content' GET "$GRAFANA_BASE_URL/api/dashboards/home" '.*"title":"IoT datastore stats".*' 

assertHttpStatus "Check Prometheus DS existance" GET "$GRAFANA_BASE_URL/api/datasources/id/$DS_NAME" "$HTTP_OK"

DS_ID=$( managedCurl -sS -f -L "$GRAFANA_BASE_URL/api/datasources/id/$DS_NAME" | jq '.id' ) 

assertTrue "Check Prom DS is found" test "$DS_ID" -gt -1 

assertHttpStatus 'Check iot_datasource read access' GET "$GRAFANA_BASE_URL/api/datasources/proxy/$DS_ID/api/v1/query?query=up" "$HTTP_OK"

assertNoError
