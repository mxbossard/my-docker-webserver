#! /bin/bash -e
SCRIPT_DIR="$( readlink -f $( dirname $0 ) )"

#source $SCRIPT_DIR/../../scripts/projectEnv.sh
source $SCRIPTS_DIR/unitTestsFramework.sh

GRAFANA_BASE_URL="http://$VIRTUAL_HOST"

assertHttpStatus 'Check Home dashboard is public' GET "$GRAFANA_BASE_URL/api/dashboards/home" "$HTTP_OK"

assertHttpStatus 'Check anonymous not authorized to browse Datasources API' GET "$GRAFANA_BASE_URL/api/datasources" "403"

assertNoError
